# STPL: A Monitor

**S**patio-**T**emporal **P**erception **L**ogic is a formal requirements language for perception systems. In this repository, the STPL monitor is provided to verify the behavior of these perception systems against an STPL requirement.

## Getting Started
To use the tool, you have two options:

1. As a MATLAB function.
2. As a Command-Line Interface (CLI) application.

To use the tool as a MATLAB function, add the project and its subdirectories to the MATLAB path. Then, run the following function within the MATLAB console from this project's root directory:

```bash
setup_stpl_taliro
```

**Note**: You must have [MATLAB](https://www.mathworks.com/products/matlab.html) installed with the Simulink add-on to use the MATLAB function, accordingly.

To build the tool as a CLI application, run the following commands from this project's root directory:

```bash
cmake -B build
cmake --build build
```

**Optional**: You can also install the resulting build targets (i.e., the binary and the static library) using the following command (p.s., if you would like to change the default prefix of where CMake will install the targets, set the `--prefix` flag, accordingly):

```bash
cmake --install build
```

## Post-Installation on Windows

For Windows, there are two additional steps to perform before using the tool from the Command Prompt ("cmd"):

1. Add the MATLAB Dynamically Linked Libraries (DLLs) to the PATH environment variable (e.g., `"C:/Program Files/MATLAB/R2022b/bin/win64"`).
2. Add the STPL tool to the PATH environment variable (e.g., `"C:/Program Files (x86)/STPL/bin"`).

**Warning**: Modifying the PATH environment variable is a potentially DESTRUCTIVE process. Therefore, please take caution when performing these steps and consult an experienced user if needed.

## Using the Tool
If you installed the targets, you can invoke the monitor from the CLI with the command `stpl-monitor`. Otherwise, you can run the executable as any other binary located under `build/src/stpl-monitor`.

As for MATLAB, once setup, the entrypiont function to call is `stpl_taliro` from `extras/matlab/stpl_taliro.m`.

## Project Structure
To make navigating this project's items easier, the [Pitchfork Layout](https://api.csswg.org/bikeshed/?force=1&url=https://raw.githubusercontent.com/vector-of-bool/pitchfork/develop/data/spec.bs) is used. Please refer to its specification for more details.

## The Paper
The publication associated with this work for details and citation can be found below:

```text
@article{hekmatnejad2022formalizing,
  title={Formalizing and Evaluating Requirements of Perception Systems for Automated Vehicles using Spatio-Temporal Perception Logic},
  author={Hekmatnejad, Mohammad and Hoxha, Bardh and Deshmukh, Jyotirmoy V and Yang, Yezhou and Fainekos, Georgios},
  journal={arXiv preprint arXiv:2206.14372},
  year={2022}
}
```
