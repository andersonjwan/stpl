
/***** mx_tp_taliro : ltl2tree.h *****/

/* Written by Georgios Fainekos, ASU, U.S.A.                              */
/* Copyright (c) 2017  Georgios Fainekos                                  */
/* Send bug-reports and/or questions to: fainekos@asu.edu                  */
/* Modified by Mohammad Hekmatnejad ASU, U.S.A. for stpl_taliro           */
/* Copyright (c) 2020  Mohammad Hekmatnejad                                  */

/* This program is free software; you can redistribute it and/or modify   */
/* it under the terms of the GNU General Public License as published by   */
/* the Free Software Foundation; either version 2 of the License, or      */
/* (at your option) any later version.                                    */
/*                                                                        */
/* This program is distributed in the hope that it will be useful,        */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of         */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          */
/* GNU General Public License for more details.                           */
/*                                                                        */
/* You should have received a copy of the GNU General Public License      */
/* along with this program; if not, write to the Free Software            */
/* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA*/

/* Most of the code in this file was taken from LTL2BA software           */
/* Written by Denis Oddoux, LIAFA, France                                  */
/* Some of the code in this file was taken from the Spin software         */
/* Written by Gerard J. Holzmann, Bell Laboratories, U.S.A.               */

#include <stdio.h>
#include <string.h>
#include <time.h>

#ifdef __unix__
#define safe_sprintf(buf,size,str_format,...) sprintf(buf,str_format,__VA_ARGS__) 
#define safe_strcpy(src,size,dst) strcpy(src,dst) 
#define safe_strncpy(src,size,dst,num) strncpy(src,dst,num) 
#define safe_strcat(src,size,dst) strcat(src,dst) 
#else
#define safe_sprintf(buf,size,str_format,...) sprintf_s(buf,size,str_format,__VA_ARGS__) 
#define safe_strcpy(src,size,dst) strcpy_s(src,size,dst) 
#define safe_strncpy(src,size,dst,num) strncpy_s(src,size,dst,num) 
#define safe_strcat(src,size,dst) strcat_s(src,size,dst) 
#endif

static const char* _TIME_STR = "C_TIME\0";
static const char* _FRAME_STR = "C_FRAME\0";
//supported spatial functions
#define NUM_FN_NAMES 13
//BB: Bounding Box for a geometric shape in a 2D space
//ED: Euqlidean Distance between two points in a 2D space
//LAT: Lateral distance between two pints in a 2D space
//LON: Longitudinal distance between two pints in a 2D space
//AREA: Area of a geometric shape in a 2D space
//RATIO: Ratio of two real valued function i.e., AREA(id1)/AREA(id2)
//SFORALL: return 1 if its geometric parameter is equal to the space universe
//SEXISTS: return 1 if its geometric parameter is not empty
//C: class of an object 
//P: probability of an object belongs to its class
//SUB: first parameter subtract second parameter
static const char* FN_FAMES[NUM_FN_NAMES] = { "BB\0","AREA\0","RATIO\0","ED\0",
                                            "LAT\0","LON\0","SFORALL\0","SEXISTS\0",
                                            "C\0","P\0", "SUB\0", "LTEB\0", "LFEB\0"};

typedef enum {
    VAR_UNKNOWN = 0, VAR_TIME, VAR_FRAME, VAR_ID, VAR_CONST, VAR_FUN, VAR_SPATIO, VAR_RESERVED
}VarType;
/*Binary operators: <=,>=,=,<,>*/
typedef enum {
    LE_OPR = 0, GE_OPR, EQ_OPR, L_OPR, G_OPR, NE_OPR
}BinOpr;
/*constant keywords that are used in spatial operators*/
typedef enum SpatioConstKW {
    CONST_UNKNOWN = 0, CONST_CT, CONST_LM, CONST_RM, CONST_TM, CONST_BM, CONST_EMPTYSET, CONST_UNIVERSE
}SpatioConstKW;
#define NUM_SPATIO_CONST 8
struct Node;
/*Function definition: FN(param_1,...,param_n)*/
typedef struct FnDef {
    int findex;
    char* fn_name;
    int num_param;
    struct FnDef* params;
    VarType fn_type;//return type or variable type
    int num_vars;//number of variable parameters i.e., variable name starts with 'Var'
    struct Node* spatio_formula;
    int spatio_idx;//index to its formula in global G_s_subformula
}FnDef;
/*Binary function expression: FN_operator(left_fn,right_fn)*/
typedef struct FnExpr {
    FnDef left_fn;
    FnDef right_fn;
    BinOpr opr;
    int num_unique_vars;//number of unique varaiable parameters 
    int num_spatio_formula;
}FnExpr;


typedef struct Symbol {
    char *name;
    ConvSet *set;
    struct Symbol *next;    /* linked list, symbol table */
    int index;
    bool Normalized; /* GF: If set to true, then the valuation of the predicate is normalized to range [-1,1] */
    double *NormBounds; /* GF: min and max interval for normalization */

    //STQL
    bool is_var;//if the symbol is a variable
    VarType var_type;//type of the variable i.e., time, frame, object id
    FnExpr* fnx;
} Symbol;

typedef struct {
    int LTL;            /* Is this an LTL formula */  
    int ConOnSamples;    /* Are the constraints on the actual time or the # of samples? */
    mwSize SysDim; /* System dimension */
    mwSize nSamp;  /* Number of Samples */
    size_t nPred;  /* Number of Predicates */
    size_t true_nPred;  /* Number of Predicates */
    mwSize tnLoc;  /* total number of control locations */
    int nInp;       /* Number of inputs to mx_dp_taliro */
    int ParON;        /* Indicator if parameter is used */

    int nCLG;       /* Number HAs in multiple H.A.s*/
    mwSize *tnLocNCLG;  /* total number of control locations for each HA in multiple H.A.s*/
    
    int TPTL;            /* Is this an TPTL formula */

    int STQL;            /* Is this an STQL formula */
    mwSize nFrame;  /* Number of Frames */
    int cur_frame_index;/* curent row of the signal that is being processed*/
    int frz_frame_index;/* frozen row of the signal that is being processed*/

} FWTaliroParam;


#define NullSymbol (Symbol *)0

typedef struct queue{

    int i;
    struct Node *first;
    struct Node *last;
}queue;


typedef union {
    struct {
        int inf;
    } num;
    struct {
        int inf;
        double f_num;
    } numf;
    struct {
        int inf;
        int i_num;
    } numi;
} Number;

typedef struct {
    Number lbd;
    int l_closed;
    Number ubd;
    int u_closed;
} Interval;

typedef struct Node {
    short ntyp;    /* node type */
    int visited;
    int index;
    int LBound;
    int UBound;
    int LBound_nxt;
    int BoundCheck;
    int UBindicator;
    int LBindicator;
    int LBindicator_nxt;
    int loop_end;
    HyDis rob; /* robustness */
    HyDis rob_sec; /* robustness_last */
    Interval time; /* lower and upper real time bounds */
    struct Symbol *sym;
    struct Node    *lft;    /* tree */
    struct Node    *rgt;    /* tree */
    struct Node    *nxt;    /* if linked list */
    Number value;
    /* for TPTL monitor */
    int Lindex;
    int Rindex;
    int group;
    /* for STQL monitor */
    int second_ntype;
    int Lsecond_ntype;
    int Rsecond_ntype;
    struct FnDef *fndef;
    int num_id_vars;
    struct Node* frz;
    
    //used for restricting the search over time horizon using time/frame constraints
    int frame_lb;
    int frame_ub;
    double time_lb;
    double time_ub;
    short otype;
    bool has_freeze_var;
} Node;


enum {
    ALWAYS=257,
    AND,        /* 258 */
    EQUIV,        /* 259 */
    EVENTUALLY,    /* 260 */
    FALSE,        /* 261 */
    IMPLIES,    /* 262 */
    NOT,        /* 263 */
    OR,            /* 264 */
    PREDICATE,    /* 265 */
    TRUE,        /* 266 */
    U_OPER,        /* 267 */
    V_OPER,        /* 268 */
    NEXT,        /* 269 */
    VALUE,        /* 270 */
    WEAKNEXT,    /* 271 */
    U_MOD,        /* 272 */
    V_MOD,        /* 273 */
    FREEZE_AT,  /* 274 */
    CONSTR_LE,  /* 275 */
    CONSTR_LS,  /* 276 */
    CONSTR_EQ,  /* 277 */
    CONSTR_GE,  /* 278 */
    CONSTR_GR,  /* 279 */
    CONSTR_NE,  /* 280 *///not equal !=
    CONSTRAINT, /* 281 */
    
    FORALL,        /*282*/
    EXISTS,        /*283*/
    INTERIOR,        /*284*/
    CLOSURE,        /*285*/
    R_OPER,        /* 286 */
    O_STRICT,    /* 287 regular strict operator */
    O_NONSTRICT,    /* 288 special non-strict operator */
    //past operators
    PREVIOUS, /* 289 */
    WEAKPREVIOUS, /* 290 */
    POSITIVE_POLAR = 1,
    NEGATIVE_POLAR = -1,
    MIXED_POLAR = 0,
    UNDEFINED_POLAR = 2,
    PRED = 1,
    PAR = 2,
    PREDPAR = 3
};

typedef Node    *Nodeptr;
#define YYSTYPE     Nodeptr
#define Nhash    255        

typedef struct Cache {
    Node *before;
    Node *after;
    int same;
    struct Cache *nxt;
} Cache;

typedef struct PMap {
    char *str;
    ConvSet set;
    bool true_pred;
    double *Range;
    /* GF: If set to true, then the valuation of the predicate is normalized to range [-1,1] */
    bool Normalized; 
    /* GF: max value for normalization, e.g., if NormBounds = 2.5, then it is expected that the robustness */
    /* should be in the interval [-2.5,2.5]. Thus, the robustness value will be mapped to the interval [-1,1]. */
    /* For hybrid distances the 2nd element is the maximum path distance on the graph  */
    double *NormBounds;
    
    bool is_expression;
    FnExpr* fn_expr;
    char* expr;
} PMap;

typedef struct ParMap {
    char *str;
    double *value;
    int index;
    double *Range;
    bool lbd;
    int type;
    bool with_value;
    /* GF: If set to true, then the valuation of the predicate is normalized to range [-1,1] */
    bool Normalized; 
    /* GF: max value for normalization, e.g., if NormBounds = 2.5, then it is expected that the robustness */
    /* should be in the interval [-2.5,2.5]. Thus, the robustness value will be mapped to the interval [-1,1]. */
    /* For hybrid distances the 3rd element is the maximum path distance on the graph  */
    double *NormBounds; 
} ParMap;

typedef struct {
    /* Peer reviewed on 2013.07.22 by Dokhanchi, Adel */
    int *pindex;
    size_t total;
    int used;
}PredList;

typedef struct {
    int polar;
} Polarity;

#define MAX_PREDICATE_EXPRESSION_IN_FORMULA 20

typedef struct {
    Number zero;
    Number inf;
    Interval zero2inf;
    Interval emptyInter;
    Interval TimeCon;
    YYSTYPE    tl_yylval;
    FWTaliroParam dp_taliro_param;
    int    tl_errs;
    FILE *tl_out;
    char    yytext[2048];
    Symbol *symtab[Nhash+1];
    ParMap *parMap;
    PMap *predMap; 
    PredList pList;
    bool lbd;
    int type_temp;
    
    int type_oper;
    int second_type_temp;
    bool has_freeze_var;
    int num_var;
    int extra_pred_from_formula;//length of extra_pmap
    PMap extra_pmap[MAX_PREDICATE_EXPRESSION_IN_FORMULA];//predicate expressions in the formula
} Miscellaneous;


Node    *Canonical(Node *, Miscellaneous *, int *, char *, int *);
Node    *canonical(Node *, Miscellaneous *, int *, char *, int *);
Node    *cached(Node *, Miscellaneous *, int *, char *, int *);
Node    *dupnode(Node *);
Node    *getnode(Node *);
Node    *in_cache(Node *, int *, char *, int *, Miscellaneous *);
Node    *push_negation(Node *, Miscellaneous *, int *, char *, int *);
Node    *switchNotTempOper(Node *n, int ntyp, Miscellaneous *, int *cnt, char *uform, int *tl_yychar);
Node    *right_linked(Node *);
Node    *tl_nn(int, Node *, Node *, Miscellaneous *);

Symbol* tl_lookup(char*, Miscellaneous* miscell);
void    tl_clearlookup(char *, Miscellaneous *miscell);
Symbol    *getsym(Symbol *);
Symbol    *DoDump(Node *, char *, Miscellaneous *miscell);
Symbol* tl_lookup_new(char*, Miscellaneous* miscell, struct FnExpr* fn_expr);

char    *emalloc(size_t);    

int    anywhere(int, Node *, Node *, int *, char *, int *, Miscellaneous *);
int    dump_cond(Node *, Node *, int);
int    isequal(Node *, Node *, int *, char *, int *, Miscellaneous *);
int    tl_Getchar(int *cnt, size_t hasuform, char *uform);

static void    non_fatal(char *, char *, int *, char *, int *, Miscellaneous *);

void    cache_stats(void);
void    dump(Node *, Miscellaneous *);
void    Fatal(char *, char *, int *, char *, int *, Miscellaneous *);
void    fatal(char *, char *, int *, char *, int *, Miscellaneous *);
void    fsm_print(void);
void    releasenode(int, Node *);
void    tl_explain(int);
void    tl_UnGetchar(int *cnt);
Node    *tl_parse(int *cnt, size_t hasuform, char *uform, Miscellaneous *miscell, int *);
void    tl_yyerror(char *s1, int *cnt, char *uform, int *, Miscellaneous *);
void    trans(Node *);

int    tl_yylex(int *cnt, size_t hasuform, char *uform, Miscellaneous *miscell, int *tl_yychar);
void    fatal(char *, char *, int *, char *, int *, Miscellaneous *);

#define ZN    (Node *)0
#define ZS    (Symbol *)0




#define True    tl_nn(TRUE,  ZN, ZN, miscell)
#define False    tl_nn(FALSE, ZN, ZN, miscell)
#define Not(a)    push_negation(tl_nn(NOT, a, ZN, miscell), miscell, cnt, uform, tl_yychar)
#define rewrite(n)    canonical(right_linked(n), miscell, cnt, uform, tl_yychar)


#define Debug(x)    { if (0) printf(x); }
#define Debug2(x,y)    { if (tl_verbose) printf(x,y); }
#define Dump(x)        { if (0) dump(x, miscell); }
#define Explain(x)    { if (tl_verbose) tl_explain(x); }

#define Assert(x, y)    { if (!(x)) { tl_explain(y); \
              Fatal(": assertion failed\n",(char *)0, cnt, uform, tl_yychar, miscell); } }
#define min(a,b)    (((a) < (b)) ? (a) : (b))

void put_uform(char *uform, Miscellaneous *);

void tl_exit(int i);

#ifndef _EXTRA_UTIL_
#define _EXTRA_UTIL_

extern bool DEBUG_MODE;// false
extern bool VERBOSE_MODE;// true
extern bool SHOW_EXE_STAT;// true
extern bool SANITY_CHECK;// false
extern bool BOOLEAN_ROB;// true
extern bool RELEASE_DP_TABLE_MEMORY;// false
extern bool RELEASE_SPATIAL_MEMORY;// false
extern bool SHOW_WARNINGS;// true
extern int UX_START;
extern int UY_START;
extern int U_WIDTH;
extern int U_HEIGHT;

extern bool API;

/**Input signal structure :
frame#, time stamp,
object id, object class, class probability,
x1, y1, x2, y2, xc, yc**/
extern unsigned int STQL_SIGNAL_DIM_SIZE;// 11
extern unsigned int MAX_STQL_FORMULA_SIZE;// 1000
#define MAX_VAR_NUM 100
#define MAX_NUM_PARAMS 20
#define MAX_UNIQUE_VAR_ID_IN_PRED 6
#define MAX_OBJ_IN_FRAME 100
#define MAX_NUM_FREEZ_VAR 100
#define MAX_NUM_PRED 1000
extern unsigned int MAX_MAP_DATA_SIZE;// 100
#define MAX_LINKED_LIST_SIZE 1000000
#define MAX_ID_TABLE_SIZE 10000000
#define MAX_STQL_SIGNAL_DIM_SIZE 100


/**Input signal structure :
frame#, time stamp, 
object id, object class, class probability,
x1, y1, x2, y2, xc, yc**/


#ifndef NULL_SN//SpatialNode Null value
#ifdef __cplusplus
#define NULL_SN 0
#else
#define NULL_SN ((SpatialNode *)0)
#endif
#endif

#ifndef NULL_SR////SpatialRegion Null value
#ifdef __cplusplus
#define NULL_SR 0
#else
#define NULL_SR ((SpatialRegion *)0)
#endif
#endif

#ifndef NULL_TN////TreeNode Null value
#ifdef __cplusplus
#define NULL_TN 0
#else
#define NULL_TN ((Node *)0)
#endif
#endif

struct Key {
    char* data;
};

struct Value {
    int data;
};

struct Map {
    struct Key key;
    struct Value value;
};

//key: variable name, value: variable's type (i.e., VAR_TIME, VAR_FRAME, VAR_ID)
//to be procecced as part of @(...) syntax 
extern struct Map type_map[MAX_VAR_NUM];
extern int size_type_map;//size of the type_map
//key: object id's variable name, value: a quantifier that applies to the variable
extern struct Map var_id_map[MAX_VAR_NUM];
extern int size_var_id_map;//size of the var_id_map
extern struct Map id_to_group_map[MAX_VAR_NUM];//maps each var_id to its freez group
extern int* size_id_to_group_map;//size of the id_to_group_map
extern struct Map id_to_rank_map[MAX_VAR_NUM];//maps each var_id to a unique rank order
extern int size_id_to_rank_map;//size of the id_to_rank_map
extern struct Map id_to_sub_idx_map[MAX_VAR_NUM];//maps each var_id to its @(..) subformula index 
extern int size_id_to_sub_idx_map;//size of the id_to_sub_idx_map
extern int freez_index_map[MAX_NUM_FREEZ_VAR];//maps each freez group to the start index of the node in the formula
extern int numTimeVariable;//number of time variables in the formula
int add_map(struct Map* map, char* key, int value, int* index);
int find_in_map(struct Map* map, char* key, int size);
int add_map_reverse(struct Map* map, int key, char* data, int* index);
char* find_in_map_reverse(struct Map* map, int key, int size);
extern struct Map id_rank_to_name_map[MAX_VAR_NUM];//maps id var's rank to their variable name
extern int size_id_rank_to_name_map;//size of the id_rank_to_name_map
//keeps the number of id vars that exists in the scope of each freeze operator
extern int freez_idx_to_num_param[MAX_NUM_FREEZ_VAR + MAX_NUM_PRED];
//list of var ids that belongs to each freez operator or predicates
extern int* freez_idx_to_list_param[MAX_NUM_FREEZ_VAR + MAX_NUM_PRED];
//sum of the number of spatial formulas in all the predicate definitions
extern int num_spatio_formulas;//number of spatial formulas as predicates
extern int* nFrame_for_spatio_formulas;//maximum number of frames for each spatial formula
//Global variables used in the DP algorithm
extern Node** G_t_subformula;//temporal subformulas
extern double* G_XTrace;//global XTrace input signal
extern double* G_time_stamps;//global time stamps
extern FWTaliroParam* G_p_par;//global predicate parameters
extern int* G_start_idx_frames;//start index of each frame on G_XTrace
extern int* G_num_obj_in_frame;//keeps number of object for each frame index
extern double _UNIVERSE[4];//gloabl universe specs: min_x,max_y,min_y,max_y
extern bool OPTIMIZE_TRJ_BASED_ON_FORMULA;//reduce the length of signal to 2 or one based on the temporal/spatial operators
extern bool T_formula_has_next_temporal_operator;//true if there is X operator in the temporal formula
extern bool T_formula_has_evolving_temporal_operator;//true if there is [], <>, U, R, V operators in the temporal formula 
extern bool* S_formula_has_next_temporal_operator;//true if there is X operator in each spatial formula
extern int* S_formula_next_temporal_count;//size of nesting for X operator in each spatial formula
extern bool* S_formula_has_evolving_temporal_operator;//true if there is [], <>, U, R, V operators in each spatial formula 
extern int G_org_nFrame;//number of frames in the input signal
extern bool MAGNIFY_ROB_4_NEG_LHS_IMPLY;
extern Garbage_collector garbage_collector;//this is a garbage collector used to release all the memories in the end
extern bool should_release_memory;
extern bool run_from_matlab;

void SignedDistForPredicatesOverVarIds(double xx[MAX_OBJ_IN_FRAME][MAX_STQL_SIGNAL_DIM_SIZE], struct FnExpr* fnx, int dim, int num_id,
                                    int pred_idx, int group, double ff[MAX_OBJ_IN_FRAME][MAX_STQL_SIGNAL_DIM_SIZE], double** IdTable,
                                    int size_xx, int size_ff, int et_idx, int f_idx, double* min_d, double* max_d);


double SignedDistForPredicates(double* xx, struct FnExpr* fnx, int ft_idx);

void hmax_new(HyDis* result, HyDis* inp1, HyDis* inp2);
void hmin_new(HyDis* result, HyDis* inp1, HyDis* inp2);
void copyToFrom(HyDis* dest, HyDis* src);
void s_copyToFrom(SpatialRegion* dest, SpatialRegion* src);

//memoery management
SpatialRegion* createSpatialRegion();
SpatialNode* createSpatialNode();
void* halloc(int size);
void hfree(void* tmp);

//SPATIAL FUNCTIONS
SpatialRegion* DP_SPATIAL(int s_sub_index, int cur_t_idx, int frx_t_idx, char* id, int phi_index);
void spatio_intersection(SpatialRegion* result, SpatialRegion* inp1, SpatialRegion* inp2);
void spatio_union(SpatialRegion* result, SpatialRegion* inp1, SpatialRegion* inp2);
void spatio_complement(SpatialRegion* result, SpatialRegion* inp1);
void spatio_interior(SpatialRegion* result, SpatialRegion* inp1);
void spatio_closure(SpatialRegion* result, SpatialRegion* inp1);
void SetTo_Universe_or_Emptyset(SpatialRegion* temp, int sign);
void copyConvSetToFrom(ConvSet* dst, ConvSet* src);
void release_Conv_set(ConvSet* tmp);
unsigned long int release_spatial_region(SpatialRegion** reg);
unsigned long int release_spatial_region_linkedlist(SpatialRegion** reg);
unsigned long int release_spatial_node(SpatialNode** inp1);
unsigned long int release_spatial_node_from_head(SpatialRegion** reg);
void print_spatial_region(SpatialRegion* reg);
SpatialRegion* compressSpatialReion(SpatialRegion* reg, bool releaseNodes);
bool equalNodes(SpatialNode* inp1, SpatialNode* inp2);
bool equalRegions(SpatialRegion* inp1, SpatialRegion* inp2);
bool equalConvSet(ConvSet* inp1, ConvSet* inp2);
bool isSpatialUniverse(SpatialRegion* inp1);
bool isSpatialEmpty(SpatialRegion* inp1);

void print_fn_def(FnDef fn);
void print_opr(int opr);
void print_complete_formula(Node* n);
char* stringFromConstKW(enum SpatioConstKW kw);
int indexFromConstStringKW(char* key);
char* from_IdTable_index_to_Var_id_values(int index, int num_var_id, int num_obj);
int from_Var_id_values_to_IdTable_index(char* id, int num_var_id, int num_obj);
Node* parse_spatio_formula(char* expression, int start, int end);
int BFS(struct queue* q, Node* root, int* i, int group);
int BFS_spatial(struct queue* q, Node* root, int* i, int group);
int count_phi_subformulas(Node* root, int* cnt, int s_formula_index, int* next_count);
void print_const_opr(int opr);
int add_unique_var_in_map(struct Map* map, FnDef* fn, int* index, FnExpr* fn_expr);
int add_unique_var_in_map_spatio(struct Map* map, Node* node, int* index, FnExpr* fn_expr);
void efree(void* tmp);

typedef struct IdTableInfo {
    int num_id_vars;//maximum number of unique ID variables in a predicate expression F1(n) ><= F2(m) for all the predicate expressions
    int num_objs;//maximum number of objects in a frame for all the frames
    int size;//num_id_vars^num_obj
    int num_sub_formula;//number of subformulas in the STQL formula
} IdTableInfo;

//test functions
void run_spatial_test1();
void run_spatial_test2();
void run_spatial_test3();
void init_garbage_collector();
SpatialNode* createRandomNode(int size);

#endif // !_EXTRA_UTIL_
