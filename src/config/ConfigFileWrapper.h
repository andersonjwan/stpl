#ifndef __CONFIG_FILE_WRAPPER_H
#define __CONFIG_FILE_WRAPPER_H


#ifdef __unix__
#define safe_sprintf(buf,size,str_format,...) sprintf(buf,str_format,__VA_ARGS__) 
#define safe_strcpy(src,size,dst) strcpy(src,dst) 
#define safe_strncpy(src,size,dst,num) strncpy(src,dst,num) 
#define safe_strcat(src,size,dst) strcat(src,dst) 
#else
#define safe_sprintf(buf,size,str_format,...) sprintf_s(buf,size,str_format,__VA_ARGS__) 
#define safe_strcpy(src,size,dst) strcpy_s(src,size,dst) 
#define safe_strncpy(src,size,dst,num) strncpy_s(src,size,dst,num) 
#define safe_strcat(src,size,dst) strcat_s(src,size,dst) 
#endif

#ifdef __cplusplus
extern "C" {
#endif

    struct FileConfiguration_stc {
        void* obj;
        int key_size;
};

    typedef struct FileConfiguration_stc FileConfig_c;

    bool open_config_file(char* name, bool verbose);
    void close_config_file();
    const char* cfg_print_config_data_map();
    const char* cfg_get_value(char* key);
    const char* cfg_get_comments(char* key);
    void cfg_set_value(char* key, char* value, char* comment);
    char** cfg_get_keys();
    void cfg_update_config_file(char* name);
    int cfg_get_size();
    bool load_signal(char* name);
    double** get_signal();
    int get_signal_rows();
    int get_signal_columns();

#ifdef __cplusplus
}
#endif
#endif
