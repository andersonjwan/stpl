﻿// ConfigFile.cpp : Defines the entry point for the application.
//
#include "ConfigFile.h"

FileConfig::FileConfig(){
}

FileConfig::FileConfig(string name, bool verbose){
    VERBOSE_MODE = verbose;
    read_config_file(name);
}

bool FileConfig::read_config_file(std::string name) {
    
    conf_file_exists = false;

    if_handle.open(name);
    
    if (if_handle.fail()) {
        cout << "Could not open config file: " << name << endl;
        return false;
    }
    else {

        file_name = name;
        data_map.clear();
        key_vec.clear();

        if_handle.seekg(0, if_handle.end);
        streampos f_size = if_handle.tellg();
        char* buff = new char[f_size];
        int rd_size = 0;
        if_handle.seekg(0, if_handle.beg);
        string token = "";
        bool key_complete = false;
        string key, value;
        string quotation_mark = "\"";
        string comments = "";

        while (if_handle.good()) {

            if_handle.getline(buff, f_size);
            string str_line = buff;
            std::istringstream fis(str_line);
            istream_iterator<string> beg(fis), end;
            key = "";
            value = "";
            key_complete = false;

            while (beg != end) {

                token = *beg;

                if (token[0] == '#' || token[0] == '%' || (token[0] == '/' && token[1] == '/')) {
                    comments += str_line;
                    break;
                }

                if (token[0] == '=') {

                    key_complete = true;

                    if (token.length() > 1) {
                        token = token.substr(1, token.length() - 1);
                    }
                    else {
                        token = "";
                    }
                }
                else if (token[0] == '"' || token[0] == '\'') {

                    if (token[0] == '\'')
                        quotation_mark = "'";

                    size_t f_index = str_line.find_first_of(quotation_mark);
                    size_t l_index = str_line.find_last_of(quotation_mark);

                    if ( f_index != l_index && l_index > 0 )
                    {
                        token = str_line.substr(f_index, (l_index - f_index) + 1);
                        value += token;
                        break;
                    }
                }

                if (key_complete)
                    value += token;
                else
                    key += token;
                beg++;
            }

            if (key_complete) {
            
                data_map[key] = value;
                key_vec.push_back(key);
                comment_vec.push_back(comments);
                comments = "";
                
                if (VERBOSE_MODE) {
                    if (key.length() > 0)
                        cout << "key: " << key << endl;
                    if (value.length() > 0)
                        cout << "value: " << value << endl;
                }
            }

        }

        if_handle.close();
        if (f_size > 0)
            delete[] buff;
    }

    conf_file_exists = true;

    return true;
}

FileConfig::~FileConfig() {

    if (if_handle.is_open())
        if_handle.close();
    if (of_handle.is_open())
        of_handle.close();
    if(VERBOSE_MODE)
        cout << "FileConfig finished.\n";
}

stringstream FileConfig::print_config_data_map() {

    stringstream ss;
    ////ss << "DATA MAP\n{\n";
    int cnt = 0;

    if (data_map.size() > 0)
        for (auto& key : key_vec) {
            ss << comment_vec.at(cnt++) << endl;
            ss << key << " = " << data_map.at(key) << endl;
        }
    ////ss << "}\n";

    return ss;
}

string FileConfig::get_value(string key) {
    if (data_map.find(key) == data_map.end()) {
        cout << "ERROR: value for key:<" << key << "> does not exist in the config file.\n";
        return "";
    }
    string value = data_map[key];
    if (value[0] == '\'' || value[0] == '"')
        value = value.substr(1, value.length() - 2);
    return value;
}

string FileConfig::get_comments(string key) {
    string comment = "";
    int cnt = 0;
    if (data_map.size() > 0)
        for (auto& keys : key_vec) {
            if (keys.compare(key) == 0) {
                comment = comment_vec.at(cnt);
                break;
            }
            else
                cnt++;
        }
    return comment;
}

void FileConfig::set_value(string key, string value, string comment) {
    if (data_map.find(key) != data_map.end()) {

        data_map[key] = value;
        int cnt = 0;

        if (data_map.size() > 0)
            for (auto& keys : key_vec) {
                if (keys.compare(key) == 0) {
                    comment_vec[cnt] = comment;
                    break;
                }
                else
                    cnt++;
            }
    }
    else {
        data_map[key] = value;
        comment_vec.push_back(comment);
        key_vec.push_back(key);
    }
}

vector<string> FileConfig::get_keys() {
    return key_vec;
}

void FileConfig::update_config_file(string name) {
    if (name != "")
        of_handle.open(name);
    else
        of_handle.open(file_name);
    if (of_handle.fail()) {
        cout << "Could not open config file: " << name << endl;
    }
    else {
        of_handle << print_config_data_map().str();
        of_handle.close();
    }
}

bool FileConfig::get_status(){
    return conf_file_exists;
}

int FileConfig::get_size(){
    size = (int)key_vec.size();
    return size;
}

bool FileConfig::read_signal(string name) {

    ifstream if_handle;
    if_handle.open(name);

    if (if_handle.fail()) {
        cout << "Could not open signal file: " << name << endl;
        return false;
    }
    else {
        int max_num_frame = INT32_MAX;
        string str_num_frame = get_value("NUM_FRAME_TO_LOAD");
        stringstream ss(str_num_frame);
        /*if (!str_num_frame.empty()) {
            ss >> max_num_frame;
            cout << "Reading <" << max_num_frame << "> frames.\n";
        }*/
        
        string token = "";
        char buff[MAX_SIGNAL_WIDTH];
        double value;
        int row = 0 , column = 0;
        int first_num_col = 0;

        while (if_handle.good()) {

            if_handle.getline(buff, MAX_SIGNAL_WIDTH);
            string str_line = buff;
            std::istringstream fis(str_line);
            istream_iterator<string> beg(fis), end;
            vector<double> row_vec;
            column = 0;
            //check if the first element in the new row is the max requested frame number
            if (beg == end || (*beg)[0]=='%' || (*beg)[0] == '#'|| (*beg)[0] == '/') {
                column = first_num_col;
                continue;
            }
            if ((*beg).compare(str_num_frame) == 0) {
                column = first_num_col;
                break;
            }
            while (beg != end ) {

                token = *beg;

                try
                {
                    std::stringstream ss(token);

                    if ((ss >> value).fail() || !(ss >> std::ws).eof())
                    {
                        throw std::bad_cast();
                    }

                }
                catch (...)
                {
                    cout << "Error in reading signal at row " << row+1 << " and column " << column+1 << endl;
                    cout << "Each element must be a number.\n";
                    signal.clear();
                    num_sig_rows = 0;
                    num_sig_columns = 0;
                    signal_loaded = false;
                    return false;
                }

                row_vec.emplace_back(value);
                column++;
                beg++;
            }

            signal.emplace_back(row_vec);
            if (row == 0)
                first_num_col = column;
            else if( column != first_num_col){
                cout << "Error in reading signal at row " << row + 1 << " and column " << column + 1 << endl;
                cout << "Each row must have the same number of columns.\n";
                signal.clear();
                return false;
            }

            row++;
        }

        if_handle.close();
        num_sig_columns = column;
        num_sig_rows = row;
        signal_loaded = true;
    }
    return true;
}

int FileConfig::get_num_sig_colums() {
    return num_sig_columns;
}

int FileConfig::get_num_sig_rows() {
    return num_sig_rows;
}

bool FileConfig::get_signal_loaded() {
    return signal_loaded;
}

vector<vector<double>>* FileConfig::get_signal() {
    return &signal;
}

bool FileConfig::is_verbose() {
    return VERBOSE_MODE;
}

/*int main()
{

    FileConfig fg("test.conf");

    //cout << fg.print_config_data_map().str() << endl;
    
    int cnt = 0;

    for (auto& key : fg.get_keys()) {
        cout << cnt++ << ": " << fg.get_comments(key) << endl;
        cout << key << " = " << fg.get_value(key) << endl;
        fg.set_value(key, fg.get_value(key) + "_extra_" + to_string(cnt), fg.get_comments(key) + "\n#new comment" + to_string(cnt));
    }
    cout << "-----------------------------------------------\n";
    cnt = 0;
    for (auto& key : fg.get_keys()) {
        cout << cnt++ << ": " << fg.get_comments(key) << endl;
        cout << key << " = " << fg.get_value(key) << endl;
    }

    fg.update_config_file("clone.conf");
    return 0;
}
*/
