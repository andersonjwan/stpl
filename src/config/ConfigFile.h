﻿// ConfigFile.h : Include file for standard system include files,
// or project specific include files.

#pragma once

#ifndef __CONFIG_FILE_H
#define __CONFIG_FILE_H

#include <iostream>
#include <fstream>
#include <unordered_map> 
#include <sstream>
#include <iterator>
#include <vector>
#include <string.h>
#include <stdio.h>

using namespace std;

//#define VERBOSE_MODE true
#define MAX_SIGNAL_WIDTH 1000000

class FileConfig {

public:
    FileConfig();
    FileConfig(string name, bool verbose);
    ~FileConfig();

    bool read_config_file(string name);
    stringstream print_config_data_map();
    string get_value(string key);
    string get_comments(string key);
    void set_value(string key, string value, string comment="");
    vector<string> get_keys();
    void update_config_file(string name="");
    bool get_status();
    int get_size();
    bool read_signal(string name);
    int get_num_sig_rows();
    int get_num_sig_colums();
    bool get_signal_loaded();
    vector<vector<double>>* get_signal();
    bool is_verbose();

private:
    bool conf_file_exists = false;
    int size = 0;
    string file_name = "";
    ifstream if_handle;
    ofstream of_handle;
    unordered_map<string, string> data_map;
    vector<string> key_vec;
    vector<string> comment_vec;

    bool signal_loaded;
    int num_sig_rows = 0;
    int num_sig_columns = 0;
    vector<vector<double>> signal;
    bool VERBOSE_MODE = false;
};

#endif
