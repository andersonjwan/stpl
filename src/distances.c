/***** mx_stpl_taliro : distances.c *****/
/* Version 0.1                     */

/* Written by Mohammad Hekmatnejad ASU, U.S.A. for stpl_taliro            */
/* Copyright (c) 2020  Mohammad Hekmatnejad                                  */
/* Send bug-reports and/or questions to: mhekmatn@asu.edu                  */
/* Written by Georgios Fainekos, ASU, U.S.A.                              */
/* Copyright (c) 2017  Georgios Fainekos                                  */
/* Send bug-reports and/or questions to: fainekos@asu.edu                  */

/* This program is free software; you can redistribute it and/or modify   */
/* it under the terms of the GNU General Public License as published by   */
/* the Free Software Foundation; either version 2 of the License, or      */
/* (at your option) any later version.                                    */
/*                                                                        */
/* This program is distributed in the hope that it will be useful,        */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of         */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          */
/* GNU General Public License for more details.                           */
/*                                                                        */
/* You should have received a copy of the GNU General Public License      */
/* along with this program; if not, write to the Free Software            */
/* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA*/

#include <math.h>
#include "undomex.h"
#include "matrix.h"
#include "distances.h"
#include "ltl2tree.h"
#include "param.h"

#define MIN_DIM 100

extern struct IdTableInfo id_table_info;

double computeArea(SpatialRegion* reg) {
    if (reg->is_NAN)
        return NAN;
    if (reg->size < 1 || reg->head == NULL)
        return 0;
    double area = 0;
    SpatialNode* node = reg->head;
    while (node != NULL) {
        if (node->conv_set != NULL)
            mexErrMsgTxt("computeArea: convex set not yet supported.");
        area += (node->bbox[1] - node->bbox[0]) * (node->bbox[3] - node->bbox[2]);
        node = node->next;
    }
    return area;
}

/* Hybrid distance computation without taking into account distance from guard sets */
HyDis SignedHDist0(double *xx, ConvSet *SS, int dim, double *LDist, mwSize tnLoc)
{
    int inLoc;
    mwIndex ii;
    HyDis dist;

    /* Are we in right control location? */
    inLoc = 0;
    for (ii=0; ii<SS->nloc; ii++)
        if (((int)xx[dim])==((int)SS->loc[ii]))
        {
            inLoc = 1;
            break;
        }
    /* Compute hybrid metric */
    if (inLoc)
    {
        dist.dl = 0;
        dist.ds = SignedDist(xx,SS,dim);
    }
    else
    {
        dist.dl = -(int)LDist[((int)xx[dim]-1)+((int)SS->idx)*tnLoc];
        dist.ds = -mxGetInf();
    }
    return(dist);
}

HyDis SignedHDistG(double *xx, ConvSet *SS, int dim, DistCompData *distData, mwSize tnLoc)
{
    bool inLoc; 
    int pathDist;
    mwIndex jj, im1, kk;
    HyDis dist;
    ConvSet GS;
    double tmp_dist, tmp_dist_min;
    mxArray *Aout, *bout, *xout;
    double *Atemp, *btemp, *xtemp; /* temporary pointers to set the values of the mxArrays */
    mxArray *lhs[1], *rhs[3];
    int i1,i2,i3,ii;
    int cloc;

    /* projection of xx to smaller yy */
    double *yy = NULL;

    cloc = (int)(xx[dim]);
    im1 = cloc-1;    
    if (im1<0)
    {
        mexErrMsgTxt("All location indices must be positive integers!"); 
    }
    inLoc = 0;
    /* Are we in right control location? */
    for (jj=0; jj<SS->nloc; jj++)
    {
        if (cloc==((int)SS->loc[jj]))
        {
            inLoc = 1;
            break;
        }
    }

        /* Compute hybrid metric */
        if (inLoc)
        {
            dist.dl = 0;
            dist.ds = SignedDist(xx,SS,dim);
        }
        else
        {
            pathDist = (int)distData->LDist[im1+((int)SS->idx)*tnLoc];
            dist.dl = -pathDist;
            dist.ds = -mxGetInf();
            if (distData->AdjLNell[im1]>0)
            {
                for (jj=0; jj<distData->AdjLNell[im1]; jj++)
                {
                    
                    if(distData->GuardMap[im1][jj].nproj>0){
                        yy = (double *)emalloc(sizeof(double)*distData->GuardMap[im1][jj].nproj);
                        for(ii=0;ii<distData->GuardMap[im1][jj].nproj;ii++){
                            yy[ii]=xx[distData->GuardMap[im1][jj].proj[ii]-1];
                        }
                    }

                    kk = (int)distData->AdjL[im1][jj]-1;
                    if ((int)distData->LDist[kk+((int)SS->idx)*tnLoc]<pathDist)
                    {
                        tmp_dist_min = -mxGetInf();
                        for (i1=0; i1<distData->GuardMap[im1][jj].nset; i1++)
                        {
                            GS.ncon = distData->GuardMap[im1][jj].ncon[i1];
                            GS.isSetRn = false;
                            GS.A = distData->GuardMap[im1][jj].A[i1];
                            GS.b = distData->GuardMap[im1][jj].b[i1];
                            /* For projection is Added*/
                            GS.proj= distData->GuardMap[im1][jj].proj;
                            GS.nproj= distData->GuardMap[im1][jj].nproj;
                            /* For projection*/
                            if (GS.ncon==1)
                                tmp_dist = SignedDist(xx,&GS,dim);
                            else
                            {
                                    /* For projection is Updated*/
                                    if(GS.nproj==0){
                                    Aout = mxCreateDoubleMatrix(GS.ncon, dim, mxREAL);
                                    bout = mxCreateDoubleMatrix(GS.ncon, 1, mxREAL);
                                    xout = mxCreateDoubleMatrix(dim, 1, mxREAL);
                                    Atemp = mxGetPr(Aout);
                                    btemp = mxGetPr(bout);
                                    xtemp = mxGetPr(xout);
                                    for (i3=0; i3<dim; i3++)
                                        xtemp[i3] = xx[i3];
                                    for (i2=0; i2<GS.ncon; i2++)
                                    {
                                        btemp[i2] = GS.b[i2];
                                        for (i3=0; i3<dim; i3++)
                                            Atemp[i3*GS.ncon+i2] = GS.A[i2][i3];
                                    }
                                    rhs[0] = xout; rhs[1] = Aout; rhs[2] = bout;
                                    mexCallMATLAB(1,lhs,3,rhs,"SignedDist");
                                    tmp_dist = *(mxGetPr(lhs[0]));
                                    mxDestroyArray(lhs[0]);
                                    mxDestroyArray(xout);
                                    mxDestroyArray(bout);
                                    mxDestroyArray(Aout);
                                }else{
                                    Aout = mxCreateDoubleMatrix(GS.ncon, GS.nproj, mxREAL);
                                    bout = mxCreateDoubleMatrix(GS.ncon, 1, mxREAL);
                                    xout = mxCreateDoubleMatrix(GS.nproj, 1, mxREAL);
                                    Atemp = mxGetPr(Aout);
                                    btemp = mxGetPr(bout);
                                    xtemp = mxGetPr(xout);
                                    for (i3=0; i3<GS.nproj; i3++)
                                        xtemp[i3] = yy[i3];
                                    for (i2=0; i2<GS.ncon; i2++)
                                    {
                                        btemp[i2] = GS.b[i2];
                                        for (i3=0; i3<GS.nproj; i3++)
                                            Atemp[i3*GS.ncon+i2] = GS.A[i2][i3];
                                    }
                                    rhs[0] = xout; rhs[1] = Aout; rhs[2] = bout;
                                    mexCallMATLAB(1,lhs,3,rhs,"SignedDist");
                                    tmp_dist = *(mxGetPr(lhs[0]));
                                    mxDestroyArray(lhs[0]);
                                    mxDestroyArray(xout);
                                    mxDestroyArray(bout);
                                    mxDestroyArray(Aout);
                                }
                                /* For projection */
                            }

                            if (tmp_dist>0)
                            {
                                tmp_dist = 0.0;
                                /*
                                mexPrintf("%s%d%s%d%s \n", "Guard: (",cloc,",",kk+1,")");
                                mexPrintf("%s%f \n", "Signed distance: ", &tmp_dist);
                                mexPrintf("%s%f \n", "Point on wrong side of guard: ", &xout);
                                mexErrMsgTxt("taliro: Above control location: The signed distance to the guard set is positive!"); 
                                */
                            }
                            /* Since the distances are always negative (the current point should never be within the current guard 
                               assuming ASAP transitions), then the required distance will be the maximum of the negative
                               distances returnde. */
                            tmp_dist_min = max(tmp_dist_min,tmp_dist);
                        }
                        dist.ds = max(dist.ds,tmp_dist_min);
                    }
                }
            }
        }

    if (yy != NULL)
            efree(yy);
    return(dist);
}

HyDis SignedHDist0NCLG(double *xx, ConvSet *SS, int dim, double **LDist, mwSize tnLoc,int clgIndex)
{
    int inLoc;
    mwIndex ii;
    HyDis dist;

    /* Are we in right control location? */
    inLoc = 0;
    for (ii = 0; ii < SS->nlocNCLG[clgIndex]; ii++) {
        /*mexPrintf("xx[dim+clgIndex]=%d\n", (int)(xx[dim + clgIndex]));
        mexPrintf("(int)SS->locNCLG[clgIndex][ii]=%d\n", (int)SS->locNCLG[clgIndex][ii]);*/
        if (((int)xx[dim + clgIndex]) == ((int)SS->locNCLG[clgIndex][ii]))
        {
            inLoc = 1;
            break;
        }
    }
    /* Compute hybrid metric */
    if (inLoc)
    {
        dist.dl = 0;
        dist.ds = SignedDist(xx,SS,dim);
    }
    else
    {
        dist.dl = -(int)LDist[clgIndex][((int)xx[dim + clgIndex] - 1) + ((int)SS->idx)*tnLoc];
        dist.ds = -mxGetInf();
    }
    return(dist);
}

/* Hybrid distance computation that takes into account distance from guard sets */
double SignedDist2(int *indx, ConvSet *SS, int dim)
{
    double dist;
    int ii, jj;
    /* Temporary vectors */
    //double x0[MIN_DIM];
    //double *x0d; /* In case dim is larger than MIN_DIM */
    double *xtemp, *Atemp, *btemp; /* temporary pointers to set the values of the mxArrays */
    /* Temporary scalars */
    //double aa, cc; 
    /* Temporary mxArrays to pass to Matlab */
    mxArray *Xout, *Aout, *bout;
    mxArray *lhs[1], *rhs[3];

    /* projection of xx to smaller yy */
    //double *yy;
    /* Temporary vectors */
    if(SS->nproj==0)
    {
        Xout = mxCreateDoubleMatrix(dim, 1, mxREAL);
        Aout = mxCreateDoubleMatrix(SS->ncon, dim, mxREAL);
        bout = mxCreateDoubleMatrix(SS->ncon, 1, mxREAL);
        xtemp = mxGetPr(Xout); 
        Atemp = mxGetPr(Aout);
        btemp = mxGetPr(bout);
        for(ii=0; ii<dim; ii++)
            xtemp[ii] = indx[ii];
        for(ii=0; ii<SS->ncon; ii++)
        {
            btemp[ii] = SS->b[ii];
            for (jj=0; jj<dim; jj++)
                Atemp[jj*SS->ncon+ii] = SS->A[ii][jj];
        }
        rhs[0] = Xout; rhs[1] = Aout; rhs[2] = bout;
        mexCallMATLAB(1,lhs,3,rhs,"SignedDist2");
        dist = *(mxGetPr(lhs[0]));
        mxDestroyArray(lhs[0]);
        mxDestroyArray(bout);
        mxDestroyArray(Aout);
        mxDestroyArray(Xout);
        return(dist);
    }
    mexErrMsgTxt("SignedDist2: not distance value!");
    return -(double)mxGetInf();
}

/* Hybrid distance computation that takes into account distance from guard sets */
double SignedDist(double *xx, ConvSet *SS, int dim)
{
    if (SS->isSetRn)
        return (mxGetInf());

    double dist;
    int ii, jj;
    /* Temporary vectors */
    double x0[MIN_DIM];
    double *x0d; /* In case dim is larger than MIN_DIM */
    double *xtemp, *Atemp, *btemp; /* temporary pointers to set the values of the mxArrays */
    /* Temporary scalars */
    double aa, cc; 
    /* Temporary mxArrays to pass to Matlab */
    mxArray *Xout, *Aout, *bout;
    mxArray *lhs[1], *rhs[3];



    /* projection of xx to smaller yy */
    /*double *yy = NULL;
    if(SS->nproj>0){
        yy = (double *)emalloc(sizeof(double)*SS->nproj);
        for(ii=0;ii<SS->nproj;ii++){
            yy[ii]=xx[SS->proj[ii]-1];
        }
    }*/
    
    /*if (SS->isSetRn) {
        if (yy != NULL)
            mxFree(yy);
        return (mxGetInf());
    }
    else*/
    {
        if (dim==1)
        {
            dist = fabs(SS->b[0]/SS->A[0][0]-*xx);
            if (SS->ncon==2)
                dist = dmin(dist,fabs(SS->b[1]/SS->A[1][0]-*xx));
            if (isPointInConvSet(xx, SS, dim))
                return(dist);
            else
                return(-dist);
        }
        else
        {
            /* if we have only one constraint in multi-Dimensional signals */
            if (SS->ncon==1)
            {
                if (dim<MIN_DIM)
                {
                    /* Projection on the plane: x0 = x+(b-a*x)*a'/norm(a)^2; */
                    /* For projection is Updated*/
                    if(SS->nproj==0){
                        aa = norm(SS->A[0],dim);
                        aa *= aa;
                        cc = ((SS->b[0]) - inner_prod(SS->A[0],xx,dim))/aa;
                        vec_scl(x0,cc,SS->A[0],dim);
                        dist = norm(x0,dim);
                        if (isPointInConvSet(xx, SS, dim))
                            return(dist);
                        else
                            return(-dist);
                    }else{
                        aa = norm(SS->A[0],SS->nproj);
                        aa *= aa;
                        /* projection of xx to smaller yy */
                        double* yy = NULL;
                        if (SS->nproj > 0) {
                            yy = (double*)emalloc(sizeof(double) * SS->nproj);
                            for (ii = 0; ii < SS->nproj; ii++) {
                                yy[ii] = xx[SS->proj[ii] - 1];
                            }
                        }
                        cc = ((SS->b[0]) - inner_prod(SS->A[0],yy,SS->nproj))/aa;
                        vec_scl(x0,cc,SS->A[0],SS->nproj);
                        dist = norm(x0,SS->nproj);
                        if (isPointInConvSet(yy, SS, SS->nproj)) {
                            if (yy != NULL)
                                efree(yy);
                            return(dist);
                        }
                        else {
                            if (yy != NULL)
                                efree(yy);
                            return(-dist);
                        }
                   }

                    /* For projection */
                }
                else
                {
                    /* Projection on the plane: x0 = x+(b-a*x)*a'/norm(a)^2; */
                    x0d = (double *)emalloc(sizeof(double)*dim);
                    aa = norm(SS->A[0],dim);
                    aa *= aa;
                    cc = ((SS->b[0]) - inner_prod(SS->A[0],xx,dim))/aa;
                    vec_scl(x0d,cc,SS->A[0],dim);
                    dist = norm(x0d,dim);
                    efree(x0d);
                    if (isPointInConvSet(xx, SS, dim))
                        return(dist);
                    else
                        return(-dist);
                }
            }
            /* if we have more than one constraints in multi-Dimensional signals */
            else
            {
                /* Prepare data and call SignedDist.m */
                /* From help: If unsuccessful in a MEX-file, the MEX-file terminates and control returns to the MATLAB prompt. */
                /* Alternatively use mexCallMATLABWithTrap */
                /* For projection is Updated*/
                if(SS->nproj==0)
                {
                    Xout = mxCreateDoubleMatrix(dim, 1, mxREAL);
                    Aout = mxCreateDoubleMatrix(SS->ncon, dim, mxREAL);
                    bout = mxCreateDoubleMatrix(SS->ncon, 1, mxREAL);
                    xtemp = mxGetPr(Xout); 
                    Atemp = mxGetPr(Aout);
                    btemp = mxGetPr(bout);
                    for(ii=0; ii<dim; ii++)
                        xtemp[ii] = xx[ii];
                    for(ii=0; ii<SS->ncon; ii++)
                    {
                        btemp[ii] = SS->b[ii];
                        for (jj=0; jj<dim; jj++)
                            Atemp[jj*SS->ncon+ii] = SS->A[ii][jj];
                    }
                    rhs[0] = Xout; rhs[1] = Aout; rhs[2] = bout;
                    mexCallMATLAB(1,lhs,3,rhs,"SignedDist");
                    dist = *(mxGetPr(lhs[0]));
                    mxDestroyArray(lhs[0]);
                    mxDestroyArray(bout);
                    mxDestroyArray(Aout);
                    mxDestroyArray(Xout);
                    return(dist);
                }else
                {
                    Xout = mxCreateDoubleMatrix(SS->nproj, 1, mxREAL);
                    Aout = mxCreateDoubleMatrix(SS->ncon, SS->nproj, mxREAL);
                    bout = mxCreateDoubleMatrix(SS->ncon, 1, mxREAL);
                    xtemp = mxGetPr(Xout); 
                    Atemp = mxGetPr(Aout);
                    btemp = mxGetPr(bout);
                    /* projection of xx to smaller yy */
                    for (ii = 0; ii < SS->nproj; ii++) 
                        xtemp[ii] = xx[SS->proj[ii] - 1];
                    /*for(ii=0; ii<SS->nproj; ii++)
                        xtemp[ii] = yy[ii];*/
                    for(ii=0; ii<SS->ncon; ii++)
                    {
                        btemp[ii] = SS->b[ii];
                        for (jj=0; jj<SS->nproj; jj++)
                            Atemp[jj*SS->ncon+ii] = SS->A[ii][jj];
                    }
                    rhs[0] = Xout; rhs[1] = Aout; rhs[2] = bout;
                    mexCallMATLAB(1,lhs,3,rhs,"SignedDist");
                    dist = *(mxGetPr(lhs[0]));
                    mxDestroyArray(lhs[0]);
                    mxDestroyArray(bout);
                    mxDestroyArray(Aout);
                    mxDestroyArray(Xout);
                    return(dist);
                }
            }
        }
    }
}

static int signal_col_index_by_fn_name[MAX_STQL_SIGNAL_DIM_SIZE];
int* get_signal_col_index_by_fn_name(char* fn_name) {
    int* res = signal_col_index_by_fn_name;
    if (strcmp(fn_name, "C") == 0) {
        //int res[2];
        res = (int*)emalloc(2 * sizeof(int));
        res[0] = 1;//number of dimensions
        res[1] = 3;//column of the input signal for the function Class
        return res;
    }
    else if (strcmp(fn_name, "P") == 0) {
        //int res[2];
        res = (int*)emalloc(2 * sizeof(int));
        res[0] = 1;//number of dimensions
        res[1] = 4;//column of the input signal for the function Probability
        return res;
    }
    else if (strcmp(fn_name, "D") == 0) {
        //int res[7];
        res = (int*)emalloc(7 * sizeof(int));
        res[0] = 6;//number of dimensions
        res[1] = 5;//x1//column of the input signal for the function Distance
        res[2] = 6;//y1
        res[3] = 7;//x2
        res[4] = 8;//y2
        res[5] = 9;//xc//x center
        res[6] = 10;//yc//y center
        return res;
    }
    else if (strncmp(fn_name, "SUBTRACT",8) == 0) {
        //int res[1];
        res = (int*)emalloc(1 * sizeof(int));
        res[0] = 0;
        return res;
    }
    //int res[1];
    res = (int*)emalloc(1 * sizeof(int));
    res[0] = -1;
    return res;
}

//static inline 
double compute_rob(int opr, double left_eval, double right_eval) {
    if (isnan(left_eval) || isnan(right_eval))
        return NAN;
    double rob;

    switch (opr) {
    case GE_OPR:
        rob = left_eval - right_eval;
        if (rob == 0)
            rob = mxGetInf();
        break;
    case LE_OPR:
        rob = right_eval - left_eval;
        if (rob == 0)
            rob = mxGetInf();
        break;
    case G_OPR:
        rob = left_eval - right_eval;
        if (rob == 0)
            rob = -1;
        break;
    case L_OPR:
        rob = right_eval - left_eval;
        if (rob == 0)
            rob = -1;
        break;
    case EQ_OPR:
        rob = left_eval - right_eval;
        if (rob != 0)
            rob = -mxGetInf();
            //rob = -1;
        else
            rob = mxGetInf();
            //rob = 1;
        break;
    case NE_OPR:
        rob = left_eval - right_eval;
        if (rob == 0)
            rob = -mxGetInf();
            //rob = -1;
        else
            rob = mxGetInf();
            //rob = 1;
        break;
    }
    return rob;
}

/*
converts any index order to Var_id values
i.e., 
2 variables V_id1, V_id2 therefore num_var_id = 2
at most 3 object at each frame therefore num_obj = 3
----------------------
index    V_id1    V_id2
----------------------
0        1        1
1        1        2
2        1        3
3        2        1
4        2        2
5        2        3
6        3        1
7        3        2
8        3        3
----------------------
for index =7, num_var_id =2, num_obj =3 the result is: {3,2}
*/
static char id_table_char_array[MAX_VAR_NUM+1];
char* from_IdTable_index_to_Var_id_values(int index, int num_var_id, int num_obj) {
    if (num_var_id > MAX_VAR_NUM)
        mexErrMsgTxt("from_IdTable_index_to_Var_id_values: overflow MAX_VAR_NUM");
    int rem;
    //int dvz;
    //char* res;
    int value = index;
    //res = (char*)emalloc((num_var_id+1) * sizeof(char));
    int i = 0;
    while(value >= 0 && i < num_var_id)
    {
        rem = value % num_obj;
        value = (int)(value / num_obj);
        id_table_char_array[i] = (char)(rem + 1);
        i++;
    }
    id_table_char_array[num_var_id] = '\0';
    return id_table_char_array;
}

//does reverse of from_IdTable_index_to_Var_id_values
int from_Var_id_values_to_IdTable_index(char* id, int num_var_id, int num_obj) {
    int res = 0;
    for (int i = 0; i < num_var_id; i++) {
        res += (int)((id[i]-1) * pow(num_obj, i));
    }
    return res;
}

/*
* This function is a helper in the below if-statement to decide if we should retrieve data from a
* frozen frame or the current one:
*         "if (gp != group || (gp == group && G_t_subformula[id_idx1]->sym != NULL && check_if_possible_frozen(pred_idx)))"
* This function receives the predicate index that includes ID variables.
* 1- It finds the outter most quantifier operator that includes the predicate, and stores its index into the qnf_idx.
*        If there is another quantifier in the next upper layer then its index will be store into qnf_idx_parent.
* 2- Only if the group associated to the two quantifers are identical, or if there is only one quantifier,
*    then returns true.
* Example 1. @(Var_x,_,FORALL,Var_id_1)( ... @(Var_y,_,FORALL,Var_id_2)( P(Var_id_1) > P(Var_id_2) ... @(FORALL,Var_id_3) ( P(Var_id_2) > P(Var_id_3) )  )  )
*
* - In the above formula, while evaluating non-equality "P(Var_id_2) > P(Var_id_3)",
*    for P(Var_id_3): group of non-equality is the same as the group of the quantifier that Var_id_3 belongs to,
*    therefore in the above if-statement we have gp == group, but as the 3rd quantifier does not have
*    time/frmae variables, then it evaluates to false. that means we use the current frame to retrieve the probability of Var_id_3.
*
* - In the above formula, while evaluating non-equality "P(Var_id_2) > P(Var_id_3)",
*    for P(Var_id_2): group of non-equality is not the same as the group of the quantifier that Var_id_3 belongs to,
*    therefore in the above if-statement we have gp != group, and the whole statement evaluates to true.
*    that means we use the frozen frame to retrieve the probability of Var_id_2.
*
* - In the above formula, while evaluating non-equality "P(Var_id_1) > P(Var_id_2)",
*    for P(Var_id_2): group of non-equality is the same as the group of the quantifier that Var_id_2 belongs to,
*    therefore in the above if-statement we have gp == group, and as the 2nd quantifier has
*    time/frmae variables, then we call "check_if_possible_frozen". In there, the group of quantifiers in which 
*    Var_id_2 and its parent belong to are not equal. That means the function returns false, therefore the if-statement becomes false.
*    Therefore, we use the current frame to calculate the P(Var_id_2).
*
* - In the above formula, while evaluating non-equality "P(Var_id_1) > P(Var_id_2)",
*    for P(Var_id_1): group of non-equality is not the same as the group of the quantifier that Var_id_1 belongs to,
*    therefore in the above if-statement we have gp != group, and the whole statement evaluates to true.
*    that means we use the frozen frame to retrieve the probability of Var_id_1.
*/
bool check_if_possible_frozen(int pred_idx) {
    bool res = false;
    int qnf_idx = -1;
    int qnf_idx_parent = -1;

    for (int i = size_id_to_sub_idx_map - 1; i >= 0; i--) {
        if (id_to_sub_idx_map[i].value.data < pred_idx) {
            qnf_idx = id_to_sub_idx_map[i].value.data;
            if (i > 0)
                qnf_idx_parent = id_to_sub_idx_map[i-1].value.data;
            break;
        }
    }

    if ( (qnf_idx > 0 && qnf_idx_parent > 0 &&
        G_t_subformula[qnf_idx]->group == G_t_subformula[qnf_idx_parent]->group)
        ||
        (qnf_idx > 0 && qnf_idx_parent < 0))
        res = true;

    return res;
}

bool division_by_zero = false;
int min_id_rank = -1;
/*double recurrent_fn_computing(FnDef* fnDef, int group, char* id, int num_id_var,
    double** xx, double** ff, int s_xx, int s_ff, int pred_idx, int et_idx, int ft_idx) {*/
double recurrent_fn_computing(FnDef* fnDef, int group, char* id, int num_id_var,
    double xx[MAX_OBJ_IN_FRAME][MAX_STQL_SIGNAL_DIM_SIZE], double ff[MAX_OBJ_IN_FRAME][MAX_STQL_SIGNAL_DIM_SIZE],
    int s_xx, int s_ff, int pred_idx, int et_idx, int ft_idx) {
    //it is very crusial to return NAN for the id values that exceeds
    //the number of objects in the current/freezed frame
    double fn_res = -mxGetInf();// NAN;//to ignore the value in future computation
    //param_res = (double*)emalloc(fnDef->num_param * sizeof(FnDef));

    //    indexFromConstStringKW
    //    {CONST_UNKNOWN = 0, CONST_CT, CONST_LM, CONST_RM, CONST_TM, CONST_BM, CONST_EMPTYSET, CONST_UNIVERSE}
    //  { "UNKNOWN_KW", "CT", "LM", "RM", "TM" , "BM" , "EMPTYSET" , "UNIVERSE" }

    if (fnDef->num_param == 0) {
        if (fnDef->fn_type == VAR_RESERVED)
            fn_res = -1;
        else if (fnDef->fn_type == VAR_CONST)
            fn_res = atof(fnDef->fn_name);
        else if (fnDef->fn_type == VAR_ID) 
            fn_res = find_in_map(id_to_rank_map, fnDef->fn_name, size_id_to_rank_map);
        //else if (fnDef->fn_type == VAR_SPATIO) {
        //    mexErrMsgTxt("Spatio formula is not supported at this version.");
        //}
        else
            fn_res = indexFromConstStringKW(fnDef->fn_name);
        return fn_res;
    }

    //start computing function values//
    //comment #1001: this is very time consuming to dynamically allocate memory and release it later
    /*double* params;
    if(fnDef->num_param > 0)
        params = (double*)emalloc(fnDef->num_param * sizeof(double));
    //static double params[MAX_NUM_PARAMS];*/
    double params[MAX_NUM_PARAMS];
    int gp;

    //not for VAR_ID parameters
    for (int i = 0; i < fnDef->num_param; i++) {
        if (fnDef->params[i].num_vars > 0 
            || fnDef->params[i].fn_type == VAR_FUN
            ) {
            params[i] = recurrent_fn_computing(&fnDef->params[i],
                group, id, num_id_var, xx, ff, s_xx, s_ff, pred_idx, et_idx, ft_idx);
            if (fnDef->params[i].fn_type == VAR_ID) {
                for (int j = 0; j < num_id_var; j++) {
                    if (freez_idx_to_list_param[pred_idx][j] == (int)params[i]) {
                        params[i] = id[num_id_var - j -1];
                        if (min_id_rank > params[i])
                            min_id_rank = (int)params[i];
                        break;
                    }
                }
            }
        }
        else {
            params[i] = indexFromConstStringKW(fnDef->params[i].fn_name);
        }
    }

    if ( strcmp(fnDef->fn_name, "C") == 0 || strcmp(fnDef->fn_name, "P") == 0) {
        int num_obj = s_xx;
        int col = 3;//column of signal for Class
        bool is_freez = false;
        if (strcmp(fnDef->fn_name, "P") == 0)
            col = 4;//column of signal for Probability

        gp = find_in_map(id_to_group_map, fnDef->params[0].fn_name, *size_id_to_group_map);

        int id_idx1 = find_in_map(id_to_sub_idx_map, fnDef->params[0].fn_name, size_id_to_sub_idx_map);

        if (id_idx1 < 0)
            mexErrMsgTxt("recurrent_fn_computing: undefined Var_id_?");

        if (gp != group || (gp == group && G_t_subformula[id_idx1]->sym != NULL && check_if_possible_frozen(pred_idx))) {
            //frame = ff;//look in the freezed frame
            num_obj = s_ff;
            is_freez = true;
        }
        if (params[0] > 0 && params[0] <= num_obj) {
            int idx = (int)params[0] - 1;
            if (is_freez)
                fn_res = ff[idx][col];
            else
                fn_res = xx[idx][col];
        }
        else
            fn_res = NAN;
    }
    //Last Frame Existed Before: LTEB(Var_id) -> time duration, -1 if none
    // if there exists an object with the same id in any previous frames, then it returns its time delay
    //Last Time Existed Before: LFEB(Var_id) -> number of frames, -1 if none 
    // if there exists an object with the same id in any previous frames, then it returns its frame difference
    else if (strcmp(fnDef->fn_name, "LTEB") == 0 || strcmp(fnDef->fn_name, "LFEB") == 0) {
        int num_obj = s_xx;
        int col = 2;//column of signal for Class
        bool is_freez = false;
        bool ret_time_duration = false;

        if (strcmp(fnDef->fn_name, "LTEB") == 0)
            ret_time_duration = true;

        gp = find_in_map(id_to_group_map, fnDef->params[0].fn_name, *size_id_to_group_map);

        int id_idx1 = find_in_map(id_to_sub_idx_map, fnDef->params[0].fn_name, size_id_to_sub_idx_map);

        if (id_idx1 < 0)
            mexErrMsgTxt("recurrent_fn_computing: undefined Var_id_?");

        if (gp != group || (gp == group && G_t_subformula[id_idx1]->sym != NULL && check_if_possible_frozen(pred_idx))) {
            num_obj = s_ff;
            is_freez = true;
        }

        if (params[0] > 0 && params[0] <= num_obj) {
            int idx = (int)params[0] - 1;
            int start_search_index = G_p_par->cur_frame_index;
            int obj_id = -1;
            fn_res = +mxGetInf();
            if (is_freez) {
                obj_id = ff[idx][col];
                start_search_index = G_p_par->frz_frame_index;
            }
            else
                obj_id = xx[idx][col];
            for (int ii = start_search_index-1; ii >= 0; ii--) {
                if (G_XTrace[ii + 2 * G_p_par->nSamp] == obj_id) {
                    if (ret_time_duration)
                        fn_res = G_XTrace[start_search_index + G_p_par->nSamp] - G_XTrace[ii + G_p_par->nSamp];
                    else
                        fn_res = G_XTrace[start_search_index] - G_XTrace[ii];
                    break;
                }
            }
        }
        else
            fn_res = NAN;
    }
    else if (strcmp(fnDef->fn_name, "ED") == 0) {//Euclidean distance between two points in a 2D-space
        if (fnDef->num_param != 4)
            mexErrMsgTxt("recurrent_fn_computing: ED has four arguments!");
        //double** frame1 = xx;//look in the current frame
        //double** frame2 = xx;//look in the current frame
        int num_obj1 = s_xx;
        int num_obj2 = s_xx;
        bool is_freez_1 = false;
        bool is_freez_2 = false;
        gp = find_in_map(id_to_group_map, fnDef->params[0].fn_name, *size_id_to_group_map);
        
        int id_idx1 = find_in_map(id_to_sub_idx_map, fnDef->params[0].fn_name, size_id_to_sub_idx_map);
        int id_idx2 = find_in_map(id_to_sub_idx_map, fnDef->params[2].fn_name, size_id_to_sub_idx_map);
        
        if (id_idx1 < 0 || id_idx2 < 0)
            mexErrMsgTxt("recurrent_fn_computing: undefined Var_id_?");

        if (gp != group || (gp == group && G_t_subformula[id_idx1]->sym != NULL && check_if_possible_frozen(pred_idx))) {
            //frame1 = ff;
            num_obj1 = s_ff;
            is_freez_1 = true;
        }
        gp = find_in_map(id_to_group_map, fnDef->params[2].fn_name, *size_id_to_group_map);
        if (gp != group || (gp == group && G_t_subformula[id_idx2]->sym != NULL && check_if_possible_frozen(pred_idx))) {
            //frame2 = ff;
            num_obj2 = s_ff;
            is_freez_2 = true;
        }
        int idx1 = -1, idx2 = -1;
        double x1, y1, x2, y2;
        /*for (int i = 0; i < num_obj1; i++) {
            if ((int)frame1[i][2] == params[0]) {
                idx1 = i;
                break;
            }
        }*/
        
        if (params[0] > 0 && params[0] <= num_obj1)
            idx1 = (int)params[0]-1;

        if (idx1 >= 0)
        {
            //return fn_res;
            /*for (int i = 0; i < num_obj2; i++) {
                if (frame2[i][2] == params[2]) {
                    idx2 = i;
                    break;
                }
            }*/
            
            if (params[2] > 0 && params[2] <= num_obj2)
                idx2 = (int)params[2]-1;

            if (idx2 >= 0)
            {
                //return fn_res;
                if ((int)params[1] == CONST_CT) {
                    x1 = 9;//index in the signal for xc
                    y1 = 10;//yc
                }
                else if ((int)params[1] == CONST_LM) {
                    x1 = 5;//index in the signal for x1
                    y1 = 6;//y1
                }
                else if ((int)params[1] == CONST_TM) {
                    x1 = 7;//x2
                    y1 = 6;//y1
                }
                else if ((int)params[1] == CONST_RM) {
                    x1 = 7;//x2
                    y1 = 8;//y2
                }
                else if ((int)params[1] == CONST_BM) {
                    x1 = 5;//x1
                    y1 = 8;//y2
                }
                else {
                    mexPrintf("Wrong parameter '%s' in function %s\n", fnDef->params[1].fn_name, fnDef->fn_name);
                    mexErrMsgTxt("Wrong parameter in function ED");
                }
                if ((int)params[3] == CONST_CT) {
                    x2 = 9;//index in the signal for xc
                    y2 = 10;//yc
                }
                else if ((int)params[3] == CONST_LM) {
                    x2 = 5;//index in the signal for x1
                    y2 = 6;//y1
                }
                else if ((int)params[3] == CONST_TM) {
                    x2 = 7;//x2
                    y2 = 6;//y1
                }
                else if ((int)params[3] == CONST_RM) {
                    x2 = 7;//x2
                    y2 = 8;//y2
                }
                else if ((int)params[3] == CONST_BM) {
                    x2 = 5;//x1
                    y2 = 8;//y2
                }
                else {
                    mexPrintf("Wrong parameter '%s' in function %s\n", fnDef->params[1].fn_name, fnDef->fn_name);
                    mexErrMsgTxt("Wrong parameter in function D");
                }
                //compute Euclidean distance
                if (is_freez_1) {
                    x1 = ff[idx1][(int)x1];
                    y1 = ff[idx1][(int)y1];
                }
                else {
                    x1 = xx[idx1][(int)x1];
                    y1 = xx[idx1][(int)y1];
                }
                if (is_freez_2) {
                    x2 = ff[idx2][(int)x2];
                    y2 = ff[idx2][(int)y2];
                }
                else {
                    x2 = xx[idx2][(int)x2];
                    y2 = xx[idx2][(int)y2];
                }

                //x1 = frame1[idx1][(int)x1];
                //y1 = frame1[idx1][(int)y1];
                //x2 = frame2[idx2][(int)x2];
                //y2 = frame2[idx2][(int)y2];
                fn_res = sqrt(pow(x2 - x1, 2) + pow(y2 - y1, 2));
            }
        }
    }
    else if (strcmp(fnDef->fn_name, "SUB") == 0) {
        if (fnDef->num_param != 2)
            mexErrMsgTxt("recurrent_fn_computing: SUB has two arguments!");
        if(DEBUG_MODE)
                mexPrintf("SUB: %d - %d\n",(int)params[0],(int)params[1]);
        
        //fn_res = params[0] - params[1];

        
        //double** frame1 = xx;//look in the current frame
        //double** frame2 = xx;//look in the current frame
        bool is_freez_1 = false;
        bool is_freez_2 = false;
        int num_obj1 = s_xx;
        int num_obj2 = s_xx;
        gp = find_in_map(id_to_group_map, fnDef->params[0].fn_name, *size_id_to_group_map);

        int id_idx1 = find_in_map(id_to_sub_idx_map, fnDef->params[0].fn_name, size_id_to_sub_idx_map);
        int id_idx2 = find_in_map(id_to_sub_idx_map, fnDef->params[1].fn_name, size_id_to_sub_idx_map);

        if (id_idx1 < 0 || id_idx2 < 0)
            mexErrMsgTxt("recurrent_fn_computing: undefined Var_id_?");

        if (gp != group || (gp == group && G_t_subformula[id_idx1]->sym != NULL && check_if_possible_frozen(pred_idx))) {
            //frame1 = ff;
            num_obj1 = s_ff;
            is_freez_1 = true;
        }
        gp = find_in_map(id_to_group_map, fnDef->params[1].fn_name, *size_id_to_group_map);
        if (gp != group || (gp == group && G_t_subformula[id_idx2]->sym != NULL && check_if_possible_frozen(pred_idx))) {
            //frame2 = ff;
            num_obj2 = s_ff;
            is_freez_2 = true;
        }
        int idx1 = -1, idx2 = -1;
        //double x1, y1, x2, y2;
        fn_res = NAN;//important to be NAN not -inf so that we take care of it after the function returned
        if (params[0] > 0 && params[0] <= num_obj1)
            idx1 = (int)params[0] - 1;

        if (idx1 >= 0)
        {
            if (params[1] > 0 && params[1] <= num_obj2)
                idx2 = (int)params[1] - 1;

            if (idx2 >= 0)
            {
                int id_val1, id_val2;
                /*if (is_freez_1) {
                    id_val1 = (int)ff[idx1][2];
                    id_val2 = (int)ff[idx2][2];
                }
                else {
                    id_val1 = (int)xx[idx1][2];
                    id_val2 = (int)xx[idx2][2];
                }*/
                
                if (is_freez_1) {
                    id_val1 = (int)ff[idx1][2];
                }
                else {
                    id_val1 = (int)xx[idx1][2];
                }
                if (is_freez_2) {
                    id_val2 = (int)ff[idx2][2];
                }
                else {
                    id_val2 = (int)xx[idx2][2];
                }

                //int id_val1 = (int)frame1[idx1][2];
                //int id_val2 = (int)frame2[idx2][2];
                fn_res = id_val1 - id_val2;

            }
        }
    }
    else if (strcmp(fnDef->fn_name, "AREA") == 0) {
    if (fnDef->num_param != 1)
        mexErrMsgTxt("recurrent_fn_computing: SFORALL has one argument!");
    SpatialRegion* sreg;
        if (params[0] < 0) {//use the spatial formula to calculate the final spatial region
            if (fnDef->params != NULL) {
                
                sreg = DP_SPATIAL(fnDef->params[0].spatio_idx, et_idx, ft_idx, id, pred_idx);
                double area = computeArea(sreg);
                ////release_spatial_node(&sreg->head);
                ////hfree(sreg);
                //release_spatial_region(&sreg);
                fn_res = area;
            }
            else
                mexErrMsgTxt("recurrent_fn_computing: Param is NULL!");
        }
        else {//use the Var_id info to retreive the bounding box of the object 
            //mexErrMsgTxt("To be completed for variable parameters.");
            //double** frame = xx;//look in the current frame
            bool is_freez = false;
            int num_obj = s_xx;
            int col = 5;//column of signal for x1
            gp = find_in_map(id_to_group_map, fnDef->params[0].fn_name, *size_id_to_group_map);
            int id_idx1 = find_in_map(id_to_sub_idx_map, fnDef->params[0].fn_name, size_id_to_sub_idx_map);

            if (id_idx1 < 0)
                mexErrMsgTxt("recurrent_fn_computing: undefined Var_id_?");

            if (gp != group || (gp == group && G_t_subformula[id_idx1]->sym != NULL && check_if_possible_frozen(pred_idx))) {
                //frame = ff;//look in the freezed frame
                num_obj = s_ff;
                is_freez = true;
            }
            for (int i = 0; i < num_obj; i++) {
                if (is_freez) {
                    if ((int)ff[i][2] == params[0]) {
                        double area = (ff[i][col + 2] - ff[i][col]) * (ff[i][col + 3] - ff[i][col + 1]);
                        fn_res = area;
                    }
                }
                else {
                    if ((int)xx[i][2] == params[0]) {
                        double area = (xx[i][col + 2] - xx[i][col]) * (xx[i][col + 3] - xx[i][col + 1]);
                        fn_res = area;
                    }
                }
            }
        }
    }
    else if (strcmp(fnDef->fn_name, "SEXISTS") == 0) {
    if (fnDef->num_param != 1)
        mexErrMsgTxt("recurrent_fn_computing: SFORALL has one argument!");
        SpatialRegion* sreg;
        if (params[0] < 0) {//use the spatial formula to calculate the final spatial region
            if (fnDef->params != NULL) {
                sreg = DP_SPATIAL(fnDef->params[0].spatio_idx, et_idx, ft_idx, id, pred_idx);
                //double result;
                if (sreg->is_NAN)
                    fn_res = NAN;
                else if (! isSpatialEmpty(sreg))
                    fn_res = mxGetInf();
                else
                    fn_res = -mxGetInf();
                //release_spatial_region(&sreg);
                ////return result;
            }
            else
                mexErrMsgTxt("recurrent_fn_computing: Param is NULL!");
        }
        else {//use the Var_id info to retreive the bounding box of the object 
            //mexErrMsgTxt("To be completed for variable parameters.");
            //double** frame = xx;//look in the current frame
            bool is_freez = false;
            int num_obj = s_xx;
            int col = 5;//column of signal for x1
            gp = find_in_map(id_to_group_map, fnDef->params[0].fn_name, *size_id_to_group_map);
            int id_idx1 = find_in_map(id_to_sub_idx_map, fnDef->params[0].fn_name, size_id_to_sub_idx_map);

            if (id_idx1 < 0)
                mexErrMsgTxt("recurrent_fn_computing: undefined Var_id_?");

            if (gp != group || (gp == group && G_t_subformula[id_idx1]->sym != NULL && check_if_possible_frozen(pred_idx))) {
                //frame = ff;//look in the freezed frame
                num_obj = s_ff;
                is_freez = true;
            }
            for (int i = 0; i < num_obj; i++) {
                if (is_freez) {
                    if ((int)ff[i][2] == params[0]) {
                        if ((ff[i][col + 2] - ff[i][col]) >= 0 &&
                            (ff[i][col + 3] - ff[i][col + 1]) >= 0)
                            fn_res = mxGetInf();
                        else
                            fn_res = -mxGetInf();
                    }
                }
                else {
                    if ((int)xx[i][2] == params[0]) {
                        if ((xx[i][col + 2] - xx[i][col]) >= 0 &&
                            (xx[i][col + 3] - xx[i][col + 1]) >= 0)
                            fn_res = mxGetInf();
                        else
                            fn_res = -mxGetInf();
                    }
                }
            }
        }
        //mexPrintf("\n\n%s function is not supported.\n\n", fnDef->fn_name);
        //mexErrMsgTxt("Unsupported function.");
    }
    else if (strcmp(fnDef->fn_name, "SFORALL") == 0) {
    if (fnDef->num_param != 1)
        mexErrMsgTxt("recurrent_fn_computing: SFORALL has one argument!");
    SpatialRegion* sreg;
        if (params[0] < 0) {//use the spatial formula to calculate the final spatial region
            if (fnDef->params != NULL) {
                sreg = DP_SPATIAL(fnDef->params[0].spatio_idx, et_idx, ft_idx, id, pred_idx);
                //double result;
                if (sreg->is_NAN)
                    fn_res = NAN;
                else if (isSpatialUniverse(sreg))
                    fn_res = mxGetInf();
                else
                    fn_res = -mxGetInf();
                //isSpatialEmpty(sreg);
                //release_spatial_region(&sreg);
                ////return fn_res;
            }
            else
                mexErrMsgTxt("recurrent_fn_computing: Param is NULL!");
        }
        else {//use the Var_id info to retreive the bounding box of the object 
            //mexErrMsgTxt("To be completed for variable parameters.");
            //double** frame = xx;//look in the current frame
            bool is_freez = false;
            int num_obj = s_xx;
            int col = 5;//column of signal for x1
            gp = find_in_map(id_to_group_map, fnDef->params[0].fn_name, *size_id_to_group_map);
            int id_idx1 = find_in_map(id_to_sub_idx_map, fnDef->params[0].fn_name, size_id_to_sub_idx_map);

            if (id_idx1 < 0)
                mexErrMsgTxt("recurrent_fn_computing: undefined Var_id_?");

            if (gp != group || (gp == group && G_t_subformula[id_idx1]->sym != NULL && check_if_possible_frozen(pred_idx))) {
                //frame = ff;//look in the freezed frame
                num_obj = s_ff;
                is_freez = true;
            }
            for (int i = 0; i < num_obj; i++) {
                if (is_freez) {
                    if ((int)ff[i][2] == params[0]) {
                        if (ff[i][col] == _UNIVERSE[0] && ff[i][col + 1] == _UNIVERSE[2] &&
                            ff[i][col + 2] == _UNIVERSE[1] && ff[i][col + 3] == _UNIVERSE[3])
                            fn_res = mxGetInf();
                        else
                            fn_res = -mxGetInf();
                    }
                }
                else {
                    if ((int)xx[i][2] == params[0]) {
                        if (xx[i][col] == _UNIVERSE[0] && xx[i][col + 1] == _UNIVERSE[2] &&
                            xx[i][col + 2] == _UNIVERSE[1] && xx[i][col + 3] == _UNIVERSE[3])
                            fn_res = mxGetInf();
                        else
                            fn_res = -mxGetInf();
                    }
                }
            }
        }
        //mexPrintf("\n\n%s function is not supported.\n\n", fnDef->fn_name);
        //mexErrMsgTxt("Unsupported function.");
    }
    else if (strcmp(fnDef->fn_name, "LAT") == 0) {
    if (fnDef->num_param != 2)
        mexErrMsgTxt("recurrent_fn_computing: LAT has two arguments!");
    //double** frame1 = xx;//look in the current frame
    bool is_freez = false;
    int num_obj1 = s_xx;
    gp = find_in_map(id_to_group_map, fnDef->params[0].fn_name, *size_id_to_group_map);
    int id_idx1 = find_in_map(id_to_sub_idx_map, fnDef->params[0].fn_name, size_id_to_sub_idx_map);

    if (id_idx1 < 0)
        mexErrMsgTxt("recurrent_fn_computing: undefined Var_id_?");

    if (gp != group || (gp == group && G_t_subformula[id_idx1]->sym != NULL && check_if_possible_frozen(pred_idx))) {
        //frame1 = ff;
        num_obj1 = s_ff;
        is_freez = true;
    }

    int idx1 = -1;
    double x1;
    /*for (int i = 0; i < num_obj1; i++) {
        if ((int)frame1[i][2] == params[0]) {
            idx1 = i;
            break;
        }
    }*/
    
    if (params[0] > 0 && params[0] <= num_obj1) {
        idx1 = (int)params[0]-1;
    }

    if (idx1 >= 0)
    {
        //return fn_res;

        if ((int)params[1] == CONST_CT) {
            x1 = 9;//index in the signal for xc
        }
        else if ((int)params[1] == CONST_LM) {
            x1 = 5;//index in the signal for x1
        }
        else if ((int)params[1] == CONST_TM) {
            x1 = 7;//x2
        }
        else if ((int)params[1] == CONST_RM) {
            x1 = 7;//x2
        }
        else if ((int)params[1] == CONST_BM) {
            x1 = 5;//x1
        }
        else {
            mexPrintf("Wrong parameter '%s' in function %s\n", fnDef->params[1].fn_name, fnDef->fn_name);
            mexErrMsgTxt("Wrong parameter in function LAT");
        }

        if(is_freez)
            fn_res = ff[idx1][(int)x1];
        else
            fn_res = xx[idx1][(int)x1];
        //return fn_res;
    }
    }
    else if (strcmp(fnDef->fn_name, "LON") == 0) {
    if (fnDef->num_param != 2)
        mexErrMsgTxt("recurrent_fn_computing: LON has two arguments!");
    //double** frame1 = xx;//look in the current frame
    bool is_freez = false;
    int num_obj1 = s_xx;
    gp = find_in_map(id_to_group_map, fnDef->params[0].fn_name, *size_id_to_group_map);
    int id_idx1 = find_in_map(id_to_sub_idx_map, fnDef->params[0].fn_name, size_id_to_sub_idx_map);

    if (id_idx1 < 0)
        mexErrMsgTxt("recurrent_fn_computing: undefined Var_id_?");

    if (gp != group || (gp == group && G_t_subformula[id_idx1]->sym != NULL && check_if_possible_frozen(pred_idx))) {
        //frame1 = ff;
        num_obj1 = s_ff;
        is_freez = true;
    }

    int idx1 = -1;
    double y1;
    /* (int i = 0; i < num_obj1; i++) {
        if ((int)frame1[i][2] == params[0]) {
            idx1 = i;
            break;
        }
    }*/
    
    if (params[0] > 0 && params[0] <= num_obj1) {
        idx1 = (int)params[0]-1;
    }

    if (idx1 >= 0)
    {
        //return fn_res;

        if ((int)params[1] == CONST_CT) {
            y1 = 10;//yc
        }
        else if ((int)params[1] == CONST_LM) {
            y1 = 6;//y1
        }
        else if ((int)params[1] == CONST_TM) {
            y1 = 6;//y1
        }
        else if ((int)params[1] == CONST_RM) {
            y1 = 8;//y2
        }
        else if ((int)params[1] == CONST_BM) {
            y1 = 8;//y2
        }
        else {
            mexPrintf("Wrong parameter '%s' in function %s\n", fnDef->params[1].fn_name, fnDef->fn_name);
            mexErrMsgTxt("Wrong parameter in function LON");
        }
        
        if(is_freez)
            fn_res = ff[idx1][(int)y1];
        else
            fn_res = xx[idx1][(int)y1];
        //return fn_res;
    }
    }
    else if (strcmp(fnDef->fn_name, "RATIO") == 0) {
        if (fnDef->num_param != 2)
            mexErrMsgTxt("recurrent_fn_computing: RATIO has two arguments!");
        division_by_zero = false;
        if (params[1] == 0)
        {
            division_by_zero = true;
            fn_res = params[0];
        }
        else {
            fn_res = params[0] / params[1];
        }
        //return fn_res;
        //mexPrintf("\n\n%s function is not supported.\n\n", fnDef->fn_name);
        //mexErrMsgTxt("Unsupported function.");
    }
    else {
        mexPrintf("\n\n%s function is not supported.\n\n", fnDef->fn_name);
        mexErrMsgTxt("Unsupported function.");
    }

    //note: is related to the comment #1001
    /*if (fnDef->num_param > 0) {
        mxFree(params);
    }*/
    return fn_res;
}

double SignedDistForPredicates(double* xx, struct FnExpr* fnx, int pred_idx) {

    double rob;
    double left_eval;
    double right_eval;
    double infval = mxGetInf();

    if (fnx == NULL || fnx->left_fn.fn_name == NULL)
        mexErrMsgTxt("Error with predicate passes to SignedDistForPredicates.");


    int* idx = get_signal_col_index_by_fn_name(fnx->left_fn.fn_name);
    if (idx[0] < 0) {
        //mxFree(idx);//no need anymore
        mexPrintf("%s is not a valid function name.", fnx->left_fn.fn_name);
        mexErrMsgTxt("Unknown function name.");
    }
    else if (idx[0] == 0) {//parameters are ID variables
        left_eval = 0;//@todo: to be fixed later
    }
    //just for now simplify this for test purposes
    else
        left_eval = xx[idx[1]];

    //mxFree(idx);//no need anymore

    idx = get_signal_col_index_by_fn_name(fnx->right_fn.fn_name);
    if (idx[0] < 0)//this has to be a number
        right_eval = atof(fnx->right_fn.fn_name);
    else//this is a function
        right_eval = xx[idx[1]];

    switch (fnx->opr) {
    case GE_OPR:
        rob = left_eval - right_eval;
        //rob = rob == 0 ? infval : rob;
        break;
    case LE_OPR:
        rob = right_eval - left_eval;
        //rob = rob == 0 ? infval : rob;
        break;
    case G_OPR:
        rob = left_eval - right_eval;
        if (rob == 0)
            rob = -1;
        break;
    case L_OPR:
        rob = right_eval - left_eval;
        if (rob == 0)
            rob = -1;
        break;
    case EQ_OPR:
        rob = left_eval - right_eval;
        if (rob != 0)
            rob = -1;
            //rob = -infval;
        else
            rob = 1;
            //rob = infval;
        break;
    }

    //mxFree(idx);//no need anymore

    return rob;
}

static bool use_id_for_freez_object[MAX_VAR_NUM];
/*void SignedDistForPredicatesOverVarIds(double** xx, struct FnExpr* fnx,
    int dim, int num_id, int pred_idx, int group,
    double** ff, double** IdTable,
    int size_xx, int size_ff, int et_idx, int ft_idx, double* min_d, double* max_d) {*/
void SignedDistForPredicatesOverVarIds(double xx[MAX_OBJ_IN_FRAME][MAX_STQL_SIGNAL_DIM_SIZE], struct FnExpr* fnx,
    int dim, int num_id, int pred_idx, int group,
    double ff[MAX_OBJ_IN_FRAME][MAX_STQL_SIGNAL_DIM_SIZE], double** IdTable,
    int size_xx, int size_ff, int et_idx, int ft_idx, double* min_d, double* max_d) {

    //double* rob = (double*)emalloc(num_id * sizeof(double));
    double left_eval;
    double right_eval;
    double infval = mxGetInf();

    if (num_id == 0)
        mexErrMsgTxt("SignedDistForPredicatesOverVarIds: number of var id must be greater than 0");

    if (DEBUG_MODE)
    {
        mexPrintf("\n");
        print_fn_def(fnx->left_fn);
        print_opr(fnx->opr);
        print_fn_def(fnx->right_fn);
        mexPrintf("\n");
        for (int i = 0; i < fnx->left_fn.num_param; i++)
            if (fnx->left_fn.params[i].fn_type == VAR_ID)
                mexPrintf("%s --> rank = %d group = %d\n", fnx->left_fn.params[i].fn_name,
                    find_in_map(id_to_rank_map, fnx->left_fn.params[i].fn_name, size_id_to_rank_map),
                    find_in_map(id_to_group_map, fnx->left_fn.params[i].fn_name, *size_id_to_group_map));
        for (int i = 0; i < fnx->right_fn.num_param; i++)
            if (fnx->right_fn.params[i].fn_type == VAR_ID)
                mexPrintf("%s -->rank = %d group = %d\n", fnx->right_fn.params[i].fn_name,
                    find_in_map(id_to_rank_map, fnx->right_fn.params[i].fn_name, size_id_to_rank_map),
                    find_in_map(id_to_group_map, fnx->right_fn.params[i].fn_name, *size_id_to_group_map));
    }

    *min_d = (double)mxGetInf();
    *max_d = -(double)mxGetInf();
    int id_table_size = (int)pow(id_table_info.num_objs, num_id);
    char* id_values;
    //bool* use_id_for_freez_object = NULL; 
    //use_id_for_freez_object = (bool*)emalloc(num_id * sizeof(bool));
    bool dont_ignore = true;

    for (int j = num_id-1; j >= (freez_idx_to_num_param[pred_idx] - fnx->num_unique_vars); j--) {
        use_id_for_freez_object[j] = true;
    }


    if (num_id > fnx->num_unique_vars) {

        for (int j = 0; j < (freez_idx_to_num_param[pred_idx] - fnx->num_unique_vars); j++) {

            char* id_name = find_in_map_reverse(id_rank_to_name_map,
                freez_idx_to_list_param[pred_idx][j], size_id_rank_to_name_map);

            if (id_name != NULL) {

                //mexPrintf("\nid_rank: %d, id_name: %s ***\n", freez_idx_to_list_param[pred_idx][j], id_name);
                int gp = find_in_map(id_to_group_map, id_name, *size_id_to_group_map);
                int id_idx1 = find_in_map(id_to_sub_idx_map, id_name, size_id_to_sub_idx_map);

                if (id_idx1 < 0)
                    mexErrMsgTxt("SignedDistForPredicatesOverVarIds: undefined/unexpected Var_id_?");
                //Note: G_t_subformula[id_idx1]->sym != NULL means that the subformula is a time-freeze subformula
                if (gp != group || (gp == group && G_t_subformula[id_idx1]->sym != NULL)) {
                    if (size_ff < size_xx || size_ff < id_table_info.num_objs)
                        use_id_for_freez_object[j] = false;
                    else
                        use_id_for_freez_object[j] = true;
                }
                else
                    use_id_for_freez_object[j] = true;
            }
        }
    }

    for (int idx_cnt = 0; idx_cnt < id_table_size; idx_cnt++) {
        //we can make this more efficient by only computing the rob values 
        //related to the current id variables not all of them
        id_values = from_IdTable_index_to_Var_id_values(idx_cnt,
            num_id, id_table_info.num_objs);

        /*if (DEBUG_MODE)
        {
            mexPrintf("ID = [");
            for (int i = 0; i < num_id; i++)
                mexPrintf("%d ", id_values[num_id - i - 1]);
            mexPrintf("]\n");
        }*/

        dont_ignore = true;

        for (int j = 0; j < num_id; j++) {
            if (!use_id_for_freez_object[j] && size_ff < id_values[num_id - j -1]) {
                dont_ignore = false;
                break;
            }
        }

        if (dont_ignore)
        {

            left_eval = recurrent_fn_computing(&fnx->left_fn, group, id_values,//<<<<<<<<<<<<<<<<<
                num_id, xx, ff, size_xx, size_ff, pred_idx, et_idx, ft_idx);
            right_eval = recurrent_fn_computing(&fnx->right_fn, group, id_values,//<<<<<<<<<<<<<<<<<
                num_id, xx, ff, size_xx, size_ff, pred_idx, et_idx, ft_idx);

            //*** resolve division by zero ambiguity ***//
            if (strcmp(fnx->left_fn.fn_name, "RATIO") == 0 && division_by_zero)
                right_eval = 0;// fn1/fn2 > a , fn2=0 is equivalent to fn1 > a*fn2 which is equivalent to fn1 > 0

            (*IdTable)[idx_cnt] = compute_rob(fnx->opr, left_eval, right_eval);
        }
        else
            (*IdTable)[idx_cnt] = -mxGetInf();

        //********* IMPORTANT ************/
        //SUBTRACT is a special function for var ids and we want
        //to nullify its robustness value so that the rest of
        //predicates determine the final robustness value
        //note: you can comment this block to reverse its effect 
        //if (strncmp(fnx->left_fn.fn_name, "SUBTRACT", 8) == 0) {//todo: replace with SUB
        if (strncmp(fnx->left_fn.fn_name, "SUB", 3) == 0) {//todo: replace with SUB
            if ((*IdTable)[idx_cnt] < 0)
                (*IdTable)[idx_cnt] = -infval;
            else if ((*IdTable)[idx_cnt] >= 0)
                (*IdTable)[idx_cnt] = infval;
            else if (isnan((*IdTable)[idx_cnt])) {
                //this is important: if there exists an ID that did not exist before
                //because of increased number of objects, then any constraint that
                //involves the missing ID has to be set to -inf
                if(size_ff < size_xx)
                    (*IdTable)[idx_cnt] = -infval;
            }
        }
        //comment this if you want [-max_class_diff +max_class_diff] robustness range
        else if (strcmp(fnx->left_fn.fn_name, "C") == 0 || strcmp(fnx->left_fn.fn_name, "P") == 0) {
            if ((*IdTable)[idx_cnt] < 0)
                (*IdTable)[idx_cnt] = -infval;
            else if ((*IdTable)[idx_cnt] >= 0)
                (*IdTable)[idx_cnt] = infval;
        }
        else if (strcmp(fnx->left_fn.fn_name, "ED") == 0) {
            if ((*IdTable)[idx_cnt] < 0)
                (*IdTable)[idx_cnt] = -infval;
            else if ((*IdTable)[idx_cnt] >= 0)
                (*IdTable)[idx_cnt] = infval;
        }



        /*if (DEBUG_MODE)
        {
            mexPrintf("ROB( ");
            print_fn_def(fnx->left_fn);
            print_opr(fnx->opr);
            print_fn_def(fnx->right_fn);
            mexPrintf(" ) = %f\n", (*IdTable)[idx_cnt]);
        }*/


        if ((*IdTable)[idx_cnt] > * max_d)
            *max_d = (*IdTable)[idx_cnt];
        if ((*IdTable)[idx_cnt] < *min_d)
            *min_d = (*IdTable)[idx_cnt];
        
        //mxFree(id_values);//no need anymore
    }

    //mxFree(use_id_for_freez_object);//no need anymore
    //fill the rest of the idTable with NAN to make them dontcare
    int id_table_size_total = (int)pow(id_table_info.num_objs, id_table_info.num_id_vars);

    if (id_table_size_total > id_table_info.size || id_table_size > id_table_info.size)
        mexErrMsgTxt("ERROR 13");

    for (int i = id_table_size; i < id_table_size_total; i++)
        (*IdTable)[i] = NAN;// -mxGetInf(); //NAN;

    //example:
    //@ (EXISTS, Var_id_1) @(FORALL, Var_id_2) (P(Var_id_1) > P(Var_id_2) /\ Var_id_1 != Var_id_2)
}

//***NOT USED***//
/*void SignedDistForPredicatesOverVarIds_all_combinations(double** xx, struct FnExpr* fnx,
    int dim, int num_id, int pred_idx, int group,
    double** ff, double** IdTable,
    int size_xx, int size_ff, int et_idx, int ft_idx, double* min_d, double* max_d) {*/
#if 0
void SignedDistForPredicatesOverVarIds_all_combinations(double xx[MAX_OBJ_IN_FRAME][STQL_SIGNAL_DIM_SIZE], struct FnExpr* fnx,
    int dim, int num_id, int pred_idx, int group,
    double ff[MAX_OBJ_IN_FRAME][STQL_SIGNAL_DIM_SIZE], double** IdTable,
    int size_xx, int size_ff, int et_idx, int ft_idx, double* min_d, double* max_d) {

    //double* rob = (double*)emalloc(num_id * sizeof(double));
    double left_eval;
    double right_eval;
    double infval = mxGetInf();

    if (num_id == 0)
        mexErrMsgTxt("SignedDistForPredicatesOverVarIds: number of var id must be greater than 0");

    if (DEBUG_MODE)
    {
        mexPrintf("\n");
        print_fn_def(fnx->left_fn);
        print_opr(fnx->opr);
        print_fn_def(fnx->right_fn);
        mexPrintf("\n");
        for (int i = 0; i < fnx->left_fn.num_param; i++)
            if (fnx->left_fn.params[i].fn_type == VAR_ID)
                mexPrintf("%s --> rank = %d group = %d\n", fnx->left_fn.params[i].fn_name,
                    find_in_map(id_to_rank_map, fnx->left_fn.params[i].fn_name, size_id_to_rank_map),
                    find_in_map(id_to_group_map, fnx->left_fn.params[i].fn_name, *size_id_to_group_map));
        for (int i = 0; i < fnx->right_fn.num_param; i++)
            if (fnx->right_fn.params[i].fn_type == VAR_ID)
                mexPrintf("%s -->rank = %d group = %d\n", fnx->right_fn.params[i].fn_name,
                    find_in_map(id_to_rank_map, fnx->right_fn.params[i].fn_name, size_id_to_rank_map),
                    find_in_map(id_to_group_map, fnx->right_fn.params[i].fn_name, *size_id_to_group_map));
    }

    *min_d = (double)mxGetInf();
    *max_d = -(double)mxGetInf();
    int id_table_size = (int)pow(id_table_info.num_objs, num_id);//@todo: id_1 id_2 id_3 when fn has only id_1
    int id_table_size_total = id_table_info.size;// (int)pow(id_table_info.num_objs, id_table_info.num_id_vars);
    double* id_rob = NULL;
    bool* id_rob_not_filled = NULL;
    char* id_values = NULL;
    int var_id_idx = 0;
    min_id_rank = MAX_UNIQUE_VAR_ID_IN_PRED;

    bool repeatitive_ids = false;

    if (num_id < id_table_info.num_id_vars) {
        repeatitive_ids = true;
        id_rob = (double*)emalloc(id_table_size * sizeof(double));
        id_rob_not_filled = (bool*)emalloc(id_table_size * sizeof(bool));
        for (int i = 0; i < id_table_size; i++)
            id_rob_not_filled[i] = true;
        id_values = (char*)emalloc((num_id+1) * sizeof(char));
    }

    /*** MAIN LOOP ***/
    for (int idx_cnt = 0; idx_cnt < id_table_size_total; idx_cnt++) {
        //we can make this more efficient by only computing the rob values 
        //related to the current id variables not all of them
        char* id_values_total = from_IdTable_index_to_Var_id_values(idx_cnt,
            id_table_info.num_id_vars, id_table_info.num_objs);
        //char* id_values = from_IdTable_index_to_Var_id_values(idx_cnt,
        //    num_id, id_table_info.num_objs);

        if (DEBUG_MODE)
        {
            mexPrintf("ID = [");
            for (int i = 0; i < num_id; i++)
                mexPrintf("%d ", id_values_total[num_id - i - 1]);
            mexPrintf("]\n");
        }


        if (min_id_rank < MAX_UNIQUE_VAR_ID_IN_PRED && repeatitive_ids) {
            safe_strncpy(id_values, num_id+1, id_values_total, num_id);
            id_values[num_id] = '\0';
            var_id_idx = from_Var_id_values_to_IdTable_index(id_values, num_id, id_table_info.num_objs);
            //sanity check and can be removed in future
            if (var_id_idx < 0 || var_id_idx >= id_table_size) {
                var_id_idx = idx_cnt;
                mexErrMsgTxt("SignedDistForPredicatesOverVarIds: error 1");
            }
        }
        else {
            var_id_idx = idx_cnt;
        }

        if (!repeatitive_ids || (idx_cnt == 0 || id_rob_not_filled)) {//note: the order of || is important

            left_eval = recurrent_fn_computing(&fnx->left_fn, group, id_values_total,
                id_table_info.num_id_vars, xx, ff, size_xx, size_ff, pred_idx, et_idx, ft_idx);
            right_eval = recurrent_fn_computing(&fnx->right_fn, group, id_values_total,
                id_table_info.num_id_vars, xx, ff, size_xx, size_ff, pred_idx, et_idx, ft_idx);
            /*
            left_eval = recurrent_fn_computing(&fnx->left_fn, group, id_values,
                num_id, xx, ff, size_xx, size_ff, pred_idx, et_idx, ft_idx);
            right_eval = recurrent_fn_computing(&fnx->right_fn, group, id_values,
                num_id, xx, ff, size_xx, size_ff, pred_idx, et_idx, ft_idx);
            */
            //*** resolve division by zero ambiguity ***//
            if (strcmp(fnx->left_fn.fn_name, "RATIO") == 0 && division_by_zero)
                right_eval = 0;// fn1/fn2 > a , fn2=0 is equivalent to fn1 > a*fn2 which is equivalent to fn1 > 0

            (*IdTable)[idx_cnt] = compute_rob(fnx->opr, left_eval, right_eval);

            //********* IMPORTANT ************/
            //SUBTRACT is a special function for var ids and we want
            //to nullify its robustness value so that the rest of
            //predicates determine the final robustness value
            //note: you can comment this block to reverse its effect 
            if (strncmp(fnx->left_fn.fn_name, "SUBTRACT", 8) == 0) {//todo: replace with SUB
                if ((*IdTable)[idx_cnt] < 0)
                    (*IdTable)[idx_cnt] = -infval;
                else if ((*IdTable)[idx_cnt] > 0)
                    (*IdTable)[idx_cnt] = infval;
            }
            if (repeatitive_ids) {
                id_rob[var_id_idx] = (*IdTable)[idx_cnt];
                id_rob_not_filled[var_id_idx] = false;
            }
        }
        else {
            //fill the IdTable with the current returned value for 
            (*IdTable)[idx_cnt] = id_rob[var_id_idx];
        }


        if (DEBUG_MODE)
        {
            mexPrintf("ROB( ");
            print_fn_def(fnx->left_fn);
            print_opr(fnx->opr);
            print_fn_def(fnx->right_fn);
            mexPrintf(" ) = %f\n", (*IdTable)[idx_cnt]);
        }


        if ((*IdTable)[idx_cnt] > * max_d)
            *max_d = (*IdTable)[idx_cnt];
        if ((*IdTable)[idx_cnt] < *min_d)
            *min_d = (*IdTable)[idx_cnt];
        
        //mxFree(id_values_total);//no need anymore
    }

    if (repeatitive_ids) {
        efree(id_rob);
        efree(id_rob_not_filled);
        efree(id_values);
    }

    /*if (id_rob != NULL)
        mexErrMsgTxt("SignedDistForPredicatesOverVarIds: error 2");
    if (id_rob_not_filled != NULL)
        mexErrMsgTxt("SignedDistForPredicatesOverVarIds: error 2");
    if (id_values != NULL)
        mexErrMsgTxt("SignedDistForPredicatesOverVarIds: error 2");*/
    //example:
    //@ (EXISTS, Var_id_1) @(FORALL, Var_id_2) (P(Var_id_1) > P(Var_id_2) /\ Var_id_1 != Var_id_2)
}
#endif
int isPointInConvSet(double *xx, ConvSet *SS, int dim)
{
    int i;
    for (i=0; i<SS->ncon; i++)
        if (inner_prod(SS->A[i],xx,dim)>SS->b[i])
            return(0);
    return(1);
}

/* Inner product of vectors vec1 and vec2 */
double inner_prod(double *vec1, double *vec2, int dim)
{
    int i;
    double sum=0.0;
    for (i=0; i<dim; i++)
        sum += vec1[i]*vec2[i];
    return(sum);
}

/* Computation of the Euclidean norm of a vector */
double norm(double *vec, int dim)
{
    int i;
    double nr=0.0;
    for (i=0; i<dim; i++)
        nr += vec[i]*vec[i];
    return(sqrt(nr));
}

/* Addition of vectors vec1 and vec2
   The result is returned at vec0 */
void vec_add(double* vec0, double *vec1, double *vec2, int dim)
{
    int i;
    for (i=0; i<dim; i++)
        vec0[i] = vec1[i]+vec2[i];
}

/* Multiplication of vector (vec1) with a scalar (scl)
   The result is returned at vec0 */  
void vec_scl(double *vec0, double scl, double *vec1, int dim)
{
    int i;
    for (i=0; i<dim; i++)
        vec0[i] = scl*vec1[i];
}
