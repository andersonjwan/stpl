/***** mx_stpl_taliro : DP.c *****/
/* Version 0.1                     */

/* Written by Mohammad Hekmatnejad, ASU, U.S.A. for stpl_taliro           */
/* Copyright (c) 2020  Mohammad Hekmatnejad                                  */
/* Written by Adel Dokhanchi, ASU, U.S.A. for tp_taliro                   */
/* Copyright (c) 2017  Adel Dokhanchi                                      */

/* Some of the code in this file was taken from fw_taliro software        */
/* Written by Georgios Fainekos, ASU, U.S.A.                              */
/* Copyright (c) 2011  Georgios Fainekos                                  */
/* Send bug-reports and/or questions to: fainekos@gmail.com                  */

/* This program is free software; you can redistribute it and/or modify   */
/* it under the terms of the GNU General Public License as published by   */
/* the Free Software Foundation; either version 2 of the License, or      */
/* (at your option) any later version.                                    */
/*                                                                        */
/* This program is distributed in the hope that it will be useful,        */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of         */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          */
/* GNU General Public License for more details.                           */
/*                                                                        */
/* You should have received a copy of the GNU General Public License      */
/* along with this program; if not, write to the Free Software            */
/* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA*/

/* Some of the code in this file was taken from LTL2BA software           */
/* Written by Denis Oddoux, LIAFA, France                                  */
/* Some of the code in this file was taken from the Spin software         */
/* Written by Gerard J. Holzmann, Bell Laboratories, U.S.A.               */





#include <time.h>
#include "undomex.h"
#include "matrix.h"
#include "distances.h"
#include "ltl2tree.h"
#include "param.h"
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <math.h>

#define intMax32bit 2147483647

#define MAX_OBJECT_IDS 100
struct IdTableInfo id_table_info;
int freez_index_map[MAX_NUM_FREEZ_VAR];
int freez_index_map_time_var[MAX_NUM_FREEZ_VAR];
int numTimeVariable = 0;
struct Map id_to_group_map[MAX_VAR_NUM];
int* size_id_to_group_map;
//keeps the number of id vars that exist in the scope of each freeze operator
int freez_idx_to_num_param[MAX_NUM_FREEZ_VAR + MAX_NUM_PRED];
//keeps the list of id vars indexes that exist in the scope of each freeze operator
int* freez_idx_to_list_param[MAX_NUM_FREEZ_VAR + MAX_NUM_PRED];
bool exists_freez_idx_to_list_param[MAX_NUM_FREEZ_VAR + MAX_NUM_PRED];
Node*** G_s_subformula;//spatial subformulas
SpatialRegion***** G_s_MonitorTable;/* monitoring table for all spatial subformulas*/
int* s_sub_size;//stores size of each G_s_subformula[]
Node** G_t_subformula;//temporal subformulas
double* G_XTrace;//global XTrace input signal
double* G_time_stamps;//global time stamps
FWTaliroParam* G_p_par;//global predicate parameters
int* G_start_idx_frames;//global index of start of each frame
int CUR_IDX = 0;//used to toggle between 0 and 1 to access the current MonitorTable item
int NXT_IDX = 1;//used to toggle between 1 and 0 to access the next MonitorTable item
int s_CUR_IDX = 0;//used to toggle between 0 and 1 to access the current spatial MonitorTable item
int s_NXT_IDX = 1;//used to toggle between 1 and 0 to access the next spatial MonitorTable item
bool T_formula_has_next_temporal_operator = false;//true if there is X or W operator in the temporal formula
bool T_formula_has_evolving_temporal_operator = false;//true if there is [], <>, U, R, V operators in the temporal formula 
bool T_formula_has_previous_temporal_operator = false;//true if there is P or Z operator in the temporal formula
//Note: TBD when othe rpast time operators were defined
bool T_formula_has_evolving_past_temporal_operator = false;//true if there is past [], <> and S operators in the temporal formula 
bool* S_formula_has_next_temporal_operator;//true if there is X operator in the spatial formula
bool* S_formula_has_evolving_temporal_operator;//true if there is [], <>, U, R, V operators in the spatial formula 
int* S_formula_next_temporal_count;//size of nesting for X operator in each spatial formula
bool* S_formula_has_previous_temporal_operator;//true if there is P or Z operator in the spatial formula
//Note: TBD when othe rpast time operators were defined
bool* S_formula_has_evolving_past_temporal_operator;//true if there is [], <>, U, R, V operators in the temporal formula 
int* S_formula_previous_temporal_count;//size of nesting for P and Z operator in each spatial formula
int* nFrame_for_spatio_formulas;//maximum number of frames for each spatial formula
int G_org_nFrame = 0;////number of frames in the input signal
struct Map id_to_sub_idx_map[MAX_VAR_NUM];//maps each var_id to its @(..) subformula index 
int size_id_to_sub_idx_map;//size of the id_to_sub_idx_map
int* G_num_obj_in_frame;//keeps number of object for each frame index
bool* S_formula_has_freeze_id;//true if there is ID which is frozen in the temporal formula 
Garbage_collector garbage_collector;//this is a garbage collector used to release all the memories in the end
bool should_release_memory = true;
bool run_from_matlab = true;
//----------------------------


//TEST TEST TEST
static unsigned int sr_mem_cnt = 0;
static unsigned int sr_mem_new = 0;
static unsigned int sn_mem_cnt = 0;
static unsigned int sn_mem_new = 0;
static unsigned int sr_mem_rel_cnt = 0;
static unsigned int sr_mem_rel_new = 0;
static unsigned int sn_mem_rel_cnt = 0;
static unsigned int sn_mem_rel_new = 0;



void setupIndeces(Node** subformula, Node* phi, int sum_var_id, 
            int last_frz_idx, int last_freeze_time_var_idx, int last_time_evolving_idx);

void release_garbage(bool print_messages);

/* 
compute predicate is used when during the dynamic programming process the type of subformula is 'Predicate'
it calls different functions in distances accroding to number of inputs    

*/

//swaps the value of CUR_IDX and NXT_IDX
//for access to the current and next table items
void toggle_table_index() {
    int tmp = CUR_IDX;
    CUR_IDX = NXT_IDX;
    NXT_IDX = tmp;
}

void reset_table_index() {
    CUR_IDX = 0;
    NXT_IDX = 1;
}

void s_toggle_table_index() {
    int tmp = s_CUR_IDX;
    s_CUR_IDX = s_NXT_IDX;
    s_NXT_IDX = tmp;
}

void s_reset_table_index() {
    s_CUR_IDX = 0;
    s_NXT_IDX = 1;
}

HyDis SetToInf(int sign, int iter, int frz_iter) {
    HyDis   temp;
    double infval = mxGetInf();
    if (sign == -1) {
        temp.dl = -infval;
        temp.ds = -infval;
        temp.min_dis = -infval;
        temp.max_dis = -infval;
    }
    else {
        temp.dl = infval;
        temp.ds = infval;
        temp.min_dis = infval;
        temp.max_dis = infval;
    }
    temp.iteration = iter;
    temp.min_iteration = iter;
    temp.max_iteration = iter;
    ////temp.preindex = -1;
    temp.predicate[0] = '\0';
    temp.freeze_iteration = frz_iter;
    ////temp.freeze_formula_index = -1;
    temp.freeze_min_preindex = frz_iter;
    temp.freeze_max_preindex = frz_iter;
    return  temp;
}

void SetToInf_new(HyDis* temp, int sign, int iter, int frz_iter) {

    double infval = mxGetInf();

    if (sign == -1) {
        temp->dl = -infval;
        temp->ds = -infval;
        temp->min_dis = -infval;
        temp->max_dis = -infval;
        for (int j = 0; j < id_table_info.size; j++)
            temp->IdTable[j] = -mxGetInf();
    }
    else {
        temp->dl = infval;
        temp->ds = infval;
        temp->min_dis = infval;
        temp->max_dis = infval;
        for (int j = 0; j < id_table_info.size; j++)
            temp->IdTable[j] = mxGetInf();
    }
    temp->iteration = iter;
    temp->min_iteration = iter;
    temp->max_iteration = iter;
    ////temp->preindex = -1;
    temp->predicate[0] = '\0';
    temp->freeze_iteration = frz_iter;
    ////temp->freeze_formula_index = -1;
    temp->freeze_min_preindex = frz_iter;
    temp->freeze_max_preindex = frz_iter;
}

int imax(int a, int b)
{
    return(((a) > (b)) ? (a) : (b));
}
int imin(int a, int b)
{
    return(((a) < (b)) ? (a) : (b));
}

/* Saturate and normalize a scalar to the interval [-1,1] */
/* x : scalar value to be scaled */
/* bd : positive bound for scaling and normalizing */
/* I.e., saturate values to [-bd,bd] and map the interval [-bd,bd] to [-1,1] */
double Normalize(double x, double bd)
{
    if (x > bd)
    {
        return(1.0);
    }
    else if (x < -bd)
    {
        return(-1.0);
    }
    else
    {
        return(x/bd);
    }
}

/* Saturate and normalize a hybrid value (l,s) to the interval [-1,1] */
/* x : hybrid value to be scaled */
/* bd_s : positive bound for scaling and normalizing the euclidean component */
/* bd_l : positive bound for scaling and normalizing the discrete component */
/* I.e., saturate values and map [-bd_l,bd_l]x[-bd,bd] to [-1,1] */
HyDis NormalizeHybrid(HyDis x, double bd_l, double bd_s)
{
    x.ds = Normalize(x.ds, bd_s);
    x.ds = Normalize(x.dl + x.ds, bd_l+1);
    x.dl = 0;
    return(x);
}

bool is_time_evolving_operator(int opr) {
    bool res = false;
    if (
        opr == ALWAYS ||
        opr == EVENTUALLY ||
        opr == NEXT ||
        opr == WEAKNEXT ||
        opr == PREVIOUS ||
        opr == WEAKPREVIOUS ||
        opr == U_OPER ||
        opr == V_OPER
        )
        res = true;
    return res;
}

/* cluster of functions for BFS */
int enqueue(struct queue *q, Node *phi, bool ignore_visited)
{

    if (phi == NULL) {
        errno = ENOMEM;
        return 1;
    }
    if(phi->visited == 0 || ignore_visited){
        phi->visited = 0;
            if (q->first == NULL){
                q->first = q->last = dupnode(phi);
                q->first = q->last = phi;                            /* point first and last in the queue to the phi passed if the queue is empty*/
            }
            else {
                q->last = dupnode(phi);                                /* stuff the phi passed in the last of the queue if the queue is not empty*/
                q->last = phi;
            }
        phi->visited = 1;
    return 0;
    }
    return -1;
}

int dequeue(struct queue *q)
{
    if (!q->first) {
        return 1;
    }
    if (q->first == q->last)                            /* if the queue has only one element*/
        q->first = q->last = NULL;
    else
        q->first = dupnode(q->last);                    /*  pop the first element out of the queue*/
        q->first = q->last;
    return 0;
}

void init_queue(struct queue *q)
{
    q->first = q->last = NULL;
}

int queue_empty_p(const struct queue *q)
{
    return q->first == NULL;
}

void print_LTL_formula(Node* node) {
    if (node == NULL)
        mexErrMsgTxt("print_LTL_formula: null/empty formula!");
    if (is_time_evolving_operator(node->ntyp)) {
        if (node->time_ub > 0)
            mexPrintf("t:[%5.3f,%5.3f]_", node->time_lb, node->time_ub);
        if (node->frame_ub > 0)
            mexPrintf("f:[%d,%d]_", node->frame_lb, node->frame_ub);
    }
    switch (node->ntyp) {
    case PREDICATE:
        if(node->fndef != NULL)
            mexPrintf("%s(%d,%d) ", node->fndef->fn_name, node->group, node->index);
        else
            mexPrintf("%s(%d,%d) ", node->sym->name, node->group, node->index);
        break;
    case NOT:
        mexPrintf("NOT(%d,%d) ", node->group, node->index);
        break;
    case AND:
        mexPrintf("AND(%d,%d) ", node->group, node->index);
        break;
    case OR:
        mexPrintf("OR(%d,%d) ", node->group, node->index);
        break;
    case ALWAYS:
        mexPrintf("[](%d,%d) ", node->group, node->index);
        break;
    case EVENTUALLY:
        mexPrintf("<>(%d,%d) ", node->group, node->index);
        break;
    case U_OPER:
        mexPrintf("U(%d,%d) ", node->group, node->index);
        break;
    case V_OPER:
        mexPrintf("R(%d,%d) ", node->group, node->index);
        break;
    case NEXT:
        mexPrintf("X(%d,%d) ", node->group, node->index);
        break;
    case WEAKNEXT:
        mexPrintf("W(%d,%d) ", node->group, node->index);
        break;
    case PREVIOUS:
        mexPrintf("P(%d,%d) ", node->group, node->index);
        break;
    case WEAKPREVIOUS:
        mexPrintf("Z(%d,%d) ", node->group, node->index);
        break;
    case FREEZE_AT:
        mexPrintf("@(%d,%d) ", node->group, node->index);
        break;
    case FALSE:
        mexPrintf("FALSE(%d,%d) ", node->group, node->index);
        break;
    case TRUE:
        mexPrintf("TRUE(%d,%d) ", node->group, node->index);
        break;
    case CONSTRAINT:
        mexPrintf("CONSTRAINT(%d,%d) ", node->group, node->index);
        break;
    case CONSTR_LE:
        mexPrintf("<=(%d,%d) ", node->group, node->index);
        break;
    case CONSTR_GE:
        mexPrintf(">=(%d,%d) ", node->group, node->index);
        break;
    case CONSTR_LS:
        mexPrintf("<(%d,%d) ", node->group, node->index);
        break;
    case CONSTR_GR:
        mexPrintf(">(%d,%d) ", node->group, node->index);
        break;
    case CONSTR_EQ:
        mexPrintf("=(%d,%d) ", node->group, node->index);
        break;
    case CONSTR_NE:
        mexPrintf("!=(%d,%d) ", node->group, node->index);
        break;
    default:
        mexPrintf("?[%d]=(%d,%d) ",node->ntyp, node->group, node->index);
        break;
    }
}

void print_formula_BFS(struct queue* q, Node* root)
{
    Node* p = NULL;
    if (root == NULL)
        return;

    enqueue(q, root, true);

    while (!queue_empty_p(q)) {
        if (!q->first) {
            p = NULL;
        }
        else {
                p = q->first;
                print_LTL_formula(p);
        }
        dequeue(q);
        if (p->lft != NULL)
            print_formula_BFS(q, p->lft);
        if (p->rgt != NULL)
            print_formula_BFS(q, p->rgt);
    }
}

void print_complete_formula(Node* root) {
    queue q;
    queue* Q = &q;
    int temp = 1;
    int* qi;
    int phi_size = 0;
    qi = &temp;
    init_queue(Q);
    print_formula_BFS(Q, root);
}

void print2file2(Node* n, FILE* f) {
    if (!n) return;
    switch (n->ntyp)
    {
    case TRUE:
        fprintf(f, "  \"TRUE(%d)\"\n", n->index);
        break;
    case FALSE:
        fprintf(f, "  \"FALSE(%d)\"\n", n->index);
        break;
    case PREDICATE:
        fprintf(f, "  \"%s(%d)\"\n", n->sym->name, n->index);
        break;
    case NOT:
        fprintf(f, "  \"NOT(%d)\"\n", n->index);
        fprintf(f, "  \"NOT(%d)\" ->", n->index);
        print2file2(n->lft, f);
        break;
    case AND:
        fprintf(f, "  \"AND(%d)\"\n", n->index);
        fprintf(f, "  \"AND(%d)\" ->", n->index);
        print2file2(n->lft, f);
        fprintf(f, "  \"AND(%d)\" ->", n->index);
        print2file2(n->rgt, f);
        break;
    case OR:
        fprintf(f, "  \"OR(%d)\"\n", n->index);
        fprintf(f, "  \"OR(%d)\" ->", n->index);
        print2file2(n->lft, f);
        fprintf(f, "  \"OR(%d)\" ->", n->index);
        print2file2(n->rgt, f);
        break;
    case IMPLIES:
        fprintf(f, "  \"IMPLIES(%d)\"\n", n->index);
        fprintf(f, "  \"IMPLIES(%d)\" ->", n->index);
        print2file2(n->lft, f);
        fprintf(f, "  \"IMPLIES(%d)\" ->", n->index);
        print2file2(n->rgt, f);
        break;
    case NEXT:
        fprintf(f, "  \"NEXT(%d)\"\n", n->index);
        fprintf(f, "  \"NEXT(%d)\" ->", n->index);
        print2file2(n->lft, f);
        break;
    case WEAKNEXT:
        fprintf(f, "  \"WEAKNEXT(%d)\"\n", n->index);
        fprintf(f, "  \"WEAKNEXT(%d)\" ->", n->index);
        print2file2(n->lft, f);
        break;
    case PREVIOUS:
        fprintf(f, "  \"PREVIOUS(%d)\"\n", n->index);
        fprintf(f, "  \"PREVIOUS(%d)\" ->", n->index);
        print2file2(n->lft, f);
        break;
    case WEAKPREVIOUS:
        fprintf(f, "  \"WEAKPREVIOUS(%d)\"\n", n->index);
        fprintf(f, "  \"WEAKPREVIOUS(%d)\" ->", n->index);
        print2file2(n->lft, f);
        break;
    case U_OPER:
        fprintf(f, "  \"U(%d)\"\n", n->index);
        fprintf(f, "  \"U(%d)\" ->", n->index);
        print2file2(n->lft, f);
        fprintf(f, "  \"U(%d)\" ->", n->index);
        print2file2(n->rgt, f);
        break;
    case V_OPER:
        fprintf(f, "  \"R(%d)\"\n", n->index);
        fprintf(f, "  \"R(%d)\" ->", n->index);
        print2file2(n->lft, f);
        fprintf(f, "  \"R(%d)\" ->", n->index);
        print2file2(n->rgt, f);
        break;
    case EVENTUALLY:
        fprintf(f, "  \"<>(%d)\"\n", n->index);
        fprintf(f, "  \"<>(%d)\" ->", n->index);
        print2file2(n->rgt, f);
        break;
    case ALWAYS:
        fprintf(f, "  \"[](%d)\"\n", n->index);
        fprintf(f, "  \"[](%d)\" ->", n->index);
        print2file2(n->rgt, f);
        break;
    case FREEZE_AT:
        fprintf(f, "  \"@ %s(%d)\"\n", n->sym->name, n->index);
        fprintf(f, "  \"@ %s(%d)\" ->", n->sym->name, n->index);
        print2file2(n->lft, f);
        break;
    case CONSTR_LE:
        fprintf(f, "  \"%s <= %f (index=%d)\"\n", n->sym->name, n->value.numf.f_num, n->index);
        break;
    case CONSTR_LS:
        fprintf(f, "  \"%s < %f (index=%d)\"\n", n->sym->name, n->value.numf.f_num, n->index);
        break;
    case CONSTR_EQ:
        fprintf(f, "  \"%s == %f (index=%d)\"\n", n->sym->name, n->value.numf.f_num, n->index);
        break;
    case CONSTR_NE:
        fprintf(f, "  \"%s != %f (index=%d)\"\n", n->sym->name, n->value.numf.f_num, n->index);
        break;
    case CONSTR_GE:
        fprintf(f, "  \"%s >= %f (index=%d)\"\n", n->sym->name, n->value.numf.f_num, n->index);
        break;
    case CONSTR_GR:
        fprintf(f, "  \"%s > %f (index=%d)\"\n", n->sym->name, n->value.numf.f_num, n->index);
        break;
    default:
        break;
    }
}

//does the same thing for the spatial formula 
//about BFS and setupIndeces
void analyze_spatio_formula(FnDef* fnd, int group, int last_frz_idx, int last_freeze_time_var_idx) {
    static int spatio_cnt = 0;
    if (fnd != NULL) {
        if (fnd->spatio_formula == NULL || fnd->fn_type != VAR_SPATIO) {
            for (int i = 0; i < fnd->num_param; i++)
                analyze_spatio_formula(&fnd->params[i], group, last_frz_idx, last_freeze_time_var_idx);
            return;
        }
    }
    else {
        return;
    }
    if (spatio_cnt > (num_spatio_formulas - 1))
        mexErrMsgTxt("analyze_spatio_formula: unmached spatial formulas!");
    queue q;
    queue* Q = &q;
    int temp = 1;
    int* qi;
    int phi_size = 0;
    qi = &temp;
    init_queue(Q);                            
    Node* phi = fnd->spatio_formula;
    
    if (VERBOSE_MODE)
        mexPrintf("\n%s:={",fnd->fn_name);
    int s_next_count = 1;
    phi_size = count_phi_subformulas(phi, qi, spatio_cnt, &s_next_count);//<<<<<<<<<<<<<
    S_formula_next_temporal_count[spatio_cnt] = s_next_count;
    G_s_subformula[spatio_cnt] = (Node**)emalloc(sizeof(Node*) * (phi_size + 1));
    s_sub_size[spatio_cnt] = phi_size;
    fnd->spatio_idx = spatio_cnt;
    temp = 1;
    BFS(Q, phi, qi, group);//<<<<<<<<<<<<<
    setupIndeces(G_s_subformula[spatio_cnt], phi, -1, last_frz_idx, last_freeze_time_var_idx, 0);//<<<<<<<<<<<<<<
    if (VERBOSE_MODE)
        mexPrintf("} ");
    spatio_cnt++;
}

//Breadth First Search and a lot more
//fills T_formula_.. and S_formula_..
int count_phi_subformulas(Node* root, int* cnt, int s_formula_index, int* next_cnt)
{
    if (root == NULL) 
        return 0;
    int opr = root->ntyp;
    //we detect operators type to avoid unnecessary computation
    if (s_formula_index < 0) {//this is a temporal operator
        if ( ! T_formula_has_evolving_temporal_operator &&
            (opr == ALWAYS ||
                opr == EVENTUALLY ||
                opr == U_OPER ||
                opr == V_OPER))
        {
            T_formula_has_evolving_temporal_operator = true;
        }
        else if (opr == NEXT || opr == WEAKNEXT )
        {
            (*next_cnt) += 1;
            T_formula_has_next_temporal_operator = true;
        }
        else if (opr == PREVIOUS || opr == WEAKPREVIOUS)
        {
            (*next_cnt) += 1;
            T_formula_has_previous_temporal_operator = true;
        }
    }
    else if (s_formula_index >= 0) {//this is a spatial operator
        if ( ! S_formula_has_evolving_temporal_operator[s_formula_index] &&
            (opr == ALWAYS ||
                opr == EVENTUALLY ||
                opr == U_OPER ||
                opr == V_OPER))
        {
            S_formula_has_evolving_temporal_operator[s_formula_index] = true;
        }
        else if (opr == NEXT || opr == WEAKNEXT)
        {
            (*next_cnt) += 1;
            S_formula_has_next_temporal_operator[s_formula_index] = true;
        }
        else if (opr == PREVIOUS ||    opr == WEAKPREVIOUS)
        {
            (*next_cnt) += 1;
            S_formula_has_previous_temporal_operator[s_formula_index] = true;
        }
    }
    if ((*cnt) > MAX_STQL_FORMULA_SIZE)
    {
        mexErrMsgTxt("The formula is too big to be stored in tree sturcture!");
    }
    else
    {
        if (root->lft != NULL) {
            (*cnt)++;
            count_phi_subformulas(root->lft, cnt, s_formula_index, next_cnt);
        }
        if (root->rgt != NULL) {
            (*cnt)++;
            count_phi_subformulas(root->rgt, cnt, s_formula_index, next_cnt);
        }
    }
    
    return (*cnt);
}

//Breadth First Search and a lot more
int BFS_spatial(struct queue* q, Node* root, int* i, int group)
{
    Node* p = NULL;
    if (root == NULL) 
        return 0;


    enqueue(q, root, true);

    while (!queue_empty_p(q)) {
        if (!q->first) {
            p = NULL;
        }
        else {
            if ((*i) > MAX_STQL_FORMULA_SIZE)
            {
                mexErrMsgTxt("The formula is too big to be stored in tree sturcture!");
                /* error message when amount of subformulas exceeds subMax*/
            }
            else
            {
                p = q->first;

                
                //check if the predicate has a spatial formula to be indexed
                if (p->ntyp == PREDICATE && p->sym->fnx != NULL && p->sym->fnx->num_spatio_formula > 0) {
                    if (freez_index_map[p->group] < 1)//freeze group index starts from 1
                        mexErrMsgTxt("There is no quantifier operator before the spatial formula!");
                    analyze_spatio_formula(&p->sym->fnx->left_fn, p->group, freez_index_map[p->group], 
                                                freez_index_map_time_var[p->group]);//<<<<<<<<<<<<<<
                    analyze_spatio_formula(&p->sym->fnx->right_fn, p->group, freez_index_map[p->group], 
                                                freez_index_map_time_var[p->group]);//<<<<<<<<<<<<<<

                }

                (*i)++;
            }
        }

        dequeue(q);
        if (p->lft != NULL)
            BFS_spatial(q, p->lft, i, group);
        if (p->rgt != NULL)
            BFS_spatial(q, p->rgt, i, group);

    }
    return (*i - 1);
}

//Breadth First Search and a lot more
int BFS(struct queue *q, Node *root, int *i, int group)
{
    Node *p = NULL;
    if (root == NULL) return 0;

    
    enqueue(q,root, false);

    while (!queue_empty_p(q)) {
        if(!q->first){
            p = NULL;
        }
        else{            
            if((*i)> MAX_STQL_FORMULA_SIZE)
            {
                mexErrMsgTxt("The formula is too big to be stored in tree sturcture!");
                /* error message when amount of subformulas exceeds subMax*/
            }
            else
            {
                p = q->first;
                p->frz = NULL;//initially no freeze parent
                p->index = *i;

                if (group >= 0)//this is only for spatial formulas to get their parent group index
                    p->group = group;
                if(VERBOSE_MODE)
                    print_LTL_formula(p);

                if (p->ntyp == FREEZE_AT) {

                    if(p->has_freeze_var)
                        freez_index_map_time_var[p->group] = *i;//keep track of the indexes of freez time/frame nodes

                    freez_index_map[p->group] = *i;//keep track of the indexes of freeze/quantifier nodes

                    for (int i = 0; i < p->fndef->num_param; i++) {
                        if (p->fndef->params[i].fn_type == VAR_ID) {
                            add_map(id_to_group_map, p->fndef->params[i].fn_name, p->group, size_id_to_group_map);
                        }
                    }
                }

                (*i)++;
            }
        }

        dequeue(q);
        if (p->lft != NULL)
            BFS( q ,p->lft, i, group);
        if (p->rgt != NULL)
            BFS( q, p->rgt, i, group);

    }
    return (*i-1);
} 

//recursively apply indexes to the right/left nodes

void setupIndeces(Node **subformula, Node *phi, int sum_var_id, 
    int last_frz_idx, int last_freeze_time_var_idx, int last_time_evolving_idx){
    subformula[phi->index] = phi;
    
    //compute the number of id vars that exists in the scope of each freeze operator
    if (phi->ntyp == FREEZE_AT) {

        int old_sum = sum_var_id;
        freez_idx_to_num_param[phi->index] = sum_var_id + phi->num_id_vars;
        sum_var_id = freez_idx_to_num_param[phi->index];

        
        if (last_frz_idx > 0)
            subformula[phi->index]->frz = G_t_subformula[last_frz_idx];//for spatial terms

        if (sum_var_id > 0) {
            freez_idx_to_list_param[phi->index] = (int*)emalloc(sum_var_id * sizeof(int));
            
            exists_freez_idx_to_list_param[phi->index] = true;
        }
        for (int i = 0; i < freez_idx_to_num_param[last_frz_idx]; i++) {
            freez_idx_to_list_param[phi->index][i] = freez_idx_to_list_param[last_frz_idx][i];
        }
        int cnt_id = freez_idx_to_num_param[last_frz_idx];
        for (int i = 0; i < phi->fndef->num_param; i++) {
            if (phi->fndef->params[i].fn_type == VAR_ID) {
                freez_idx_to_list_param[phi->index][cnt_id++] = 
                        find_in_map(id_to_rank_map, phi->fndef->params[i].fn_name, size_id_to_rank_map);
                
                // //commented to support repeatitive id variables for breaking
                // down nested freeze quantifiers into one quantifier followed by another freeze quanifier
                // both sharing the same variable name
                /*
                * EXAMPLE:
                * #Req.10
                #input_signal_file = 'kitti.txt'
                #input_signal_file = 'kitti-req-10-pos.txt'
                #formula = '[] (@(Var_x,Var_f,FORALL,Var_id_1)( W( @(FORALL,Var_id_2)(  ({Var_id_1 - Var_id_2 == 0} /\ moved_right_1_2) -> @(Var_y,_,FORALL,Var_id_2) (W [] @(FORALL,Var_id_3)({Var_id_2 - Var_id_3 == 0} -> not_moved_right_2_3 ))   ) ) )   )'
                #formula.moved_right_1_2 = 'RATIO(LAT(Var_id_1,LM), LAT(Var_id_2,LM)) < 1'
                #formula.not_moved_right_2_3 = 'RATIO(LAT(Var_id_2,LM), LAT(Var_id_3,LM)) >= 1'
                */
                //if (find_in_map(id_to_sub_idx_map, phi->fndef->params[i].fn_name, size_id_to_sub_idx_map) > 0)
                //    mexErrMsgTxt("ID Variables must be defined uniquely!");
                //else
                    add_map(id_to_sub_idx_map, phi->fndef->params[i].fn_name, phi->index, &size_id_to_sub_idx_map);
            }
        }
        if (phi->has_freeze_var)
            last_freeze_time_var_idx = phi->index;
        last_frz_idx = phi->index;
        if (id_table_info.num_id_vars < freez_idx_to_num_param[phi->index])
            id_table_info.num_id_vars = freez_idx_to_num_param[phi->index];
    }
    //takes care of predicates that contain spatial formulas
    //and assign the freeze parent to its childs for predicates 
    //and constraints and LHS of imply operator() if MAGNIFY_ROB_4_NEG_LHS_IMPLY is true 
    else if (phi->ntyp == PREDICATE || 
                phi->second_ntype == CONSTRAINT ||
                phi->second_ntype == IMPLIES){
        
        if(last_frz_idx > 0)// && sum_var_id > 0)
            subformula[phi->index]->frz = G_t_subformula[last_frz_idx];//for spatial terms
        if (sum_var_id >= 0) {
            freez_idx_to_num_param[phi->index] = freez_idx_to_num_param[last_frz_idx];
            freez_idx_to_list_param[phi->index] = freez_idx_to_list_param[last_frz_idx];
            if (id_table_info.num_id_vars < freez_idx_to_num_param[phi->index])
                id_table_info.num_id_vars = freez_idx_to_num_param[phi->index];
        }
    }

    
    if (is_time_evolving_operator(phi->ntyp))
        last_time_evolving_idx = phi->index;

    if (phi->ntyp == PREDICATE || phi->ntyp == TRUE || 
            phi->ntyp == FALSE || phi->ntyp == VALUE) 
    {
        phi->Lindex = 0;
        phi->Rindex = 0;
        phi->Lsecond_ntype = -1;
        phi->Rsecond_ntype = -1;
        return;
    }
    else if ( phi->ntyp == CONSTR_LE || phi->ntyp == CONSTR_LS ||
            phi->ntyp == CONSTR_EQ || phi->ntyp == CONSTR_GE || 
            phi->ntyp == CONSTR_GR || phi->ntyp == CONSTR_NE) 
    {
        phi->Lindex = 0;
        phi->Rindex = 0;
        phi->Lsecond_ntype = -1;
        phi->Rsecond_ntype = -1;
        
        //find lower and upper bound for each time/frame variable
        //using the constriants
        if (phi->has_freeze_var) {
            if (phi->sym->var_type == VAR_TIME && (phi->ntyp == CONSTR_GE || phi->ntyp == CONSTR_GR)) {
                if (G_t_subformula[last_freeze_time_var_idx]->time_lb == 0 || 
                        G_t_subformula[last_freeze_time_var_idx]->time_lb > phi->value.numf.f_num)
                    G_t_subformula[last_freeze_time_var_idx]->time_lb = phi->value.numf.f_num;
            }
            else if (phi->sym->var_type == VAR_TIME && (phi->ntyp == CONSTR_LE || phi->ntyp == CONSTR_LS)) {
                if (G_t_subformula[last_freeze_time_var_idx]->time_ub == 0 || 
                        G_t_subformula[last_freeze_time_var_idx]->time_ub < phi->value.numf.f_num)
                    G_t_subformula[last_freeze_time_var_idx]->time_ub = phi->value.numf.f_num;
            }
            else if (phi->sym->var_type == VAR_FRAME && (phi->ntyp == CONSTR_GE || phi->ntyp == CONSTR_GR)) {
                if (G_t_subformula[last_freeze_time_var_idx]->frame_lb == 0 || 
                        G_t_subformula[last_freeze_time_var_idx]->frame_lb > phi->value.numf.f_num)
                    G_t_subformula[last_freeze_time_var_idx]->frame_lb = (int)phi->value.numf.f_num;
            }
            else if (phi->sym->var_type == VAR_FRAME && (phi->ntyp == CONSTR_LE || phi->ntyp == CONSTR_LS)) {
                if (G_t_subformula[last_freeze_time_var_idx]->frame_ub == 0 || 
                        G_t_subformula[last_freeze_time_var_idx]->frame_ub < phi->value.numf.f_num)
                    G_t_subformula[last_freeze_time_var_idx]->frame_ub = (int)phi->value.numf.f_num;
            }
            //propagate time/frame intervals into time evolving operators
            if (last_time_evolving_idx > 0) {
                if (phi->sym->var_type == VAR_TIME && (phi->ntyp == CONSTR_GE || phi->ntyp == CONSTR_GR)) {
                    G_t_subformula[last_time_evolving_idx]->time_lb = phi->value.numf.f_num;
                }
                else if (phi->sym->var_type == VAR_TIME && (phi->ntyp == CONSTR_LE || phi->ntyp == CONSTR_LS)) {
                    G_t_subformula[last_time_evolving_idx]->time_ub = phi->value.numf.f_num;
                }
                else if (phi->sym->var_type == VAR_FRAME && (phi->ntyp == CONSTR_GE || phi->ntyp == CONSTR_GR)) {
                    G_t_subformula[last_time_evolving_idx]->frame_lb = (int)phi->value.numf.f_num;
                }
                else if (phi->sym->var_type == VAR_FRAME && (phi->ntyp == CONSTR_LE || phi->ntyp == CONSTR_LS)) {
                    G_t_subformula[last_time_evolving_idx]->frame_ub = (int)phi->value.numf.f_num;
                }
            }
        }
        return;
    }
    if (phi->lft != NULL){
        phi->Lindex = phi->lft->index;
        phi->Lsecond_ntype = -1;
        if (phi->lft->ntyp == FREEZE_AT) {
            phi->Lsecond_ntype = phi->lft->second_ntype;
        }
        setupIndeces(subformula, phi->lft, sum_var_id, 
                    last_frz_idx, last_freeze_time_var_idx, last_time_evolving_idx);
    }
    else{
        phi->Lindex = 0;
    }
    if (phi->rgt != NULL){
        phi->Rindex = phi->rgt->index;
        phi->Rsecond_ntype = -1;
        if (phi->rgt->ntyp == FREEZE_AT) {
            phi->Rsecond_ntype = phi->rgt->second_ntype;
        }
        setupIndeces(subformula, phi->rgt, sum_var_id, 
                    last_frz_idx, last_freeze_time_var_idx, last_time_evolving_idx);
    }
    else{
        phi->Rindex = 0;
    }
    return;
}

void printTable(HyDis **MonitorTable, FWTaliroParam *p_par, int phi_size){
     int  j, k;
     double   infval = mxGetInf();
     for (j = 0; j < p_par->nSamp; j++){
         mexPrintf("----------------");
     }
     mexPrintf("\n");
     for (k = 1; k <= phi_size; k++){
         mexPrintf("%d |", k);
         for (j = 0; j < p_par->nSamp; j++){
                 mexPrintf(" %f(%d) |", MonitorTable[k][j].ds,MonitorTable[k][j].iteration);
                
        }
         mexPrintf("\n");
     }
     for (j = 0; j < p_par->nSamp; j++){
         mexPrintf("----------------");
     }
     mexPrintf("\n\n");
 }

void print_spatial_region(SpatialRegion* reg) {
    SpatialNode* node = reg->head;
    if (reg->is_NAN) {
        mexPrintf("Bounding Boxes: { NaN }\n");
    }
    else if (reg == NULL_SR || node == NULL_SN)
        mexPrintf("Bounding Boxes: {}\n");
    else {
        if (!reg->head->has_bbox) {
            mexPrintf("printing HyDis is not supported.\n");
            return;
        }
        mexPrintf("Bounding Boxes: {\n");
        char b[4];
        unsigned long int cnt = 0;
        while (node != NULL_SN && cnt < reg->size) {
            b[0] = node->itv[0] > 0 ? '[' : '(';
            b[1] = node->itv[1] > 0 ? ']' : ')';
            b[2] = node->itv[2] > 0 ? '[' : '(';
            b[3] = node->itv[3] > 0 ? ']' : ')';
            mexPrintf("\tBB={x = %c%.3f\t%.3f%c\t,\ty = %c%.3f\t%.3f%c}\n",
                b[0], node->bbox[0], node->bbox[1], b[1],
                b[2], node->bbox[2], node->bbox[3], b[3]);
            node = node->next;
            cnt++;
        }
        mexPrintf("}\n");
    }
}

bool followed_by_freeze_time_subformula(int opr, int stype) {
    bool res = false;
    if (
        opr == ALWAYS||
        opr == EVENTUALLY||
        opr == NEXT||
        opr == WEAKNEXT||
        opr == U_OPER||
        opr == V_OPER ||
        //@todo: check later to avoid formula restriction: @ pushed as out as possible
        //done
        opr == OR ||
        opr == AND ||
        opr == NOT 
        )
        res = true;
    if (stype == EXISTS || stype == FORALL || stype == FREEZE_AT || stype == -1)
        res = res & true;
    else
        res = false;
    return res;
}

void Evaluate_Predicate_TEMPORAL(Node* subformula, HyDis* MonitorTable, double* XTrace,
    int next_idx_frame, int et_index,// int SysDim, int nSamp,
    int ft_idx, int freez_idx_frame, int phi_idx, int t_access)
{
    int SysDim = G_p_par->SysDim;
    int nSamp = G_p_par->nSamp;
    int ii = t_access;
    double cur_frame = XTrace[next_idx_frame];
    int fidx = next_idx_frame;
    double dist;
    double infval = mxGetInf();
    MonitorTable[ii].min_dis = infval;
    MonitorTable[ii].max_dis = -infval;
    int counter = 0;
    int num_obj = 0;

    static double tmpSysTraj_1d[MAX_STQL_SIGNAL_DIM_SIZE];
    static double tmpSysTraj_2d[MAX_OBJ_IN_FRAME][MAX_STQL_SIGNAL_DIM_SIZE];
    static double ffSysTraj_2d[MAX_OBJ_IN_FRAME][MAX_STQL_SIGNAL_DIM_SIZE];

    if (DEBUG_MODE) {
        mexPrintf("Atomic Symbol: %s\n", subformula->sym->name);
        if (subformula->sym->fnx != NULL) {
            mexPrintf("Predicate expression: ");
            print_fn_def(subformula->sym->fnx->left_fn);
            print_opr(subformula->sym->fnx->opr);
            print_fn_def(subformula->sym->fnx->right_fn);
            mexPrintf("\n");
            mexPrintf("num_vars: left= %d , right= %d, num_unique_var= %d",
                subformula->sym->fnx->left_fn.num_vars,
                subformula->sym->fnx->right_fn.num_vars,
                subformula->sym->fnx->num_unique_vars);
            mexPrintf("\n\n");
        }
    }

    if (subformula->sym->fnx != NULL &&
        subformula->sym->fnx->num_unique_vars > 2) {//ERROR
        mexPrintf("Predicate expression: ");
        print_fn_def(subformula->sym->fnx->left_fn);
        print_opr(subformula->sym->fnx->opr);
        print_fn_def(subformula->sym->fnx->right_fn);
        mexPrintf("\n");
        mexPrintf("num_vars: left= %d , right= %d", subformula->sym->fnx->left_fn.num_vars, subformula->sym->fnx->right_fn.num_vars);
        mexPrintf("\n");
        mexErrMsgTxt("Number of variables in the function parameters cannot be greater than 2.\n");
    }
    else if (false && subformula->sym->fnx != NULL &&//<<<<<<<<<<<<<<<<<<< FALSE to void
        subformula->sym->fnx->num_unique_vars == 0) {//FORALL or EXISTS over one Var_id
        //double* tmpSysTraj = (double*)emalloc((SysDim) * sizeof(double));

        while (cur_frame == XTrace[fidx]) {

            for (int jj = 0; jj < SysDim; jj++)
                tmpSysTraj_1d[jj] = XTrace[fidx + jj * nSamp];

            dist = SignedDistForPredicates(tmpSysTraj_1d, subformula->sym->fnx, ft_idx);

            if (dist < MonitorTable[ii].min_dis) {
                MonitorTable[ii].min_dis = dist;
                MonitorTable[ii].min_id = (int)tmpSysTraj_1d[2];//object id
            }
            if (dist > MonitorTable[ii].max_dis) {
                MonitorTable[ii].max_dis = dist;
                MonitorTable[ii].max_id = (int)tmpSysTraj_1d[2];//object id
            }
            fidx++;
            counter++;
        }
        num_obj = fidx - next_idx_frame;

        //is this EXISTS or FORALL predicate expression
        int q_type = EXISTS;
        if (subformula->sym->fnx->left_fn.num_vars > 0) {//!!!! ONLY VERY SIMPLE CASE
            q_type = find_in_map(var_id_map, subformula->sym->fnx->left_fn.params[0].fn_name, size_var_id_map);
        }

        if (q_type == FORALL)
            MonitorTable[ii].ds = MonitorTable[ii].min_dis;
        else
            MonitorTable[ii].ds = MonitorTable[ii].max_dis;//!!!!!!! for test purposes
        //mxFree(tmpSysTraj_1d);//no need anymore
    }
    else if (subformula->sym->fnx != NULL &&
        subformula->sym->fnx->num_unique_vars >= 1) //FORALL or EXISTS over two Var_ids
    {
        if (!G_p_par->STQL)
            mexErrMsgTxt("The input signal is in a wrong format! Expected a frame based signal.");

        //@todo: later improve for efficiency by call by reference from the original trace
        //while (cur_frame == XTrace[fidx] && fidx < nSamp)
        //    fidx++;
        //num_obj = fidx - next_idx_frame;
        num_obj = G_num_obj_in_frame[et_index];
        //if (num_obj == 0 && fidx == G_p_par->nSamp)
        //    num_obj = 1;
        if (num_obj > id_table_info.num_objs || num_obj == 0)
            mexErrMsgTxt("Evaluate_Predicate_TEMPORAL: error 1 in number of objects.");
        //double** tmpSysTraj = (double**)emalloc(num_obj * sizeof(double*));
        fidx = next_idx_frame;
        for (int jj = 0; jj < num_obj; jj++) {
            //tmpSysTraj[jj] = (double*)emalloc((SysDim) * sizeof(double));
            for (int kk = 0; kk < SysDim; kk++)
                tmpSysTraj_2d[jj][kk] = XTrace[fidx + kk * nSamp];
            fidx++;
        }
        //this is for the freezed frame data
        /*cur_frame = XTrace[freez_idx_frame];
        fidx = freez_idx_frame;
        while (cur_frame == XTrace[fidx] && fidx < nSamp)
            fidx++;
        int num_obj_freez = fidx - freez_idx_frame;*/
        
        int num_obj_freez = G_num_obj_in_frame[ft_idx];//@todo:add this later instead of the above code

        if (num_obj_freez > id_table_info.num_objs)
            mexErrMsgTxt("Evaluate_Predicate_TEMPORAL: error 2 in number of objects.");

        //double** ffSysTraj = (double**)emalloc(num_obj_freez * sizeof(double*));
        fidx = freez_idx_frame;
        for (int jj = 0; jj < num_obj_freez; jj++) {
            //ffSysTraj[jj] = (double*)emalloc((SysDim) * sizeof(double));
            for (int kk = 0; kk < SysDim; kk++)
                ffSysTraj_2d[jj][kk] = XTrace[fidx + kk * nSamp];
            fidx++;
        }
        //used in LFEB and LTEB functions
        G_p_par->cur_frame_index = next_idx_frame;
        G_p_par->frz_frame_index = freez_idx_frame;
        //compute id_map
        //NOTE: this function is more expensive computationally
        //SignedDistForPredicatesOverVarIds_all_combinations(tmpSysTraj, subformula->sym->fnx, SysDim,
        SignedDistForPredicatesOverVarIds(tmpSysTraj_2d, subformula->sym->fnx, SysDim,
                freez_idx_to_num_param[subformula->index],
            phi_idx, subformula->group, ffSysTraj_2d, &(MonitorTable[ii].IdTable),
            num_obj, num_obj_freez, et_index, ft_idx,
            &MonitorTable[ii].min_dis, &MonitorTable[ii].max_dis);//<<<<<<<<

        //for (int jj = 0; jj < num_obj; jj++)//no need anymore
        //    mxFree(tmpSysTraj_2d[jj]);
        //for (int jj = 0; jj < num_obj_freez; jj++)//no need anymore
        //    mxFree(ffSysTraj_2d[jj]);

    }
    else if (subformula->sym->fnx == NULL) {//no quantifier specified

        if (subformula->sym->set == NULL) {
            mexPrintf("Wrong redicate: %s\n", subformula->sym->name);
            mexErrMsgTxt("Evaluate_Predicate_TEMPORAL: predicate is not recognized.");
        }

        //double* tmpSysTraj = (double*)emalloc((SysDim) * sizeof(double));

        while (cur_frame == XTrace[fidx] && fidx < nSamp) {//@todo: change this using G_num_obj_in_frame

            for (int jj = 0; jj < SysDim; jj++)
                tmpSysTraj_1d[jj] = XTrace[fidx + jj * nSamp];

            dist = SignedDist(tmpSysTraj_1d, subformula->sym->set, SysDim);
            MonitorTable[ii].ds = dist;

            if (dist < MonitorTable[ii].min_dis) {
                MonitorTable[ii].min_dis = dist;
                MonitorTable[ii].min_id = (int)tmpSysTraj_1d[2];//object id
            }
            if (dist > MonitorTable[ii].max_dis) {
                MonitorTable[ii].max_dis = dist;
                MonitorTable[ii].max_id = (int)tmpSysTraj_1d[2];//object id
            }
            fidx++;
            counter++;
        }
        num_obj = fidx - next_idx_frame;

        //is this EXISTS or FORALL predicate expression
        int q_type = EXISTS;
        ////int q_type = FORALL;
        if (DEBUG_MODE)
            if (q_type == FORALL)
                mexPrintf("Classic Predicate and Default Quantifier: FORALL\n");
            else
                mexPrintf("Classic Predicate and Default Quantifier: EXISTS\n");

        if (q_type == FORALL)
            MonitorTable[ii].ds = MonitorTable[ii].min_dis;
        else
            MonitorTable[ii].ds = MonitorTable[ii].max_dis;
        //mxFree(tmpSysTraj);//no need anymore
    }

    MonitorTable[ii].dl = 0;
}

//returns true if there is a frozen ID variable 
bool Evaluate_Predicate_SPATIO(Node* subformula, SpatialRegion* s_MonitorTable, char* id,
    int next_idx_frame, int et_index, int ft_idx, int phi_idx, int freez_idx_frame,
    int k, int t_access, int t_cur_idx_index, int cur_t_idx)
{

    double cur_frame = G_XTrace[next_idx_frame];
    int fidx = next_idx_frame;
    int counter = 0;
    int num_obj = 0;
    int num_obj_freez = 0;
    int SysDim = G_p_par->SysDim;
    int nSamp = G_p_par->nSamp;
    bool frozen_id = false;


    if (s_MonitorTable == NULL_SR || s_MonitorTable->size < 0)
        mexErrMsgTxt("Evaluate_Predicate_SPATIO: null pointer!");

    if (DEBUG_MODE) {
        mexPrintf("Atomic Spatial Function Symbol: %s\n", subformula->sym->name);
        if (subformula->sym->fnx != NULL) {
            mexPrintf("Predicate expression: ");
            print_fn_def(subformula->sym->fnx->left_fn);
            print_opr(subformula->sym->fnx->opr);
            print_fn_def(subformula->sym->fnx->right_fn);
            mexPrintf("\n");
            mexPrintf("num_vars: left= %d , right= %d, num_unique_var= %d",
                subformula->sym->fnx->left_fn.num_vars,
                subformula->sym->fnx->right_fn.num_vars,
                subformula->sym->fnx->num_unique_vars);
            mexPrintf("\n\n");
        }
    }

    if (subformula->sym->fnx != NULL &&
        subformula->sym->fnx->num_unique_vars > 2) {//ERROR
        mexPrintf("Predicate expression: ");
        print_fn_def(subformula->sym->fnx->left_fn);
        print_opr(subformula->sym->fnx->opr);
        print_fn_def(subformula->sym->fnx->right_fn);
        mexPrintf("\n");
        mexPrintf("num_vars: left= %d , right= %d", subformula->sym->fnx->left_fn.num_vars, 
            subformula->sym->fnx->right_fn.num_vars);
        mexPrintf("\n");
        mexErrMsgTxt("Number of variables in the function parameters cannot be greater than 2.\n");
    }
    else if (subformula->fndef != NULL &&
         subformula->fndef->num_vars >= 1) //FORALL or EXISTS over one Var_ids in BB()
    {
        num_obj = G_num_obj_in_frame[et_index];
        num_obj_freez = G_num_obj_in_frame[ft_idx];

        //find bounding box

        if (strcmp(subformula->fndef->fn_name, "BB") != 0 || subformula->fndef->num_param != 1)
            mexErrMsgTxt("Evaluate_Predicate_SPATIO: Error in Bounding Box Function name. Must be 'BB(Var_id_?)'");
        
        
        int var_idx_group = find_in_map(id_to_group_map, subformula->fndef->params[0].fn_name, *size_id_to_group_map);
        int var_idx = find_in_map(id_to_rank_map, subformula->fndef->params[0].fn_name, size_id_to_rank_map);
        if(var_idx < 0 || 
            //var_idx > id_table_info.num_id_vars || //@todo: check this for the occlusion requirement example
            var_idx_group < 0)
            mexErrMsgTxt("Evaluate_Predicate_SPATIO: Error in Bounding Box Function name. No valid variable id");
        if (subformula->frz == NULL || subformula->frz->index < 0 || freez_idx_to_num_param[subformula->frz->index] < 1)
            mexErrMsgTxt("Evaluate_Predicate_SPATIO: wrong freezed parent node information.");

        int num_id_var = freez_idx_to_num_param[subformula->frz->index];
        int obj_id = -1;
        for (int j = 0; j < num_id_var; j++) {
            if (freez_idx_to_list_param[subformula->frz->index][j] == var_idx) {
                obj_id = id[num_id_var-j-1];
                break;
            }
        }
        if (DEBUG_MODE) 
        {
            mexPrintf("ID = [");
            for (int i = 0; i < num_id_var; i++)
                mexPrintf("%d ", id[num_id_var - i - 1]);
            mexPrintf("], ");
            mexPrintf("%s(%s=%d)\n", subformula->fndef->fn_name, subformula->fndef->params[0].fn_name, obj_id);
        }
        

        int id_idx_var_id = find_in_map(id_to_sub_idx_map, subformula->fndef->params[0].fn_name, size_id_to_sub_idx_map);
        if(id_idx_var_id < 0)
            mexErrMsgTxt("Evaluate_Predicate_SPATIO: Error in Bounding Box Function name. No valid variable id");

        int num_objects = num_obj;
        int gp = subformula->frz->group;
        if (gp != subformula->group || (gp == subformula->group 
                && G_t_subformula[id_idx_var_id]->sym != NULL)//there is a time/frame variable
            ) 
        {
            num_objects = num_obj_freez;
            frozen_id = true;
        }


        int idx = -1;
        
        //@todo: this part needs cleanup
        bool retrieve_by_frozen_id_from_frozen_frame = true;

        if (frozen_id && obj_id <= num_objects) {
            //if (num_id_var > 1) 
            {//find the index of the object-id from the frozen frame
                num_objects = num_obj > num_obj_freez ? num_obj : num_obj_freez;
                for (int i = 0; i < num_objects; i++) {
                    if ((freez_idx_frame + obj_id -1) < nSamp && (next_idx_frame + i) < nSamp) {
                        if ((int)G_XTrace[freez_idx_frame + obj_id - 1 + 2 * nSamp] ==
                            (int)G_XTrace[next_idx_frame + i + 2 * nSamp]) {
                            idx = obj_id - 1;
                            break;
                        }
                    }
                }
            }
            /*else {
                num_objects = num_obj> num_obj_freez ? num_obj: num_obj_freez;
                for (int i = 0; i < num_objects; i++) {
                    if ((freez_idx_frame + obj_id - 1) < nSamp && 
                            (freez_idx_frame + i) < nSamp &&
                                (next_idx_frame + i) < nSamp) {
                        if ((int)G_XTrace[freez_idx_frame + obj_id - 1 + 2 * nSamp] ==
                            (int)G_XTrace[next_idx_frame + i + 2 * nSamp]) {
                            idx = i;
                            retrieve_by_frozen_id_from_frozen_frame = false;
                            break;
                        }
                    }
                }
            }*/
            
        }
        else if(obj_id <= G_num_obj_in_frame[cur_t_idx]){
            
            //idx = obj_id - 1;
            //find the index of the object-id from the currnet frame in the temporal parent subformula
            num_objects = num_obj > num_obj_freez ? num_obj : num_obj_freez;

            for (int i = 0; i < num_objects; i++) {
                if ((t_cur_idx_index + obj_id - 1) < nSamp && (next_idx_frame + i) < nSamp) {
                    if ((int)G_XTrace[t_cur_idx_index + obj_id - 1 + 2 * nSamp] ==
                        (int)G_XTrace[next_idx_frame + i + 2 * nSamp]) {
                        idx = i;
                        break;
                    }
                }
            }

        }



        if (idx >= 0) {
            
            if (s_MonitorTable->head == NULL_SN)
                s_MonitorTable->head = createSpatialNode(1);

            SpatialNode* node = s_MonitorTable->head;
            s_MonitorTable->size = 1;
            if (frozen_id && retrieve_by_frozen_id_from_frozen_frame) {
                node->bbox[0] = G_XTrace[freez_idx_frame + idx + 5 * nSamp];
                node->bbox[1] = G_XTrace[freez_idx_frame + idx + 7 * nSamp];
                node->bbox[2] = G_XTrace[freez_idx_frame + idx + 6 * nSamp];
                node->bbox[3] = G_XTrace[freez_idx_frame + idx + 8 * nSamp];
            }
            else {
                node->bbox[0] = G_XTrace[next_idx_frame + idx + 5 * nSamp];
                node->bbox[1] = G_XTrace[next_idx_frame + idx + 7 * nSamp];
                node->bbox[2] = G_XTrace[next_idx_frame + idx + 6 * nSamp];
                node->bbox[3] = G_XTrace[next_idx_frame + idx + 8 * nSamp];
            }

            node->itv[0] = 1;
            node->itv[1] = 1;
            node->itv[2] = 1;
            node->itv[3] = 1;
            node->conv_set = NULL;
            node->next = NULL_SN;
            node->has_bbox = true;
            s_MonitorTable->is_NAN = false;
        }
        else {
            s_MonitorTable->is_NAN = true;
        }

    }
    else {
        mexErrMsgTxt("Error in Evaluate_Predicate_SPATIO");
    }
    return frozen_id;
}

#if 0
bool Evaluate_Predicate_SPATIO(Node* subformula, SpatialRegion* s_MonitorTable, char* id,
    int next_idx_frame, int et_index, int ft_idx, int phi_idx, int freez_idx_frame, int k, int t_access)
{

    double cur_frame = G_XTrace[next_idx_frame];
    int fidx = next_idx_frame;
    int counter = 0;
    int num_obj = 0;
    int SysDim = G_p_par->SysDim;
    int nSamp = G_p_par->nSamp;
    bool frozen_id = false;


    if (s_MonitorTable == NULL_SR || s_MonitorTable->size < 0)
        mexErrMsgTxt("Evaluate_Predicate_SPATIO: null pointer!");

    if (DEBUG_MODE) {
        mexPrintf("Atomic Spatial Function Symbol: %s\n", subformula->sym->name);
        if (subformula->sym->fnx != NULL) {
            mexPrintf("Predicate expression: ");
            print_fn_def(subformula->sym->fnx->left_fn);
            print_opr(subformula->sym->fnx->opr);
            print_fn_def(subformula->sym->fnx->right_fn);
            mexPrintf("\n");
            mexPrintf("num_vars: left= %d , right= %d, num_unique_var= %d",
                subformula->sym->fnx->left_fn.num_vars,
                subformula->sym->fnx->right_fn.num_vars,
                subformula->sym->fnx->num_unique_vars);
            mexPrintf("\n\n");
        }
    }

    if (subformula->sym->fnx != NULL &&
        subformula->sym->fnx->num_unique_vars > 2) {//ERROR
        mexPrintf("Predicate expression: ");
        print_fn_def(subformula->sym->fnx->left_fn);
        print_opr(subformula->sym->fnx->opr);
        print_fn_def(subformula->sym->fnx->right_fn);
        mexPrintf("\n");
        mexPrintf("num_vars: left= %d , right= %d", subformula->sym->fnx->left_fn.num_vars,
            subformula->sym->fnx->right_fn.num_vars);
        mexPrintf("\n");
        mexErrMsgTxt("Number of variables in the function parameters cannot be greater than 2.\n");
    }
    else if (subformula->fndef != NULL &&
        subformula->fndef->num_vars >= 1) //FORALL or EXISTS over one Var_ids in BB()
    {

        num_obj = G_num_obj_in_frame[et_index];
        int num_obj_freez = G_num_obj_in_frame[ft_idx];

        //find bounding box

        if (strcmp(subformula->fndef->fn_name, "BB") != 0 || subformula->fndef->num_param != 1)
            mexErrMsgTxt("Evaluate_Predicate_SPATIO: Error in Bounding Box Function name. Must be 'BB(Var_id_?)'");


        int var_idx_group = find_in_map(id_to_group_map, subformula->fndef->params[0].fn_name, *size_id_to_group_map);
        int var_idx = find_in_map(id_to_rank_map, subformula->fndef->params[0].fn_name, size_id_to_rank_map);
        if (var_idx < 0 ||
            //var_idx > id_table_info.num_id_vars || //@todo: check this for the occlusion requirement example
            var_idx_group < 0)
            mexErrMsgTxt("Evaluate_Predicate_SPATIO: Error in Bounding Box Function name. No valid variable id");
        if (subformula->frz == NULL || subformula->frz->index < 0 || freez_idx_to_num_param[subformula->frz->index] < 1)
            mexErrMsgTxt("Evaluate_Predicate_SPATIO: wrong freezed parent node information.");

        int num_id_var = freez_idx_to_num_param[subformula->frz->index];
        int obj_id = -1;
        for (int j = 0; j < num_id_var; j++) {
            if (freez_idx_to_list_param[subformula->frz->index][j] == var_idx) {
                obj_id = id[num_id_var - j - 1];
                break;
            }
        }
        if (DEBUG_MODE)
        {
            mexPrintf("ID = [");
            for (int i = 0; i < num_id_var; i++)
                mexPrintf("%d ", id[num_id_var - i - 1]);
            mexPrintf("], ");
            mexPrintf("%s(%s=%d)\n", subformula->fndef->fn_name, subformula->fndef->params[0].fn_name, obj_id);
        }


        int id_idx_var_id = find_in_map(id_to_sub_idx_map, subformula->fndef->params[0].fn_name, size_id_to_sub_idx_map);
        if (id_idx_var_id < 0)
            mexErrMsgTxt("Evaluate_Predicate_SPATIO: Error in Bounding Box Function name. No valid variable id");

        //double** frame = tmpSysTraj;//look in the current frame
        int num_objects = num_obj;
        int gp = subformula->frz->group;
        if (gp != subformula->group || (gp == subformula->group
            && G_t_subformula[id_idx_var_id]->sym != NULL)//there is a time/frame variable
            )
        {
            //frame = ffSysTraj;
            num_objects = num_obj_freez;
            frozen_id = true;
        }


        int idx = -1;
        
        //@todo: this part needs cleanup
        bool retrieve_by_frozen_id_from_frozen_frame = true;
        int obj_look_cnt = num_objects;
        if (!frozen_id)
            obj_look_cnt = num_obj_freez;
        if (frozen_id && obj_id <= num_objects) {
            if (num_id_var > 1) {//@todo: needs more investigation
                num_objects = num_obj > num_obj_freez ? num_obj : num_obj_freez;
                for (int i = 0; i < num_objects; i++) {
                    //@todo: check overflow of the matrix indexes: (reez_idx_frame + obj_id - 1) < nSamp
                    if ((freez_idx_frame + obj_id - 1) < nSamp && (next_idx_frame + i) < nSamp) {
                        if ((int)G_XTrace[freez_idx_frame + obj_id - 1 + 2 * nSamp] ==
                            (int)G_XTrace[next_idx_frame + i + 2 * nSamp]) {
                            idx = obj_id - 1;
                            break;
                        }
                    }
                }
            }
            else {
                num_objects = num_obj > num_obj_freez ? num_obj : num_obj_freez;
                for (int i = 0; i < num_objects; i++) {
                    if ((freez_idx_frame + i) < nSamp && (next_idx_frame + i) < nSamp) {
                        if ((int)G_XTrace[freez_idx_frame + obj_id - 1 + 2 * nSamp] ==
                            (int)G_XTrace[next_idx_frame + i + 2 * nSamp]) {
                            idx = i;
                            retrieve_by_frozen_id_from_frozen_frame = false;
                            break;
                        }
                    }
                }
            }

        }
        else if (obj_id <= num_objects) {
            /*if ((int)G_XTrace[next_idx_frame + i + 2 * nSamp] == obj_id) {
                idx = i;
                break;
            }*/
            idx = obj_id - 1;
        }



        if (idx >= 0) {
            
            if (s_MonitorTable->head == NULL_SN)
                s_MonitorTable->head = createSpatialNode(1);

            SpatialNode* node = s_MonitorTable->head;
            //s_MonitorTable->head = node;
            s_MonitorTable->size = 1;
            if (frozen_id && retrieve_by_frozen_id_from_frozen_frame) {
                node->bbox[0] = G_XTrace[freez_idx_frame + idx + 5 * nSamp];
                node->bbox[1] = G_XTrace[freez_idx_frame + idx + 7 * nSamp];
                node->bbox[2] = G_XTrace[freez_idx_frame + idx + 6 * nSamp];
                node->bbox[3] = G_XTrace[freez_idx_frame + idx + 8 * nSamp];
            }
            else {
                node->bbox[0] = G_XTrace[next_idx_frame + idx + 5 * nSamp];
                node->bbox[1] = G_XTrace[next_idx_frame + idx + 7 * nSamp];
                node->bbox[2] = G_XTrace[next_idx_frame + idx + 6 * nSamp];
                node->bbox[3] = G_XTrace[next_idx_frame + idx + 8 * nSamp];
            }

            node->itv[0] = 1;
            node->itv[1] = 1;
            node->itv[2] = 1;
            node->itv[3] = 1;
            node->conv_set = NULL;
            node->next = NULL_SN;
            node->has_bbox = true;
            s_MonitorTable->is_NAN = false;
        }
        else {
            s_MonitorTable->is_NAN = true;
        }

    }
    else {
        mexErrMsgTxt("Error in Evaluate_Predicate_SPATIO");
    }
    return frozen_id;
}
#endif

void DP_STQL_TEMPORAL(Node** all_subformula, HyDis** MonitorTable, HyDis** FreezTable, 
    double* XTrace, double* time_stamp,
    FWTaliroParam* p_par, int phi_size, int et_index, int ft_index, int next_idx_frame, int k, 
    int freez_idx_frame, int t_access, int f_access) {

    int i = t_access;
    int iplus = t_access > 0 ? 0 : 1;
    int fplus = f_access > 0 ? 0 : 1;

    //to gain some performance
    static double tmpDis_sec_IdTable[MAX_ID_TABLE_SIZE];
    static double tmpDis_IdTable[MAX_ID_TABLE_SIZE];

    Node* subformula = all_subformula[k];
    //prepare monitor table's cells for the freez predicates
    //only if this current subformula is followed by a freeze subformula (not only quantifiers)
    if (followed_by_freeze_time_subformula(subformula->ntyp, subformula->Lsecond_ntype) &&
        all_subformula[subformula->Lindex]->group != subformula->group) 
    {
        copyToFrom(&MonitorTable[subformula->Lindex][i], &FreezTable[fplus][et_index]);
        if (et_index < p_par->nFrame - 1)
            copyToFrom(&MonitorTable[subformula->Lindex][iplus], &FreezTable[fplus][et_index + 1]);
        else
            SetToInf_new(&MonitorTable[subformula->Lindex][iplus], -1, et_index, ft_index);
    }
    if (followed_by_freeze_time_subformula(subformula->ntyp, subformula->Rsecond_ntype) &&
        all_subformula[subformula->Rindex]->group != subformula->group) 
    {
        copyToFrom(&MonitorTable[subformula->Rindex][i], &FreezTable[fplus][et_index]);
        if (et_index < p_par->nFrame - 1)
            copyToFrom(&MonitorTable[subformula->Rindex][iplus], &FreezTable[fplus][et_index + 1]);
        else
            SetToInf_new(&MonitorTable[subformula->Rindex][iplus], -1, et_index, ft_index);
    }
    static bool first_time_true = true;
    static bool first_time_false = true;
    switch (subformula->ntyp)
    {
    case TRUE:
        
        if (first_time_true) {//only one evaluation is enough
            SetToInf_new(&MonitorTable[k][i], +1, et_index, ft_index);
            if (i > 0)
                first_time_true = false;
        }
        break;
    case FALSE:
        
        if (first_time_false) {
            SetToInf_new(&MonitorTable[k][i], -1, et_index, ft_index);
            if (i > 0)
                first_time_true = false;
        }
        break;
    case PREDICATE:

        Evaluate_Predicate_TEMPORAL(subformula, MonitorTable[k], XTrace, next_idx_frame, et_index, 
            ft_index, freez_idx_frame, k, t_access);//<<<<<<<<<<<<<<
        MonitorTable[k][i].iteration = et_index;
        MonitorTable[k][i].preindex = k;
        MonitorTable[k][i].freeze_min_preindex = k;
        MonitorTable[k][i].freeze_max_preindex = k;
        MonitorTable[k][i].min_iteration = et_index;
        MonitorTable[k][i].max_iteration = et_index;
        safe_strcpy(MonitorTable[k][i].predicate, MAX_VAR_NAME_LENGTH, subformula->sym->name);

        if (G_p_par->TPTL) {
            MonitorTable[k][i].freeze_iteration = ft_index;
            if (subformula->frz != NULL) 
                MonitorTable[k][i].freeze_formula_index = subformula->frz->index;
            else
                MonitorTable[k][i].freeze_formula_index = -1;
        }

        break;
    case CONSTR_LE:
    case CONSTR_GE:
    case CONSTR_LS:
    case CONSTR_GR:
    case CONSTR_EQ:
    case CONSTR_NE:
        /*if (DEBUG_MODE)
        {
            mexPrintf("Constraint: %s ", subformula->sym->name);
            print_const_opr(subformula->ntyp);
            mexPrintf(" %f\n", subformula->value.numf.f_num);
        }
        //test test
        if (subformula->fndef != NULL) {
            mexPrintf("Division Reminder Function: ");
            print_fn_def(*subformula->fndef);
            mexPrintf("\n");
        }*/
        ;
        double ts = time_stamp[et_index];//time variable by default
        double frz = time_stamp[ft_index];
        if (subformula->sym->var_type == VAR_FRAME) {
            ts = et_index;//frame variable
            frz = ft_index;
        }
        else if (subformula->sym->var_type != VAR_TIME) {
            mexErrMsgTxt("Only FRAME and DELAY variable names are valid.\n");
        }
        double diff_val = ts - frz;
        int dc = 0;
        if (subformula->fndef != NULL && strcmp(subformula->fndef->fn_name, "DREM") == 0) {
            dc = (int)(atof(subformula->fndef->params[1].fn_name));
            diff_val = ((int)diff_val) % dc;
        }

        if (DEBUG_MODE) {
            if (subformula->sym->var_type == VAR_TIME)
                mexPrintf("\nTIME contraint: (ct = %.3f) - (%s = %.3f) ", 
                    ts, subformula->sym->name, frz);
            else
                mexPrintf("\nFRAME contraint: (cf = %.3f) - (%s = %.3f) ", 
                    ts, subformula->sym->name, frz);
            if (subformula->fndef != NULL)
                mexPrintf(" % %d ", dc);
            print_const_opr(subformula->ntyp);
            mexPrintf(" %.3f\n", subformula->value.numf.f_num);
        }

        bool set_to_pos_inf = false;
        switch (subformula->ntyp)
        {
            case CONSTR_LE:
                if (diff_val <= subformula->value.numf.f_num)
                    set_to_pos_inf = true;
                break;
            case CONSTR_GE:
                if (diff_val >= subformula->value.numf.f_num)
                    set_to_pos_inf = true;
                break;
            case CONSTR_LS:
                if (diff_val < subformula->value.numf.f_num)
                    set_to_pos_inf = true;
                break;
            case CONSTR_GR:
                if (diff_val > subformula->value.numf.f_num)
                    set_to_pos_inf = true;
                break;
            case CONSTR_EQ:
                if (diff_val == subformula->value.numf.f_num)
                    set_to_pos_inf = true;
                break;
            case CONSTR_NE:
                if (diff_val != subformula->value.numf.f_num)
                    set_to_pos_inf = true;
                break;
            default:
                break;
        }
        if (set_to_pos_inf)
            SetToInf_new(&MonitorTable[k][i], +1, et_index, ft_index);
        else
            SetToInf_new(&MonitorTable[k][i], -1, et_index, ft_index);

        /*switch (subformula->ntyp)
        {
        case CONSTR_LE:
            if (ts - frz <= subformula->value.numf.f_num)
                SetToInf_new(&MonitorTable[k][i], +1, et_index, ft_index);
            else
                SetToInf_new(&MonitorTable[k][i], -1, et_index, ft_index);
            break;
        case CONSTR_GE:
            if (ts - frz >= subformula->value.numf.f_num)
                SetToInf_new(&MonitorTable[k][i], +1, et_index, ft_index);
            else
                SetToInf_new(&MonitorTable[k][i], -1, et_index, ft_index);
            break;
        case CONSTR_LS:
            if (ts - frz < subformula->value.numf.f_num)
                SetToInf_new(&MonitorTable[k][i], +1, et_index, ft_index);
            else
                SetToInf_new(&MonitorTable[k][i], -1, et_index, ft_index);
            break;
        case CONSTR_GR:
            if (ts - frz > subformula->value.numf.f_num)
                SetToInf_new(&MonitorTable[k][i], +1, et_index, ft_index);
            else
                SetToInf_new(&MonitorTable[k][i], -1, et_index, ft_index);
            break;
        case CONSTR_EQ:
            if ((ts - frz) == subformula->value.numf.f_num)
                SetToInf_new(&MonitorTable[k][i], +1, et_index, ft_index);
            else
                SetToInf_new(&MonitorTable[k][i], -1, et_index, ft_index);
            break;
        case CONSTR_NE:
            if ((ts - frz) == subformula->value.numf.f_num)
                SetToInf_new(&MonitorTable[k][i], -1, et_index, ft_index);
            else
                SetToInf_new(&MonitorTable[k][i], +1, et_index, ft_index);
            break;
        default:
            break;
        }*/
        
        MonitorTable[k][i].min_dis = MonitorTable[k][i].ds;
        MonitorTable[k][i].max_dis = MonitorTable[k][i].ds;
        safe_strcpy(MonitorTable[k][i].predicate, MAX_VAR_NAME_LENGTH, subformula->sym->name);
        MonitorTable[k][i].iteration = et_index;
        MonitorTable[k][i].min_iteration = et_index;
        MonitorTable[k][i].max_iteration = et_index;
        MonitorTable[k][i].preindex = k;
        MonitorTable[k][i].freeze_min_preindex = k;
        MonitorTable[k][i].freeze_max_preindex = k;
        {
            MonitorTable[k][i].freeze_iteration = ft_index;
            if (subformula->frz != NULL)
                MonitorTable[k][i].freeze_formula_index = subformula->frz->index;
            else
                MonitorTable[k][i].freeze_formula_index = -1;
        }
        break;
    case VALUE:
        mexErrMsgTxt("Do not expect VALUE type!");
        break;
    case AND:
        if (MonitorTable[k][i].IdTable == NULL)
            MonitorTable[k][i] = hmin(MonitorTable[subformula->Lindex][i], MonitorTable[subformula->Rindex][i]);
        else
            
            hmin_new(&MonitorTable[k][i], &MonitorTable[subformula->Lindex][i], &MonitorTable[subformula->Rindex][i]);
        break;
    case OR:
        if (MonitorTable[k][i].IdTable == NULL)
            MonitorTable[k][i] = hmax(MonitorTable[subformula->Lindex][i], MonitorTable[subformula->Rindex][i]);
        else
            
            hmax_new(&MonitorTable[k][i], &MonitorTable[subformula->Lindex][i], &MonitorTable[subformula->Rindex][i]);
        break;
    case NOT:
        MonitorTable[k][i].ds = (-1) * (MonitorTable[subformula->Lindex][i].ds);
        MonitorTable[k][i].dl = (-1) * (MonitorTable[subformula->Lindex][i].dl);
        MonitorTable[k][i].iteration = MonitorTable[subformula->Lindex][i].iteration;
        MonitorTable[k][i].preindex = MonitorTable[subformula->Lindex][i].preindex;
        MonitorTable[k][i].freeze_min_preindex = MonitorTable[subformula->Lindex][i].freeze_min_preindex;
        MonitorTable[k][i].freeze_max_preindex = MonitorTable[subformula->Lindex][i].freeze_max_preindex;
        safe_strcpy(MonitorTable[k][i].predicate, MAX_VAR_NAME_LENGTH, MonitorTable[subformula->Lindex][i].predicate);
        MonitorTable[k][i].freeze_iteration = MonitorTable[subformula->Lindex][i].freeze_iteration;
        MonitorTable[k][i].freeze_formula_index = MonitorTable[subformula->Lindex][i].freeze_formula_index;
        MonitorTable[k][i].min_iteration = MonitorTable[subformula->Lindex][i].min_iteration;
        MonitorTable[k][i].max_iteration = MonitorTable[subformula->Lindex][i].max_iteration;
        
        MonitorTable[k][i].min_dis = -MonitorTable[subformula->Lindex][i].max_dis;
        MonitorTable[k][i].max_dis = -MonitorTable[subformula->Lindex][i].min_dis;
        for (int j = 0; j < id_table_info.size; j++)
            MonitorTable[k][i].IdTable[j] = -MonitorTable[subformula->Lindex][i].IdTable[j];
        break;
    case NEXT:
        if (et_index < p_par->nFrame - 1)
            if (MonitorTable[k][i].IdTable == NULL)
                MonitorTable[k][i] = MonitorTable[subformula->Lindex][iplus];
            else
                copyToFrom(&MonitorTable[k][i], &MonitorTable[subformula->Lindex][iplus]);
        else {
            SetToInf_new(&MonitorTable[k][i], -1, et_index, ft_index);
        }
        break;
    case WEAKNEXT:
        if (et_index < p_par->nFrame - 1)
            if (MonitorTable[k][i].IdTable == NULL)
                MonitorTable[k][i] = MonitorTable[subformula->Lindex][iplus];
            else
                copyToFrom(&MonitorTable[k][i], &MonitorTable[subformula->Lindex][iplus]);
        else {
            SetToInf_new(&MonitorTable[k][i], +1, et_index, ft_index);
        }
        break;
    case PREVIOUS:
        if (et_index > 0)
            if (MonitorTable[k][i].IdTable == NULL)
                MonitorTable[k][i] = MonitorTable[subformula->Lindex][iplus];
            else
                copyToFrom(&MonitorTable[k][i], &MonitorTable[subformula->Lindex][iplus]);
        else {
            SetToInf_new(&MonitorTable[k][i], -1, et_index, ft_index);
        }
        break;
    case WEAKPREVIOUS:
        if (et_index > 0)
            if (MonitorTable[k][i].IdTable == NULL)
                MonitorTable[k][i] = MonitorTable[subformula->Lindex][iplus];
            else
                copyToFrom(&MonitorTable[k][i], &MonitorTable[subformula->Lindex][iplus]);
        else {
            SetToInf_new(&MonitorTable[k][i], +1, et_index, ft_index);
        }
        break;
    case ALWAYS:
        if (et_index < p_par->nFrame - 1)
            if(MonitorTable[k][i].IdTable == NULL)
                MonitorTable[k][i] = hmin(MonitorTable[k][iplus], MonitorTable[subformula->Rindex][i]);
            else
                hmin_new(&MonitorTable[k][i], &MonitorTable[k][iplus], &MonitorTable[subformula->Rindex][i]);
        else
            if (MonitorTable[k][i].IdTable == NULL) {
                MonitorTable[k][i] = MonitorTable[subformula->Rindex][i];
                safe_strcpy(MonitorTable[k][i].predicate, MAX_VAR_NAME_LENGTH, MonitorTable[subformula->Rindex][i].predicate);
            }
            else
                copyToFrom(&MonitorTable[k][i], &MonitorTable[subformula->Rindex][i]);
        break;
    case EVENTUALLY:
        if (et_index < p_par->nFrame - 1)
            if (MonitorTable[k][i].IdTable == NULL)
                MonitorTable[k][i] = hmax(MonitorTable[k][iplus], MonitorTable[subformula->Rindex][i]);
            else
                hmax_new(&MonitorTable[k][i], &MonitorTable[k][iplus], &MonitorTable[subformula->Rindex][i]);
        else
            if (MonitorTable[k][i].IdTable == NULL) {
                MonitorTable[k][i] = MonitorTable[subformula->Rindex][i];
                safe_strcpy(MonitorTable[k][i].predicate, MAX_VAR_NAME_LENGTH, MonitorTable[subformula->Rindex][i].predicate);
            }
            else
                copyToFrom(&MonitorTable[k][i], &MonitorTable[subformula->Rindex][i]);
        break;
    case U_OPER:
        if (et_index < p_par->nFrame - 1) {
            if (MonitorTable[k][i].IdTable == NULL)
                if (subformula->otype == O_STRICT)
                    MonitorTable[k][i] = hmax(MonitorTable[subformula->Rindex][i], 
                        hmin(MonitorTable[k][iplus], MonitorTable[subformula->Lindex][i]));
                else
                    MonitorTable[k][i] = hmax(
                        hmin(MonitorTable[subformula->Rindex][i], MonitorTable[subformula->Lindex][i]),
                        hmin(MonitorTable[k][iplus], MonitorTable[subformula->Lindex][i]));
            else {
                HyDis tmpDis;
                bool free_id_table = false;
                if (id_table_info.size > 0 &&
                    MAX_ID_TABLE_SIZE < pow((double)MAX_OBJ_IN_FRAME, (double)MAX_UNIQUE_VAR_ID_IN_PRED)) {
                    tmpDis.IdTable = (double*)emalloc(id_table_info.size * sizeof(double));
                    free_id_table = true;
                }
                else
                    tmpDis.IdTable = tmpDis_IdTable;

                if (subformula->otype == O_STRICT) {
                    hmin_new(&tmpDis, &MonitorTable[k][iplus], &MonitorTable[subformula->Lindex][i]);
                    hmax_new(&MonitorTable[k][i], &MonitorTable[subformula->Rindex][i], &tmpDis);
                }
                else {
                    HyDis tmpDis_sec;
                    bool free_sec_id_table = false;
                    if (id_table_info.size > 0 &&
                        MAX_ID_TABLE_SIZE < pow((double)MAX_OBJ_IN_FRAME, (double)MAX_UNIQUE_VAR_ID_IN_PRED)) {
                        tmpDis_sec.IdTable = (double*)emalloc(id_table_info.size * sizeof(double));
                        free_sec_id_table = true;
                    }
                    else
                        tmpDis_sec.IdTable = tmpDis_sec_IdTable;
                    hmin_new(&tmpDis, &MonitorTable[k][iplus], &MonitorTable[subformula->Lindex][i]);
                    hmin_new(&tmpDis_sec, &MonitorTable[subformula->Rindex][i], &MonitorTable[subformula->Lindex][i]);
                    hmax_new(&MonitorTable[k][i], &tmpDis_sec, &tmpDis);
                    
                    if (id_table_info.size > 0 && free_sec_id_table)
                        efree(tmpDis_sec.IdTable);
                }
                if (id_table_info.size > 0 && free_id_table)
                    efree(tmpDis.IdTable);
            }
        }
        else
            if (MonitorTable[k][i].IdTable == NULL) {
                if (subformula->otype == O_STRICT)
                    MonitorTable[k][i] = MonitorTable[subformula->Rindex][i];
                else
                    MonitorTable[k][i] = hmin(MonitorTable[subformula->Rindex][i],
                        MonitorTable[subformula->Lindex][i]);
                safe_strcpy(MonitorTable[k][i].predicate, MAX_VAR_NAME_LENGTH, MonitorTable[subformula->Rindex][i].predicate);
            }
            else
                if (subformula->otype == O_STRICT)
                    copyToFrom(&MonitorTable[k][i], &MonitorTable[subformula->Rindex][i]);
                else 
                    hmin_new(&MonitorTable[k][i], &MonitorTable[subformula->Rindex][i], &MonitorTable[subformula->Lindex][i]);
                
        break;
    case V_OPER:
        if (et_index < p_par->nFrame - 1) {
            if (MonitorTable[k][i].IdTable == NULL)
                if(subformula->otype == O_STRICT)
                    MonitorTable[k][i] = hmin(MonitorTable[subformula->Rindex][i], 
                        hmax(MonitorTable[k][iplus], MonitorTable[subformula->Lindex][i]));
                else
                    MonitorTable[k][i] = hmin(
                        hmax(MonitorTable[subformula->Rindex][i], MonitorTable[subformula->Lindex][i]),
                        hmax(MonitorTable[k][iplus], MonitorTable[subformula->Lindex][i]));
            else {
                HyDis tmpDis;
                bool free_id_table = false;
                if (id_table_info.size > 0 &&
                    MAX_ID_TABLE_SIZE < pow((double)MAX_OBJ_IN_FRAME, (double)MAX_UNIQUE_VAR_ID_IN_PRED)) {
                    tmpDis.IdTable = (double*)emalloc(id_table_info.size * sizeof(double));
                    free_id_table = true;
                }
                else
                    tmpDis.IdTable = tmpDis_IdTable;

                if (subformula->otype == O_STRICT) {
                    hmax_new(&tmpDis, &MonitorTable[k][iplus], &MonitorTable[subformula->Lindex][i]);
                    hmin_new(&MonitorTable[k][i], &MonitorTable[subformula->Rindex][i], &tmpDis);
                }
                else {
                    HyDis tmpDis_sec;
                    bool free_sec_id_table = false;
                    if (id_table_info.size > 0 &&
                        MAX_ID_TABLE_SIZE < pow((double)MAX_OBJ_IN_FRAME, (double)MAX_UNIQUE_VAR_ID_IN_PRED)) {
                        tmpDis_sec.IdTable = (double*)emalloc(id_table_info.size * sizeof(double));
                        free_sec_id_table = true;
                    }
                    else
                        tmpDis_sec.IdTable = tmpDis_sec_IdTable;

                    hmax_new(&tmpDis, &MonitorTable[k][iplus], &MonitorTable[subformula->Lindex][i]);
                    hmax_new(&tmpDis_sec, &MonitorTable[subformula->Rindex][i], &MonitorTable[subformula->Lindex][i]);
                    hmin_new(&MonitorTable[k][i], &tmpDis_sec, &tmpDis);
                    if (id_table_info.size > 0 && free_sec_id_table)
                        efree(tmpDis_sec.IdTable);
                }
                if (id_table_info.size > 0 && free_id_table)
                    efree(tmpDis.IdTable);
            }
        }
        else
            if (MonitorTable[k][i].IdTable == NULL) {
                if(subformula->otype == O_STRICT)
                    MonitorTable[k][i] = MonitorTable[subformula->Rindex][i];
                else
                    MonitorTable[k][i] = hmax(MonitorTable[subformula->Rindex][i],
                         MonitorTable[subformula->Lindex][i]);
                safe_strcpy(MonitorTable[k][i].predicate, MAX_VAR_NAME_LENGTH, MonitorTable[subformula->Rindex][i].predicate);
            }
            else
                if (subformula->otype == O_STRICT)
                    copyToFrom(&MonitorTable[k][i], &MonitorTable[subformula->Rindex][i]);
                else 
                    hmax_new(&MonitorTable[k][i], &MonitorTable[subformula->Rindex][i], &MonitorTable[subformula->Lindex][i]);
                
        break;
    case FREEZE_AT:
        if (MonitorTable[k][i].IdTable == NULL) {
            MonitorTable[k][i] = MonitorTable[subformula->Lindex][i];
            safe_strcpy(MonitorTable[k][i].predicate, MAX_VAR_NAME_LENGTH, MonitorTable[subformula->Lindex][i].predicate);
        }
        else
            copyToFrom(&MonitorTable[k][i], &MonitorTable[subformula->Lindex][i]);

        
        if (subformula->second_ntype == EXISTS) {
            
            MonitorTable[k][i].ds = MonitorTable[k][i].max_dis;
            MonitorTable[k][i].freeze_min_preindex = MonitorTable[k][i].freeze_max_preindex;
            MonitorTable[k][i].preindex = MonitorTable[k][i].freeze_max_preindex;
            MonitorTable[k][i].iteration = MonitorTable[k][i].max_iteration;
            MonitorTable[k][i].min_iteration = MonitorTable[k][i].max_iteration;
            MonitorTable[k][i].min_dis = MonitorTable[k][i].max_dis;

            if (MonitorTable[k][i].freeze_max_preindex > 0 &&
                MonitorTable[k][i].freeze_max_preindex <= phi_size) {
                if (all_subformula[MonitorTable[k][i].freeze_max_preindex]->sym != NULL)
                    safe_strcpy(MonitorTable[k][i].predicate, MAX_VAR_NAME_LENGTH,
                                all_subformula[MonitorTable[k][i].freeze_max_preindex]->sym->name);
            }
        }
        else if (subformula->second_ntype == FORALL) {

            MonitorTable[k][i].ds = MonitorTable[k][i].min_dis;
            MonitorTable[k][i].freeze_max_preindex = MonitorTable[k][i].freeze_min_preindex;
            MonitorTable[k][i].preindex = MonitorTable[k][i].freeze_min_preindex;
            MonitorTable[k][i].iteration = MonitorTable[k][i].min_iteration;
            MonitorTable[k][i].max_iteration = MonitorTable[k][i].min_iteration;
            MonitorTable[k][i].max_dis = MonitorTable[k][i].min_dis;

            if (MonitorTable[k][i].freeze_min_preindex > 0 &&
                MonitorTable[k][i].freeze_min_preindex <= phi_size) 
            {
                if (all_subformula[MonitorTable[k][i].freeze_min_preindex]->sym != NULL)
                    safe_strcpy(MonitorTable[k][i].predicate, MAX_VAR_NAME_LENGTH,
                            all_subformula[MonitorTable[k][i].freeze_min_preindex]->sym->name);
            }
        }

        //put the min/max values into the upper portion of the IdTable
        if (subformula->num_id_vars < id_table_info.num_id_vars) {
            int id_table_size = (int)pow(id_table_info.num_objs, id_table_info.num_id_vars - subformula->num_id_vars);
            int step_size = (int)pow(id_table_info.num_objs, subformula->num_id_vars);
            double min = +mxGetInf();
            double max = -mxGetInf();
            double g_min = +mxGetInf();
            double g_max = -mxGetInf();
            int num_obj = G_num_obj_in_frame[et_index];
            //this inner step size is for the samaller num of objs in the current frame
            int inner_step_size = (int)pow(num_obj, subformula->num_id_vars);
            //int inner_step_size = (int)pow(num_obj, id_table_info.num_id_vars - subformula->num_id_vars);
            //int inner_step_size = (int)pow(num_obj, freez_idx_to_num_param[subformula->index] - subformula->num_id_vars);
            

            if (id_table_size > id_table_info.size)
                mexErrMsgTxt("ERROR 9");

            for (int j = 0; j < id_table_size; j++)
            {
                int start = j * step_size;
                int end2 = start + step_size;
                //end1 can be smaller than end2 due to less number of objects in the current frame
                int end1 = start + inner_step_size;
                min = +mxGetInf();
                max = -mxGetInf();
                bool set_nan_min = true;
                bool set_nan_max = true;
                if (start >= id_table_info.size || end2 > id_table_info.size)
                    mexErrMsgTxt("ERROR 10");
                for (int jj = start; jj < end1; jj++) {
                    if (min >= MonitorTable[k][i].IdTable[jj] || 
                        (isnan(MonitorTable[k][i].IdTable[jj]) && min == +mxGetInf() && set_nan_min)) {
                        min = MonitorTable[k][i].IdTable[jj];
                        if (min == +mxGetInf())
                            set_nan_min = false;
                    }
                    //if (max < MonitorTable[k][i].IdTable[jj])// || isnan(MonitorTable[k][i].IdTable[jj]))
                    if (max < MonitorTable[k][i].IdTable[jj] ||
                        (isnan(MonitorTable[k][i].IdTable[jj]) && max == -mxGetInf() && set_nan_max)) {
                        max = MonitorTable[k][i].IdTable[jj];
                        if (max == -mxGetInf())
                            set_nan_max = false;
                    }
                }
                for (int jj = end1; jj < end2; jj++) {
                    MonitorTable[k][i].IdTable[jj] = NAN;
                }
                //change the nan min/max to +-inf
                if (isnan(min))
                    min = -mxGetInf();
                if (isnan(max))
                    max = -mxGetInf();

                if (g_min > min)
                    g_min = min;
                if (g_max < max)
                    g_max = max;

                if (subformula->second_ntype == FORALL) {

                    MonitorTable[k][i].IdTable[j] = min;
                    
                }
                else {

                    MonitorTable[k][i].IdTable[j] = max;
                    
                }
            }
            if (subformula->second_ntype == FORALL)
                MonitorTable[k][i].min_dis = g_min;
            else
                MonitorTable[k][i].max_dis = g_max;
        }
        
        
        if (subformula->num_id_vars == id_table_info.num_id_vars && id_table_info.size > 0) {
            int num_obj = G_num_obj_in_frame[et_index];
            int id_table_size = (int)pow(num_obj, subformula->num_id_vars);
            double min = +mxGetInf();
            double max = -mxGetInf();

            if (subformula->second_ntype == FORALL) {
                for (int jj = 0; jj < id_table_size; jj++) {
                    if (MonitorTable[k][i].IdTable[jj] < min || isnan(MonitorTable[k][i].IdTable[jj]))
                        min = MonitorTable[k][i].IdTable[jj];
                }
                MonitorTable[k][i].min_dis = min;
                MonitorTable[k][i].ds = min;
            }
            else {
                for (int jj = 0; jj < id_table_size; jj++) {
                    if (MonitorTable[k][i].IdTable[jj] > max || isnan(MonitorTable[k][i].IdTable[jj]))
                        max = MonitorTable[k][i].IdTable[jj];
                }
                MonitorTable[k][i].min_dis = max;
                MonitorTable[k][i].ds = max;
            }
            //for (int jj = id_table_size; jj < id_table_info.size; jj++)
            //    MonitorTable[k][i].IdTable[jj] = NAN;
        }

        //fill the rest unrelated part of the IdTable with NAN
        //note: this must be commented if SignedDistForPredicatesOverVarIds_all_combinations
        //is used
        if (subformula->num_id_vars <= id_table_info.num_id_vars) {

            //int id_table_size = (int)pow(id_table_info.num_objs, subformula->num_id_vars);
            int id_table_size = (int)pow(id_table_info.num_objs, id_table_info.num_id_vars - subformula->num_id_vars);

            //if (id_table_size >= id_table_info.size )
            //    mexErrMsgTxt("ERROR 11");

            for (int j = id_table_size; j < id_table_info.size; j++)
                MonitorTable[k][i].IdTable[j] = NAN;
        }
        
        //if (subformula->second_ntype != FREEZE_AT &&
        if (subformula->ntyp != FREEZE_AT &&
                MonitorTable[k][i].freeze_formula_index == subformula->index)
        {

            MonitorTable[k][i].freeze_iteration = ft_index;
            if (subformula->frz != NULL) {
                MonitorTable[k][i].freeze_formula_index = subformula->frz->index;// freez_index_map[subformula->group];
            }
            else
                MonitorTable[k][i].freeze_formula_index = k;
        }
        else
            MonitorTable[k][i].freeze_formula_index = k;
        /*
        * This is a critical action for storing the DP monitoring data into the FreezTable for 
        * each frozen time, and update them if necessary. 
        */
        
        if (FreezTable[f_access][ft_index].preindex == 0 //first time
            ||
            (//subformula->sym != NULL &&
                //(subformula->second_ntype == FREEZE_AT
                (subformula->ntyp == FREEZE_AT
                    ||
                    (subformula->second_ntype == EXISTS && FreezTable[f_access][ft_index].ds < MonitorTable[k][i].ds)
                    ||
                    (subformula->second_ntype == FORALL && FreezTable[f_access][ft_index].ds > MonitorTable[k][i].ds)))
            )
            copyToFrom(&FreezTable[f_access][ft_index], &MonitorTable[k][i]);

        break;
    default:
        break;
    }
    //undermine the effect of small negative robustness value for the precedent of imply formula
    if (subformula->second_ntype == IMPLIES && MAGNIFY_ROB_4_NEG_LHS_IMPLY && 
        G_p_par->TPTL && freez_idx_to_num_param[subformula->index]) {//<<<<<<<<<<<<<
        //mexPrintf("\n*PRECEDENT OF IMPLICATION*");
        int id_table_size = (int)pow(id_table_info.num_objs, id_table_info.num_id_vars);

        if (id_table_size >= id_table_info.size)
            mexErrMsgTxt("ERROR 12");

        //note: the assumption is that the formula is in the NNF form
        for (int j = 0; j < id_table_size; j++)
            if (MonitorTable[k][i].IdTable[j] < 0)
                MonitorTable[k][i].IdTable[j] = -mxGetInf();

    }

    
    if (DEBUG_MODE) 
    {

        if (subformula->second_ntype == IMPLIES && MAGNIFY_ROB_4_NEG_LHS_IMPLY)
            mexPrintf("\n*PRECEDENT OF IMPLICATION set to -inf for negative robustness values*");

        mexPrintf("\niteration = \t%d", MonitorTable[k][i].iteration);

        if (subformula->ntyp != TRUE && subformula->ntyp != FALSE) {
            mexPrintf("\nOP: ");
            print_LTL_formula(subformula);
            mexPrintf("\t<min> = %f, <max> = %f\n", MonitorTable[k][i].min_dis, MonitorTable[k][i].max_dis);
            mexPrintf("ft_idx = %d, et_idx = %d ,\t ft = %.3f, et = %.3f\n",
                    ft_index, et_index, time_stamp[ft_index], time_stamp[et_index]);
            if (id_table_info.size > 0 && id_table_info.num_id_vars > 0) {
                mexPrintf("ID variables: {");
                for (int i = 0; i < id_table_info.num_id_vars; i++) {
                    mexPrintf("%s", find_in_map_reverse(id_rank_to_name_map, i, size_id_rank_to_name_map));
                    if (i != (id_table_info.num_id_vars - 1))
                        mexPrintf(", ");
                }
                mexPrintf("}\n");
                char* id_values;
                if(id_table_info.num_id_vars > 0)
                for (int pp = 0; pp < id_table_info.size; pp++) {
                    id_values = from_IdTable_index_to_Var_id_values(pp,
                        id_table_info.num_id_vars, id_table_info.num_objs);
                    mexPrintf("ID(%d)\t\t[",pp);
                    for (int i = 0; i < id_table_info.num_id_vars; i++) {
                        mexPrintf("%d", id_values[id_table_info.num_id_vars - i - 1]);
                        if (i != (id_table_info.num_id_vars - 1))
                            mexPrintf("\t");
                    }
                    mexPrintf("] = \t%f\n", MonitorTable[k][i].IdTable[pp]);
                }
            }
        }
        else {
            if(subformula->ntyp == TRUE)
                mexPrintf("\nOP: TRUE\n");
            else
                mexPrintf("\nOP: FALSE\n");
        }
        mexPrintf("MonitorTable[%d][%d].ds = %f\n", k, et_index, MonitorTable[k][i].ds);
    }

    //test test test
    /*mexPrintf("\nOP: ");
    print_LTL_formula(subformula);
    mexPrintf("\t<min> = %f, <max> = %f\n", MonitorTable[k][i].min_dis, MonitorTable[k][i].max_dis);
    mexPrintf("ft_idx = %d, et_idx = %d ,\t ft = %.3f, et = %.3f\n",
        ft_index, et_index, time_stamp[ft_index], time_stamp[et_index]);
    mexPrintf("MonitorTable[%d][%d].ds = %f\n", k, et_index, MonitorTable[k][i].ds);*/

}

void DP_STQL_SPATIO_TEMPORAL(Node* subformula, SpatialRegion*** MonitorTable, char* id,
    int phi_size, int et_index, int phi_index, int ft, 
    int next_idx_frame, int k, int freez_idx_frame, int access, int s_sub_index,
    int t_cur_idx_index, int cur_t_idx) {


    int i = access;
    int iplus = access > 0 ? 0 : 1;
    int spatial_nFrame = nFrame_for_spatio_formulas[s_sub_index];
    //update the number of frames after optimization
    if (spatial_nFrame < G_org_nFrame) {//optimized for shorter frame length
        spatial_nFrame += cur_t_idx;
        if (spatial_nFrame >= G_org_nFrame)
            spatial_nFrame = G_org_nFrame;
    }
    else if (spatial_nFrame > G_org_nFrame) {
        spatial_nFrame = G_org_nFrame;
        //mexErrMsgTxt("DP_STQL_SPATIO_TEMPORAL: unexpected number of frames from 'nFrame_for_spatio_formulas'");
    }

    bool has_frozen_id = false;

    if (k > phi_size || k < 1)
        mexErrMsgTxt("Error 0");
    if (i > 1 || i < 0)
        mexErrMsgTxt("Error 1");
    if (subformula == NULL)
        mexErrMsgTxt("DP_STPL_SPATIO_TEMPORAL: null spatial formula!");
    switch (subformula->ntyp)
    {

    case PREDICATE:
        
        has_frozen_id = Evaluate_Predicate_SPATIO(subformula, MonitorTable[k][i], id,
                        next_idx_frame, et_index, ft, phi_index, 
                        freez_idx_frame, k, access,
                        t_cur_idx_index, cur_t_idx);//<<<<<<<<<<<<<<
                        
        if (MonitorTable[k][i]->size > 1)
            mexErrMsgTxt("Each BB has to be singleton");
        if (!S_formula_has_freeze_id[s_sub_index] && has_frozen_id)
            S_formula_has_freeze_id[s_sub_index] = true;

        break;
    case INTERIOR://interior
        if (subformula->Lindex > phi_size || subformula->Lindex < 1)
            mexErrMsgTxt("Error 2");
        spatio_interior(MonitorTable[k][i], MonitorTable[subformula->Lindex][i]);
        break;
    case CLOSURE://closure
        if (subformula->Lindex > phi_size || subformula->Lindex < 1)
            mexErrMsgTxt("Error 2");
        spatio_closure(MonitorTable[k][i], MonitorTable[subformula->Lindex][i]);
        break;
    case AND://intersection
        if (subformula->Lindex > phi_size || subformula->Lindex < 1)
            mexErrMsgTxt("Error 2");
        if (subformula->Rindex > phi_size || subformula->Rindex < 1)
            mexErrMsgTxt("Error 3");
        spatio_intersection(MonitorTable[k][i], MonitorTable[subformula->Lindex][i], MonitorTable[subformula->Rindex][i]);
        break;
    case OR://union
        if (subformula->Lindex > phi_size || subformula->Lindex < 1)
            mexErrMsgTxt("Error 2");
        if (subformula->Rindex > phi_size || subformula->Rindex < 1)
            mexErrMsgTxt("Error 3");
        spatio_union(MonitorTable[k][i], MonitorTable[subformula->Lindex][i], MonitorTable[subformula->Rindex][i]);
        break;
    case NOT://complement
        if (subformula->Lindex > phi_size || subformula->Lindex < 1)
            mexErrMsgTxt("Error 2");
        spatio_complement(MonitorTable[k][i], MonitorTable[subformula->Lindex][i]);
        break;
    case NEXT:
        if (subformula->Lindex > phi_size || subformula->Lindex < 1)
            mexErrMsgTxt("Error 2");
        if (et_index < spatial_nFrame - 1)
            s_copyToFrom(MonitorTable[k][i], MonitorTable[subformula->Lindex][iplus]);
        else {
            SetTo_Universe_or_Emptyset(MonitorTable[k][i], -1);
        }
        break;
    case WEAKNEXT:
        if (subformula->Lindex > phi_size || subformula->Lindex < 1)
            mexErrMsgTxt("Error 2");
        if (et_index < spatial_nFrame - 1)
            s_copyToFrom(MonitorTable[k][i], MonitorTable[subformula->Lindex][iplus]);
        else {
            SetTo_Universe_or_Emptyset(MonitorTable[k][i], +1);
        }
        break;
    case PREVIOUS:
        if (subformula->Lindex > phi_size || subformula->Lindex < 1)
            mexErrMsgTxt("Error 2");
        if (et_index >0)
            s_copyToFrom(MonitorTable[k][i], MonitorTable[subformula->Lindex][iplus]);
        else {
            SetTo_Universe_or_Emptyset(MonitorTable[k][i], -1);
        }
        break;
    case WEAKPREVIOUS:
        if (subformula->Lindex > phi_size || subformula->Lindex < 1)
            mexErrMsgTxt("Error 2");
        if (et_index >0)
            s_copyToFrom(MonitorTable[k][i], MonitorTable[subformula->Lindex][iplus]);
        else {
            SetTo_Universe_or_Emptyset(MonitorTable[k][i], +1);
        }
        break;
    case ALWAYS:
        if (subformula->Rindex > phi_size || subformula->Rindex < 1)
            mexErrMsgTxt("Error 3");
        if (et_index < spatial_nFrame - 1)
            spatio_intersection(MonitorTable[k][i], MonitorTable[k][iplus], MonitorTable[subformula->Rindex][i]);
        else
            s_copyToFrom(MonitorTable[k][i], MonitorTable[subformula->Rindex][i]);
        break;
    case EVENTUALLY:
        if (subformula->Rindex > phi_size || subformula->Rindex < 1)
            mexErrMsgTxt("Error 3");
        if (et_index < spatial_nFrame - 1)
            spatio_union(MonitorTable[k][i], MonitorTable[k][iplus], MonitorTable[subformula->Rindex][i]);
        else
            s_copyToFrom(MonitorTable[k][i], MonitorTable[subformula->Rindex][i]);
        break;
    case U_OPER:
        if (subformula->Lindex > phi_size || subformula->Lindex < 1)
            mexErrMsgTxt("Error 2");
        if (subformula->Rindex > phi_size || subformula->Rindex < 1)
            mexErrMsgTxt("Error 3");
        if (et_index < spatial_nFrame - 1) {
            SpatialRegion* tmpDis = createSpatialRegion(1);
            spatio_intersection(tmpDis, MonitorTable[k][iplus], MonitorTable[subformula->Lindex][i]);
            spatio_union(MonitorTable[k][i], MonitorTable[subformula->Rindex][i], tmpDis);
            release_spatial_region(&tmpDis);
        }
        else
            s_copyToFrom(MonitorTable[k][i], MonitorTable[subformula->Rindex][i]);
        break;
    case V_OPER:
        if (subformula->Lindex > phi_size || subformula->Lindex < 1)
            mexErrMsgTxt("Error 2");
        if (subformula->Rindex > phi_size || subformula->Rindex < 1)
            mexErrMsgTxt("Error 3");
        if (et_index < spatial_nFrame - 1) {
            SpatialRegion* tmpDis = createSpatialRegion(1);
            spatio_union(tmpDis, MonitorTable[k][iplus], MonitorTable[subformula->Lindex][i]);
            spatio_intersection(MonitorTable[k][i], MonitorTable[subformula->Rindex][i], tmpDis);
            release_spatial_region(&tmpDis);
        }
        else
            s_copyToFrom(MonitorTable[k][i], MonitorTable[subformula->Rindex][i]);
        break;

    default:
        mexErrMsgTxt("Unsupported Spatio-Temporal operator!");
        break;
    }
    
    if (MonitorTable[k][i]->size > 0 && !MonitorTable[k][i]->is_NAN && 
            (MonitorTable[k][i]->head == NULL_SN || 
                (!MonitorTable[k][i]->is_NAN && !MonitorTable[k][i]->head->has_bbox)))
        mexErrMsgTxt("Error 4");
    
    if (DEBUG_MODE)
    {
        mexPrintf("\nspatio-iteration = \t%d", cur_t_idx);
        mexPrintf("\tft_idx = %d, et_idx = %d \n",ft, et_index);
        if (subformula->ntyp != TRUE && subformula->ntyp != FALSE) {
            mexPrintf("\nSpatial OP: s_");
            print_LTL_formula(subformula);
            mexPrintf("\n");
        }
        print_spatial_region(MonitorTable[k][i]);
    }
}

static long int spatial_memory_counter = 0;
//MAIN LOOP for SPATIO-TEMPORAL FORMULAS
SpatialRegion* DP_SPATIAL(int s_sub_index, int cur_t_idx, int frx_t_idx, char* id, int phi_index) {


    if (s_sub_index < 0 || s_sub_index >num_spatio_formulas)
        mexErrMsgTxt("DP_SPATIAL: Array out of bound access: 'G_s_subformula'");
    int id_index = from_Var_id_values_to_IdTable_index(id, id_table_info.num_id_vars, id_table_info.num_objs);
    if (id_index < 0 || id_index > id_table_info.size)
        mexErrMsgTxt("DP_SPATIAL: unexpected ID index");

    SpatialRegion*** s_MonitorTable = G_s_MonitorTable[s_sub_index][id_index];
    int phi_size = s_sub_size[s_sub_index];
    
    static int prev_frx_t_idx = 0;//to check if the frx_t_idx is new 

    //update: we have to recompute all the table's data for each frozen time
    //because if the Id that is used in BB(Id) was quantified in a freeze quantifier
    //then the bounding boxes are changed
    //        //the below if-statement is inconsistent for some cases like:
    //        forall id1@x. [] forall id2. ratio(area({BB(id1)}),area({BB(id2)})) > 1
    //        therefore commented but should be revised for performance gain
    //if (frx_t_idx == 0 || S_formula_has_freeze_id[s_sub_index] )
    {    //we only need to compute table's data once per spatial subformulas for the whole signal
        //that is because we do not let time constraints (TPTL) in the spatial formulas 
        //@todo: in future we let time intervals as part of spatio-temporal operators
        Node** subformula = G_s_subformula[s_sub_index];
        
        int spatial_nFrame = nFrame_for_spatio_formulas[s_sub_index];
        if (spatial_nFrame < G_org_nFrame) {//optimized for shorter frame length
            spatial_nFrame += cur_t_idx;
            if (spatial_nFrame > G_org_nFrame)
                spatial_nFrame = G_org_nFrame;
        }
        else if (spatial_nFrame > G_org_nFrame) {
            spatial_nFrame = G_org_nFrame;
            //mexErrMsgTxt("DP_SPATIAL: unexpected number of frames from 'nFrame_for_spatio_formulas'");
        }

        int start_loop = cur_t_idx;
        bool loop_activated = false;
        //if temporal formula is optimized but the spatial needs iteration over the whole signal
        if (spatial_nFrame >= G_p_par->nFrame) {
            start_loop = spatial_nFrame - 1;
            loop_activated = true;
        }

        //@todo: optimize this outer loop for non-temporal formulas for which there is no U R G E

        ////for (int et_idx = spatial_nFrame - 1; et_idx >= cur_t_idx; et_idx--)
        if ( (spatial_nFrame - 1) >= cur_t_idx)
        {
            
            /*if (S_formula_has_freeze_id[s_sub_index] && prev_frx_t_idx != frx_t_idx) {
                for (int ii = phi_size; ii >= 1; ii--)
                {
                    bool tmp_s_r_m = should_release_memory;
                    should_release_memory = true;
                    release_spatial_node_from_head(&s_MonitorTable[ii][0]);
                    release_spatial_node_from_head(&s_MonitorTable[ii][1]);
                    should_release_memory = tmp_s_r_m;
                }
                prev_frx_t_idx = frx_t_idx;
            }*/
            
            for (int et_idx = start_loop; et_idx >= cur_t_idx; et_idx--) {
                for (int ii = phi_size; ii >= 1; ii--)//@todo: move this loop in the DP_STQL function for more efficiency
                {
                    DP_STQL_SPATIO_TEMPORAL(subformula[ii], s_MonitorTable, id,
                        phi_size,
                        et_idx,
                        phi_index,
                        frx_t_idx,
                        G_start_idx_frames[et_idx],//the sample index of the current time index
                        ii,//subformula index
                        G_start_idx_frames[frx_t_idx],//the sample index of the freez time index
                        s_CUR_IDX,
                        s_sub_index,
                        G_start_idx_frames[cur_t_idx],
                        cur_t_idx
                    );

                }
                if(loop_activated)
                    s_toggle_table_index();
            }
            if (loop_activated)
                s_toggle_table_index();
        }
        //s_toggle_table_index();
        spatial_memory_counter++;
        //mexPrintf("memory cleanup at iteration: %d Ft= %d Ct= %d\n", spatial_memory_counter,frx_t_idx, cur_t_idx);
    }

    //SpatialRegion* spatio_res = createSpatialRegion(1);

    //s_copyToFrom(spatio_res, &s_MonitorTable[1][s_CUR_IDX]);


    return s_MonitorTable[1][s_CUR_IDX];

    //release memory;
    /*{
        spatial_memory_counter++;
        bool cleanup_memory = (spatial_memory_counter % id_table_info.size) == 0 ? true : false;

        for (int i = 0; i <= phi_size; i++) {
            release_spatial_node_from_head(&s_MonitorTable[i][0]);
            release_spatial_node_from_head(&s_MonitorTable[i][1]);
            //hfree(s_MonitorTable[i]);
            if (cleanup_memory)
                hfree(G_s_MonitorTable[s_sub_index][i]);
        }
        //hfree(s_MonitorTable);
        if (cleanup_memory) {
            //if(DEBUG_MODE)
            mexPrintf("memory cleanup at iteration: %d\n", spatial_memory_counter);
            hfree(G_s_MonitorTable[s_sub_index]);
            G_s_MonitorTable[s_sub_index] = (SpatialRegion**)halloc(sizeof(SpatialRegion*) * (s_sub_size[s_sub_index] + 1));
            for (int pc = 0; pc <= s_sub_size[s_sub_index]; pc++)
                G_s_MonitorTable[s_sub_index][pc] = (SpatialRegion*)halloc(2 * sizeof(SpatialRegion));//2= current and future
        }
    }*/
    
    //return spatio_res;
}

void update_monitor_table_coloumns(HyDis** MonitorTable, int phi_size) {
    for (int i = 1; i <= phi_size; i++)
    {
        copyToFrom(&MonitorTable[i][1], &MonitorTable[i][0]);
    }
}

void update_freez_table_coloumns(HyDis** FreezTable, int numFrame) {
    for (int i = 0; i < numFrame; i++)
    {
        copyToFrom(&FreezTable[1][i], &FreezTable[0][i]);
    }
}

//____________________________________________________________________________________//
/*Dynamic Programing Algorithm for Monitoring STQL*/
mxArray *DP(Node *phi, PMap *predMap, double *XTrace, double *TStamps, double *LTrace, 
    DistCompData *p_distData, FWTaliroParam *p_par, Miscellaneous *miscell)
{
    mwIndex ii = 0;                            /* used for mapping predicate and passing state vector*/
    mwIndex jj = 0;                            /* used for passing state vector*/
    mwIndex kk = 0;                            /* used for passing state vector*/
    Symbol *tmpsym;
    double infval;                            /*    infinite value*/
    char last = 0;
    const char *fields[] = { "dl", "ds", "most_related_iteration", "most_related_predicate_index"
                                        , "most_related_freeze_iteration", "most_related_freeze_operator_index" };
    mxArray *tmp;
    int iii = 0;                                /* used for check the index for subformula*/
    int jjj = 0;                                /* length-1 of the subformula array */
    int *qi;
    int temp = 1;
    /* Initialize some variables for BFS */
    int phi_size;
    int maxTimeVariable=0;
    //double freezeTimeValue;
    Node **subformula;/* subformula array as a cross-reference for STQL phi*/
    HyDis **MonitorTable;/* monitoring table for STQL phi*/

    double  *time_stamps;
    queue q;
    queue *Q = &q;
    init_queue(Q);                            /*initial the queue*/
    //HyDis  *robHA;
    //int i;
    //HyDis  robustness;
    int id_to_group_map_size = 0;
    size_id_to_group_map = &id_to_group_map_size;
    qi = &temp;
    infval = mxGetInf();

    for (int i = 0; i < (MAX_NUM_FREEZ_VAR + MAX_NUM_PRED); i++)
        exists_freez_idx_to_list_param[i] = false;
    
    //@todo: memory release in the end
    if (num_spatio_formulas > 0) {
        G_s_subformula = (Node***)emalloc(num_spatio_formulas * sizeof(Node**));
        s_sub_size = (int*)emalloc(num_spatio_formulas * sizeof(int));
        nFrame_for_spatio_formulas = (int*)emalloc(num_spatio_formulas * sizeof(int));
        S_formula_has_next_temporal_operator = (bool*)emalloc(num_spatio_formulas * sizeof(bool));
        S_formula_next_temporal_count = (int*)emalloc(num_spatio_formulas * sizeof(int));
        S_formula_has_evolving_temporal_operator = (bool*)emalloc(num_spatio_formulas * sizeof(bool));
        S_formula_has_freeze_id = (bool*)emalloc(num_spatio_formulas * sizeof(bool));
        for (int i = 0; i < num_spatio_formulas; i++) {
            S_formula_has_evolving_temporal_operator[i] = false;
            S_formula_has_next_temporal_operator[i] = false;
            nFrame_for_spatio_formulas[i] = p_par->nFrame;
            S_formula_has_freeze_id[i] = false;
        }
    }
    
    //NOTE: we switch from STQL to TPTL
    if (p_par->nFrame == 0)
        p_par->nFrame = p_par->nSamp;

    /* map each spatio predicate to its function expression (1st time)*/
    for (ii = 0; ii < p_par->nPred; ii++)
    {
        if (predMap[ii].true_pred)
        {
            
            //map each symbol from the formula into its corresponding predicate expression
            tmpsym = tl_lookup_new(predMap[ii].str, miscell, predMap[ii].fn_expr);//<<<<<<<<<
        }
    }

    /*-----BFS for formula--------------*/
    freez_index_map[0] = phi->index;
    freez_index_map_time_var[0] = phi->index;
    freez_idx_to_list_param[0] = (int*)0;
    for (int i = 0; i < (MAX_NUM_FREEZ_VAR + MAX_NUM_PRED); i++)
        freez_idx_to_num_param[i] = 0;
    //init the id_table_info
    id_table_info.num_objs = 0;
    id_table_info.num_id_vars = 0;//maximum number of unique id variables in a predicate
    
/*---------------------------------------------------------*/
/*---------------------------------------------------------*/
        int t_next_count = 1;
    phi_size = count_phi_subformulas(phi, qi, -1, &t_next_count);//<<<<<<<<<<<<<
    
    subformula = (Node**)emalloc(sizeof(Node*) * (phi_size + 1));
    id_table_info.num_sub_formula = phi_size;
    G_t_subformula = subformula;
    temp = 1;
    BFS(Q, phi, qi, -1);//<<<<<<<<<<<<<
    setupIndeces(subformula, phi, 0, 0, 0, 0);//<<<<<<<<<<<<<<
    temp = 1;
    queue q2;
    Q = &q2;
    init_queue(Q);
    BFS_spatial(Q, phi, qi, -1);//<<<<<<<<<<<<<

    /*@@@@@@@@@@@@@ IMPORTANT @@@@@@@@@@@*/
        G_org_nFrame = p_par->nFrame;
    if (OPTIMIZE_TRJ_BASED_ON_FORMULA) {
        if (VERBOSE_MODE)
            mexPrintf("\n[Note] OPTIMIZE_TRJ_BASED_ON_FORMULA is active.\n");
        if (!T_formula_has_evolving_temporal_operator && 
            (T_formula_has_next_temporal_operator || T_formula_has_previous_temporal_operator)) {
            if (t_next_count < 2)
                mexErrMsgTxt("DP: Error 1");
            if(G_org_nFrame > t_next_count)
                p_par->nFrame = t_next_count;//for the current and as many nesting next time as exists 
        }
        else if (!T_formula_has_evolving_temporal_operator && 
            (!T_formula_has_next_temporal_operator && !T_formula_has_previous_temporal_operator))
            p_par->nFrame = 1;
        if (VERBOSE_MODE)
            if (G_org_nFrame > p_par->nFrame)
            {
                mexPrintf("[Note] Number of frames reduced from %d to %d for temporal formula:\n", G_org_nFrame, p_par->nFrame);
                for (int j = 1; j < phi_size+1; j++) {
                    print_LTL_formula(subformula[j]);
                }
                mexPrintf("\n\n");
            }

        for (int i = 0; i < num_spatio_formulas; i++) {
            int spatio_nFrame = nFrame_for_spatio_formulas[i];
            if (!S_formula_has_evolving_temporal_operator[i] && S_formula_has_next_temporal_operator[i]) {
                if (S_formula_next_temporal_count[i] < 2)
                    mexErrMsgTxt("DP: Error 1");
                if (G_org_nFrame > S_formula_next_temporal_count[i])
                    nFrame_for_spatio_formulas[i] = S_formula_next_temporal_count[i];//for the current and next time
            }
            else if (!S_formula_has_evolving_temporal_operator[i] && !S_formula_has_next_temporal_operator[i])
                nFrame_for_spatio_formulas[i] = 1;
            if(VERBOSE_MODE)
                if (s_sub_size[i] > 0 && spatio_nFrame > nFrame_for_spatio_formulas[i]) 
                {
                    mexPrintf("[Note] Number of frames reduced from %d to %d for spatial formula:\n{",
                        spatio_nFrame, nFrame_for_spatio_formulas[i]);
                    for (int j = 1; j < s_sub_size[i] + 1; j++) {
                        print_LTL_formula(G_s_subformula[i][j]);
                    }
                    mexPrintf("}\n\n");
                }
        }
    }
    /*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/
/*---------------------------------------------------------*/
/*---------------------------------------------------------*/

    /*  For GraphViz  *
    FILE * pFile;
     pFile = fopen("GraphViz.txt", "w");
     if (pFile != NULL)
     {
         fputs("Check with: http://www.webgraphviz.com/ :", pFile);
         fputs("\ndigraph G {\n", pFile);
     }
    print2file2(phi, pFile);
    fputs("}\n", pFile);
    fclose(pFile);
    /*  For GraphViz */

    
    //some sanity checks plus print outs
    for (iii = 1; iii <= phi_size; iii++)            /*    check the index for subformula*/
    {
        if (DEBUG_MODE)
        {
            if (subformula[iii]->ntyp == FREEZE_AT) {
                mexPrintf("@(%d,%d) |Var_id|=%d\n", subformula[iii]->group,
                    subformula[iii]->index,
                    freez_idx_to_num_param[subformula[iii]->index]);
                for (int i = 0; i < freez_idx_to_num_param[subformula[iii]->index]; i++) 
                    mexPrintf("id_%d ", freez_idx_to_list_param[subformula[iii]->index][i]);
                mexPrintf("\n");
            }
            else if (subformula[iii]->ntyp == PREDICATE || subformula[iii]->second_ntype == CONSTRAINT)
                mexPrintf("%s(%d,%d) |Var_id|=%d\n", subformula[iii]->sym->name,
                    subformula[iii]->group,
                    subformula[iii]->index,
                    freez_idx_to_num_param[subformula[iii]->index]);
        }
        if (iii != subformula[iii]->index)
            mexErrMsgTxt("Breadth-First-Traversal failed, subformulas are not matched to right index!");
    }

    MonitorTable = (HyDis**)emalloc(sizeof(HyDis*) * (phi_size + 1));
    //to reduce memory allocation, we only use two the monitoring table for the current time and the next time
    for (iii = 1; iii <= phi_size; iii++) {
        MonitorTable[iii] = (HyDis*)emalloc(2 * sizeof(HyDis));//2= current and future
        if (subformula[iii]->group > maxTimeVariable)
            maxTimeVariable = subformula[iii]->group;
    }

        HyDis** FreezTable;/* monitoring table for tqo freez rows in phi*/
    FreezTable = (HyDis**)emalloc(sizeof(HyDis*) * 2);
    for (iii = 0; iii < 2; iii++) 
        FreezTable[iii] = (HyDis*)emalloc(p_par->nFrame * sizeof(HyDis));

    /* map each predicate to a set (2nd time)*/
    for (ii = 0; ii<p_par->nPred; ii++)
    {
        if (predMap[ii].true_pred)
        {
                        //map each symbol from the formula into its corresponding predicate expression
            tmpsym = tl_lookup_new(predMap[ii].str, miscell, predMap[ii].fn_expr);//<<<<<<<<<
            
            //tmpsym = tl_lookup(predMap[ii].str, miscell);
            tmpsym->set = &(predMap[ii].set);
            tmpsym->Normalized = predMap[ii].Normalized;
            tmpsym->NormBounds = predMap[ii].NormBounds;
        }
    }
    /*
    if (p_par->nInp>4 && p_par->nCLG>=1)
        sysTraj = (double *)emalloc((p_par->SysDim+ p_par->nCLG)*sizeof(double));
    else
        sysTraj = (double *)emalloc((p_par->SysDim)*sizeof(double));
  */

        time_stamps = (double*)emalloc((G_org_nFrame) * sizeof(double));
    int start_idx_frame = 0;
    int *start_idx_frames;
    int num_obj = 1;//number of objects in a frame
    int next_idx_frame = 0;

    start_idx_frames = (int*)emalloc((p_par->nSamp) * sizeof(int));
    G_num_obj_in_frame = (int*)emalloc((G_org_nFrame) * sizeof(int));

    //extract time-stamps, and start index of frames
    //also, does some sanity tests
    for (ii = 0; ii < G_org_nFrame; ii++) {

        if (p_par->STQL) {
            time_stamps[ii] = XTrace[next_idx_frame + p_par->nSamp];

            if ((XTrace[next_idx_frame]) != ii) {
                mexPrintf("expected frame# %d but read: %d\n", ii, (int)XTrace[next_idx_frame]);
                mexErrMsgTxt("Sudden jump in frame numbers!");
            }

            start_idx_frames[ii] = next_idx_frame;

                        double cur_frame = XTrace[next_idx_frame];
            int fidx = next_idx_frame;

            while (cur_frame == XTrace[fidx] && fidx < p_par->nSamp)
                fidx++;
            num_obj = fidx - next_idx_frame;
            next_idx_frame += num_obj;

                        if (num_obj > id_table_info.num_objs)
                id_table_info.num_objs = num_obj;

            G_num_obj_in_frame[ii] = num_obj;
        }
        else {
            time_stamps[ii] = TStamps[ii];
            G_num_obj_in_frame[ii] = 1;
            start_idx_frames[ii] = ii;
        }
        /*
        for (jj = 0; jj < p_par->SysDim; jj++)        
        {
            sysTraj[jj] = XTrace[next_idx_frame + jj*p_par->nSamp];
        }
        if (p_par->nInp>4 && p_par->nCLG==1)
            sysTraj[p_par->SysDim] = LTrace[ii];
        else if (p_par->nInp>4 && p_par->nCLG>1) {
            for (jj = 0; jj < p_par->nCLG; jj++)            
            {
                sysTraj[p_par->SysDim + jj] = LTrace[ii + jj*p_par->nSamp];
            }
        }
    */
        if(SANITY_CHECK)
        for (iii = 1; iii <= phi_size; iii++){
            switch (subformula[iii]->ntyp)
            {
            case PREDICATE:
                if (!subformula[iii]->sym->set && subformula[iii]->fndef==NULL)
                {
                    mexPrintf("%s%s\n", "Predicate: ", subformula[iii]->sym->name);
                    mexErrMsgTxt("The set for the above predicate has not been defined!\n");
                }
                else//<<<<<<<<<<<<<<< DEFAULT CASE FOR STQL
                {

                    //int id[MAX_OBJECT_IDS];
                    //double value[MAX_OBJECT_IDS];
                    int counter = 0;


                    if (DEBUG_MODE) {
                        mexPrintf("Atomic Symbol: %s\n", subformula[iii]->sym->name);
                        if (subformula[iii]->sym->fnx != NULL) {
                            mexPrintf("Predicate expression: ");
                            print_fn_def(subformula[iii]->sym->fnx->left_fn);
                            print_opr(subformula[iii]->sym->fnx->opr);
                            print_fn_def(subformula[iii]->sym->fnx->right_fn);
                            mexPrintf("\n");
                            mexPrintf("num_vars: left= %d , right= %d, num_unique_var= %d",
                                subformula[iii]->sym->fnx->left_fn.num_vars,
                                subformula[iii]->sym->fnx->right_fn.num_vars,
                                subformula[iii]->sym->fnx->num_unique_vars);
                            mexPrintf("\n\n");
                        }
                    }

                    if (subformula[iii]->sym->fnx != NULL &&
                        subformula[iii]->sym->fnx->num_unique_vars > 2) {//ERROR
                        mexPrintf("Predicate expression: ");
                        print_fn_def(subformula[iii]->sym->fnx->left_fn);
                        print_opr(subformula[iii]->sym->fnx->opr);
                        print_fn_def(subformula[iii]->sym->fnx->right_fn);
                        mexPrintf("\n");
                        //mexPrintf("num_vars: left= %d , right= %d", subformula[iii]->sym->fnx->left_fn.num_vars, subformula[iii]->sym->fnx->right_fn.num_vars);
                        //mexPrintf("\n");
                        mexErrMsgTxt("Number of variables in the function parameters cannot be greater than 2.\n");
                    }

                    //if (subformula[iii]->sym->fnx != NULL)
                    //    if (subformula[iii]->sym->fnx->num_unique_vars > id_table_info.num_id_vars)
                    //        id_table_info.num_id_vars = subformula[iii]->sym->fnx->num_unique_vars;
                }
                break;
            default:
                break;
            }
        }
    }

    if(!p_par->STQL)
        id_table_info.num_objs = 1;

    //memory allocation for the id_table
    id_table_info.size = (int)pow(id_table_info.num_objs, id_table_info.num_id_vars);
    if (id_table_info.size > pow(MAX_OBJ_IN_FRAME, MAX_UNIQUE_VAR_ID_IN_PRED))
        mexErrMsgTxt("Number of items for the ID variable table size exceeds its maximum allowed number.");

    //if the formula is TPTL or LTL we have to consider
    //the given time stamp parameter rather than the 
    //supposed computed time stamps from the frames
    //which does not exist in this case
    //if (!p_par->STQL) {
    //    mxFree(time_stamps);
    //    time_stamps = TStamps;
    //}
    if (!p_par->STQL)
        id_table_info.size = 0;


    if (id_table_info.size >= 1) {//if there is need to create this table (more than one id variable combination)
        for (int i = 1; i <= phi_size; i++)
        {
            MonitorTable[i][0].IdTable = (double*)emalloc(id_table_info.size * sizeof(double));
            MonitorTable[i][1].IdTable = (double*)emalloc(id_table_info.size * sizeof(double));
        }
        for (ii = 0; ii < p_par->nFrame; ii++) {
            FreezTable[0][ii].IdTable = (double*)emalloc(id_table_info.size * sizeof(double));
            FreezTable[1][ii].IdTable = (double*)emalloc(id_table_info.size * sizeof(double));
        }
    }
    
    //initialize spatial formula DP tables
    if (num_spatio_formulas > 0) {
        G_s_MonitorTable = (SpatialRegion*****)halloc(sizeof(SpatialRegion****) * (num_spatio_formulas));
        for (int sc = 0; sc < num_spatio_formulas; sc++) {
            G_s_MonitorTable[sc] = (SpatialRegion****)halloc(sizeof(SpatialRegion***) * (id_table_info.size));
            for (int ic = 0; ic < id_table_info.size; ic++) {
                G_s_MonitorTable[sc][ic] = (SpatialRegion***)halloc(sizeof(SpatialRegion**) * (s_sub_size[sc] + 1));
                for (int pc = 0; pc <= s_sub_size[sc]; pc++) {
                    //G_s_MonitorTable[sc][ic][pc] = createSpatialRegion(2);
                    G_s_MonitorTable[sc][ic][pc] = (SpatialRegion**)halloc(2 * sizeof(SpatialRegion*));//2= current and future
                    G_s_MonitorTable[sc][ic][pc][0] = createSpatialRegion(1);
                    G_s_MonitorTable[sc][ic][pc][1] = createSpatialRegion(1);
                }
            }
        }
    }

    numTimeVariable = maxTimeVariable;
    //Global pointer variable assignments
    G_p_par = p_par;
    G_XTrace = XTrace;
    G_time_stamps = time_stamps;
    G_start_idx_frames = start_idx_frames;
    
    //**********************//
    //start of DP algorithm
    //**********************//
    int t_access = 0;//swaps between 0 and 1: used for current and next time in DP MonitorTable
    int f_access = 0;//swaps between 0 and 1: used for current and next time in DP FreezTable

        init_garbage_collector();

    //TEST TEST TEST
    float** RobResult;
    int num_neg_freeze_tive_values = 0;
    if (maxTimeVariable > 0) {
        RobResult = (float**)emalloc(p_par->nFrame * maxTimeVariable);
        for(int i=0; i < maxTimeVariable; i++)
            RobResult[i] = (float*)emalloc(p_par->nFrame * sizeof(float));
    }

    //measure executing time
    clock_t exe_time_start;
    if (true)
    {
        bool has_past_opr = T_formula_has_previous_temporal_operator || S_formula_has_previous_temporal_operator ? true : false;
        if (maxTimeVariable > 0)
        {//STQL

            int ft_idx;//freezing time index
            int et_idx;//evaluating time index

            //stamp the current time
            exe_time_start = clock();
            if(VERBOSE_MODE)
                mexPrintf("Augmented STPL formua:\n");

            double time_lb = 0;
            double time_ub = time_stamps[p_par->nFrame - 1];
            int frame_lb = 0;
            int frame_ub = p_par->nFrame - 1;
            for (int i = 1; i <= phi_size; i++) {
                if (subformula[i]->time_ub == 0)
                    subformula[i]->time_ub = time_ub;
                if (subformula[i]->frame_ub == 0)
                    subformula[i]->frame_ub = frame_ub;
                if (VERBOSE_MODE)
                    print_LTL_formula(subformula[i]);
            }
            if (VERBOSE_MODE)
                mexPrintf("\n");
                mexPrintf("Monitoring process started...\n");
            if(run_from_matlab)
            mexCallMATLAB(0, NULL, 0, NULL, "drawnow");//to flush printouts in Matlab

            for (int gg = maxTimeVariable; gg >= 1; gg--)
            {
                //@todo: check for time/frame constraints
                if (subformula[freez_index_map_time_var[gg]]->fndef != NULL &&
                    subformula[freez_index_map_time_var[gg]]->ntyp == FREEZE_AT &&
                    subformula[freez_index_map_time_var[gg]]->has_freeze_var)
                {

                    if (subformula[freez_index_map_time_var[gg]]->time_lb > 0)
                        time_lb = subformula[freez_index_map_time_var[gg]]->time_lb;
                    if (subformula[freez_index_map_time_var[gg]]->time_ub > 0)
                        time_ub = subformula[freez_index_map_time_var[gg]]->time_ub;
                    if (subformula[freez_index_map_time_var[gg]]->frame_lb > 0)
                        frame_lb = subformula[freez_index_map_time_var[gg]]->frame_lb;
                    if (subformula[freez_index_map_time_var[gg]]->frame_ub > 0)
                        frame_ub = subformula[freez_index_map_time_var[gg]]->frame_ub;
                }

                for (ft_idx = 0; ft_idx < p_par->nFrame; ft_idx++)//freezing index
                {
                    for (et_idx = p_par->nFrame - 1; et_idx >= ft_idx; et_idx--) //evaluating index
                    {
                        for (ii = phi_size; ii >= 1; ii--)//@todo: move this loop in the DP_STQL function for more efficiency
                        {
                            //@todo: check the time/frame constraints
                            if (((time_stamps[et_idx] - time_stamps[ft_idx]) >= time_lb &&
                                (time_stamps[et_idx] - time_stamps[ft_idx]) <= time_ub &&
                                (et_idx - ft_idx) >= frame_lb &&
                                (et_idx - ft_idx) <= frame_ub)
                                || et_idx == (p_par->nFrame - 1)
                                || ft_idx == et_idx
                                || ft_idx == 0
                                )
                            {
                                if (subformula[ii]->group == gg 
                                    || subformula[ii]->ntyp == FALSE || subformula[ii]->ntyp == TRUE) {

                                    int num_extra_run = 0;//if there is no past time operators
                                    
                                    /*if (has_past_opr &&
                                            !is_past_opr(subformula[ii]->ntyp) &&
                                            p_par->nFrame > 1 &&
                                            et_idx == (p_par->nFrame - 1))
                                        num_extra_run = 1;*/

                                    for (int num_run = 0; num_run <= num_extra_run; num_run++) {

                                        int net_idx = et_idx - num_run;
                                        
                                        /*if (has_past_opr &&
                                                !is_past_opr(subformula[ii]->ntyp) &&
                                                p_par->nFrame > 1 &&
                                                et_idx < (p_par->nFrame - 1))
                                            net_idx = et_idx - 1;*/

                                        DP_STQL_TEMPORAL(subformula, MonitorTable, FreezTable, XTrace, time_stamps, p_par,
                                            phi_size,
                                            net_idx,//evaluating time index
                                            ft_idx,//freezing time index
                                            start_idx_frames[net_idx],//the sample index of the current time index
                                            ii,//subformula index
                                            start_idx_frames[ft_idx],//the sample index of the freez time index
                                            t_access,
                                            f_access
                                        );
                                        /*if (num_extra_run > 0) {
                                            t_access = t_access > 0 ? 0 : 1;
                                            s_toggle_table_index();
                                        }*/
                                    }
                                }
                            }
                            //@todo: else condition for checking the time/frame constraints
                            else {
                                int t_access_old = t_access > 0 ? 0 : 1;
                                if(id_table_info.size > 0 )
                                    copyToFrom(&MonitorTable[ii][t_access], &MonitorTable[ii][t_access_old]);
                                else
                                    MonitorTable[ii][t_access].ds = MonitorTable[ii][t_access_old].ds;
                            }

                        }
                        t_access = t_access > 0 ? 0 : 1;
                        s_toggle_table_index();
                    }
                    RobResult[gg-1][ft_idx] = FreezTable[f_access][ft_idx].ds;
                    if (RobResult[gg - 1][ft_idx] < 0)
                        num_neg_freeze_tive_values++;
                }
                f_access = f_access > 0 ? 0 : 1;
            }

            if (subformula[1]->ntyp != FREEZE_AT)
            {
                for (et_idx = p_par->nFrame - 1; et_idx >= 0; et_idx--)
                {
                    for (ii = phi_size; ii >= 1; ii--)//@todo: move this loop in the DP_STQL function for more efficiency
                    {
                        if (subformula[ii]->group == 0 
                            || subformula[ii]->ntyp == FALSE || subformula[ii]->ntyp == TRUE) {

                            DP_STQL_TEMPORAL(subformula, MonitorTable, FreezTable, XTrace, time_stamps, p_par,
                                phi_size,
                                et_idx,//et_idx
                                0,//ft_idx
                                start_idx_frames[et_idx],//the sample index of the current time index
                                ii,//subformula index
                                0,//the sample index of the freez time index
                                t_access,
                                f_access
                            );
                        }
                    }
                    t_access = t_access > 0 ? 0 : 1;
                    s_toggle_table_index();
                }
                t_access = t_access > 0 ? 0 : 1;
            }
            else
            {
                t_access = t_access > 0 ? 0 : 1;
                f_access = f_access > 0 ? 0 : 1;
                copyToFrom(&MonitorTable[1][t_access], &FreezTable[f_access][0]);
            }
        }
        else {
            if(VERBOSE_MODE)
                mexPrintf("Monitoring process started...\n");
            if (run_from_matlab)
                mexCallMATLAB(0, NULL, 0, NULL, "drawnow");//to flush printouts in Matlab

            int et_idx;//evaluating time index

            //stamp the current time
            exe_time_start = clock();

            for (et_idx = p_par->nFrame - 1; et_idx >= 0; et_idx--) //evaluating index
            {
                for (ii = phi_size; ii >= 1; ii--)//@todo: move this loop in the DP_STQL function for more efficiency
                {
                    if (subformula[ii]->group == 0
                        || subformula[ii]->ntyp == FALSE || subformula[ii]->ntyp == TRUE) {

                        int num_extra_run = 0;//if there is no past time operators

                        /*if (has_past_opr &&
                            !is_past_opr(subformula[ii]->ntyp) &&
                            p_par->nFrame > 1 &&
                            et_idx == (p_par->nFrame - 1))
                            num_extra_run = 1;*/

                        for (int num_run = 0; num_run <= num_extra_run; num_run++) {
                            
                            int net_idx = et_idx - num_run;
                            
                            DP_STQL_TEMPORAL(subformula, MonitorTable, FreezTable, XTrace, time_stamps, p_par,
                                phi_size,
                                net_idx,//evaluating time index
                                0,//freezing time index
                                start_idx_frames[net_idx],//the sample index of the current time index
                                ii,//subformula index
                                start_idx_frames[0],//the sample index of the freez time index
                                t_access,
                                f_access
                            );
                            /*if (num_extra_run > 0) {
                                t_access = t_access > 0 ? 0 : 1;
                                s_toggle_table_index();
                            }*/
                        }

                    }
                    else {
                        mexErrMsgTxt("Wrong formula.");
                    }
                }
                t_access = t_access > 0 ? 0 : 1;
                s_toggle_table_index();
            }
            t_access = t_access > 0 ? 0 : 1;
        }
    }

    phi->rob = MonitorTable[1][t_access];//here is the final result!!!


    clock_t exe_time_end = clock();
    double elapsed_time = (exe_time_end - exe_time_start)/(double)(CLOCKS_PER_SEC);
    //mexPrintf("Monitoring done!\n");

    if(!API) {
        mexPrintf(">> Running Time: %.6f seconds\n", elapsed_time);
    }

    if (SHOW_EXE_STAT) {
	mexPrintf("ID table size: %d\n", id_table_info.size);
        mexPrintf("ID table num_obj: %d\n", id_table_info.num_objs);
        mexPrintf("ID table num_id: %d\n", id_table_info.num_id_vars);
        mexPrintf("Temporal formula size: %d\n", phi_size);
        for(int i=0; i < num_spatio_formulas; i++)
            mexPrintf("Spatial formula[%d] size: %d\n", i, s_sub_size[i]);
        //mexPrintf("DP_SPATIAL iteration: %d\n", spatial_memory_counter);
        if (run_from_matlab)
            mexCallMATLAB(0, NULL, 0, NULL, "drawnow");//to flush printouts in Matlab
    }

    //print out the result before releasing memory (might be time consuming)
    if(!API) {
        if (phi->ntyp == TRUE)
	    if (BOOLEAN_ROB) {
		mexPrintf("\n*** Robustness: %s\n", "True");
	    } else {
		mexPrintf("\nRobustness: %f\n", infval);
	    }
        else if (phi->ntyp == FALSE)
            if (BOOLEAN_ROB) {
		mexPrintf("\n*** Robustness: %s\n", "False");
	    } else {
		mexPrintf("\n*** Robustness: %f\n", -infval);
	    }
        else
        {
            if (BOOLEAN_ROB) {
		mexPrintf("\n*** Robustness: %s\n", phi->rob.ds > 0 ? "True" : "False");
	    } else {
		mexPrintf("\n*** Robustness: %g\n", phi->rob.ds);
	    }

            if (VERBOSE_MODE)
            {
                mexPrintf("*** Most Related Iteration: %d\n", phi->rob.iteration);
                if (phi->rob.preindex >= 0 && phi->rob.preindex < G_p_par->nPred) {
                    //mexPrintf("Most Related Predicate Index: %d\n", phi->rob.predicate);
                    mexPrintf("*** Predicate: %s\n", predMap[phi->rob.preindex].str);
                }
                if (phi->rob.freeze_formula_index < 0)
                    phi->rob.freeze_iteration = -1;
                mexPrintf("*** Most Related Freeze Iteration: %d\n", phi->rob.freeze_iteration);
                mexPrintf("*** Most Related Freeze Operator Index: %d\n", phi->rob.freeze_formula_index);
            }
        }
        if (run_from_matlab)
            mexCallMATLAB(0, NULL, 0, NULL, "drawnow");//to flush printouts in Matlab
    }

    //TEST TEST TEST
    if (maxTimeVariable > 0 && VERBOSE_MODE && num_neg_freeze_tive_values) {
        int j = 0;
        int neg_cnt = 1;
        printf("\nFreeze-time variable table with negative robust values:\n");
        for (j = maxTimeVariable-1 ; j >=0 ; j--) 
        {
            for (int i = 0; i < p_par->nFrame - 1; i++) {
                if (RobResult[j][i] < 0) {
                    printf("%d:\t time-variable[%s]\t at ts[%d]\t with Robustness %.6f\n", neg_cnt++,
                        subformula[freez_index_map_time_var[j + 1]]->sym->name, i, RobResult[j][i]);
                }
            }
        }
    }

    exe_time_start = clock();

    //@todo: release other allocated memories 
    bool release_memory = RELEASE_DP_TABLE_MEMORY;

    if (!release_memory && num_spatio_formulas > 0)
    {
        release_garbage(false);
        should_release_memory = false;
        release_memory = true;
        if (VERBOSE_MODE)
            if (run_from_matlab)
                mexCallMATLAB(0, NULL, 0, NULL, "drawnow");//to flush printouts in Matlab
    }

    //if (VERBOSE_MODE)
    //    mexPrintf("Releasing DP table memories and it may take a while!\n");
    unsigned long int size = 0;
    for (int i = 0; i < num_spatio_formulas; i++)
        size += id_table_info.size * (s_sub_size[i]+1) * 2;

    if (VERBOSE_MODE && SHOW_EXE_STAT) {
        mexPrintf("Spatial Region size from ID table: %d\n", size);
        if (run_from_matlab)
            mexCallMATLAB(0, NULL, 0, NULL, "drawnow");//to flush printouts in Matlab
    }

    if (DEBUG_MODE && VERBOSE_MODE && SHOW_EXE_STAT) {
        mexPrintf("sr_mem_cnt: %d sr_mem_new: %d\n", sr_mem_cnt, sr_mem_new);
        mexPrintf("sn_mem_cnt: %d sn_mem_new: %d\n", sn_mem_cnt, sn_mem_new);
        if (run_from_matlab)
            mexCallMATLAB(0, NULL, 0, NULL, "drawnow");//to flush printouts in Matlab
    }

    /*RELEASE DP TABLES*/
    if (release_memory)
    {

        size = 0;
        if (num_spatio_formulas > 0) {
            should_release_memory = true;
            for (int i = 0; i < num_spatio_formulas; i++) {
                for (int k = 0; k < id_table_info.size; k++) {
                    for (int j = 0; j <= s_sub_size[i]; j++) {
                        //if (RELEASE_DP_TABLE_MEMORY) 
                        {
                            size += release_spatial_node(&(G_s_MonitorTable[i][k][j][0]->head));
                            size += release_spatial_node(&(G_s_MonitorTable[i][k][j][1]->head));

                            size += release_spatial_region(&(G_s_MonitorTable[i][k][j][0]));
                            size += release_spatial_region(&(G_s_MonitorTable[i][k][j][1]));
                            
                            ////size += G_s_MonitorTable[i][k][j][0]->size;//TEST TEST TEST
                            ////size += G_s_MonitorTable[i][k][j][1]->size;

                            //size += 2;
                            hfree(G_s_MonitorTable[i][k][j]);
                        }
                    }
                    hfree(G_s_MonitorTable[i][k]);
                }
                hfree(G_s_MonitorTable[i]);
            }
            hfree(G_s_MonitorTable);
        }

        for (iii = 0; iii < p_par->nFrame; iii++) {
            if (id_table_info.size >= 1) {
                efree(FreezTable[0][iii].IdTable);
                efree(FreezTable[1][iii].IdTable);
            }
        }
        efree(FreezTable[0]);
        efree(FreezTable[1]);
        efree(FreezTable);

        for (iii = 1; iii <= phi_size; iii++) {
            if (id_table_info.size >= 1) {
                efree(MonitorTable[iii][0].IdTable);
                efree(MonitorTable[iii][1].IdTable);
            }
            efree(MonitorTable[iii]);
            //efree(subformula[iii]);
        }
        efree(MonitorTable);
        efree(subformula);

        //if(p_par->STQL)
        efree(time_stamps);
        efree(start_idx_frames);
        if (num_spatio_formulas > 0) {
            for (int i = 0; i < num_spatio_formulas; i++) {
                if (G_s_subformula[i] != NULL) {
                    for (int j=0; j <= s_sub_size[i]; j++)
                            efree(G_s_subformula[i][j]);
                    efree(G_s_subformula[i]);
                }
            }
            efree(G_s_subformula);
        }
        
        for (int i = 0; i < MAX_NUM_FREEZ_VAR + MAX_NUM_PRED; i++)
            if ( exists_freez_idx_to_list_param[i] )
                efree(freez_idx_to_list_param[i]);
        efree(s_sub_size);
        efree(nFrame_for_spatio_formulas);
        efree(S_formula_has_next_temporal_operator);
        efree(S_formula_next_temporal_count);
        efree(S_formula_has_evolving_temporal_operator);
        efree(S_formula_has_freeze_id);
        efree(G_num_obj_in_frame);

    }    
    exe_time_end = clock();
    elapsed_time = (exe_time_end - exe_time_start) / (double)(CLOCKS_PER_SEC);

    if (DEBUG_MODE && VERBOSE_MODE && SHOW_EXE_STAT) {
        mexPrintf("sr_mem_rel_cnt: %d sr_mem_rel_new: %d\n", sr_mem_rel_cnt, sr_mem_rel_new);
        mexPrintf("sn_mem_rel_cnt: %d sn_mem_rel_new: %d\n", sn_mem_rel_cnt,sn_mem_rel_new);
        mexPrintf("Released Spatial Region/Node memory from ID table: %d\n", size);
        if (run_from_matlab)
            mexCallMATLAB(0, NULL, 0, NULL, "drawnow");//to flush printouts in Matlab
    }
    
    if (VERBOSE_MODE && SHOW_EXE_STAT)
        mexPrintf("\n>> Memory Releasing Time: %.6f seconds\n", elapsed_time);
    if (VERBOSE_MODE)
        mexPrintf("Exiting monitoring algorithm.\n");


    tmp = mxCreateStructMatrix(1, 1, 6, fields);

    if (BOOLEAN_ROB) 
    {
        if (phi->ntyp == TRUE)
        {
            mxSetField(tmp, 0, "dl", mxCreateLogicalScalar(true));
            mxSetField(tmp, 0, "ds", mxCreateLogicalScalar(true));
            mxSetField(tmp, 0, "most_related_iteration", mxCreateDoubleScalar(infval));
            mxSetField(tmp, 0, "most_related_predicate_index", mxCreateDoubleScalar(infval));
            mxSetField(tmp, 0, "most_related_freeze_iteration", mxCreateDoubleScalar(infval));
            mxSetField(tmp, 0, "most_related_freeze_operator_index", mxCreateDoubleScalar(infval));
        }
        else if (phi->ntyp == FALSE)
        {
            mxSetField(tmp, 0, "dl", mxCreateLogicalScalar(false));
            mxSetField(tmp, 0, "ds", mxCreateLogicalScalar(false));
            mxSetField(tmp, 0, "most_related_iteration", mxCreateDoubleScalar(-infval));
            mxSetField(tmp, 0, "most_related_predicate_index", mxCreateDoubleScalar(-infval));
            mxSetField(tmp, 0, "most_related_freeze_iteration", mxCreateDoubleScalar(-infval));
            mxSetField(tmp, 0, "most_related_freeze_operator_index", mxCreateDoubleScalar(-infval));
        }
        else
        {
            mxSetField(tmp, 0, "dl", mxCreateLogicalScalar(phi->rob.dl > 0));
            mxSetField(tmp, 0, "ds", mxCreateLogicalScalar(phi->rob.ds > 0));
            mxSetField(tmp, 0, "most_related_iteration", mxCreateDoubleScalar(phi->rob.iteration));
            mxSetField(tmp, 0, "most_related_predicate_index", mxCreateString(phi->rob.predicate));
            if (DEBUG_MODE)
            {
                if (phi->rob.preindex >= 0 && phi->rob.preindex < G_p_par->nPred)
                    mexPrintf("\nPredicate: %s\n", predMap[phi->rob.preindex].str);
            }
            if (phi->rob.freeze_formula_index < 0)
                phi->rob.freeze_iteration = -1;
            mxSetField(tmp, 0, "most_related_freeze_iteration", mxCreateDoubleScalar(phi->rob.freeze_iteration));
            mxSetField(tmp, 0, "most_related_freeze_operator_index", mxCreateDoubleScalar(phi->rob.freeze_formula_index));
        }
    }
    else 
    {
        if (phi->ntyp == TRUE)
        {
            mxSetField(tmp, 0, "dl", mxCreateDoubleScalar(infval));
            mxSetField(tmp, 0, "ds", mxCreateDoubleScalar(infval));
            mxSetField(tmp, 0, "most_related_iteration", mxCreateDoubleScalar(infval));
            mxSetField(tmp, 0, "most_related_predicate_index", mxCreateDoubleScalar(infval));
            mxSetField(tmp, 0, "most_related_freeze_iteration", mxCreateDoubleScalar(infval));
            mxSetField(tmp, 0, "most_related_freeze_operator_index", mxCreateDoubleScalar(infval));
        }
        else if (phi->ntyp == FALSE)
        {
            mxSetField(tmp, 0, "dl", mxCreateDoubleScalar(-infval));
            mxSetField(tmp, 0, "ds", mxCreateDoubleScalar(-infval));
            mxSetField(tmp, 0, "most_related_iteration", mxCreateDoubleScalar(-infval));
            mxSetField(tmp, 0, "most_related_predicate_index", mxCreateDoubleScalar(-infval));
            mxSetField(tmp, 0, "most_related_freeze_iteration", mxCreateDoubleScalar(-infval));
            mxSetField(tmp, 0, "most_related_freeze_operator_index", mxCreateDoubleScalar(-infval));
        }
        else
        {
            mxSetField(tmp, 0, "dl", mxCreateDoubleScalar(phi->rob.dl));
            mxSetField(tmp, 0, "ds", mxCreateDoubleScalar(phi->rob.ds));
            mxSetField(tmp, 0, "most_related_iteration", mxCreateDoubleScalar(phi->rob.iteration));
            //mxSetField(tmp, 0, "most_related_predicate_index", mxCreateDoubleScalar(phi->rob.preindex + 1));
            mxSetField(tmp, 0, "most_related_predicate_index", mxCreateString(phi->rob.predicate));
            if (DEBUG_MODE)
            {
                if (phi->rob.preindex >= 0 && phi->rob.preindex < G_p_par->nPred)
                    mexPrintf("\nPredicate: %s\n", predMap[phi->rob.preindex].str);
            }
            if (phi->rob.freeze_formula_index < 0)
                phi->rob.freeze_iteration = -1;
            mxSetField(tmp, 0, "most_related_freeze_iteration", mxCreateDoubleScalar(phi->rob.freeze_iteration));
            mxSetField(tmp, 0, "most_related_freeze_operator_index", mxCreateDoubleScalar(phi->rob.freeze_formula_index));
        }
    }

    return(tmp);
}
//____________________________________________________________________________________//

bool is_past_opr(int type) {
    if (type == PREVIOUS || type == WEAKPREVIOUS)
        return true;
    return false;
}

void moveNode2to1(Node **pt_node1, Node *node2)
{
    Node *tmpN;
    tmpN = (*pt_node1);
    (*pt_node1) = node2;
    releasenode(0,tmpN);
}

void moveNodeFromRight(Node **pt_node)
{
    Node *tmpN;
    tmpN = (*pt_node)->rgt;
    (*pt_node)->rgt = ZN;
    releasenode(1,(*pt_node));
    (*pt_node) = tmpN;
}

void moveNodeFromLeft(Node **pt_node)
{
    Node *tmpN;
    tmpN = (*pt_node)->lft;
    (*pt_node)->lft = ZN;
    releasenode(1,(*pt_node));
    (*pt_node) = tmpN;
}

Node *SimplifyNodeValue(Node *node)
{
    switch (node->ntyp)
    {
        /* Or node */
        case OR :
            node = SimplifyBoolConn(OR,node,moveNodeFromLeft,moveNodeFromRight,hmax);
            break;
            
        /* AND node    */
        case AND :
            node = SimplifyBoolConn(AND,node,moveNodeFromRight,moveNodeFromLeft,hmin);
            break;

        default:
            break;
    }
    return(node);
}        

Node *SimplifyBoolConn(int BCon, Node *node, void (*MoveNodeL)(Node **), void (*MoveNodeR)(Node **), HyDis (*Comparison)(HyDis,HyDis))
{
    /* If both nodes are values, then convert to value node */
    if (node->lft->ntyp == VALUE && node->rgt->ntyp == VALUE)
    {
        node->ntyp = VALUE;
        node->rob = (*Comparison)(node->lft->rob, node->rgt->rob);
        moveNode2to1(&(node->rgt),ZN); 
        moveNode2to1(&(node->lft),ZN);
        return(node);
    }
    /* Simplify nested boolean connectives (of the same type) */ 
    /* If left leaf is a value and right node is a boolean connective */
    else if (node->lft->ntyp == VALUE && node->rgt->ntyp == BCon)
    {
        /* If the left node of the child boolean connective node is a value */
        if (node->rgt->lft->ntyp == VALUE)
        {
            node->lft->rob = (*Comparison)(node->lft->rob,node->rgt->lft->rob);
            moveNodeFromRight(&(node->rgt));
            /* node->rgt = moveNodeFromRight(node->rgt);*/
            node = SimplifyNodeValue(node);
            return(node);
        }
        /* If the right node of the child boolean connective node is a value */
        if (node->rgt->rgt->ntyp == VALUE)
        {
            node->lft->rob = (*Comparison)(node->lft->rob,node->rgt->rgt->rob);
            moveNodeFromLeft(&(node->rgt));
            /* node->rgt = moveNodeFromLeft(node->rgt);*/
            node = SimplifyNodeValue(node);
            return(node);
        }
        /* no value node found */
        return(node);
    }
    /* If right node is a value and left node is a boolean connective */
    else if (node->rgt->ntyp == VALUE && node->lft->ntyp == BCon)
    {
        /* If the left node of the child boolean connective node is a value */
        if (node->lft->lft->ntyp == VALUE)
        {
            node->rgt->rob = (*Comparison)(node->rgt->rob,node->lft->lft->rob);
            moveNodeFromRight(&(node->lft));
            /* node->lft = moveNodeFromRight(node->lft);*/
            node = SimplifyNodeValue(node);
            return(node);
        }
        /* If the right node of the child boolean connective node is a value */
        if (node->lft->rgt->ntyp == VALUE)
        {
            node->rgt->rob = (*Comparison)(node->rgt->rob,node->lft->rgt->rob);
            moveNodeFromLeft(&(node->lft));
            /* node->lft = moveNodeFromLeft(node->lft);*/
            node = SimplifyNodeValue(node);
            return(node);
        }
        /* no value node found */
        return(node);
    }
    /* If left leaf is top : case OR : +inf \/ phi <-> +inf */
    /* If left leaf is top : case AND : +inf /\ phi <-> phi */
    else if (node->lft->ntyp == TRUE)
    {
        (*MoveNodeL)(&node);
        if (BCon==AND)
            node = SimplifyNodeValue(node);
        return(node);
    }
    /* If left leaf is bottom : case OR : -inf \/ phi <-> phi */
    /* If left leaf is bottom : case AND : -inf /\ phi <-> -inf */
    else if (node->lft->ntyp == FALSE)
    {
        (*MoveNodeR)(&node);
        if (BCon==OR)
            node = SimplifyNodeValue(node);
        return(node);
    }
    /* If right leaf is top : case OR : phi \/ +inf <-> +inf */
    /* If right leaf is top : case AND : phi /\ +inf <-> phi */
    else if (node->rgt->ntyp == TRUE)
    {
        (*MoveNodeR)(&node);
        if (BCon==AND)
            node = SimplifyNodeValue(node);
        return(node);
    }
    /* If right leaf is bottom : case OR : phi \/ -inf <-> phi */
    /* If right leaf is bottom : case AND: phi /\ -inf <-> -inf */
    else if (node->rgt->ntyp == FALSE)
    {
        (*MoveNodeL)(&node);
        if (BCon==OR)
            node = SimplifyNodeValue(node);
        return(node);
    }
    else
        return(node);
}

HyDis hmax(HyDis inp1, HyDis inp2)
{
    if ((inp1.dl < inp2.dl) || ((inp1.dl == inp2.dl) && (inp1.ds <= inp2.ds))) 
        return(inp2);
    
    else 
        return(inp1);
    
}

HyDis hmin(HyDis inp1, HyDis inp2)
{
    if ((inp1.dl<inp2.dl) || ((inp1.dl==inp2.dl) && (inp1.ds<=inp2.ds)))
        return(inp1);
    else
        return(inp2);
}

void hmax_new(HyDis* result, HyDis* inp1, HyDis* inp2)
{//@todo: to be checked for efficiency improvement
    HyDis* src;
    if ((inp1->dl < inp2->dl) || ((inp1->dl == inp2->dl) && (inp1->ds <= inp2->ds))) {
        src = inp2;
    }
    else {
        src = inp1;
    }
    double* tmp_id = result->IdTable;
    *result = *src;
    result->IdTable = tmp_id;
    safe_strcpy(result->predicate, MAX_VAR_NAME_LENGTH, src->predicate);
    result->min_iteration = src->iteration;
    result->max_iteration = src->iteration;

    if (src->IdTable != NULL) {
        result->min_dis = +mxGetInf();// ->IdTable[0];
        result->max_dis = -mxGetInf();// ->IdTable[0];
        for (int i = 0; i < id_table_info.size; i++) {
            
            if (isnan(inp1->IdTable[i]) && !isnan(inp2->IdTable[i]))
                result->IdTable[i] = inp1->IdTable[i];
            else if (!isnan(inp1->IdTable[i]) && isnan(inp2->IdTable[i]))
                result->IdTable[i] = inp2->IdTable[i];
            else
                result->IdTable[i] = inp1->IdTable[i] > inp2->IdTable[i] ?
                inp1->IdTable[i] : inp2->IdTable[i];

            if (result->min_dis > result->IdTable[i]) 
            {
                result->min_dis = result->IdTable[i];
            }
            if (result->max_dis < result->IdTable[i]) 
            {
                result->max_dis = result->IdTable[i];
            }
            result->freeze_min_preindex = inp1->IdTable[i] > inp2->IdTable[i] ?
                inp1->freeze_min_preindex : inp2->freeze_min_preindex;//<<<min
            result->freeze_max_preindex = inp1->IdTable[i] > inp2->IdTable[i] ?
                inp1->freeze_max_preindex : inp2->freeze_max_preindex;
            result->max_iteration = inp1->IdTable[i] > inp2->IdTable[i] ?
                inp1->iteration : inp2->iteration;
            result->min_iteration = inp1->IdTable[i] > inp2->IdTable[i] ?
                inp1->iteration : inp2->iteration;
        }
    }
    else {
        if (inp1->min_dis < inp2->min_dis)
            result->min_dis = inp2->min_dis;
        else
            result->min_dis = inp1->min_dis;
    }
}

void hmin_new(HyDis* result, HyDis* inp1, HyDis* inp2)
{//@todo: to be checked for efficiency improvement
    HyDis* src;
    if ((inp1->dl < inp2->dl) || ((inp1->dl == inp2->dl) && (inp1->ds <= inp2->ds))) {
        src = inp1;
    }
    else {
        src = inp2;
    }
    double* tmp_id = result->IdTable;
    *result = *src;
    result->IdTable = tmp_id;
    safe_strcpy(result->predicate, MAX_VAR_NAME_LENGTH, src->predicate);
    result->min_iteration = src->iteration;
    result->max_iteration = src->iteration;

    if (src->IdTable != NULL)
    {
        //result->max_dis = result->IdTable[0];
        result->min_dis = +mxGetInf();// ->IdTable[0];
        result->max_dis = -mxGetInf();// ->IdTable[0];
        for (int i = 0; i < id_table_info.size; i++) {
            ///if(isnan(inp1->IdTable[i]) || isnan(inp2->IdTable[i]))
            ///    result->IdTable[i] = NAN;
            ///else
            if (isnan(inp1->IdTable[i]) && !isnan(inp2->IdTable[i]))
                result->IdTable[i] = inp1->IdTable[i];
            else if (!isnan(inp1->IdTable[i]) && isnan(inp2->IdTable[i]))
                result->IdTable[i] = inp2->IdTable[i];
            else
                result->IdTable[i] = inp1->IdTable[i] < inp2->IdTable[i] ? 
                                        inp1->IdTable[i] : inp2->IdTable[i];
            if (result->max_dis < result->IdTable[i]) 
            {
                result->max_dis = result->IdTable[i];
            }
            if (result->min_dis > result->IdTable[i]) 
            {
                result->min_dis = result->IdTable[i];
            }
            result->freeze_max_preindex = inp1->IdTable[i] < inp2->IdTable[i] ?
                inp1->freeze_max_preindex : inp2->freeze_max_preindex;//<<<max
            result->freeze_min_preindex = inp1->IdTable[i] < inp2->IdTable[i] ?
                inp1->freeze_min_preindex : inp2->freeze_min_preindex;
            result->max_iteration = inp1->IdTable[i] < inp2->IdTable[i] ?
                inp1->iteration : inp2->iteration;
            result->min_iteration = inp1->IdTable[i] < inp2->IdTable[i] ?
                inp1->iteration : inp2->iteration;
        }
    }
    else {
        if (inp1->max_dis > inp2->max_dis)
            result->max_dis = inp2->max_dis;
        else
            result->max_dis = inp1->max_dis;
    }
}

void copyToFrom(HyDis* dest, HyDis* src) {
    //double** tmp_id = &dest->IdTable;
    double* tmp_id = dest->IdTable;
    *dest = *src;
    safe_strcpy(dest->predicate, MAX_VAR_NAME_LENGTH, src->predicate);
    //dest->IdTable = *tmp_id;
    dest->IdTable = tmp_id;
    for (int i = 0; i < id_table_info.size; i++) {
        dest->IdTable[i] = src->IdTable[i];
    }
}

/* Define operations on intervals */
/* Addition */
/* Double precision bounds are assumed */
Interval NumberPlusInter(Number num, Interval inter)
{
    if (inter.lbd.num.inf==0)
        inter.lbd.numf.f_num += num.numf.f_num;
    if (inter.ubd.num.inf==0)
        inter.ubd.numf.f_num += num.numf.f_num;
    return(inter);
}

/* Define comparisons over the extended real line */
int e_le(Number num1, Number num2, FWTaliroParam *p_par)
{
    if ((num1.num.inf==-1 && num2.num.inf>-1) || (num1.num.inf<1 && num2.num.inf==1))
        return(1);
    else if (num1.num.inf==0 && num2.num.inf==0)
    {
        if (p_par->ConOnSamples)
            return (num1.numi.i_num < num2.numi.i_num);
        else
            return (num1.numf.f_num < num2.numf.f_num);
    }
    else
        return(0);
}

/* Note : depending on the application it might be advisable to define        */
/*           approximate equality for comparing double precision numbers        */
int e_eq(Number num1, Number num2, FWTaliroParam *p_par)
{
    if (p_par->ConOnSamples)
        return(num1.num.inf==num2.num.inf && num1.numi.i_num==num2.numi.i_num);
    else
        return(num1.num.inf==num2.num.inf && num1.numf.f_num==num2.numf.f_num);
}

int e_leq(Number num1, Number num2, FWTaliroParam *p_par)
{
    return(e_le(num1,num2,p_par) || e_eq(num1,num2,p_par));
}

int e_ge(Number num1, Number num2, FWTaliroParam *p_par)
{
    return(!e_leq(num1,num2,p_par));
}

int e_geq(Number num1, Number num2, FWTaliroParam *p_par)
{
    return(!e_le(num1,num2,p_par));
}

/**
* It is crucial to note that we keep a topological subspace as a union of
* their bounding boxes. We store them as a linked-list, and after any spatial
* operator on a topological subspace, we have to merge any two subspaces that can
* form a new bounding box. Therefore, if a fractured topological subspace is equial
* to the universe, then it always will be represented as a singular linked list
* that has the specification of the universe.
**/
void s_copyToFrom(SpatialRegion* dest, SpatialRegion* src) {
    if (dest == NULL_SR)
        mexErrMsgTxt("destination cannot be NULL_SR");
    if (dest == NULL_SR)
    {
        dest = createSpatialRegion(1);
    }
    if (src == NULL_SR) {
        mexErrMsgTxt("s_copyToFrom: source is NULL!");
    }
    if (src->is_NAN) {
        release_spatial_node_from_head(&dest);
        dest->is_NAN = true;
        return;
    }
    if (src->size < 1) {
        if (dest->head != NULL_SN) {
            release_spatial_node_from_head(&dest);
            //dest->size = 0;
        }
        dest->is_NAN = src->is_NAN;
        dest->size = src->size;
        return;
    }

    //TEST TEST TEST
    //dest->head = createRandomNode(1);
    //return;

    if(dest->head == NULL_SN)
        dest->head = createSpatialNode(1);
    
    SpatialNode* s_node = src->head;
    SpatialNode* d_node = dest->head;
    SpatialNode* p_node = dest->head;
    unsigned long int cnt = 0;

    //TEST TEST TEST !!!!!
    
    //release_spatial_node_from_head(&dest);//@todo: we do not need to do deep copy anymore, but double check
    //dest = src;
    //return;

#if 1
    while (s_node != NULL_SN && cnt < src->size) {

        if (d_node == NULL_SN) {

            p_node->next = createSpatialNode(1);
            d_node = p_node->next;


            /*if(p_node == NULL_SN)
                d_node = createSpatialNode(1);
            else {
                if (p_node->next == NULL_SN) {
                    p_node->next = createSpatialNode(1);
                    d_node = p_node->next;
                }
            }
            if (cnt < 1)
                dest->head = d_node;*/
            //else 
            //    p_node->next = d_node;
        }

        d_node->bbox[0] = s_node->bbox[0];
        d_node->bbox[1] = s_node->bbox[1];
        d_node->bbox[2] = s_node->bbox[2];
        d_node->bbox[3] = s_node->bbox[3];
        d_node->itv[0] = s_node->itv[0];
        d_node->itv[1] = s_node->itv[1];
        d_node->itv[2] = s_node->itv[2];
        d_node->itv[3] = s_node->itv[3];
        d_node->has_bbox = s_node->has_bbox;
        if (s_node->conv_set != NULL)
            copyConvSetToFrom(d_node->conv_set, s_node->conv_set);
        
        p_node = d_node;
        s_node = s_node->next;
        d_node = d_node->next;

        cnt++;
    }
    dest->size = cnt;
    dest->is_NAN = src->is_NAN;
    if(p_node != NULL_SN)
        p_node->next = NULL_SN;
    if(d_node != NULL_SN)
        release_spatial_node(&d_node);//remove the extra nodes if there exists any
#endif
}

void spatio_intersection(SpatialRegion* result, SpatialRegion* inp1, SpatialRegion* inp2) {
    if (result == NULL_SR)
        mexErrMsgTxt("result cannot be NULL_SR");
    if (inp1 == NULL_SR || inp2 == NULL_SR) {
        mexErrMsgTxt("spatio_intersection: null/empty param.");
    }
    
    if (inp1->head == NULL_SN || inp2->head == NULL_SN 
            || inp1->size < 1  || inp2->size < 1 ||
                inp1->is_NAN || inp2->is_NAN) {
        if (result != NULL_SR && result->head != NULL_SN) {
            release_spatial_node_from_head(&result);
            result->size = 0;
        }
        else if (result == NULL_SR) {
            result = createSpatialRegion(1);
        }
        result->is_NAN = inp1->is_NAN || inp2->is_NAN;
        return;
    }

    if ((!inp1->head->has_bbox && inp1->size > 0) || (!inp2->head->has_bbox && inp2->size > 0))
    {
        mexErrMsgTxt("spatio_intersection: Polygons except rectangles are not supported yet!");
    }
    
    if (result == NULL_SR) {
        mexErrMsgTxt("input cannot be NULL_SR");
        //result = createSpatialRegion(1);
    }
    if (result->head != NULL_SN) {
        //mexErrMsgTxt("spatio_intersection: given SpatialRegion must be empty!");
        release_spatial_node_from_head(&result);
    }

    //TEST TEST TEST
    //result->head = createRandomNode(1);
    //return;

    result->size = 0;// inp1->size* inp2->size;
    struct SpatialNode* node = result->head;
    struct SpatialNode* next;
    struct SpatialNode* n1 = inp1->head;
    struct SpatialNode* n2 = inp2->head;
    //note: bbox[0]=x_min, bbox[1]=x_max, bbox[2]=y_min, bbox[3]=y_max
    double min_x = -mxGetInf();
    double min_y = -mxGetInf();
    double max_x = -mxGetInf();
    double max_y = -mxGetInf();
    int in_size = 0;
    char itv[4];//open/close boundaries
    //bounding box intersection computing
    for (unsigned long int i1 = 0; i1 < inp1->size; i1++) {
        n2 = inp2->head;
        for (unsigned long int i2 = 0; i2 < inp2->size; i2++) {
            if (n1 == NULL_SN || n2 == NULL_SN)
                mexErrMsgTxt("spatio_intersection: wrong union set size!");
            min_x = -mxGetInf();
            min_y = -mxGetInf();
            max_x = -mxGetInf();
            max_y = -mxGetInf();

            //check x intersections
            if (n2->bbox[0] >= n1->bbox[0] && n2->bbox[0] <= n1->bbox[1]) {
                min_x = n2->bbox[0];
                itv[0] = n2->itv[0];
                if (n2->bbox[1] >= n1->bbox[0] && n2->bbox[1] <= n1->bbox[1]) {
                    max_x = n2->bbox[1];
                    itv[1] = n2->itv[1];
                }
                else if (n1->bbox[1] >= n2->bbox[0] && n1->bbox[1] <= n2->bbox[1]) {
                    max_x = n1->bbox[1];
                    itv[1] = n1->itv[1];
                }
            }
            else if (n1->bbox[0] >= n2->bbox[0] && n1->bbox[0] <= n2->bbox[1]) {
                min_x = n1->bbox[0];
                itv[0] = n1->itv[0];
                if (n1->bbox[1] >= n2->bbox[0] && n1->bbox[1] <= n2->bbox[1]) {
                    max_x = n1->bbox[1];
                    itv[1] = n1->itv[1];
                }
                else if (n2->bbox[1] >= n1->bbox[0] && n2->bbox[1] <= n1->bbox[1]) {
                    max_x = n2->bbox[1];
                    itv[1] = n2->itv[1];
                }
            }
            //check y intersections
            if (n2->bbox[2] >= n1->bbox[2] && n2->bbox[2] <= n1->bbox[3]) {
                min_y = n2->bbox[2];
                itv[2] = n2->itv[2];
                if (n2->bbox[3] >= n1->bbox[2] && n2->bbox[3] <= n1->bbox[3]) {
                    max_y = n2->bbox[3];
                    itv[3] = n2->itv[3];
                }
                else if (n1->bbox[3] >= n2->bbox[2] && n1->bbox[3] <= n2->bbox[3]) {
                    max_y = n1->bbox[3];
                    itv[3] = n1->itv[3];
                }
            }
            else if (n1->bbox[2] >= n2->bbox[2] && n1->bbox[2] <= n2->bbox[3]) {
                min_y = n1->bbox[2];
                itv[2] = n1->itv[2];
                if (n1->bbox[3] >= n2->bbox[2] && n1->bbox[3] <= n2->bbox[3]) {
                    max_y = n1->bbox[3];
                    itv[3] = n1->itv[3];
                }
                else if (n2->bbox[3] >= n1->bbox[2] && n2->bbox[3] <= n1->bbox[3]) {
                    max_y = n2->bbox[3];
                    itv[3] = n2->itv[3];
                }
            }
            //check if the intersection is an empty set
            if (min_x >= 0 && min_y >= 0 && max_x >= 0 && max_y >= 0 ) {
                //check if the intersection is an empty set
                if ( (min_x == max_x && (itv[0] == 0 || itv[1] == 0))
                    ||
                    (min_y == max_y && (itv[2] == 0 || itv[3] == 0))
                    ) 
                {//this intersection is empty, so ignore it
                    //left empty purposefully
                }
                else {
                    if (in_size < 1) {
                        result->head = createSpatialNode(1);
                        node = result->head;
                    }
                    else {
                        next = createSpatialNode(1);
                        node->next = next;
                        node = next;
                    }
                    node->bbox[0] = min_x;
                    node->bbox[1] = max_x;
                    node->bbox[2] = min_y;
                    node->bbox[3] = max_y;
                    node->itv[0] = itv[0];
                    node->itv[1] = itv[1];
                    node->itv[2] = itv[2];
                    node->itv[3] = itv[3];
                    node->has_bbox = true;
                    in_size++;
                }
            }
            n2 = n2->next;
        }
        n1 = n1->next;
    }
    result->size = in_size;
    //@test: 07/02/2020
    result->is_NAN = false;

    if(result->size > 1)
        *result = *compressSpatialReion(result, true);

}

void spatio_union(SpatialRegion* result, SpatialRegion* inp1, SpatialRegion* inp2) {
    if (result == NULL_SR)
        mexErrMsgTxt("result cannot be NULL_SR");
    if (result == inp1 || result == inp2)
        mexErrMsgTxt("spatio_union: result pointer cannot be the same as a parameter.");
    if (inp1 == NULL_SR || inp2 == NULL_SR) {
        mexErrMsgTxt("spatio_union: null/empty param.");
    }
    if (inp1->head == NULL_SN && inp2->head == NULL_SN) {
        if (result == NULL_SR) {
            result = createSpatialRegion(1);
            result->size = 0;
        }
        else if(result->head != NULL_SN)
            release_spatial_node_from_head(&result);
        //result->head = NULL_SN;
        //result->size = 0;
        if (inp1->is_NAN || inp2->is_NAN)
            result->is_NAN = true;
        result->size = 0;
        return;
    }
    else if (inp1->head == NULL_SN) {
        s_copyToFrom(result, inp2);
        if (inp1->is_NAN || inp2->is_NAN)
            result->is_NAN = true;
        return;
    }
    else if (inp2->head == NULL_SN) {
        s_copyToFrom(result, inp1);
        if (inp1->is_NAN || inp2->is_NAN)
            result->is_NAN = true;
        return;
    }
    if (!inp1->head->has_bbox || !inp2->head->has_bbox)
    {
        mexErrMsgTxt("spatio_union: Polygons except rectangles are not supported yet!");
    }
    if(result == NULL_SR)
        result = createSpatialRegion(1);;
    if (result->head != NULL_SN) {
        //mexErrMsgTxt("spatio_union: given SpatialRegion must be empty!");
        //release_spatial_region(result);
        release_spatial_node_from_head(&result);
        //result->head = NULL_SN;
    }
    if (inp1->is_NAN || inp2->is_NAN) {
        result->is_NAN = true;
        
        return;
    }
    
    //TEST TEST TEST
    //result->head = createRandomNode(1);
    //return;

    result->size = inp1->size + inp2->size;
    result->head = createSpatialNode(1);
    struct SpatialNode* node = result->head;
    struct SpatialNode* next = NULL_SN;
    struct SpatialNode* n1 = inp1->head;
    struct SpatialNode* n2 = inp2->head;
    SpatialRegion merge;
    merge.head = NULL_SN;
    struct SpatialNode* m = merge.head;
    merge.size = 0;
    //note: bbox[0]=x_min, bbox[1]=x_max, bbox[2]=y_min, bbox[3]=y_max
    int un_size = 0;

    //this helps to gain some performance 
    static char static_cn1[MAX_LINKED_LIST_SIZE];//clone of n1 inclusive indexes
    static char static_cn2[MAX_LINKED_LIST_SIZE];//clone of n2 inclusive indexes

    char* cn1 = NULL;//clone of n1 inclusive indexes
    char* cn2 = NULL;//clone of n2 inclusive indexes
    bool free_cn1 = false;
    bool free_cn2 = false;
    if (inp1->size > MAX_LINKED_LIST_SIZE) {
        cn1 = (char*)emalloc(inp1->size * sizeof(char));
        free_cn1 = true;
    }
    else
        cn1 = static_cn1;
    if (inp2->size > MAX_LINKED_LIST_SIZE) {
        cn2 = (char*)emalloc(inp2->size * sizeof(char));
        free_cn2 = true;
    }
    else
        cn2 = static_cn2;
    //if (inp1->size > 0)
    //    cn1 = (char*)emalloc(inp1->size * sizeof(char));
    //if (inp2->size > 0)
    //    cn2 = (char*)emalloc(inp2->size * sizeof(char));
    for (unsigned long int i2 = 0; i2 < inp2->size; i2++)
        cn2[i2] = 1;
    for (unsigned long int i1 = 0; i1 < inp1->size; i1++)
        cn1[i1] = 1;
    //bounding box intersection computing
    for (unsigned long int i1 = 0; i1 < inp1->size; i1++) {
        cn1[i1] = 1;
        n2 = inp2->head;
        for (unsigned long int i2 = 0; i2 < inp2->size; i2++) {
            if (cn2[i2] > 0) {
                if (n1 == NULL_SN || n2 == NULL_SN)
                    mexErrMsgTxt("spatio_union: wrong union set size!");

                //check if n2 is subset of n1
                if (n2->bbox[0] >= n1->bbox[0] && n2->bbox[0] <= n1->bbox[1]
                    && n2->bbox[1] >= n1->bbox[0] && n2->bbox[1] <= n1->bbox[1]) {

                    if (n2->bbox[2] >= n1->bbox[2] && n2->bbox[2] <= n1->bbox[3]
                        && n2->bbox[3] >= n1->bbox[2] && n2->bbox[3] <= n1->bbox[3]) {

                        cn2[i2] = 0;//ignore n2

                        //check if the bounding box sets are the same or 
                        //maybe different on their boundaries
                        if (n1->bbox[0] == n2->bbox[0] && n1->bbox[1] == n2->bbox[1] &&
                            n1->bbox[2] == n2->bbox[2] && n1->bbox[3] == n2->bbox[3])
                            if (n2->itv[0] != n1->itv[0] || n2->itv[1] != n1->itv[1] ||
                                n2->itv[2] != n1->itv[2] || n2->itv[3] != n1->itv[3])
                            {
                                if (merge.size == 0) {
                                    merge.head = createSpatialNode(1);
                                    m = merge.head;
                                }
                                else {
                                    next = createSpatialNode(1);
                                    m->next = next;
                                    m = next;
                                }
                                m->bbox[0] = n1->bbox[0];
                                m->bbox[1] = n1->bbox[1];
                                m->bbox[2] = n1->bbox[2];
                                m->bbox[3] = n1->bbox[3];
                                //choose close boundaries if possible
                                m->itv[0] = n1->itv[0] > n2->itv[0] ? n1->itv[0] : n2->itv[0];
                                m->itv[1] = n1->itv[1] > n2->itv[1] ? n1->itv[1] : n2->itv[1];
                                m->itv[2] = n1->itv[2] > n2->itv[2] ? n1->itv[2] : n2->itv[2];
                                m->itv[3] = n1->itv[3] > n2->itv[3] ? n1->itv[3] : n2->itv[3];
                                m->has_bbox = true;
                                merge.size++;
                                cn1[i1] = 0;//ignore n1 and break
                                break;
                            }

                    }
                }
                //check if n1 is subset of n2
                else if (n1->bbox[0] >= n2->bbox[0] && n1->bbox[0] <= n2->bbox[1]
                    && n1->bbox[1] >= n2->bbox[0] && n1->bbox[1] <= n2->bbox[1]) {

                    if (n1->bbox[2] >= n2->bbox[2] && n1->bbox[2] <= n2->bbox[3]
                        && n1->bbox[3] >= n2->bbox[2] && n1->bbox[3] <= n2->bbox[3]) {

                        cn1[i1] = 0;//ignore n1
                        break;//skip the rest of n2 for the current n1
                    }
                }
                else {
                    bool n1belown2 = false;
                    bool n2belown1 = false;
                    bool n1leftn2 = false;
                    bool n2leftn1 = false;
                    if (n1->bbox[0] == n2->bbox[0] && n1->bbox[1] == n2->bbox[1]) {
                        if (n1->bbox[2] >= n2->bbox[2] && n1->bbox[2] <= n2->bbox[3] && (n1->itv[2] == 1 || n2->itv[3] == 1))
                            n1belown2 = true;
                        else if (n2->bbox[2] >= n1->bbox[2] && n2->bbox[2] <= n1->bbox[3] && (n1->itv[3] == 1 || n2->itv[2] == 1))
                            n2belown1 = true;
                    }
                    else if (n1->bbox[2] == n2->bbox[2] && n1->bbox[3] == n2->bbox[3]) {
                        if (n2->bbox[1] >= n1->bbox[0] && n2->bbox[1] <= n1->bbox[1] && (n1->itv[0] == 1 || n2->itv[1] == 1))
                            n2leftn1 = true;
                        else if (n1->bbox[1] >= n2->bbox[0] && n1->bbox[1] <= n2->bbox[1] && (n1->itv[1] == 1 || n2->itv[0] == 1))
                            n1leftn2 = true;
                    }
                    if (n1belown2 || n1leftn2 || n2belown1 || n2leftn1) {
                        if (merge.size == 0) {
                            merge.head = createSpatialNode(1);
                            m = merge.head;
                        }
                        else {
                            next = createSpatialNode(1);
                            m->next = next;
                            m = next;
                        }
                        if (n1belown2) {
                            m->bbox[0] = n1->bbox[0];
                            m->bbox[1] = n1->bbox[1];
                            m->bbox[2] = n2->bbox[2];
                            m->bbox[3] = n1->bbox[3];
                            //choose close boundaries if possible
                            m->itv[0] = n1->itv[0];
                            m->itv[1] = n1->itv[1];
                            m->itv[2] = n2->itv[2];
                            m->itv[3] = n1->itv[3];
                        }
                        else if (n2belown1) {
                            m->bbox[0] = n1->bbox[0];
                            m->bbox[1] = n1->bbox[1];
                            m->bbox[2] = n1->bbox[2];
                            m->bbox[3] = n2->bbox[3];
                            //choose close boundaries if possible
                            m->itv[0] = n1->itv[0];
                            m->itv[1] = n1->itv[1];
                            m->itv[2] = n1->itv[2];
                            m->itv[3] = n2->itv[3];
                        }
                        else if (n1leftn2) {
                            m->bbox[0] = n1->bbox[0];
                            m->bbox[1] = n2->bbox[1];
                            m->bbox[2] = n1->bbox[2];
                            m->bbox[3] = n1->bbox[3];
                            //choose close boundaries if possible
                            m->itv[0] = n1->itv[0];
                            m->itv[1] = n2->itv[1];
                            m->itv[2] = n1->itv[2];
                            m->itv[3] = n1->itv[3];
                        }
                        else if (n2leftn1) {
                            m->bbox[0] = n2->bbox[0];
                            m->bbox[1] = n1->bbox[1];
                            m->bbox[2] = n1->bbox[2];
                            m->bbox[3] = n1->bbox[3];
                            //choose close boundaries if possible
                            m->itv[0] = n2->itv[0];
                            m->itv[1] = n1->itv[1];
                            m->itv[2] = n1->itv[2];
                            m->itv[3] = n1->itv[3];
                        }
                        m->has_bbox = true;
                        merge.size++;
                        cn1[i1] = 0;//ignore n1 and break
                        cn2[i2] = 0;//ignore n2 and break
                        //if (i1 == (inp1->size - 1)) 
                        break;
                    }
                }
                n2 = n2->next;
            }
        }
        n1 = n1->next;
    }

    n1 = inp1->head;
    n2 = inp2->head;
    m = merge.head;
    bool head_passed = false;
    //copy from the first union
    for (unsigned long int i1 = 0; i1 < inp1->size; i1++) {
        if (cn1[i1] > 0) {
            //if (i1 > 0) {
            if (head_passed) {
                next = createSpatialNode(1);
                node->next = next;
                node = node->next;
                /*next = createSpatialNode(1);
                node->next = next;
                node = next;
                next = next->next;*/
            }
            else
                head_passed = true;
            node->bbox[0] = n1->bbox[0];
            node->bbox[1] = n1->bbox[1];
            node->bbox[2] = n1->bbox[2];
            node->bbox[3] = n1->bbox[3];
            node->itv[0] = n1->itv[0];
            node->itv[1] = n1->itv[1];
            node->itv[2] = n1->itv[2];
            node->itv[3] = n1->itv[3];
            node->has_bbox = true;
            if (n1->conv_set != NULL)
                copyConvSetToFrom(node->conv_set, n1->conv_set);
            un_size++;
        }
        n1 = n1->next;
    }
    //copy from the second union
    for (unsigned long int i2 = 0; i2 < inp2->size; i2++) {
        if (cn2[i2] > 0) {
            //if (i2 > 0) {
            if (head_passed) {
                next = createSpatialNode(1);
                node->next = next;
                node = node->next;
                /*next = createSpatialNode(1);
                node->next = next;
                node = next;
                next = next->next;*/
            }
            else
                head_passed = true;
            node->bbox[0] = n2->bbox[0];
            node->bbox[1] = n2->bbox[1];
            node->bbox[2] = n2->bbox[2];
            node->bbox[3] = n2->bbox[3];
            node->itv[0] = n2->itv[0];
            node->itv[1] = n2->itv[1];
            node->itv[2] = n2->itv[2];
            node->itv[3] = n2->itv[3];
            node->has_bbox = true;
            if (n2->conv_set != NULL)
                copyConvSetToFrom(node->conv_set, n2->conv_set);
            un_size++;
        }
        n2 = n2->next;
    }
#if 0    
    //copy from the merged union
    for (int i3 = 0; i3 < merge.size; i3++) {
        //if (i3 > 0) {
        if (head_passed) {
            next = createSpatialNode(1);
            node->next = next;
            node = next;
            next = next->next;
        }
        else
            head_passed = true;
        node->bbox[0] = m->bbox[0];
        node->bbox[1] = m->bbox[1];
        node->bbox[2] = m->bbox[2];
        node->bbox[3] = m->bbox[3];
        node->itv[0] = m->itv[0];
        node->itv[1] = m->itv[1];
        node->itv[2] = m->itv[2];
        node->itv[3] = m->itv[3];
        node->has_bbox = true;
        if (m->conv_set != NULL)
            copyConvSetToFrom(node->conv_set, n2->conv_set);
        un_size++;
        m = m->next;
    }
#endif    

    //copy from the merged union
    if (node->has_bbox) {
        if (node->next != NULL_SN)
            release_spatial_node(&node->next);
        node->next = m;
    }
    else {
        //if (m == NULL_SN || node != NULL_SN)
        if (m == NULL_SN)
            mexErrMsgTxt("spatio_union: error 1");
        release_spatial_node(&node);
        result->head = m;
        //*node = *m;
        //m->next = NULL_SN;
        //release_spatial_node(&m);
    }

    un_size += merge.size;

    merge.head = NULL_SN;

    if (un_size == 0) {
        release_spatial_node_from_head(&result);
        //result->head = NULL_SN;
    }
    result->size = un_size;
    //if (inp1->size > 0)
    if (free_cn1)
        efree(cn1);
    //if (inp2->size > 0)
    if (free_cn2)
        efree(cn2);
    //@test: 07/02/2020
    result->is_NAN = false;
    *result = *compressSpatialReion(result, true);
    /*SpatialRegion* cp = compressSpatialReion(result, true);
    s_copyToFrom(result, cp);
    release_spatial_region(cp);*/
}

void spatio_complement(SpatialRegion* result, SpatialRegion* inp1) {
    if (result == NULL_SR)
        mexErrMsgTxt("result cannot be NULL_SR");
    if (inp1 == NULL_SR) {
        mexErrMsgTxt("spatio_complement: null/empty param.");
    }
    if (result == NULL_SR) {
        mexErrMsgTxt("input cannot be NULL_SR");
        //result = createSpatialRegion(1);;
    }
    if (result->head != NULL_SN) {
        //mexErrMsgTxt("spatio_complement: given SpatialRegion must be empty!");
        //release_spatial_region(result);
        release_spatial_node_from_head(&result);
        result->head = NULL_SN;
    }

    if (inp1->is_NAN) {
        result->is_NAN = true;
        result->size = 0;
        
        return;
    }

    result->is_NAN = false;


    //TEST TEST TEST
    //result->head = createRandomNode(1);
    //return;

    //result->head = createSpatialNode(1);
    SpatialNode* U;
    SpatialNode* D;
    SpatialNode* L;
    SpatialNode* R;
    SpatialNode* in_node = inp1->head;
    SpatialNode* node = NULL_SN;
    double ux1 = _UNIVERSE[0];
    double ux2 = _UNIVERSE[1];
    double uy1 = _UNIVERSE[2];
    double uy2 = _UNIVERSE[3];
    result->size = 0;
    int cmt_size = 0;
    if (in_node == NULL_SN) {//an empty region
        result->size = 1;
        SpatialNode* node = createSpatialNode(1);
        node->next = NULL_SN;
        node->bbox[0] = ux1;
        node->itv[0] = 1;
        node->bbox[1] = ux2;
        node->itv[1] = 1;
        node->bbox[2] = uy1;
        node->itv[2] = 1;
        node->bbox[3] = uy2;
        node->itv[3] = 1;
        node->has_bbox = true;
        result->head = node;
        return;
    }
    else if (inp1->size == 1 && in_node->bbox[0] == ux1 && in_node->bbox[1] == ux2
        && in_node->bbox[2] == uy1 && in_node->bbox[3] == uy2) {//universe
        return;//empty region
    }
    unsigned long int cnt = 0;
    SpatialRegion* comp = createSpatialRegion(1);
    SpatialRegion* clone = NULL_SR;// = createSpatialRegion(1);
    comp->size = 0;
    while (in_node != NULL_SN && cnt < inp1->size)
    {
        if (in_node->bbox[0] > ux2 || in_node->bbox[1] > ux2 ||
            in_node->bbox[2] > uy2 || in_node->bbox[3] > uy2) {
            SetTo_Universe_or_Emptyset(result, 1);
            mexPrintf("The current set universe is:\n");
            print_spatial_region(result);
            mexErrMsgTxt("spatio_complement: Spatial universe must subsume any given region!");
        }
        if (in_node->bbox[2] > uy1 || (in_node->bbox[2] == uy1 && in_node->itv[2] == 0))
        {
            //UP fragment
            U = createSpatialNode(1);
            U->bbox[0] = ux1;
            U->itv[0] = 1;
            U->bbox[1] = ux2;
            U->itv[1] = 1;
            U->bbox[2] = uy1;
            U->itv[2] = 1;
            U->bbox[3] = in_node->bbox[2];
            U->itv[3] = in_node->itv[2] > 0 ? 0 : 1;
            U->has_bbox = true;
            cmt_size++;
            if (node == NULL_SN) {
                //*result->head = *U;
                comp->head = U;
                node = U;
            }
            else {
                node->next = U;
                node = U;
            }
        }
        if (in_node->bbox[3] < uy2 || (in_node->bbox[3] == uy2 && in_node->itv[3] == 0))
        {
            //DOWN fragment
            D = createSpatialNode(1);
            D->bbox[0] = ux1;
            D->itv[0] = 1;
            D->bbox[1] = ux2;
            D->itv[1] = 1;
            D->bbox[2] = in_node->bbox[3];
            D->itv[2] = in_node->itv[3] > 0 ? 0 : 1;
            D->bbox[3] = uy2;
            D->itv[3] = 1;
            D->has_bbox = true;
            cmt_size++;
            if (node == NULL_SN) {
                //*result->head = *D;
                comp->head = D;
                node = D;
            }
            else {
                node->next = D;
                node = D;
            }
        }
        if (in_node->bbox[0] > ux1 || (in_node->bbox[0] == ux1 && in_node->itv[0] == 0))
        {
            //LEFT fragment
            L = createSpatialNode(1);
            L->bbox[0] = ux1;
            L->itv[0] = 1;
            L->bbox[1] = in_node->bbox[0];
            L->itv[1] = in_node->itv[0] > 0 ? 0 : 1;
            L->bbox[2] = in_node->bbox[2];
            L->itv[2] = in_node->itv[2];
            L->bbox[3] = in_node->bbox[3];
            L->itv[3] = in_node->itv[3];
            L->has_bbox = true;
            cmt_size++;
            if (node == NULL_SN) {
                //*result->head = *L;
                comp->head = L;
                node = L;
            }
            else {
                node->next = L;
                node = L;
            }
        }
        if (in_node->bbox[1] < ux2 || (in_node->bbox[1] == ux2 && in_node->itv[1] == 0))
        {
            //RIGHT fragment
            R = createSpatialNode(1);
            R->bbox[0] = in_node->bbox[1];
            R->itv[0] = in_node->itv[1] > 0 ? 0 : 1;
            R->bbox[1] = ux2;
            R->itv[1] = 1;
            R->bbox[2] = in_node->bbox[2];
            R->itv[2] = in_node->itv[2];
            R->bbox[3] = in_node->bbox[3];
            R->itv[3] = in_node->itv[3];
            R->has_bbox = true;
            cmt_size++;
            if (node == NULL_SN) {
                //*result->head = *R;
                comp->head = R;
                node = R;
            }
            else {
                node->next = R;
                node = R;
            }
        }
        //if (cmt_size == 0)
        //    mexErrMsgTxt("spatio_complement: error 1");
        comp->size = cmt_size;
        if (node == NULL_SN)
            mexErrMsgTxt("spatio_complement: error 1");
        node->next = NULL_SN;
        //check for at least two regions to compute their intersecting complements
        if (inp1->size > 1) {
            if (cnt > 0) {
                clone = createSpatialRegion(1);
                spatio_intersection(clone, comp, result);
                s_copyToFrom(result, clone);
                //*result = *clone;
                clone->head = NULL_SN;
                //release_spatial_node_from_head(&clone);
                release_spatial_region(&clone);
            }
            else {
                s_copyToFrom(result, comp);
                //*result = *comp;
                comp->head = NULL_SN;
                //clone = NULL_SR;
                //release_spatial_node_from_head(&clone);
            }
        }
        else {//single node complementation done
            s_copyToFrom(result, comp);
            //note: shallow copying causes too many erased node in the end 
            //memcpy(result, comp, sizeof(*result));
            //*result = *comp;
            comp->head = NULL_SN;
            release_spatial_region(&comp);
            //release_spatial_region(&clone);
            return;
        }

        //release_spatial_node_from_head(&comp);
        
        //release_spatial_node_from_head(&clone);
        node = NULL_SN;
        cmt_size = 0;
        in_node = in_node->next;
        cnt++;
    }
    //clone->head = NULL_SN;
    comp->head = NULL_SN;
    //release_spatial_region(&clone);
    release_spatial_region(&comp);

}

void spatio_interior(SpatialRegion* result, SpatialRegion* inp1) {
    if (inp1 == NULL_SR) {
        mexErrMsgTxt("spatio_interior: null/empty param.");
    }
    if (result == NULL_SR) {
        mexErrMsgTxt("input cannot be NULL_SR");
        //result = createSpatialRegion(1);
    }
    if (result->head != NULL_SN) {
        release_spatial_node_from_head(&result);
        result->head = NULL_SN;
    }

    if (inp1->is_NAN) {
        result->is_NAN = true;
        result->size = 0;
        
        return;
    }

    s_copyToFrom(result, inp1);

    /**
    * It is crucial to note that we keep a topological subspace as a union of
    * their bounding boxes. We store them as a linked-list, and after any spatial
    * operator on a topological subspace, we have to merge any two subspaces that can
    * form a new bounding box. Therefore, if a fractured topological subspace is equial 
    * to the universe, then it always will be represented as a singular linked list
    * that has the specification of the universe.
    * This is essential for the interior operator as well, because the I(Universe)==Universe.
    **/
    if (isSpatialUniverse(inp1))//interior of the universe is the universe
        return;

    SpatialNode* node = result->head;

    for (unsigned long int i1 = 0; i1 < result->size; i1++) {

        if (node == NULL_SN)//sanity check: can be commented
            mexErrMsgTxt("spatio_interior: null/empty node.");
        node->itv[0] = 0;
        node->itv[1] = 0;
        node->itv[2] = 0;
        node->itv[3] = 0;
        node = node->next;
    }
}

void spatio_closure(SpatialRegion* result, SpatialRegion* inp1) {
    if (inp1 == NULL_SR) {
        mexErrMsgTxt("spatio_interior: null/empty param.");
    }
    if (result == NULL_SR) {
        mexErrMsgTxt("input cannot be NULL_SR");
        //result = createSpatialRegion(1);
    }
    if (result->head != NULL_SN) {
        release_spatial_node_from_head(&result);
        result->head = NULL_SN;
    }

    if (inp1->is_NAN) {
        result->is_NAN = true;
        result->size = 0;
        
        return;
    }

    s_copyToFrom(result, inp1);
    SpatialNode* node = result->head;

    for (unsigned long int i1 = 0; i1 < result->size; i1++) {

        if (node == NULL_SN)//sanity check: can be commented
            mexErrMsgTxt("spatio_interior: null/empty node.");
        node->itv[0] = 1;
        node->itv[1] = 1;
        node->itv[2] = 1;
        node->itv[3] = 1;
        node = node->next;
    }
}

void SetTo_Universe_or_Emptyset(SpatialRegion* temp, int sign) {
    if (temp == NULL_SR)
        mexErrMsgTxt("SetTo_Universe_or_Emptyset: null input!");
    if (temp->size > 0 || temp->head != NULL_SN) {
        release_spatial_node_from_head(&temp);
        temp->head = NULL_SN;
    }
        //release_spatial_region(temp);
    if (sign > 0) {//set to UNIVERSE
        temp->size = 1;
        temp->head = createSpatialNode(1);
        temp->head->bbox[0] = _UNIVERSE[0];
        temp->head->bbox[1] = _UNIVERSE[1];
        temp->head->bbox[2] = _UNIVERSE[2];
        temp->head->bbox[3] = _UNIVERSE[3];
        temp->head->itv[0] = 1;
        temp->head->itv[1] = 1;
        temp->head->itv[2] = 1;
        temp->head->itv[3] = 1;
        temp->head->next = NULL_SN;
        temp->head->conv_set = NULL;
        temp->head->has_bbox = true;
    }
    else {//set to EMPTY set
        temp->size = 0;
    }
}

unsigned long int release_spatial_region(SpatialRegion** reg) {
    unsigned long int size = 0;
    unsigned long int total_size = 0;
    sr_mem_rel_cnt++;
    
    if (!should_release_memory) {
        if (*reg == NULL_SR)
            return 0;
        
        if (garbage_collector.sr_size == 0) {
            garbage_collector.sr_head = (*reg);
            garbage_collector.sr_end = (*reg);// garbage_collector.sr_head;
            total_size = release_spatial_node_from_head(reg);//+= is new
            size = 1;
        }
        else {
            garbage_collector.sr_end->next = (*reg);
            SpatialRegion* next;
            if (garbage_collector.sr_end == NULL_SR)
                mexErrMsgTxt("release_spatial_region: error 1");
            while (garbage_collector.sr_end->next != NULL_SR) {
                next = garbage_collector.sr_end->next;
                garbage_collector.sr_end = garbage_collector.sr_end->next;
                total_size += release_spatial_node_from_head(&next);
                size++;
                if (size > 1000000)
                    mexErrMsgTxt("Error in release_spatial_region: remove the guard");
            }
        }

        garbage_collector.sr_size += size;
        *reg = NULL_SR;
        sr_mem_rel_new += total_size;
        return total_size;
    }

    if (*reg == NULL_SR)
        return 0;
    size = release_spatial_node(&(*reg)->head);
    hfree(*reg);
    *reg = NULL_SR;
    sr_mem_rel_new += size+1;
    return size + 1;
}

unsigned long int release_spatial_region_linkedlist(SpatialRegion** inp1) {

    if (*inp1 == NULL_SR)
        return 0;

    SpatialRegion** start = inp1;
    SpatialRegion* next = *inp1;
    unsigned long int size = 0;

    while (*inp1 != NULL_SR) {
        next = (*inp1)->next;
        if ((*inp1)->size > 0) {
            mexErrMsgTxt("release_spatial_region_linkedlist: SpatialRegion supposed to be empty!");
            //release_spatial_node(&(*inp1)->head);
        }
        (*inp1)->next = NULL_SR;
        hfree(*inp1);
        *inp1 = next;
        size++;
    }
    *start = NULL_SR;
    sr_mem_rel_new += size;
    return size;
}

unsigned long int release_spatial_node(SpatialNode** inp1) {
    
    sn_mem_rel_cnt++;

    unsigned long int size = 0;

    
    if (!should_release_memory) {
        if (*inp1 == NULL_SN)
            return 0;
        if (garbage_collector.sn_size == 0) {
            garbage_collector.sn_head = (*inp1);
            garbage_collector.sn_end = garbage_collector.sn_head;
            size = 1;
        }
        else {
            garbage_collector.sn_end->next = (*inp1);
            if (garbage_collector.sn_end == NULL_SN)
                mexErrMsgTxt("release_spatial_node: error 1");
            while (garbage_collector.sn_end->next != NULL_SN) {
                garbage_collector.sn_end = garbage_collector.sn_end->next;
                size++;
                if (size > 1000000)
                    mexErrMsgTxt("Error in release_spatial_node: remove the guard");
            }
        }

        garbage_collector.sn_size += size;
        *inp1 = NULL_SN;
        sn_mem_rel_new += size;
        return size;
    }

    if (*inp1 == NULL_SN)
        return 0;
    SpatialNode** start = inp1;
    SpatialNode* next = *inp1;

    while (*inp1 != NULL_SN) {
        next = (*inp1)->next;
        if ((*inp1)->conv_set != NULL)
            release_Conv_set((*inp1)->conv_set);
        (*inp1)->next = NULL_SN;
        hfree(*inp1);
        *inp1 = next;
        size++;
    }
    *start = NULL_SN;
    sn_mem_rel_new += size;
    return size;
}

unsigned long int release_spatial_node_from_head(SpatialRegion** reg) {

    if (*reg == NULL_SR)
        return 0;
    if (!should_release_memory) {
        if ((*reg)->head == NULL_SN)
            return 0;
        unsigned long int size = 0;
        if (garbage_collector.sn_size == 0) {
            garbage_collector.sn_head = (*reg)->head;
            garbage_collector.sn_end = (*reg)->head;//garbage_collector.sn_head;
            size = 1;
        }
        else
            garbage_collector.sn_end->next = (*reg)->head;

        if (garbage_collector.sn_end == NULL_SN)
            mexErrMsgTxt("release_spatial_node_from_head: error 1");
        while (garbage_collector.sn_end->next != NULL_SN) {
            size++;
            garbage_collector.sn_end = garbage_collector.sn_end->next;
        }

        garbage_collector.sn_size += size;
        (*reg)->next = NULL_SR;
        (*reg)->head = NULL_SN;
        (*reg)->size = 0;
        return size;
    }

    if (*reg == NULL_SR)
        return 0;
    return release_spatial_node(&((*reg)->head));
}

/*void release_spatial_node_recursive(SpatialNode* inp1) {

    if (inp1 == NULL_SN)
        return;
    release_spatial_node(&inp1->next);
    hfree(inp1);

}*/

void copyConvSetToFrom(ConvSet* dst, ConvSet* src) {
    mexErrMsgTxt("copyConvSetToFrom: Not supported yet!");
}

void release_Conv_set(ConvSet* tmp) {
    mexErrMsgTxt("release_Conv_set: Not supported yet!");
}

bool equalConvSet(ConvSet* inp1, ConvSet* inp2) {
    if (inp1 == NULL && inp2 == NULL)
        return true;
    mexErrMsgTxt("equalConvSet: Not supported yet!");
    return false;
}

SpatialRegion* compressSpatialReion(SpatialRegion* inp1, bool releaseNodes) {

    SpatialRegion* merge = createSpatialRegion(1);
    if(inp1 == NULL_SR || inp1->size <= 1 || inp1->head == NULL_SN)
        if (releaseNodes)
        {
            s_copyToFrom(merge, inp1);
            release_spatial_node_from_head(&inp1);
            
            ////inp1->head = NULL_SN;
            //release_spatial_region(&inp1);
            return merge;
        }
        else
            return inp1;
    s_copyToFrom(merge, inp1);
    SpatialNode* n1 = merge->head;
    if (merge->head == NULL_SN)
        mexErrMsgTxt("Error in compressSpatialReion");
    SpatialNode* n2 = merge->head->next;
    SpatialNode* first_merged = merge->head;
    SpatialNode* m = merge->head;
    unsigned long int merge_size = 0;
    bool changed = true;
    
    
    //this helps to gain some performance 
    static char static_cn1[MAX_LINKED_LIST_SIZE];//clone of n1 inclusive indexes
    static char static_cn2[MAX_LINKED_LIST_SIZE];//clone of n2 inclusive indexes

    char* cn1 = NULL;//clone of n1 inclusive indexes
    char* cn2 = NULL;//clone of n2 inclusive indexes
    bool free_cn1 = false;
    bool free_cn2 = false;
    if (inp1->size > MAX_LINKED_LIST_SIZE) {
        cn1 = (char*)emalloc(inp1->size * sizeof(char));
        cn2 = (char*)emalloc(inp1->size * sizeof(char));
        free_cn1 = true;
        free_cn2 = true;
    }
    else {
        cn1 = static_cn1;
        cn2 = static_cn2;
    }
    memset(cn1, 1, inp1->size);
    memset(cn2, 1, inp1->size);

    /*cn1 = (char*)emalloc(inp1->size * sizeof(char));
    cn2 = (char*)emalloc(inp1->size * sizeof(char));
    for (int i2 = 0; i2 < inp1->size; i2++)
        cn2[i2] = 1;
    for (int i1 = 0; i1 < inp1->size; i1++)
        cn1[i1] = 1;*/

    int n1cnt = 0;//first node
    int n2cnt = 1;//second node
    int first_cnt = 0;
    int safety_counter = 0;
    //maximum number of possible comparison between regions of a set of size n:
    //(n^3+n^2-2n)/2
    int SAFETY_COUNT = (int)(0.5*(pow(inp1->size,3) + pow(inp1->size,2) - 2 * inp1->size));
    bool show_warning = DEBUG_MODE & VERBOSE_MODE;
    while (changed) {
        changed = false;
        while (n1 != NULL_SN) {
            first_merged = n1;
            first_cnt = n1cnt;
            while (n2 != NULL_SN){
                if (cn2[n2cnt] < 1) {
                    n2 = n2->next;
                    n2cnt++;
                    continue;
                }
                bool n1belown2 = false;
                bool n2belown1 = false;
                bool n1leftn2 = false;
                bool n2leftn1 = false;
                bool n1subn2 = false;
                bool n2subn1 = false;
                safety_counter++;

                if (safety_counter > 0.2*SAFETY_COUNT && show_warning && SAFETY_COUNT > 1000) {
                    mexPrintf("\n[NOTE]: Defragmenting %d union of regions for maximum size of %d ...\n",inp1->size, SAFETY_COUNT);
                    mexPrintf("\ncompressSpatialReio: This process is reducing the performance, consider less defragmented regions.\n\n");
                    show_warning = false;
                }
                //note: the order of checking does matter:
                //1: check inclusion
                //2:check partial side-based intersection
                //check if n2 is subset of n1
                if (n2->bbox[0] >= n1->bbox[0] && n2->bbox[0] <= n1->bbox[1]
                    && n2->bbox[1] >= n1->bbox[0] && n2->bbox[1] <= n1->bbox[1]) {

                    if (n2->bbox[2] >= n1->bbox[2] && n2->bbox[2] <= n1->bbox[3]
                        && n2->bbox[3] >= n1->bbox[2] && n2->bbox[3] <= n1->bbox[3]) {
                        n2subn1 = true;
                    }
                }
                //check if n1 is subset of n2
                if(!n2subn1)
                    if (n1->bbox[0] >= n2->bbox[0] && n1->bbox[0] <= n2->bbox[1]
                        && n1->bbox[1] >= n2->bbox[0] && n1->bbox[1] <= n2->bbox[1]) {

                        if (n1->bbox[2] >= n2->bbox[2] && n1->bbox[2] <= n2->bbox[3]
                            && n1->bbox[3] >= n2->bbox[2] && n1->bbox[3] <= n2->bbox[3]) {
                            n1subn2 = true;
                        }
                    }
                if (n1subn2 || n2subn1);
                    //do nothing
                //check if any two bounding box share a same side (connected from one side)
                else if (n1->bbox[0] == n2->bbox[0] && n1->bbox[1] == n2->bbox[1]) {
                    if (n1->bbox[2] >= n2->bbox[2] && n1->bbox[2] <= n2->bbox[3] && (n1->itv[2] == 1 || n2->itv[3] == 1))
                        n1belown2 = true;
                    else if (n2->bbox[2] >= n1->bbox[2] && n2->bbox[2] <= n1->bbox[3] && (n1->itv[3] == 1 || n2->itv[2] == 1))
                        n2belown1 = true;
                }
                else if (n1->bbox[2] == n2->bbox[2] && n1->bbox[3] == n2->bbox[3]) {
                    if (n2->bbox[1] >= n1->bbox[0] && n2->bbox[1] <= n1->bbox[1] && (n1->itv[0] == 1 || n2->itv[1] == 1))
                        n2leftn1 = true;
                    else if (n1->bbox[1] >= n2->bbox[0] && n1->bbox[1] <= n2->bbox[1] && (n1->itv[1] == 1 || n2->itv[0] == 1))
                        n1leftn2 = true;
                }

                if (n1belown2 || n1leftn2 || n2belown1 || n2leftn1 || n1subn2 || n2subn1) {

                    if (n1belown2) {
                        m->bbox[0] = n1->bbox[0];
                        m->bbox[1] = n1->bbox[1];
                        m->bbox[2] = n2->bbox[2];
                        m->bbox[3] = n1->bbox[3];
                        //choose close boundaries if possible
                        m->itv[0] = n1->itv[0];
                        m->itv[1] = n1->itv[1];
                        m->itv[2] = n2->itv[2];
                        m->itv[3] = n1->itv[3];
                    }
                    else if (n2belown1) {
                        m->bbox[0] = n1->bbox[0];
                        m->bbox[1] = n1->bbox[1];
                        m->bbox[2] = n1->bbox[2];
                        m->bbox[3] = n2->bbox[3];
                        //choose close boundaries if possible
                        m->itv[0] = n1->itv[0];
                        m->itv[1] = n1->itv[1];
                        m->itv[2] = n1->itv[2];
                        m->itv[3] = n2->itv[3];
                    }
                    else if (n1leftn2) {
                        m->bbox[0] = n1->bbox[0];
                        m->bbox[1] = n2->bbox[1];
                        m->bbox[2] = n1->bbox[2];
                        m->bbox[3] = n1->bbox[3];
                        //choose close boundaries if possible
                        m->itv[0] = n1->itv[0];
                        m->itv[1] = n2->itv[1];
                        m->itv[2] = n1->itv[2];
                        m->itv[3] = n1->itv[3];
                    }
                    else if (n2leftn1) {
                        m->bbox[0] = n2->bbox[0];
                        m->bbox[1] = n1->bbox[1];
                        m->bbox[2] = n1->bbox[2];
                        m->bbox[3] = n1->bbox[3];
                        //choose close boundaries if possible
                        m->itv[0] = n2->itv[0];
                        m->itv[1] = n1->itv[1];
                        m->itv[2] = n1->itv[2];
                        m->itv[3] = n1->itv[3];
                    }
                    else if (n1subn2 || n2subn1) {
                        if (n1subn2) {
                            m->bbox[0] = n2->bbox[0];
                            m->bbox[1] = n2->bbox[1];
                            m->bbox[2] = n2->bbox[2];
                            m->bbox[3] = n2->bbox[3];
                            m->itv[0] = n2->itv[0];
                            m->itv[1] = n2->itv[1];
                            m->itv[2] = n2->itv[2];
                            m->itv[3] = n2->itv[3];
                        }
                        else {
                            m->bbox[0] = n1->bbox[0];
                            m->bbox[1] = n1->bbox[1];
                            m->bbox[2] = n1->bbox[2];
                            m->bbox[3] = n1->bbox[3];
                            m->itv[0] = n1->itv[0];
                            m->itv[1] = n1->itv[1];
                            m->itv[2] = n1->itv[2];
                            m->itv[3] = n1->itv[3];
                        }
                        //choose close boundaries if possible
                        if (n1->bbox[0] == n2->bbox[0])
                            m->itv[0] = n1->itv[0] > n2->itv[0] ? n1->itv[0] : n2->itv[0];
                        if (n1->bbox[1] == n2->bbox[1])
                            m->itv[1] = n1->itv[1] > n2->itv[1] ? n1->itv[1] : n2->itv[1];
                        if (n1->bbox[2] == n2->bbox[2])
                            m->itv[2] = n1->itv[2] > n2->itv[2] ? n1->itv[2] : n2->itv[2];
                        if (n1->bbox[3] == n2->bbox[3])
                            m->itv[3] = n1->itv[3] > n2->itv[3] ? n1->itv[3] : n2->itv[3];

                    }
                    
                    m->has_bbox = true;
                    merge_size++;
                    //if (m->bbox[0] == 0 && m->bbox[1] == 800 && m->bbox[2] == 0 && m->bbox[3] == 600)
                    //    break;

                    //cn1[n1cnt] = 0;
                    cn2[n2cnt] = 0;
                    if (!changed) {
                        first_merged = m;
                        first_cnt = n1cnt;
                    }
                    n1 = n1->next;
                    m = m->next;
                    //n1cnt++;
                    changed = true;
                    //break;
                }
                n2 = n2->next;
                n2cnt++;
                if (safety_counter > SAFETY_COUNT)
                    mexErrMsgTxt("compressSpatialReion: ERROR_1");
                if (n1 == NULL_SN)
                    break;
            }//while n2
            if (merge_size > (inp1->size - 1))
                break;
            //n1 = first_merged->next;
            if (n1 == NULL_SN)
                break;
            n1 = n1->next;
            n1cnt++;
            m = n1;// m->next;
            if (!changed) 
            {
                if (m != NULL_SN) {
                    n2 = m->next;
                    n2cnt = n1cnt + 1;
                }
                else
                    break;
            }
            else {
                //n2 = first_merged;
                break;
            }
                
            if (safety_counter > SAFETY_COUNT)
                mexErrMsgTxt("compressSpatialReion: ERROR_2");
        }//while n1
        if (merge_size > (inp1->size - 1))
            break;
        if (safety_counter > SAFETY_COUNT) {
            print_spatial_region(merge);
            mexErrMsgTxt("compressSpatialReion: ERROR_3");
        }
        n1 = merge->head;
        if (changed && first_cnt > 0) {
            n2 = first_merged;
            n2cnt = first_cnt;
        }
        else {
            n2 = merge->head->next;
            n2cnt = 1;
        }
        //n2 = m;
        m = merge->head;
        n1cnt = 0;
    }//while changed

    int old_size = inp1->size;
    if (releaseNodes) {
        release_spatial_node_from_head(&inp1);
        ////release_spatial_region(&inp1);
    }
    //remove inclusive nodes
    m = merge->head;
    n1 = merge->head;
    SpatialNode* tmpNode;
    int i2 = 0;
    while (m != NULL_SN) {
        //remove n2 and relink
        if (cn2[i2] == 0) {
            if (i2 == 0) {
                
                tmpNode = merge->head;
                merge->head = m->next;
                tmpNode->next = NULL_SN;
                release_spatial_node(&tmpNode);
            }
            n1->next = m->next;
            
            tmpNode = m;
            m = n1->next;
            tmpNode->next = NULL_SN;
            release_spatial_node(&tmpNode);
        }
        else {
            n1 = m;
            m = m->next;
        }
        i2++;
    }
    
    if(m != NULL_SN && m->next != NULL_SN)
        release_spatial_node(&m->next);

    merge->size = old_size - merge_size;
    if (merge->size < 1)
        mexErrMsgTxt("compressSpatialReion: wrong size!");
    //if (inp1->size > 0)
    if (free_cn1)
        efree(cn1);
    //if (inp2->size > 0)
    if (free_cn2)
        efree(cn2);
    return merge;
}

SpatialRegion* createSpatialRegion(int size) {
    sr_mem_cnt += size;
    
    SpatialRegion* reg = NULL_SR;
    if (garbage_collector.sr_size < 1 || size > 1) {
        reg = (SpatialRegion*)halloc(size * sizeof(SpatialRegion));
        sr_mem_new += size;
        for (int i = 0; i < size; i++) {
            reg[i].size = 0;
            reg[i].head = NULL_SN;
            reg[i].next = NULL_SR;
            //reg[i].IdTable = NULL;
            reg[i].is_NAN = false;
        }
    }
    else if (size == 1) {//only single node from the garbage collection
        reg = garbage_collector.sr_head;
        reg->size = 0;
        reg->head = NULL_SN;
        reg->is_NAN = false;
        garbage_collector.sr_head = garbage_collector.sr_head->next;
        garbage_collector.sr_size--;
        reg->next = NULL_SR;
    }
    return reg;
}

SpatialNode* createSpatialNode(int size) {
    sn_mem_cnt += size;
    
    SpatialNode* node = NULL_SN;
    if (garbage_collector.sn_size < 1 || size > 1) {
        node = (SpatialNode*)halloc(size * sizeof(SpatialNode));
        sn_mem_new += size;
        for (int i = 0; i < size; i++) {
            node[i].conv_set = NULL;
            node[i].has_bbox = false;
            node[i].next = NULL_SN;
        }
    }
    else if (size == 1) {//only single node from the garbage collection
        node = garbage_collector.sn_head;
        node->conv_set = NULL;
        node->has_bbox = false;
        garbage_collector.sn_head = garbage_collector.sn_head->next;
        garbage_collector.sn_size--;
        node->next = NULL_SN;
    }
    return node;
}

SpatialNode* createRandomNode(int size) {
    if (size < 1)
        return NULL_SN;

    time_t t;
    srand((unsigned)time(&t));

    int w = (int)(_UNIVERSE[1] - _UNIVERSE[0]);
    int h = (int)(_UNIVERSE[3] - _UNIVERSE[2]);
    int x1, x2, y1, y2;
    SpatialNode* result = NULL_SN;
    SpatialNode* tmp;

    for (int i = 0; i < size; i++) {
        x1 = (int)(rand() % w + _UNIVERSE[0]);
        x2 = (int)(rand() % w + _UNIVERSE[0]);
        y1 = (int)(rand() % h + _UNIVERSE[2]);
        y2 = (int)(rand() % h + _UNIVERSE[2]);
        tmp = createSpatialNode(1);
        if (result == NULL_SN)
            result = tmp;
        else {
            result->next = tmp;
            result = result->next;
        }
        result->bbox[0] = x1 < x2 ? x1 : x2;
        result->bbox[1] = x1 > x2 ? x1 : x2;
        result->bbox[2] = y1 < y2 ? y1 : y2;
        result->bbox[3] = y1 > y2 ? y1 : y2;
        result->has_bbox = true;
        result->conv_set = NULL;
        result->itv[0] = rand() > 0.5 * RAND_MAX ? 1 : 0;
        result->itv[1] = rand() > 0.5 * RAND_MAX ? 1 : 0;
        result->itv[2] = rand() > 0.5 * RAND_MAX ? 1 : 0;
        result->itv[3] = rand() > 0.5 * RAND_MAX ? 1 : 0;
    }

    return result;
}

bool equalNodes(SpatialNode* inp1, SpatialNode* inp2) {
    if (inp1 == NULL_SN && inp2 == NULL_SN)
        return true;
    else if (inp1 == NULL_SN || inp2 == NULL_SN)
        return false;
    else if (inp1->has_bbox != inp2->has_bbox)
        return false;
    else if (inp1->bbox[0] != inp2->bbox[0] || inp1->bbox[1] != inp2->bbox[1] ||
        inp1->bbox[2] != inp2->bbox[2] || inp1->bbox[3] != inp2->bbox[3])
        return false;
    else if (inp1->itv[0] != inp2->itv[0] || inp1->itv[1] != inp2->itv[1] ||
        inp1->itv[2] != inp2->itv[2] || inp1->itv[3] != inp2->itv[3])
        return false;
    else if ((inp1->conv_set == NULL && inp2->conv_set != NULL) ||
        inp1->conv_set != NULL && inp2->conv_set == NULL)
        return false;
    else if (!equalConvSet(inp1->conv_set, inp2->conv_set))
        return false;

    return equalNodes(inp1->next, inp2->next);
}

bool equalRegions(SpatialRegion* inp1, SpatialRegion* inp2) {
    if (inp1 == NULL_SR && inp2 == NULL_SR)
        return true;
    else if (inp1 == NULL_SR || inp2 == NULL_SR)
        return false;
    else if (inp1->size != inp2->size)
        return false;
    else
        return equalNodes(inp1->head, inp2->head);
}

bool isSpatialUniverse(SpatialRegion* inp1) {
    SpatialRegion* reg = createSpatialRegion(1);
    SetTo_Universe_or_Emptyset(reg, 1);
    bool res = equalRegions(reg, inp1);
    release_spatial_region(&reg);
    return res;
}

bool isSpatialEmpty(SpatialRegion* inp1) {
    SpatialRegion* reg = createSpatialRegion(1);
    SetTo_Universe_or_Emptyset(reg, -1);
    bool res = equalRegions(reg, inp1);
    release_spatial_region(&reg);
    return res;
}

void* halloc(int size) {
    if (size == 0) {
        if (SHOW_WARNINGS)
            mexPrintf("WARNING: memory allocation with size 0\n");
        return NULL;
    }
    char* tmp;
    if (!(tmp = (char*)malloc(size)))
    //if (!(tmp = (char*)mxMalloc(size)))
            mexErrMsgTxt("halloc: not enough memory!");
    memset(tmp, 0, size);
    return tmp;
}

void inline hfree(void* tmp) {
    free(tmp);
    //mxFree(tmp);
    tmp = NULL;
}

void init_garbage_collector() {
    garbage_collector.sr_size = 0;
    garbage_collector.sr_head = NULL_SR;
    garbage_collector.sr_end = garbage_collector.sr_head;
    garbage_collector.sn_size = 0;
    garbage_collector.sn_head = NULL_SN;
    garbage_collector.sn_end = garbage_collector.sn_head;
}

void release_garbage(bool print_messages) {
    should_release_memory = true;
    if (VERBOSE_MODE && SHOW_EXE_STAT && print_messages) {
        mexPrintf("\n>>> Garbage Recycle: sr_size = %d , sn_size = %d\n", garbage_collector.sr_size, garbage_collector.sn_size);
        mexPrintf("Releasing spatial region memories and it may take a while!\n");
        if (run_from_matlab)
            mexCallMATLAB(0, NULL, 0, NULL, "drawnow");//to flush printouts in Matlab
    }
    int print_counter = print_counter = release_spatial_region_linkedlist(&garbage_collector.sr_head);
    if (VERBOSE_MODE && SHOW_EXE_STAT && print_messages) {
        mexPrintf("Total released spatial region: %d\n", print_counter);

        mexPrintf("Releasing spatial node memories and it may take a while!\n");
        if (run_from_matlab)
            mexCallMATLAB(0, NULL, 0, NULL, "drawnow");//to flush printouts in Matlab
    }
    print_counter = release_spatial_node(&garbage_collector.sn_head);

    if (VERBOSE_MODE && SHOW_EXE_STAT && print_messages) {
        mexPrintf("Total released spatial node: %d\n", print_counter);
        if (run_from_matlab)
            mexCallMATLAB(0, NULL, 0, NULL, "drawnow");//to flush printouts in Matlab
    }
}
