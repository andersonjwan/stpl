/***** mx_tp_taliro : lex.c *****/

/* Written by Georgios Fainekos, ASU, U.S.A.                              */
/* Copyright (c) 2017  Georgios Fainekos                                  */
/* Send bug-reports and/or questions to: fainekos@asu.edu                  */
/* Modified by Mohammad Hekmatnejad ASU, U.S.A. for stpl_taliro           */
/* Copyright (c) 2020  Mohammad Hekmatnejad                                  */

/* This program is free software; you can redistribute it and/or modify   */
/* it under the terms of the GNU General Public License as published by   */
/* the Free Software Foundation; either version 2 of the License, or      */
/* (at your option) any later version.                                    */
/*                                                                        */
/* This program is distributed in the hope that it will be useful,        */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of         */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          */
/* GNU General Public License for more details.                           */
/*                                                                        */
/* You should have received a copy of the GNU General Public License      */
/* along with this program; if not, write to the Free Software            */
/* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA*/

/* Most of the code in this file was taken from LTL2BA software           */
/* Written by Denis Oddoux, LIAFA, France                                  */
/* Some of the code in this file was taken from the Spin software         */
/* Written by Gerard J. Holzmann, Bell Laboratories, U.S.A.               */

#include <stdlib.h>
#include <ctype.h>
#include "undomex.h"
#include "matrix.h"
#include "distances.h"
#include "ltl2tree.h"
#include "param.h"

static int tl_lex(int *, size_t, char *, Miscellaneous *, int *);


#define Token(y)            miscell->tl_yylval = tl_nn(y,ZN,ZN,miscell); return y
#define MetricToken(y)        miscell->tl_yylval = tl_nn(y,ZN,ZN,miscell); miscell->tl_yylval->time = miscell->TimeCon; return y

struct Map type_map[MAX_VAR_NUM];
int size_type_map = 0;

struct Map var_id_map[MAX_VAR_NUM];
int size_var_id_map = 0;

struct Map id_to_rank_map[MAX_VAR_NUM];
int size_id_to_rank_map = 0;
struct Map id_rank_to_name_map[MAX_VAR_NUM];
int size_id_rank_to_name_map = 0;

int add_map(struct Map* map, char* key, int value, int* index) {
    map[*index].value.data = value;
    map[*index].key.data = (char*)emalloc((strlen(key)+1) * sizeof(char));
    safe_strcpy(map[*index].key.data, strlen(key) + 1, key);
    return ++(*index);
}

int find_in_map(struct Map* map, char* key, int size) {
    for (int i = 0; i < size; i++) {
        if (strcmp(key, map[i].key.data) == 0)
            return map[i].value.data;
    }
    return -1;
}

int add_map_reverse(struct Map* map, int key, char* data, int* index) {
    map[*index].value.data = key;
    map[*index].key.data = (char*)emalloc((strlen(data)+1) * sizeof(char));
    safe_strcpy(map[*index].key.data, strlen(data) + 1, data);
    return ++(*index);
}

char* find_in_map_reverse(struct Map* map, int key, int size) {
    for (int i = 0; i < size; i++) {
        if (map[i].value.data == key)
            return map[i].key.data;
    }
    return NULL;
}

int isalnum_(int c)
{       
    return (isalnum(c) || c == '_');
}

int isSpatioPred(int c) {
    return (isalnum_(c));// || c == ')' || c == '(');
}

int hash(char *s)
{       
    int h=0;
    while (*s)
    {       
        h += *s++;
        h <<= 1;
        if (h&(Nhash+1))
            h |= 1;
    }
    return h&Nhash;
}

static void getword(int first, int (*tst)(int),int *cnt, size_t hasuform, char *uform, Miscellaneous *miscell)
{    
    int i=0; char c;

    miscell->yytext[i++]= (char ) first;
    while (tst(c = tl_Getchar(cnt, hasuform, uform)))
        miscell->yytext[i++] = c;
    miscell->yytext[i] = '\0';
    tl_UnGetchar(cnt);
}

Number getnumber(char cc, int *cnt, size_t hasuform, char *uform, int *tl_yychar, Miscellaneous *miscell) /* get a number from input string */
{
    /* Peer reviewed on 2013.07.22 by Dokhanchi, Adel */
    int sign = 1;
    int ii = 0; 
    int jj = 0;
    char strnum[80];
    Number num;
    char temp[80];
    int match = 0;

    if (cc=='-')
    {    
        sign = -1;
        do {    
            cc = tl_Getchar(cnt, hasuform, uform);
        } while (cc == ' ');
    }
    else if (cc == '+')
    {    
        do {    
            cc = tl_Getchar(cnt, hasuform, uform); 
        } while (cc == ' ');
    }
    
    if (cc=='i')
    {    cc = tl_Getchar(cnt, hasuform, uform);
        if (cc=='n')
        {    cc = tl_Getchar(cnt, hasuform, uform);
            if (cc=='f')
            {    if (miscell->dp_taliro_param.ConOnSamples)
                {    
                    num.numi.inf = sign;
                    num.numi.i_num = 0;
                       tl_UnGetchar(cnt);
                       tl_yyerror("Constraints on the number of samples is not supported", cnt, uform, tl_yychar, miscell);
                    tl_exit(0);
                }
                else
                {    
                    num.numf.inf = sign;
                    num.numf.f_num = 0.0;
                }
            }
            else
            {    tl_UnGetchar(cnt);
                tl_yyerror("expected a number or a (-)inf in timing constraints!", cnt, uform, tl_yychar, miscell);
                tl_exit(0);
            }
        }
        else
        {    tl_UnGetchar(cnt);
            tl_yyerror("expected a number or a (-)inf in timing constraints!", cnt, uform, tl_yychar, miscell);
            tl_exit(0);
        }
    }
    else if (('0'<=cc && cc<='9') || cc=='.')
    {
        strnum[ii++] = cc;
        //cc!='}' added by Mohammad
        for (cc = tl_Getchar(cnt, hasuform, uform); cc!=' '&& cc!=',' && cc!=']' && cc!=')' && cc!='}'; cc = tl_Getchar(cnt, hasuform, uform))
        {     
            if (ii>=80)
            {    
                tl_UnGetchar(cnt);
                tl_yyerror("numeric constants must have length less than 80 characters.", cnt, uform, tl_yychar, miscell);
                tl_exit(0);
            }
            strnum[ii++] = cc;
        }
        tl_UnGetchar(cnt);
        strnum[ii] = '\0';
        safe_strncpy(miscell->yytext, ii + 1, strnum, ii+1);
        if (miscell->dp_taliro_param.ConOnSamples)
        {    num.numi.inf = 0;
            num.numi.i_num = sign*atoi(strnum);
             tl_UnGetchar(cnt);
            tl_yyerror("Constraints on the number of samples is not supported", cnt, uform, tl_yychar, miscell);
            tl_exit(0);
        }
        else
        {    num.numf.inf = 0;
            num.numf.f_num = (double)sign*atof(strnum);
        }
    }
    else
    {
        temp[jj++] = cc;
        //cc!='}' added by Mohammad
        for (cc = tl_Getchar(cnt, hasuform, uform); cc!=' '&& cc!=',' && cc!=']' && cc!=')' && cc!='}'; cc = tl_Getchar(cnt, hasuform, uform))
        {     
            if (ii>=80)
            {    
                tl_UnGetchar(cnt);
                tl_yyerror("numeric constants must have length less than 80 characters.", cnt, uform, tl_yychar, miscell);
                tl_exit(0);
            }
            temp[jj++] = cc;
        }
        tl_UnGetchar(cnt);
        temp[jj] = '\0';

        for(ii = 0; ii < miscell->dp_taliro_param.nPred; ii++)
        {
            if(miscell->parMap[ii].str != NULL)
            {
                if(strcmp(temp, miscell->parMap[ii].str)==0)
                {
                    miscell->parMap[ii].type = miscell->type_temp;
                    match = 1;
                    miscell->pList.pindex[ii] = PAR;
                    if(miscell->parMap[ii].value != NULL)
                    {
                        miscell->parMap[ii].with_value = true;
                        if (miscell->dp_taliro_param.ConOnSamples)
                        {
                            num.numi.inf = 0;
                            num.numi.i_num = sign*(int)(*(miscell->parMap[ii].value));
                            tl_UnGetchar(cnt);
                            tl_yyerror("Constraints on the number of samples is not supported", cnt, uform, tl_yychar, miscell);
                            tl_exit(0);
                        }
                        else
                        {
                            num.numf.inf = 0;
                            num.numf.f_num = (double)sign*(*(miscell->parMap[ii].value));
                        }
                    }
                    else
                    {
                        miscell->parMap[ii].with_value = false;
                        if (miscell->dp_taliro_param.ConOnSamples)
                        {
                            num.numi.inf = 0;
                            num.numi.i_num = sign*(int)(miscell->parMap[ii].Range[0]);
                             tl_UnGetchar(cnt);
                            tl_yyerror("Constraints on the number of samples is not supported", cnt, uform, tl_yychar, miscell);
                            tl_exit(0);
                        }
                        else
                        {
                            num.numf.inf = 0;
                            num.numf.f_num = (double)sign*(miscell->parMap[ii].Range[0]);
                        }
                    }
                    if(miscell->lbd)
                    {
                        miscell->parMap[ii].lbd = true;
                    }
                    else
                    {
                        miscell->parMap[ii].lbd = false;
                    }
                }
            }
        }
        if(match == 0)
        {
            tl_UnGetchar(cnt);
            tl_yyerror("expected a number or inf or a paramter or parameter not matched", cnt, uform, tl_yychar, miscell);
            tl_exit(0);
        }
    }
    return(num);
}

Interval getbounds(int *cnt, size_t hasuform, char *uform, Miscellaneous *miscell, int *tl_yychar)
{    
    char cc;
    Interval time;

    /* remove spaces */
    do 
    {    cc = tl_Getchar(cnt, hasuform, uform);
    } while (cc == ' ');
    
    if (cc!='[' && cc!='(')
    {
        tl_UnGetchar(cnt);
        tl_yyerror("expected '(' or '[' after _", cnt, uform, tl_yychar, miscell);
        tl_exit(0);
    }

    /* is interval closed? */
    if (cc=='[')
        time.l_closed = 1;
    else
        time.l_closed = 0;

    /* remove spaces */
    do 
    {    cc = tl_Getchar(cnt, hasuform, uform);
    } while (cc == ' ');
    
    /* get lower bound */
    miscell->lbd = true;
    time.lbd = getnumber(cc, cnt, hasuform, uform, tl_yychar, miscell);
    if (e_le(time.lbd,miscell->zero,&(miscell->dp_taliro_param)))
    {
        tl_UnGetchar(cnt);
        tl_yyerror("past time operators are not allowed - only future time intervals.", cnt, uform, tl_yychar, miscell);
        tl_exit(0);
    }

    /* remove spaces */
    do 
    {    cc = tl_Getchar(cnt, hasuform, uform);
    } while (cc == ' ');

    if (cc!=',')
    {    
        tl_UnGetchar(cnt);
        tl_yyerror("timing constraints must have the format <num1,num2>.", cnt, uform, tl_yychar, miscell);
        tl_exit(0);
    }

    /* remove spaces */
    do 
    {    cc = tl_Getchar(cnt, hasuform, uform);
    } while (cc == ' ');

    /* get upper bound */
    miscell->lbd = false;
    time.ubd = getnumber(cc, cnt, hasuform, uform, tl_yychar, miscell);

    if (e_ge(time.lbd,time.ubd,&(miscell->dp_taliro_param)))
    {    tl_UnGetchar(cnt);
        tl_yyerror("timing constraints must have the format <num1,num2> with num1 <= num2.", cnt, uform, tl_yychar, miscell);
        tl_exit(0);
    }

    /* remove spaces */
    do 
    {    cc = tl_Getchar(cnt, hasuform, uform);
    } while (cc == ' ');

    if (cc!=']' && cc!=')')
    {
        tl_UnGetchar(cnt);
        tl_yyerror("timing constraints must have the format <num1,num2>, where > is from the set {),]}", cnt, uform, tl_yychar, miscell);
        tl_exit(0);
    }

    /* is interval closed? */
    if (cc==']')
        time.u_closed = 1;
    else
        time.u_closed = 0;

    return(time);

}

static int follow(int tok, int ifyes, int ifno, int *cnt, size_t hasuform, char *uform, int *tl_yychar, Miscellaneous *miscell)
{    
    int c;
    char buf[32];

    if ((c = tl_Getchar(cnt, hasuform, uform)) == tok)
        return ifyes;
    tl_UnGetchar(cnt);
    *tl_yychar = c;
    safe_sprintf(buf, 32, "expected '%c'", tok);
    tl_yyerror(buf, cnt, uform, tl_yychar, miscell);    /* no return from here */
    return ifno;
}


static bool check_follow(int tok1, int tok2, int* cnt, size_t hasuform, char* uform, int* tl_yychar, Miscellaneous* miscell)
{
    int c;
    if ((c = tl_Getchar(cnt, hasuform, uform)) == tok1) {
        if ((c = tl_Getchar(cnt, hasuform, uform)) == tok2)
            return true;
        else
            tl_UnGetchar(cnt);
    }
    tl_UnGetchar(cnt);
    if (c == '_' || c == ' ' || c == '(')
        return false;
    char buf[32];
    safe_sprintf(buf, 32,"expected '%c'", tok1);
    tl_yyerror(buf, cnt, uform, tl_yychar, miscell);    /* no return from here */
    return false;
}

static void mtl_con(int *cnt, size_t hasuform, char *uform, Miscellaneous *miscell, int *tl_yychar)
{
    char c;
    c = tl_Getchar(cnt, hasuform, uform);
    if (c == '_')
    {
        miscell->dp_taliro_param.LTL = 0;
        miscell->TimeCon = getbounds(cnt, hasuform, uform, miscell, tl_yychar);
    }
    else
    {
        miscell->TimeCon = miscell->zero2inf;
        tl_UnGetchar(cnt);
    }
}

static int mtl_follow(int tok, int ifyes, int ifno, int *cnt, size_t hasuform, char *uform, Miscellaneous *miscell, int *tl_yychar)
{    
    int c;
    char buf[32];

    if ((c = tl_Getchar(cnt, hasuform, uform)) == tok)
    {
        miscell->type_temp = ifyes;
        mtl_con(cnt, hasuform, uform, miscell,tl_yychar);
        return ifyes;
    }
    tl_UnGetchar(cnt);
    *tl_yychar = c;
    safe_sprintf(buf, 32, "expected '%c'", tok);
    tl_yyerror(buf, cnt, uform, tl_yychar, miscell);    /* no return from here */
    return ifno;
}

int
tl_yylex(int *cnt, size_t hasuform, char *uform, Miscellaneous *miscell, int *tl_yychar)
{    
    int c = tl_lex(cnt, hasuform, uform, miscell, tl_yychar);
    return c;
}

//MAIN SYNTAX TOKENIZER FUNCTION
static int tl_lex(int *cnt, size_t hasuform, char *uform, Miscellaneous *miscell, int *tl_yychar)
{    
    int c,ii;

    do {
        c = tl_Getchar(cnt, hasuform, uform);
        if (c <= 0)
        {    Token(';');
        }
        else {
            miscell->yytext[0] = (char)c;
            miscell->yytext[1] = '\0';
        }
    } while (c == ' ');    /* '\t' is removed in tl_main.c */

    
    if (c == 'B') {//predicate BB(...) bounding box
        c = follow('B', PREDICATE, 'B', cnt, hasuform, uform, tl_yychar, miscell);
        do {
            c = tl_Getchar(cnt, hasuform, uform);
        } while (c == ' ');
        if(c != '(')
            tl_yyerror("expected (", cnt, uform, tl_yychar, miscell);
        c = tl_Getchar(cnt, hasuform, uform);
        getword(c, isSpatioPred, cnt, hasuform, uform, miscell);
        c = uform[*cnt];
        while (c == ' ') {
            c = tl_Getchar(cnt, hasuform, uform);
        }
        if (c != ')')
            tl_yyerror("expected )", cnt, uform, tl_yychar, miscell);
        c = tl_Getchar(cnt, hasuform, uform);

        miscell->tl_yylval = tl_nn(PREDICATE, ZN, ZN, miscell);
        miscell->type_temp = PREDICATE;
        int plen = (int)strlen(miscell->yytext);
        miscell->type_temp = PREDICATE;
        miscell->tl_yylval->fndef = (FnDef*)emalloc(sizeof(FnDef));
        miscell->tl_yylval->fndef->fn_name = (char*)emalloc(3 * sizeof(char));
        safe_strcpy(miscell->tl_yylval->fndef->fn_name, 3, "BB\0");
        miscell->second_type_temp = PREDICATE;
        miscell->tl_yylval->fndef->num_param = 1;
        miscell->tl_yylval->fndef->num_vars = 1;
        miscell->tl_yylval->fndef->params = (FnDef*)emalloc(sizeof(FnDef));
        miscell->tl_yylval->fndef->params[0].fn_name = (char*)emalloc((plen + 1) * sizeof(char));
        safe_strncpy(miscell->tl_yylval->fndef->params[0].fn_name, plen + 1, miscell->yytext, plen+1);
        miscell->tl_yylval->fndef->params[0].fn_name[plen] = '\0';
        miscell->tl_yylval->fndef->params[0].fn_type = VAR_ID;
        miscell->tl_yylval->sym = (Symbol*)emalloc(sizeof(Symbol));

        char* buf = (char*)emalloc((plen + 5) * sizeof(char));
        buf[0] = 'B';
        buf[1] = 'B';
        buf[2] = '(';
        buf[3] = '\0';
        safe_strcat(buf, plen + 5, miscell->yytext);
        safe_strcat(buf, plen + 5, ")");
        miscell->tl_yylval->sym->name = (char*)emalloc((plen + 5) * sizeof(char));
        safe_strcpy(miscell->tl_yylval->sym->name, plen+5, buf);
        miscell->tl_yylval->sym->is_var = true;
        //buf[plen] = '\0';
        miscell->tl_yylval->sym = tl_lookup(buf, miscell);
        return PREDICATE;
    }

    if (c == '@'){
        static int type_map_index = 0;
        static int var_id_map_index = 0;
        static int id_to_rank_map_size = 0;
        int num_param = 0;
        bool has_time_var = true;
        bool has_frame_var = true;
        bool has_id_var = true;

        miscell->tl_yylval = tl_nn(FREEZE_AT, ZN, ZN, miscell);
        miscell->type_temp = FREEZE_AT;
        miscell->tl_yylval->fndef = (FnDef*)emalloc(sizeof(FnDef));
        //miscell->tl_yylval->fndef->param = NULL;
        //miscell->tl_yylval->fndef->num_param = 4;
        miscell->tl_yylval->fndef->fn_name = (char*)emalloc(2 * sizeof(char));
        safe_strcpy(miscell->tl_yylval->fndef->fn_name, 2, "@\0");
        //miscell->tl_yylval->fndef->param = (char**)emalloc(4 * sizeof(char*));
        miscell->second_type_temp = FREEZE_AT;

        //FnDef freez_params[MAX_NUM_PARAMS];
        miscell->tl_yylval->fndef->params = (FnDef*)emalloc(MAX_NUM_PARAMS * sizeof(FnDef));
        FnDef* freez_params = miscell->tl_yylval->fndef->params;

        do {
            c = tl_Getchar(cnt, hasuform, uform);
        } while (c == ' ');
        if (c != '(')
            tl_yyerror("expected (", cnt, uform, tl_yychar, miscell);
        do {
            c = tl_Getchar(cnt, hasuform, uform);
        } while (c == ' ');
        if (c == 'V' || c == '_') {
            if (c == 'V')//time variable
            {
                getword(c, isalnum_, cnt, hasuform, uform, miscell);
                miscell->tl_yylval->sym = (Symbol*)emalloc(sizeof(Symbol));
                miscell->tl_yylval->sym->name = (char*)emalloc((strlen(miscell->yytext) + 1) * sizeof(char));
                safe_strcpy(miscell->tl_yylval->sym->name, strlen(miscell->yytext) + 1, miscell->yytext);
                miscell->num_var = add_map(type_map, miscell->yytext, VAR_TIME, &type_map_index);
                size_type_map++;
                freez_params[num_param].fn_name = (char*)emalloc((strlen(miscell->yytext) + 1) * sizeof(char));
                freez_params[num_param].num_param = 0;
                safe_strcpy(freez_params[num_param].fn_name, strlen(miscell->yytext) + 1, miscell->yytext);
                freez_params[num_param].fn_type = VAR_TIME;
                num_param++;
                //miscell->tl_yylval->fndef->param[0] = (char*)emalloc(strlen(miscell->yytext) + 1);
                //safe_strcpy(miscell->tl_yylval->fndef->param[0], miscell->yytext);
            }
            else if (c == '_') {//don't care param
                //do nothing
                has_time_var = false;
            }
            else {
                tl_yyerror("expected time variable with naming converntion Var_", cnt, uform, tl_yychar, miscell);
            }
            do {
                c = tl_Getchar(cnt, hasuform, uform);
            } while (c == ' ');
            if (c != ',')
                tl_yyerror("expected ,", cnt, uform, tl_yychar, miscell);
            do {
                c = tl_Getchar(cnt, hasuform, uform);
            } while (c == ' ');

            if (c == 'V')//frame variable
            {
                getword(c, isalnum_, cnt, hasuform, uform, miscell);
                if (!has_time_var) {
                    miscell->tl_yylval->sym = (Symbol*)emalloc(sizeof(Symbol));
                    miscell->tl_yylval->sym->name = (char*)emalloc((strlen(miscell->yytext) + 1) * sizeof(char));
                    safe_strcpy(miscell->tl_yylval->sym->name, strlen(miscell->yytext) + 1, miscell->yytext);
                }
                //miscell->tl_yylval->fndef->param[1] = (char*)emalloc(strlen(miscell->yytext) + 1);
                //safe_strcpy(miscell->tl_yylval->fndef->param[1], miscell->yytext);
                freez_params[num_param].fn_name = (char*)emalloc((strlen(miscell->yytext) + 1) * sizeof(char));
                freez_params[num_param].num_param = 0;
                safe_strcpy(freez_params[num_param].fn_name, strlen(miscell->yytext) + 1, miscell->yytext);
                freez_params[num_param].fn_type = VAR_FRAME;
                freez_params[num_param].num_param = 0;
                num_param++;
                miscell->num_var = add_map(type_map, miscell->yytext, VAR_FRAME, &type_map_index);
                size_type_map++;
            }
            else if (c == '_') {//don't care param
                //do nothing
                has_frame_var = false;
            }
            else {
                tl_yyerror("expected frame variable with naming converntion Var_", cnt, uform, tl_yychar, miscell);
            }
            do {
                c = tl_Getchar(cnt, hasuform, uform);
            } while (c == ' ');
            if (c == ')')
                has_id_var = false;
            else {
                if (c != ',')
                    tl_yyerror("expected ','", cnt, uform, tl_yychar, miscell);
                do {
                    c = tl_Getchar(cnt, hasuform, uform);
                } while (c == ' ');
            }
        }
        else {
            has_time_var = false;
            has_frame_var = false;
        }

        if (c == 'F' || c == 'E')
        {
            getword(c, isalnum_, cnt, hasuform, uform, miscell);

            if (strcmp(miscell->yytext, "FORALL") != 0 && strcmp(miscell->yytext, "EXISTS") != 0)
                tl_yyerror("expected FORALL or EXISTS", cnt, uform, tl_yychar, miscell);

            if (c == 'F') {
                miscell->second_type_temp = FORALL;
                //miscell->tl_yylval->second_ntype = FORALL;
                //miscell->tl_yylval->fndef->param[2] = (char*)emalloc(strlen(miscell->yytext) + 1);
                //safe_strcpy(miscell->tl_yylval->fndef->param[2], miscell->yytext);
            }
            else {
                miscell->second_type_temp = EXISTS;
                //miscell->tl_yylval->second_ntype = EXISTS;
                //miscell->tl_yylval->fndef->param[2] = (char*)emalloc(strlen(miscell->yytext) + 1);
                //safe_strcpy(miscell->tl_yylval->fndef->param[2], miscell->yytext);
            }
            freez_params[num_param].fn_name = (char*)emalloc((strlen(miscell->yytext) + 1) * sizeof(char));
            freez_params[num_param].num_param = 0;
            safe_strcpy(freez_params[num_param].fn_name, strlen(miscell->yytext) + 1, miscell->yytext);
            freez_params[num_param].fn_type = VAR_RESERVED;
            num_param++;

        }
        else if (c == ')') {
            //there is no id variables
            has_id_var = false;
        }
        else
            tl_yyerror("expected FORALL or EXISTS quantifier", cnt, uform, tl_yychar, miscell);
        if (has_id_var) {
            do {
                c = tl_Getchar(cnt, hasuform, uform);
            } while (c == ' ');

            do {
                if (c != ',')
                    tl_yyerror("expected ',' ", cnt, uform, tl_yychar, miscell);
                do {
                    c = tl_Getchar(cnt, hasuform, uform);
                } while (c == ' ');
                if (c == 'V')//object id variable
                {
                    getword(c, isalnum_, cnt, hasuform, uform, miscell);

                    if (!has_time_var && !has_frame_var && num_param == 0) {
                        miscell->tl_yylval->sym = (Symbol*)emalloc(sizeof(Symbol));
                        miscell->tl_yylval->sym->name = (char*)emalloc((strlen(miscell->yytext) + 1) * sizeof(char));
                        safe_strcpy(miscell->tl_yylval->sym->name, strlen(miscell->yytext) + 1, miscell->yytext);
                    }
                    //miscell->tl_yylval->fndef->param[num_param] = (char*)emalloc(strlen(miscell->yytext) + 1);
                    //safe_strcpy(miscell->tl_yylval->fndef->param[num_param], miscell->yytext);
                    freez_params[num_param].fn_name = (char*)emalloc((strlen(miscell->yytext) + 1) * sizeof(char));
                    freez_params[num_param].num_param = 0;
                    safe_strcpy(freez_params[num_param].fn_name, strlen(miscell->yytext) + 1, miscell->yytext);
                    freez_params[num_param].fn_type = VAR_ID;
                    miscell->num_var = add_map(type_map, miscell->yytext, VAR_ID, &type_map_index);
                    size_type_map++;
                    add_map(var_id_map, miscell->yytext, miscell->second_type_temp, &var_id_map_index);
                    size_var_id_map++;
                    add_map_reverse(id_rank_to_name_map, size_id_to_rank_map, miscell->yytext, &size_id_rank_to_name_map);
                    add_map(id_to_rank_map, miscell->yytext, size_id_to_rank_map, &size_id_to_rank_map);
                    num_param++;
                }
                else {
                    tl_yyerror("expected object ID variable with naming converntion Var_", cnt, uform, tl_yychar, miscell);
                }
                do {
                    c = tl_Getchar(cnt, hasuform, uform);
                } while (c == ' ');

            } while (c == ',');
        }
        if (c != ')')
            tl_yyerror("expected )", cnt, uform, tl_yychar, miscell);
        //miscell->tl_yylval->fndef->params = (FnDef*)emalloc(num_param * sizeof(FnDef));
        miscell->tl_yylval->fndef->num_param = num_param;
        if (has_id_var) {
            miscell->tl_yylval->fndef->num_vars = num_param - 1;
            miscell->tl_yylval->num_id_vars = num_param - 1;
            if (has_frame_var)
                miscell->tl_yylval->num_id_vars--;
            if (has_time_var)
                miscell->tl_yylval->num_id_vars--;
        }
        else
            miscell->tl_yylval->fndef->num_vars = num_param;
        //for (int np = 0; np < num_param; np++) {
        //    miscell->tl_yylval->fndef->params[np] = freez_params[np];
        //}
        
        if (has_frame_var || has_time_var)
            miscell->has_freeze_var = true;
        else
            miscell->has_freeze_var = false;
        return FREEZE_AT;
    }

    else if (c == '{'){
        /* remove spaces */
        do 
        {    
            c = tl_Getchar(cnt, hasuform, uform);
        } while (c == ' ');
        bool is_delay_keyword = true;
        bool is_id_keyword = false;
        bool has_drem_operator = false;

        FnExpr* subFnExpr;//only for Var_id_? - Var_id_? >==<!= CONST_VAL as SUB(Var_id_?,Var_id_?) >==<!= CONST_VAL
        FnDef leftFnDef;//only for the left hand side of Var_id_? - Var_id_? >=< CONST_VAL as SUB(Var_id_?,Var_id_?) >==<!= CONST_VAL
        subFnExpr = (FnExpr*)emalloc(sizeof(FnExpr));
        leftFnDef.num_param = 2;
        leftFnDef.num_vars = 2;
        int num_param = 0;

        if (c == _FRAME_STR[0] || c == _TIME_STR[0])//FRAME or DELAY
        {
            getword(c, isalnum_, cnt, hasuform, uform, miscell);
            if (strcmp(miscell->yytext, _TIME_STR) != 0 &&
                strcmp(miscell->yytext, _FRAME_STR) != 0) {
                mexPrintf("expected {%s/%s - Var_name ><==!= CONST_NUMBER}",_TIME_STR,_FRAME_STR);
                tl_yyerror("Error ", cnt, uform, tl_yychar, miscell);
            }
            if (strcmp(miscell->yytext, _FRAME_STR) == 0)
                is_delay_keyword = false;
            do
            {
                c = tl_Getchar(cnt, hasuform, uform);
            } while (c == ' ');
            if (c != '-') {
                mexPrintf("expected {%s/%s - Var_name ><==!= CONST_NUMBER}", _TIME_STR, _FRAME_STR);
                tl_yyerror("Error ", cnt, uform, tl_yychar, miscell);
            }
            do
            {
                c = tl_Getchar(cnt, hasuform, uform);
            } while (c == ' ');
        }
        else if (c == 'V')//id variable
        {
            getword(c, isalnum_, cnt, hasuform, uform, miscell);
            miscell->second_type_temp = CONSTRAINT;
            miscell->tl_yylval = tl_nn(CONSTRAINT, ZN, ZN, miscell);
            miscell->type_temp = CONSTRAINT;
            miscell->tl_yylval->sym = (Symbol*)emalloc(sizeof(Symbol));
            miscell->tl_yylval->sym->name = (char*)emalloc((strlen(miscell->yytext) + 1) * sizeof(char));
            safe_strcpy(miscell->tl_yylval->sym->name, strlen(miscell->yytext) + 1, miscell->yytext);
            miscell->tl_yylval->sym->is_var = true;
            c = find_in_map(type_map, miscell->yytext, miscell->num_var);
            if (c < 0)
                tl_yyerror("This variable is not defined in @() block ", cnt, uform, tl_yychar, miscell);

            miscell->tl_yylval->sym->var_type = c;
            if (c != VAR_ID)
                tl_yyerror("ID keyword expected! ", cnt, uform, tl_yychar, miscell);
            is_id_keyword = true;
            miscell->tl_yylval->fndef = (FnDef*)emalloc(sizeof(FnDef));
            miscell->tl_yylval->fndef->num_param = 2;
            miscell->tl_yylval->fndef->fn_name = (char*)emalloc(9 * sizeof(char));
            safe_strcpy(miscell->tl_yylval->fndef->fn_name, 4, "SUB\0");
            leftFnDef.params = (FnDef*)emalloc(2*sizeof(FnDef));
            leftFnDef.fn_name = (char*)emalloc(4 * sizeof(char));
            safe_strcpy(leftFnDef.fn_name, 4, "SUB\0");
            leftFnDef.params[0].fn_name = (char*)emalloc((strlen(miscell->yytext) + 1) * sizeof(char));
            leftFnDef.params[0].num_param = 0;
            safe_strcpy(leftFnDef.params[0].fn_name, strlen(miscell->yytext) + 1, miscell->yytext);
            leftFnDef.params[0].fn_type = VAR_ID;
            leftFnDef.params[0].num_vars = 1;
            do
            {
                c = tl_Getchar(cnt, hasuform, uform);
            } while (c == ' ');
            if (c != '-')
                tl_yyerror("expected {Var_id_name1 - Var_id_name2 ><= CONST_NUMBER}", cnt, uform, tl_yychar, miscell);
            do
            {
                c = tl_Getchar(cnt, hasuform, uform);
            } while (c == ' ');
        }
        else {
            mexPrintf("expected {%s/%s - Var_name ><= CONST_NUMBER}", _TIME_STR, _FRAME_STR);
            tl_yyerror("Error ", cnt, uform, tl_yychar, miscell);
        }

        if (c == 'V')//time/frame variable
        {
            getword(c, isalnum_,cnt, hasuform, uform, miscell);
            miscell->second_type_temp = CONSTRAINT;
            miscell->tl_yylval = tl_nn(CONSTRAINT, ZN, ZN, miscell);
            miscell->type_temp = CONSTRAINT;
            miscell->tl_yylval->sym = (Symbol *) emalloc(sizeof(Symbol));
            miscell->tl_yylval->sym->name = (char *) emalloc((strlen(miscell->yytext) + 1) * sizeof(char));
            safe_strcpy(miscell->tl_yylval->sym->name, strlen(miscell->yytext) + 1, miscell->yytext);
            miscell->tl_yylval->sym->is_var = true;

            c = find_in_map(type_map, miscell->yytext, miscell->num_var);
            if(c < 0)
                tl_yyerror("This variable is not defined in the @(...) before being used in the {CONSTRAINT} block ", cnt, uform, tl_yychar, miscell);

            miscell->tl_yylval->sym->var_type = c;
            if( c != VAR_ID && is_id_keyword)
                tl_yyerror("ID keyword expected! ", cnt, uform, tl_yychar, miscell);
            if (c == VAR_FRAME && is_delay_keyword)
                tl_yyerror("FRAME keyword expected! ", cnt, uform, tl_yychar, miscell);
            if (c == VAR_TIME && !is_delay_keyword)
                tl_yyerror("DELAY keyword expected! ", cnt, uform, tl_yychar, miscell);
            if (is_id_keyword) {
                leftFnDef.params[1].fn_name = (char*)emalloc((strlen(miscell->yytext) + 1) * sizeof(char));
                leftFnDef.params[1].num_param = 0;
                safe_strcpy(leftFnDef.params[1].fn_name, strlen(miscell->yytext) + 1, miscell->yytext);
                leftFnDef.params[1].fn_type = VAR_ID;
                leftFnDef.params[1].num_vars = 1;
                subFnExpr->left_fn = leftFnDef;
            }
            else {//new syntax for periodical checks
                do
                {
                    c = tl_Getchar(cnt, hasuform, uform);
                } while (c == ' ');

                if (c == '%') {
                    efree(subFnExpr);
                    do
                    {
                        c = tl_Getchar(cnt, hasuform, uform);
                    } while (c == ' ');

                    if (c != '.' && c < '0' && c > '9')
                        mexErrMsgTxt("expected: {C_TIME/C_FRMAE - Var_x/Var_f % p >==<!= r} where p and r are real numbers");
                    has_drem_operator = true;
                    FnDef* fndef = (FnDef*)emalloc(sizeof(FnDef));
                    fndef->params = (FnDef*)emalloc(2 * sizeof(FnDef));
                    fndef->fn_name = (char*)emalloc(5 * sizeof(char));
                    safe_strcpy(fndef->fn_name, 5, "DREM\0");//division remainder
                    fndef->params[0].fn_name = (char*)emalloc((strlen(miscell->yytext) + 1) * sizeof(char));
                    fndef->params[0].num_param = 0;
                    safe_strcpy(fndef->params[0].fn_name, strlen(miscell->yytext) + 1, miscell->yytext);
                    if (is_delay_keyword)
                        fndef->params[0].fn_type = VAR_TIME;
                    else
                        fndef->params[0].fn_type = VAR_FRAME;
                    fndef->params[1].num_vars = 0;
                    char buf[100];
                    Number sec_val = getnumber(c, cnt, hasuform, uform, tl_yychar, miscell);
                    safe_sprintf(buf, 100, "%.5f", sec_val.numf.f_num);
                    fndef->params[1].fn_name = (char*)emalloc((strlen(buf) + 1) * sizeof(char));
                    safe_strcpy(fndef->params[1].fn_name, strlen(buf) + 1, buf);//a constant number
                    fndef->params[1].num_param = 0;
                    miscell->tl_yylval->fndef = fndef;
                }
                else {
                    tl_UnGetchar(cnt);
                }
            }
        }
        else{
            tl_yyerror("expected time/frame variable with naming converntion Var_", cnt, uform, tl_yychar, miscell);
        }
        /* remove spaces */
        do 
        {    
            c = tl_Getchar(cnt, hasuform, uform);
        } while (c == ' ');
        switch (c) 
        {
            case '<' :
                c = tl_Getchar(cnt, hasuform, uform);
                if (c == '=') 
                {
                    miscell->tl_yylval->ntyp = CONSTR_LE;
                    miscell->type_temp = CONSTR_LE;
                    subFnExpr->opr = LE_OPR;
                }
                else{
                    miscell->tl_yylval->ntyp = CONSTR_LS;
                    miscell->type_temp = CONSTR_LS;
                    subFnExpr->opr = L_OPR;
                    (*cnt)--;
                }
            break;
            case '>' :
                c = tl_Getchar(cnt, hasuform, uform);
                if (c == '=') 
                {
                    miscell->tl_yylval->ntyp = CONSTR_GE;
                    miscell->type_temp = CONSTR_GE;
                    subFnExpr->opr = GE_OPR;
                }
                else{
                    miscell->tl_yylval->ntyp = CONSTR_GR;
                    miscell->type_temp = CONSTR_GR;
                    subFnExpr->opr = G_OPR;
                    (*cnt)--;
                }
            break;
            case '=' :
                c = tl_Getchar(cnt, hasuform, uform);
                if (c == '=') 
                {
                    miscell->tl_yylval->ntyp = CONSTR_EQ;
                    miscell->type_temp = CONSTR_EQ;
                    subFnExpr->opr = EQ_OPR;
                }
                else{
                    tl_yyerror("expected '==' ", cnt, uform, tl_yychar, miscell);
                }
            break;
            case '!':
                c = tl_Getchar(cnt, hasuform, uform);
                if (c == '=')
                {
                    miscell->tl_yylval->ntyp = CONSTR_NE;
                    miscell->type_temp = CONSTR_NE;
                    subFnExpr->opr = NE_OPR;
                }
                else {
                    tl_yyerror("expected '!=' ", cnt, uform, tl_yychar, miscell);
                }
                break;
            default: 
                tl_yyerror("expected a comparison operator", cnt, uform, tl_yychar, miscell);
                break;
        }

        /* remove spaces */
        do 
        {    
            c = tl_Getchar(cnt, hasuform, uform);
        } while (c == ' ');

        miscell->tl_yylval->value = getnumber(c, cnt, hasuform, uform, tl_yychar, miscell);

        if (is_id_keyword) {

            static int extra_pred_count = 0;
            extra_pred_count++;
            char buf[12];
            safe_sprintf(buf, 12, "%d\0", extra_pred_count);
            subFnExpr->right_fn.fn_type = VAR_CONST;
            subFnExpr->right_fn.num_vars = 0;
            subFnExpr->right_fn.num_param = 0;
            subFnExpr->right_fn.fn_name = (char*)emalloc((strlen(miscell->yytext) + 1) * sizeof(char));
            safe_strcpy(subFnExpr->right_fn.fn_name, strlen(miscell->yytext) + 1, miscell->yytext);
            miscell->tl_yylval = tl_nn(PREDICATE, ZN, ZN, miscell);
            miscell->type_temp = PREDICATE;
            safe_strcat(buf, 12, "-ID-SYM\0");
            //miscell->tl_yylval->sym = (Symbol*)emalloc(sizeof(Symbol));
            //miscell->tl_yylval->sym->name = (char*)emalloc(strlen(buf) + 1);
            //safe_strcpy(miscell->tl_yylval->sym->name, buf);
            miscell->tl_yylval->sym = tl_lookup(buf, miscell);
            miscell->tl_yylval->sym->is_var = false;
            miscell->tl_yylval->sym->index = (int)(miscell->dp_taliro_param.nPred + extra_pred_count);
            miscell->pList.pindex[miscell->tl_yylval->sym->index] = PRED;
            //update the extra_pmap
            miscell->extra_pmap[extra_pred_count - 1].fn_expr = subFnExpr;
            miscell->extra_pmap[extra_pred_count - 1].is_expression = true;
            miscell->extra_pmap[extra_pred_count - 1].str = (char*)emalloc((strlen(buf) + 1) * sizeof(char));
            safe_strcpy(miscell->extra_pmap[extra_pred_count - 1].str, strlen(buf) + 1, buf);
            miscell->extra_pmap[extra_pred_count - 1].set.idx = (int)miscell->tl_yylval->sym->index;
            miscell->extra_pmap[extra_pred_count - 1].true_pred = true;

            miscell->extra_pred_from_formula++;
        }

        /* remove spaces */
        do {
            c = tl_Getchar(cnt, hasuform, uform);
        } while (c == ' ');
        if (c == '}'){
            if (is_id_keyword) {
                miscell->has_freeze_var = false;
                return PREDICATE;
            }
            else {
                miscell->has_freeze_var = true;
                return CONSTRAINT;
            }
        }
        else{
            tl_yyerror("expected '}' ", cnt, uform, tl_yychar, miscell);
        }
    }

    /* get the truth constants true and false and predicates */
    if (islower(c))
    {    getword(c, isalnum_,cnt, hasuform, uform, miscell);
        if (strcmp("true", miscell->yytext) == 0)
        {    Token(TRUE);
        }
        if (strcmp("false", miscell->yytext) == 0)
        {    Token(FALSE);
        }
        miscell->tl_yylval = tl_nn(PREDICATE,ZN, ZN, miscell);
        miscell->type_temp = PREDICATE;
        miscell->tl_yylval->sym = tl_lookup(miscell->yytext, miscell);

        /* match predicate index*/
        for(ii = 0; ii < miscell->dp_taliro_param.nPred; ii++)
        {
            if(miscell->predMap[ii].str != NULL)
            {
                if(strcmp(miscell->tl_yylval->sym->name, miscell->predMap[ii].str)==0)
                {
                    miscell->pList.pindex[ii] = PRED;
                    miscell->tl_yylval->sym->index = ii+1;
                }
            }
        }

        return PREDICATE;
    }
    /* get temporal operators */
    if (c == '<')
    {    
        c = tl_Getchar(cnt, hasuform, uform);
        if (c == '>') 
        {
            miscell->tl_yylval = tl_nn(EVENTUALLY,ZN,ZN,miscell);
            miscell->type_temp = EVENTUALLY;
            mtl_con(cnt, hasuform, uform, miscell,tl_yychar);
            return EVENTUALLY;
        }
        if (c != '-')
        {    
            tl_UnGetchar(cnt);
            tl_yyerror("expected '<>' or '<->'", cnt, uform, tl_yychar, miscell);
        }
        c = tl_Getchar(cnt, hasuform, uform);
        if (c == '>')
        {    
            Token(EQUIV);
        }
        tl_UnGetchar(cnt);
        tl_yyerror("expected '<->'", cnt, uform, tl_yychar, miscell);
    }

    switch (c) 
    {
/*        case '@' : 
            c = FREEZE_AT; 
            break;
        case '{' : 
            c = CONSTRAINT; 
            break;*/
        case '/' : 
            c = follow('\\', AND, '/', cnt, hasuform, uform, tl_yychar, miscell); 
            break;
        case '\\': 
            c = follow('/', OR, '\\', cnt, hasuform, uform, tl_yychar, miscell); 
            break;
        case '&' : 
            c = follow('&', AND, '&', cnt, hasuform, uform, tl_yychar, miscell); 
            break;
        case '|' : 
            c = follow('|', OR, '|', cnt, hasuform, uform, tl_yychar, miscell); 
            break;
        case '[' : 
            c = mtl_follow(']', ALWAYS, '[', cnt, hasuform, uform, miscell, tl_yychar); 
            break;
        case '-' : 
            c = follow('>', IMPLIES, '-', cnt, hasuform, uform, tl_yychar, miscell); 
            break;
        case '!' : 
            c = NOT; 
            break;
        case 'U' : 
            miscell->type_temp = U_OPER;
            miscell->type_oper = O_STRICT;
            if (check_follow('n', 's', cnt, hasuform, uform, tl_yychar, miscell))
                miscell->type_oper = O_NONSTRICT;
            mtl_con(cnt, hasuform, uform, miscell,tl_yychar);
            c = U_OPER;
            break;
        case 'R' : 
            miscell->type_temp = V_OPER;
            miscell->type_oper = O_STRICT;
            if (check_follow('n', 's', cnt, hasuform, uform, tl_yychar, miscell))
                miscell->type_oper = O_NONSTRICT;
            mtl_con(cnt, hasuform, uform, miscell,tl_yychar);
            c = V_OPER;
            break;
        case 'X' : 
            miscell->type_temp = NEXT;
            mtl_con(cnt, hasuform, uform, miscell,tl_yychar);
            c = NEXT;
            break;
        case 'W':
            miscell->type_temp = WEAKNEXT;
            mtl_con(cnt, hasuform, uform, miscell, tl_yychar);
            c = WEAKNEXT;
            break;
        case 'P':
            miscell->type_temp = PREVIOUS;
            mtl_con(cnt, hasuform, uform, miscell, tl_yychar);
            c = PREVIOUS;
            break;
        case 'Z':
            miscell->type_temp = WEAKPREVIOUS;
            mtl_con(cnt, hasuform, uform, miscell, tl_yychar);
            c = WEAKPREVIOUS;
            break;
            /*        case 'V':
            miscell->type_temp = WEAKNEXT;
            mtl_con(cnt, hasuform, uform, miscell, tl_yychar);
            c = WEAKNEXT;
            break;*/
        default: break;
    }
    Token(c);
}

Symbol *tl_lookup(char *s, Miscellaneous *miscell)
{    
    Symbol *sp;
    int h = hash(s);

    for (sp = miscell->symtab[h]; sp; sp = sp->next)
        if (strcmp(sp->name, s) == 0)
            return sp;

    sp = (Symbol *) emalloc(sizeof(Symbol));
    sp->name = (char *) emalloc((strlen(s) + 1) * sizeof(char));
    safe_strcpy(sp->name, strlen(s)+1, s);
    sp->next = miscell->symtab[h];
    sp->set = NullSet;
    miscell->symtab[h] = sp;

    return sp;
}

Symbol* tl_lookup_new(char* s, Miscellaneous* miscell, struct FnExpr* fn_expr)
{
    Symbol* sp;
    int h = hash(s);

    for (sp = miscell->symtab[h]; sp; sp = sp->next)
        if (strcmp(sp->name, s) == 0) {
            sp->fnx = fn_expr;
            return sp;
        }

    sp = (Symbol*)emalloc(sizeof(Symbol));
    sp->name = (char*)emalloc((strlen(s) + 1) * sizeof(char));
    safe_strcpy(sp->name, strlen(s)+1, s);
    sp->next = miscell->symtab[h];
    sp->set = NullSet;
    sp->fnx = fn_expr;
    miscell->symtab[h] = sp;

    return sp;
}

void tl_clearlookup(char *s, Miscellaneous *miscell)
{
    int ii;
    Symbol *sp = NULL, *sp_old = NULL;
    
    int h = hash(s);

    for (sp = miscell->symtab[h], ii=0; sp; sp_old = sp, sp = sp->next, ii++)
        if (strcmp(sp->name, s) == 0)
        {
            if (ii==0)
                miscell->symtab[h] = sp->next;
            else
                sp_old->next = sp->next;
            efree(sp->name);
            efree(sp);
            return;
        }

}

Symbol *getsym(Symbol *s)
{    Symbol *n = (Symbol *) emalloc(sizeof(Symbol));

    n->name = s->name;
    return n;
}

