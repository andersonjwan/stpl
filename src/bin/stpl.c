#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "undomex.h"

#include "config/ConfigFileWrapper.h"
#include "distances.h"
#include "ltl2tree.h"

/* forward declarations */
bool read_global_variables_from_config_file(char* input_file_name, bool verbose_from_command);

void nonmexFunction(char* stpl_formula, int size_formula, int num_predicates,
            char* pred_str[], char* pred_expr[], double* input_signal,
            int num_rows, int num_columns, double* TStamps);

/* main */
int main(int argc, char* argv[]){

    char* input_file_name;
    bool read_input_from_command = false;
    bool verbose_from_command = false;
    bool error_in_argument = false;

    if (argc == 3) {
        if (strcmp(argv[2], "-v") == 0 || strcmp(argv[2], "-V") == 0 || strcmp(argv[2], "--verbose") == 0)
            verbose_from_command = true;
        else
            error_in_argument = true;
        if (!error_in_argument)
            input_file_name = argv[1];

    }
    else if (argc == 2) {
        if (strcmp(argv[1], "-h") == 0 || strcmp(argv[1], "-H") == 0 || strcmp(argv[1], "--help") == 0)
            error_in_argument = true;
        else if (strcmp(argv[1], "-v") == 0 || strcmp(argv[1], "-V") == 0 || strcmp(argv[1], "--verbose") == 0) {
            verbose_from_command = true;
            input_file_name = (char*)emalloc(10 * sizeof(char));
            safe_strcpy(input_file_name, 10, "stpl.conf\0");
            read_input_from_command = true;
        }
        else
            input_file_name = argv[1];
    }
    else if(argc == 1){
        input_file_name = (char*)emalloc(10 * sizeof(char));
        safe_strcpy(input_file_name, 10, "stpl.conf\0");
        read_input_from_command = true;
    }
    else {
        error_in_argument = true;
    }
    if(error_in_argument){
        printf("Use the STPL monitoring tool in one of the below formats:\n");
        printf("1) no argument:\n STPL_Monitor\n");
        printf("2) only passing config file argument:\n STPL_Monitor <stpl.conf configuration file>\n");
        printf("3) only passing verbose argument:\n STPL_Monitor <-v | -V | --verbose>\n");
        printf("4) passing both config file and verbose arguments:\n STPL_Monitor <stpl.conf configuration file> <-v | -V | --verbose>\n");
        return 0;
    }

    //if (verbose_from_command)
    printf("<<< STPL Monitoring Tool >>>\n");

    bool is_open = read_global_variables_from_config_file(input_file_name, verbose_from_command);


    char* sig_file_name = (char*)cfg_get_value("input_signal_file");
    if (VERBOSE_MODE)
        printf("Input Signal File Name: %s\n",sig_file_name);


    is_open = load_signal(sig_file_name);
    if (!is_open)
        return 0;

    if (VERBOSE_MODE)
        printf("Printing input signal:\n");
    double** signal = get_signal();
    int r = get_signal_rows();
    int c = get_signal_columns();
    if (VERBOSE_MODE)
        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                printf("%.3f\t", signal[i][j]);
            }
            //free(signal[i]);
            printf("\n");
    }

    char* stpl_formula = (char*)cfg_get_value("formula");
    int size_formula = strlen(stpl_formula);
    if (size_formula == 0) {
        printf("No formula in the stpl.cfg file\n");
        return 0;
    }
    int num_predicates = 0;
    char* pred_str[100];
    char* pred_expr[100];
    double* input_signal;
    int num_rows = get_signal_rows();
    int num_columns = get_signal_columns();
    double* TStamps;

    char** f_keys = cfg_get_keys();
    int num_keys = cfg_get_size();
    int cnt_pred = 0;
    char subbuff[100];
    int buf_size = 0;

    for (int i = 0; i < num_keys; i++) {
        memcpy(subbuff, f_keys[i], 8);
        subbuff[8] = '\0';
        if (strcmp(subbuff, "formula.") == 0) {
            buf_size = strlen(f_keys[i]) - 8;
            memcpy(subbuff, f_keys[i] + 8, buf_size);
            subbuff[buf_size] = '\0';
            pred_str[cnt_pred] = malloc((buf_size + 1) * sizeof(char));
            safe_strcpy(pred_str[cnt_pred], buf_size + 1, subbuff);
            char* tmp_expt = (char*)cfg_get_value(f_keys[i]);
            pred_expr[cnt_pred] = tmp_expt;
            if (VERBOSE_MODE)
                printf("str: %s, expr: %s\n", pred_str[cnt_pred], pred_expr[cnt_pred]);
            cnt_pred++;
        }
    }

    num_predicates = cnt_pred;
    TStamps = malloc(num_rows * sizeof(double));;
    input_signal = malloc(num_rows*num_columns*sizeof(double));
    int cnt = 0;

    for (int i = 0; i < num_rows; i++) {
        TStamps[i] = signal[i][1];
    }
    for (int j = 0; j < num_columns; j++) {
        for (int i = 0; i < num_rows; i++) {
            input_signal[cnt] = signal[i][j];
            cnt++;
        }
    }



    nonmexFunction(stpl_formula, size_formula,
        num_predicates, pred_str, pred_expr, input_signal,
        num_rows, num_columns, TStamps);
    //---------------------------//


    for (int i = 0; i < r; i++) 
        free(signal[i]);
    for (int i = 0; i < num_keys; i++) 
        free(f_keys[i]);
    for (int i = 0; i < cnt_pred; i++) {
        free(pred_expr[i]);
        free(pred_str[i]);
    }
    free(stpl_formula);
    free(sig_file_name);
    if(read_input_from_command)
        free(input_file_name);

    return 0;
}
