
function signal=kitti_occlusion_extractor(kitti_data_tracking_label_file, fps, ignore_occluded)
%https://github.com/JonathonLuiten/TrackEval/blob/master/docs/KITTI-format.txt
% #Values    Name      Description
% ----------------------------------------------------------------------------
%    1    frame        Frame within the sequence where the object appearers
%    1    track id     Unique tracking id of this object within this sequence
%    1    type         Describes the type of object: 'Car', 'Van', 'Truck',
%                      'Pedestrian', 'Person_sitting', 'Cyclist', 'Tram',
%                      'Misc' or 'DontCare'
%    1    truncated    Integer (0,1,2) indicating the level of truncation.
%                      Note that this is in contrast to the object detection
%                      benchmark where truncation is a float in [0,1].
%    1    occluded     Integer (0,1,2,3) indicating occlusion state:
%                      0 = fully visible, 1 = partly occluded
%                      2 = largely occluded, 3 = unknown
%    1    alpha        Observation angle of object, ranging [-pi..pi]
%    4    bbox         2D bounding box of object in the image (0-based index):
%                      contains left, top, right, bottom pixel coordinates
%    3    dimensions   3D object dimensions: height, width, length (in meters)
%    3    location     3D object location x,y,z in camera coordinates (in meters)
%    1    rotation_y   Rotation ry around Y-axis in camera coordinates [-pi..pi]
%    1    score        Only for results: Float, indicating confidence in
%                      detection, needed for p/r curves, higher is better.

    %kitti_data_tracking_label_file = 'data_tracking_label_2-training-label_02-0008.txt'
    data_file = fopen(kitti_data_tracking_label_file,'r');
    sline = fgetl(data_file);
    signal=[];
    j=0;
    %fps = 20;
    dt = 1.0/fps;
    old_frame = -1;
    frame_cnt = 0;
    max_x = 0;
    max_y = 0;
    neg_id_cnt = -1;

    while ischar(sline) 
        [frame, remain]= strtok(sline);
        frame = str2num(frame);
        if(old_frame == -1)
            old_frame = frame;
        elseif(old_frame ~= frame)
            frame_cnt = frame_cnt + 1;
            old_frame = frame;
        end
        [track_id, remain]= strtok(remain);
        track_id = str2num(track_id);
        if track_id < 0
            track_id = neg_id_cnt;
            neg_id_cnt = neg_id_cnt - 1;
        end
        [type, remain]= strtok(remain);
        type = findTypeIndex(type);
        [truncated, remain]= strtok(remain);
        truncated = str2num(truncated);
        [occluded, remain]= strtok(remain);
        occluded = str2num(occluded);
        [alpha, remain]= strtok(remain);
        [bbox_x1, remain]= strtok(remain);
        bbox_x1 = str2double(bbox_x1);
        [bbox_y1, remain]= strtok(remain);
        bbox_y1 = str2double(bbox_y1);
        [bbox_x2, remain]= strtok(remain);
        bbox_x2 = str2double(bbox_x2);
        [bbox_y2, remain]= strtok(remain);
        bbox_y2 = str2double(bbox_y2);
        bbox_xc = (bbox_x1 + bbox_x2) / 2;
        bbox_yc = (bbox_y1 + bbox_y2) / 2;
        if max_x < bbox_x1
            max_x = bbox_x1;
        end
        if max_y < bbox_y1
            max_y = bbox_y1;
        end
        time = frame_cnt * dt;
        probability = 1;
        stpl_data = [frame, time, track_id, type, probability,...
                        bbox_x1, bbox_y1, bbox_x2, bbox_y2, bbox_xc, bbox_yc];
        %if track_id >= 0 && ~isempty(type) && (type < 6) && ((ignore_occluded && (occluded ~= 2)) || ~ignore_occluded)
        %if track_id >= 0 && ~isempty(type) && ((ignore_occluded && (occluded ~= 2)) || ~ignore_occluded)
        if ~isempty(type) && ((ignore_occluded && (occluded ~= 2)) || ~ignore_occluded)
            j = j+1;
            signal = [signal; stpl_data];
        end
        sline = fgetl(data_file);
    end
disp(['MAX_X: ', num2str(max_x)])
disp(['MAX_Y: ', num2str(max_y)])
end
function index = findTypeIndex(type)
    %'Car', 'Van', 'Truck', 'Pedestrian', 'Person_sitting', 
    %'Cyclist', 'Tram', 'Misc' or 'DontCare'
    all_types = {'Car', 'Cyclist', 'Pedestrian', 'Van', 'Truck', ...
                    'Person', 'Person_sitting', 'Tram', 'Misc', 'DontCare'};
    index = find(strcmp(all_types, type));
end
