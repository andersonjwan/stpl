


clc
clear all
close all

[pwd,'/',mfilename]; %#ok<VUNUS>
%addpath([pwd,'/../stpl-monitor']);
addpath([pwd,'/..']);
addpath(pwd);

%% Load pre-computed data stream from the BDD dataset
load('InputFrameSignal.mat');
disp('Loading BDD data to compute fourth section (Formula (6)) of the Table 2 in the Sensitivity Analysis section.');

% Resolution of the BDD images are 1280x720 pixels
% Therefore, the universe is a rectangle with the size of 1280 x 720  
%Input signal structure:
%frame#, time stamp, object id, object class, class probability, x1, y1,
%           x2, y2, xc, yc

%Car class = 1
%Cyclist class = 2
%Pedestrian class = 3


%%------------------------- RTSS paper examples----------------------------
%%In the STPL formulas, we used the following operators 
%% ! is the logical NOT, and spatial COMPLEMENT
%% /\ is the logical AND, and spatial INTERSECTION
%% \/ is the logical OR, and spatial UNION
%% X is the spatio-temporal NEXT
%% [] is the spatio-temporal ALWAYS
%% <> is the spatio-temporal EVENTUALLY
%% @(Var_x,Var_f) freeze the frame and assign the current time and frame
%% to Var_x and Var_f time and frame variables.
%% @(FORALL,Var_id_1,Var_id_2) universal quantifier for object IDs and over
%% ID variables Var_id_1 and Var_id_2 (as many variables as needed)
%% @(EXISTS,Var_id_1,Var_id_2) existential quantifier for object IDs and over
%% ID variables Var_id_1 and Var_id_2 (as many variables as needed)
%% @(Var_x,Var_f,EXISTS,Var_id_1,...) and @(Var_x,Var_f,FORALL,Var_id_1,...)
%% combination of freeze and quantifier operators over variables 
%% note:(time and/or frame variables can be do-not-care by putting '_')
%% {C_TIME - Var_x > r} time constraint
%% {C_FRAME - Var_f > r} frame constraint
%% {Var_id_1 - Var_id_2 == r} ID variable constraint
%% BB(VAR_id_1) boundig box of the object identified by variable Var_id_1
%% SEXISTS(BB(VAR_id_1)) spatial exists operator applied on a bounding box
%% SEXISTS({\varphi}) spatial exists operator applied on a spatial formula
%% SFORALL(BB(VAR_id_1)) spatial forall operator applied on a bounding box
%% SFORALL({\varphi}) spatial forall operator applied on a spatial formula
%% NOTE: any formula in the predicate expr field that is inside {} is a
%% spatial formula
%% LON, LAT, ED, AREA, RATIO are the functions for calculating longitudinal
%% distance, lateral distance, area of a geometric subspace, ratio of two
%% other functions
%%-------------------------------------------------------------------------

%Example 3, Eq. (6)
STPLstr = '[]( @ (FORALL, Var_id_1)( lat_ge /\ lat_le /\ lon_ge /\ lon_le ))';

%% Predicate definitionas
%% predicate.str is the used predicate name in the STPL formula
%% predicate.expr is the spatio-temporal definition of the predicate
PredMap = [];

ii = 1;
PredMap(ii).str = 'lat_ge';
PredMap(ii).expr = 'LAT(Var_id_1,LM) >= 10 ';

ii = ii + 1;
PredMap(ii).str = 'lat_le';
PredMap(ii).expr = 'LAT(Var_id_1,RM) <= 400 ';

ii = ii + 1;
PredMap(ii).str = 'lon_ge';
PredMap(ii).expr = 'LON(Var_id_1,TM) >= 10 ';

ii = ii + 1;
PredMap(ii).str = 'lon_le';
PredMap(ii).expr = 'LON(Var_id_1,BM) <= 500 ';


%% Choose a size for the input signal, and prepare the image data stream
START_FRAMES = 0;
END_FRAMES = 25;% max is 202 %<<<<==== change the end of signal here


for loop_cnt = 1:4

    disp('-----------------------------------');
    disp(['Running with signal length of ',int2str(END_FRAMES)]);
    disp('-----------------------------------');

INPUT_SIGNAL = InputFrameSignal; %#ok<NASGU>
[sig_size,sig_dym] = size(InputFrameSignal); %#ok<ASGLU>
start_cut = 0;
end_cut = sig_size;%NUM_FRAMES;
%select a segment from the input frames
for c=1:sig_size
    if InputFrameSignal(c,1) == START_FRAMES && start_cut < 1
        start_cut = c;
    elseif InputFrameSignal(c,1) == END_FRAMES
        end_cut = c;
    end
end
if start_cut == 0
    start_cut = 1;
end
INPUT_SIGNAL = InputFrameSignal(start_cut:end_cut,:);
[sig_size,sig_dym] = size(INPUT_SIGNAL);


InFrameTime = 0:1:sig_size-1;
InFrameTime = InFrameTime';
%% Call the STPL Monitor
tic
[evaluation, aux] = stpl_taliro(STPLstr,PredMap,INPUT_SIGNAL,InFrameTime);
toc

%% Show the result
aux %#ok<NOPTS>
evaluation%#ok<NOPTS>
clear mex;%#ok<*CLMEX>

END_FRAMES = END_FRAMES *2;

end
