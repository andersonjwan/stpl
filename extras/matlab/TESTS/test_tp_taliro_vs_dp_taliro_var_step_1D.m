% Conformance testing between tp_taliro and dp_taliro on 1D signal
% with variable sampling step size
%
% [con,out,phi,phi2] = test_tp_taliro_vs_dp_taliro_var_step_1D(inp_arr,tsamp)
%
% inp_array - which formulas to test (type 'all' for all)
% tsamp - the maximum number of samples for the signal 
% con - conformance 1 (true) or 0 (false)
% out - array with columns
%     tp_taliro     dp_taliro
% 1    2     3        4   5           6
% n   rob1  time    rob2 time    rob1==rob2? 
%
%
% Tests to be executed:
% [con,out,phi,phi2] = test_tp_taliro_vs_dp_taliro_var_step_1D('all',0)
% [con,out,phi,phi2] = test_tp_taliro_vs_dp_taliro_var_step_1D('all',1)
% [con,out,phi,phi2] = test_tp_taliro_vs_dp_taliro_var_step_1D('all',2)
% [con,out,phi,phi2] = test_tp_taliro_vs_dp_taliro_var_step_1D('all',10)
% [con,out,phi,phi2] = test_tp_taliro_vs_dp_taliro_var_step_1D('all',30)

function [con,out,phi,phi2] = test_tp_taliro_vs_dp_taliro_var_step_1D(inp_arr,tsamp)
clc
clear all
close all
% number of samples to be tested
if nargin==0
    inp_arr = 'all';
    tsamp = 500;%27,28,...
end
if nargin==1
    tsamp = 50;
end

%% Signals
sig = @(t) sin(t) + sin(2*t);
sam = @(t) 1*t + 0.5*sin(2*t);
tau = 0.2*(0:tsamp)';
eta = sig(tau);
tau = sam(tau);

%% Formulas

iform = 1;%1
phi{iform} = 'b \/ a';
phi2{iform} = 'b \/ a';

iform = iform+1;%2
phi{iform} = '<>a';
phi2{iform} = phi{iform} ;

iform = iform+1;%3
phi{iform} = '[]b';
phi2{iform} = phi{iform} ;

iform = iform+1;%4
phi{iform} = 'X a';
phi2{iform} = phi{iform} ;

iform = iform+1;%5
phi{iform} = 'X X a';
phi2{iform} = phi{iform} ;

iform = iform+1;%6
phi{iform} = 'X X X a';
phi2{iform} = phi{iform} ;

iform = iform+1;%7
phi{iform} = '[](<>(b /\ <> a))';
phi2{iform} = phi{iform} ;

iform = iform+1;%8
phi{iform} = 'a -> b';
phi2{iform} = phi{iform} ;

iform = iform+1;%9
phi{iform} = '<>a /\ [](a -> <> b)';
phi2{iform} = phi{iform} ;

iform = iform+1;%10
phi{iform} = '[](a-><>!a)';
phi2{iform} = phi{iform} ;

iform = iform+1;%11
phi{iform} = '[](!a \/ <>[]!a)';
phi2{iform} = phi{iform} ;

iform = iform+1;%12
phi{iform} = '[] X a';
phi2{iform} = phi{iform} ;

iform = iform+1;%13
phi{iform} = '<> X a';
phi2{iform} = phi{iform} ;

iform = iform+1;%14
phi{iform} = '[] XX a';
phi2{iform} = phi{iform} ;

iform = iform+1;%15
phi{iform} = 'X <> X a';
phi2{iform} = phi{iform} ;

iform = iform+1;%16
phi{iform} = 'a U b';
phi2{iform} = phi{iform} ;

iform = iform+1;%17
phi{iform} = 'a U b U a';
phi2{iform} = phi{iform} ;

iform = iform+1;%18
phi{iform} = 'a U b R a U (b /\ <> [](a/\<>b))';
phi2{iform} = phi{iform} ;

iform = iform+1;%19
phi{iform} = 'a R b';
phi2{iform} = phi{iform} ;

iform = iform+1;%20
phi{iform} = 'a R b R a';
phi2{iform} = phi{iform} ;

iform = iform+1;%21
phi{iform} = 'a U b R a U b';
phi2{iform} = phi{iform} ;

iform = iform+1;%22
phi{iform} = ' @ Var_x ( a U ( b /\{  Var_x>=0 }/\{  Var_x<=10 }))';
phi2{iform} = 'a U_[0,10] b';

iform = iform+1;%23
phi{iform} = ' @ Var_x ! <>({  Var_x>=0 }/\{  Var_x<=3.5 }/\ ! (b /\ a) )';
phi2{iform} = '[]_[0,3.5]( a /\ b)';

iform = iform+1;%24
phi{iform} = '!(@ Var_x <>( !a /\{  Var_x>=0 }/\{  Var_x<=1 }) )';
%phi{iform} = '(@ Var_x !<>( !a /\{  Var_x>=0 }/\{  Var_x<=1 }) )';
phi2{iform} = '[]_[0,1] a';

iform = iform+1;%25
phi{iform} = '@ Var_x <>( a /\{  Var_x>=0.1 }/\{  Var_x<=30 })';
phi2{iform} = '<>_[0.1,30] a';

iform = iform+1;%26
phi{iform} = '[]a /\ @ Var_x <>( b /\{  Var_x>=21.9911 })';
%phi{iform} = ' @ Var_x([]a /\ <>( b /\{  Var_x>=21.9911 }) )';
phi2{iform} = '([]a /\ <>_[21.9911,inf) b)';

iform = iform+1;%27
phi{iform} = '[](a-> @ Var_x <>( !a /\{  Var_x> 0 }/\{  Var_x< 1 }))';
%phi{iform} = '[] @ Var_x(a ->  <>( !a /\{  Var_x> 0 }/\{  Var_x< 1 }))';
phi2{iform} = '[](a-> <>_(0,1)!a)';

iform = iform+1;%28
phi{iform} = '@ Var_x <>( !( @ Var_y <>(a /\{  Var_y> 0 }/\{  Var_y< 10 }) )  /\{  Var_x> 0 }/\{  Var_x< 5 })';
%%%phi{iform} = '@ Var_x <>( {  Var_x> 0 }/\{  Var_x< 5 } /\  @ Var_y [](!a /\{  Var_y> 0 }/\{  Var_y< 10 })  )';
%phi{iform} = '@ Var_x <>( {  Var_x> 0 }/\{  Var_x< 5 } /\  @ Var_y [](({  Var_y> 0 }/\{  Var_y< 10 }) -> !a )  )';
phi2{iform} = '<>_(0,5)[]_(0,10)!a';

iform = iform+1;%29
phi{iform} = '@ Var_y ( <> ( !a /\{  Var_y> 0 }/\{  Var_y< 10 }) ) ';
phi2{iform} = '<>_(0,10)!a';

iform = iform+1;%30
phi{iform} = '@ Var_x <>({  Var_x > 0 }/\{  Var_x< 5 }/\( @ Var_y <>(!a /\{  Var_y> 0 }/\{  Var_y< 10 }) ) )';
phi2{iform} = '<>_(0,5)<>_(0,10)!a';

iform = iform+1;%31
phi{iform} = '[]( a ->  @ Var_x <>( !( @ Var_y <>(a /\{  Var_y> 0 }/\{  Var_y< 10 }) ) /\{  Var_x> 0 }/\{  Var_x< 5 }))';
%phi{iform} = '[] @ Var_x( a ->  <>( ( @ Var_y !<>(a /\{  Var_y> 0 }/\{  Var_y< 10 }) ) /\{  Var_x> 0 }/\{  Var_x< 5 }))';
phi2{iform} = '[](a-><>_(0,5)!<>_(0,10)a)';

iform = iform+1;%32
phi{iform} =  '! @ Var_x <>({  Var_x>=0 }/\{  Var_x<=12.57 }/\ !( @ Var_y <> ({  Var_y>=0 }/\{  Var_y<=6.28 }/\ (b /\ @ Var_z <>( a /\{  Var_z>=0 }/\{  Var_z<=3.14 }) ) ) ) )';
%phi{iform} =  ' @ Var_x !<>({  Var_x>=0 }/\{  Var_x<=12.57 }/\ ( @ Var_y !<> ({  Var_y>=0 }/\{  Var_y<=6.28 }/\ (b /\ @ Var_z <>( a /\{  Var_z>=0 }/\{  Var_z<=3.14 }) ) ) ) )';
phi2{iform} = '[]_[0,12.57] (<>_[0,6.28](b /\ <>_[0,3.14]a))';

iform = iform+1;%33
phi{iform} =  '@ Var_x <>({  Var_x>=0 }/\{  Var_x<=6.28 }/\( b /\ @ Var_y <> ({  Var_y>=0 }/\{  Var_y<=3.14 }/\(a /\ @ Var_z <>( b /\{  Var_z>=0 }/\{  Var_z<=3.14 }) ) ) ) )';
phi2{iform} = '<>_[0,6.28](b /\ <>_[0,3.14] (a /\ <>_[0,3.14]b))';

iform = iform+1;%34
phi{iform} =  '@ Var_x <>({  Var_x>=2.28 }/\ ( b /\ @ Var_y <> ({  Var_y> 3.14 }/\ (a /\ @ Var_z <>( b /\{  Var_z> 0 }) ) ) ) )';
%phi{iform} =  '@ Var_x <>({  Var_x>=2.28 }/\ ( b /\ @ Var_y <> ({  Var_y> 3.14 }/\ (a /\ @ Var_z <>( b /\{  Var_z> 0 }) ) ) ) )';
phi2{iform} = '<>_[2.28,inf)(b /\ <>_(3.14,inf) (a /\ <>_(0,inf)b))';

iform = iform+1;%35
phi{iform} =  '@ Var_x <>({  Var_x>=2.28 }/\ ( b /\ ! @ Var_y <> ({  Var_y> 3.14 }/\ !(a /\ @ Var_z <>( b /\{  Var_z> 0 }) ) ) ) )';
%phi{iform} =  '@ Var_x <>({  Var_x>=2.28 }/\ ( b /\  @ Var_y !<> ({  Var_y> 3.14 }/\ !(a /\ @ Var_z <>( b /\{  Var_z> 0 }) ) ) ) )';
phi2{iform} = '<>_[2.28,inf)(b /\ []_(3.14,inf) (a /\ <>_(0,inf)b))';

iform = iform+1;%36
phi{iform} = '@ Var_x <>( a /\{  Var_x>=0.1 })';
phi2{iform} = '<>_[0.1,inf) a';

iform = iform+1;%37
phi{iform} = '@ Var_x <>( a /\{  Var_x>=10 })';
phi2{iform} = '<>_[10,inf) a';

iform = iform+1;%38
phi{iform} = '@ Var_x <>( a /\{  Var_x>=100 })';
phi2{iform} = '<>_[100,inf) a';

iform = iform+1;%39
phi{iform} = '@ Var_x <>( a /\{  Var_x>=1000 })';
phi2{iform} = '<>_[1000,inf) a';

iform = iform+1;%40
phi{iform} = '@ Var_x <>( a /\{  Var_x>=0 })';
phi2{iform} = '<>_[0,inf) a';

iform = iform+1;%41
phi{iform} = '! @ Var_x <>( ! a /\{  Var_x>=0 })';
%phi{iform} = ' @ Var_x !<>( ! a /\{  Var_x>=0 })';
phi2{iform} = '[]_[0,inf) a';

iform = iform+1;%42
phi{iform} = '@ Var_x ( b U( a /\{  Var_x>=0 }) )';
phi2{iform} = 'b U_[0,inf) a';

iform = iform+1;%43
%phi{iform} = '!(@ Var_x ( !b U( !a /\{  Var_x>10 }) ))';
phi{iform} = '@ Var_x ( b R ( {  Var_x > 10 } -> a ) )';
%%@todo: phi{iform} = '!@ Var_x ( !b U( !a /\{ Var_x>10 }) )';%syntax bug if there
%%is no space after >, then it reads 0 rather than 10

phi2{iform} = 'b R_(10,inf) a';

iform = iform+1;%44
phi{iform} = '!(@ Var_x <>( !( X a )/\{  Var_x>=0 }/\{  Var_x<=1 }) )';
%phi{iform} = '(@ Var_x !<>( !( X a )/\{  Var_x>=0 }/\{  Var_x<=1 }) )';
phi2{iform} = '[]_[0,1] X a';

iform = iform+1;%45
phi{iform} = '!(@ Var_x <>( !( X a )/\{  Var_x>=2 }/\{  Var_x<=3 }) )';
%phi{iform} = '(@ Var_x !<>( !( X a )/\{  Var_x>=2 }/\{  Var_x<=3 }) )';
phi2{iform} = '[]_[2,3] X a';

iform = iform+1;%46
phi{iform} = '[] X a';
phi2{iform} = '[] X a';

iform = iform+1;%47
phi{iform} = '@ Var_y ( <> ( ( X a )/\{  Var_y>=0 }/\{  Var_y<=1 }) ) ';
phi2{iform} = '<>_[0,1] X a';

iform = iform+1;%48
phi{iform} = '@ Var_y ( <> ( ( X a ) /\{  Var_y>=4 }/\{  Var_y<=15 }) ) ';
phi2{iform} = '<>_[4,15] X a';

iform = iform+1;%49
phi{iform} = '@ Var_x ( <> ( ( X a ) /\{  Var_x>=1 }/\{  Var_x<=2 }) ) ';
phi2{iform} = '<>_[1,2] X a';


iform = iform+1;%50
phi{iform} = '@ Var_x ( [] ( ( a ) /\{  Var_x>=0 }) ) ';
phi2{iform} = '[]_[0,inf) a';

iform = iform+1;%51
phi{iform} = 'a U b R a U b';
phi2{iform} = 'a U b R a U b';

iform = iform+1;%52
phi{iform} = '@ Var_x ( a U ( {  Var_x>=0 }/\{  Var_x<=2 }/\ ! @ Var_y ( ! b U !( {  Var_y>=0 }/\{  Var_y<=2 }/\ @ Var_z ( a U ( b /\{  Var_z>=0 }/\{  Var_z<=2 } ) ) ) ) ) )';
%phi{iform} = '@ Var_x ( a U ( {  Var_x>=0 }/\{  Var_x<=2 }/\  @ Var_y (  b R (( {  Var_y>=0 }/\{  Var_y<=2 }) /\ @ Var_z ( a U ( b /\{  Var_z>=0 }/\{  Var_z<=2 } ) ) ) ) ) )';
phi2{iform} = 'a U_[0,2] ( b R_[0,2] ( a U_[0,2] b ) )';


iform = iform+1;%53
phi{iform} = '@ Var_x (b R (({  Var_x > 1 } /\ {  Var_x < 150 }) -> a) )';
phi2{iform} = 'b R_(1,150) a';

iform = iform+1;%54
phi{iform} = '@ Var_x  (b U (({  Var_x > 1 } /\ {  Var_x < 150 }) /\ a) )';
phi2{iform} = 'b U_(1,150) a';

iform = iform+1;%55
phi{iform} = '@ Var_x  ([] (({  Var_x > 1 } /\ {  Var_x < 150 }) -> a) )';
phi2{iform} = '[]_(1,150) a';

iform = iform+1;%56
phi{iform} = '@ Var_x  (<> (({  Var_x > 1 } /\ {  Var_x < 150 }) /\ a) )';
phi2{iform} = '<>_(1,150) a';

iform = iform+1;%57
phi{iform} = '@ Var_x  (b R (({  Var_x > 100 } /\ {  Var_x < 1500 }) -> a) )';
phi2{iform} = 'b R_(100,1500) a';

iform = iform+1;%58
phi{iform} = '@ Var_x  (b U (({  Var_x > 100 } /\ {  Var_x < 1500 }) /\ a) )';
phi2{iform} = 'b U_(100,1500) a';


%% Predicates
Pred(1).str = 'a';
Pred(1).A = -1.0;
Pred(1).b = 2.0;

Pred(2).str = 'b';
Pred(2).A = 1.0;
Pred(2).b = 2.0;

if (nargin==0 || (nargin>0 && ischar(inp_arr)))
    inp_arr = 1:iform;
end

out = zeros(length(inp_arr),3);
disp([' Running ',num2str(iform),' tests ...'])
disp(' ')

for ii = 1:length(inp_arr)
%for ii = 1:21
%for ii = 53:length(inp_arr)
%for ii = 23:23


    jj = inp_arr(ii);

    disp([num2str(ii),'. ',phi{jj},' vs. ',phi2{jj}])   
    out(ii,1) = jj;
    tic
    rob1 = tp_taliro(phi{jj},Pred,eta,tau);
    %%rob1 = tp_taliro(phi{jj},Pred,eta,tau);
    out(ii,2) = rob1;
    out(ii,3) = toc;
    disp(['tp_taliro done, rob = ',num2str(out(ii,2))])

    tic
    rob2 = dp_taliro(phi2{jj},Pred,eta,tau);
    out(ii,4) = rob2;
    out(ii,5) = toc;
    disp(['dp_taliro done,   rob = ',num2str(out(ii,4))])
    disp(['tp_time = ',num2str(out(ii,3))])
    disp(['dp_time   = ',num2str(out(ii,5))])
    disp(' ')
    out(ii,6) = (out(ii,2) == out(ii,4));
    if( out(ii,6))
        disp('Test <OK>');
    else
        disp('Test <Failed>');
    end

end

total_time = sum(out,1);
time_stql =  total_time(1,3);
time_dp =  total_time(1,5);

disp(['Total TPTL time: ',num2str(time_stql)])
disp(['Total MTL time : ',num2str(time_dp)])

confirm = min(out(:,6));

if confirm
    disp('********************')
    disp(' All tests conform.')
    disp('********************')
else
    disp('****************************')
    disp(' Some tests do not conform.')
    disp('****************************')
end
    
end
