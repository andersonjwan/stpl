function setup_stpl_taliro()
    mex -silent -O -compatibleArrayDims src/mx_stpl_taliro.c src/cache.c src/distances.c src/lex.c src/DP-fast.c src/parse.c src/rewrt.c src/ConfigFile/ConfigFileWrapper.cpp src/ConfigFile/ConfigFile.cpp

    disp('***************************************************************************')
    disp('You are all set to use STPL-TaLiRo!')
    disp('Type "help stpl_taliro" to get a detailed description of using the tool.')
    disp('***************************************************************************')
end
