function compile_debug_mx_stpl_taliro()
    clc
    %note: this is for a silent compilation
    %mex -silent -O -compatibleArrayDims mx_stql_taliro.c cache.c distances.c lex.c DP-fast.c parse.c rewrt.c
    mex -g -v -compatibleArrayDims ../mx_stpl_taliro.c ../cache.c ../distances.c ../lex.c ../DP-fast.c ../parse.c ../rewrt.c ../ConfigFile/ConfigFileWrapper.cpp ../ConfigFile/ConfigFile.cpp

end
%%Note: this is for test purposes
% function compile_debug_mx_stql_taliro(fast_mode)
% clc
% if isempty(fast_mode) || fast_mode == true %stable mode
%     %note: this compiles the stable algorithm without considering time-constraints to gain efficiency 
%     mex -g -v -compatibleArrayDims mx_stql_taliro.c cache.c distances.c lex.c DP.c parse.c rewrt.c
%     %note: this is for a silent compilation
%     %mex -silent -O -compatibleArrayDims mx_stql_taliro.c cache.c distances.c lex.c DP.c parse.c rewrt.c
% else %test mode
%     %note: this compiles the test code that exploits time-constraints to gain some efficiency 
%     mex -g -v -compatibleArrayDims mx_stql_taliro.c cache.c distances.c lex.c DP-fast.c parse.c rewrt.c
% end
% end